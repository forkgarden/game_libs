package
{
	import fg.dialogs.DialogController;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;

	/**
	 * ...
	 * @author test
	 */
	[Frame(factoryClass="Preloader")]
	public class Main extends Sprite 
	{
		[Embed(source = "../bin/back.jpg")]
		private var ImgBack:Class;
		
		private var dialogView:DialogView;
		private var dialogCtrl:DialogController;
		private var data:Array = [];
		
		public function Main() 
		{
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}

		private function init(e:Event = null):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			// entry point
			addChild(new ImgBack());
			
			dialogView = new DialogView();
			addChild(dialogView);
			dialogView.addEventListener(MouseEvent.CLICK, dialogViewClick);
			
			dialogCtrl = new DialogController();
			dialogCtrl.changeCallBack = dialogOnChange;
			dialogCtrl.finishCallBack = dialogOnFinish;
			
			addEventListener(Event.ENTER_FRAME, update);
			
			data = [
				{
					nama : 'Pembeli',
					teks : 'Bang, mau beli benderanya. Berapa harga satunya?'
				},
				{
					nama : 'Penjual',
					teks : '35 Ribu kalau yang kecil, kalo yang gede 50 Ribu'
				},
				{
					nama : 'Pembeli',
					teks : 'Kurangin dikit dong Bang'
				},
				{
					nama : 'Penjual',
					teks : 'Harga bendera 35 ribu. NKRI Harga Mati!'
				}
			];
			
			dialogCtrl.start([
				data[0].teks,
				data[1].teks,
				data[2].teks,
				data[3].teks
			]);
		}
		
		protected function dialogOnChange(idx:Number):void {
			dialogView.profile.removeChildren();
			if (data[idx].nama == 'Pembeli') {
				dialogView.profile.addChild(dialogView.photos[0]);
				dialogView.label.text = 'Pembeli';
			}
			else if (data[idx].nama == 'Penjual') {
				dialogView.profile.addChild(dialogView.photos[1]);
				dialogView.label.text = 'Penjual';
			}
		}
		
		protected function dialogOnFinish():void {
			trace('dialog finish');
		}
		
		//fungsi ini dipanggil saat dialog di click
		protected function onMouseClick(evt:MouseEvent):void {
			dialogCtrl.onMouseClick();
		}
		
		//update dialog, tampilkan teks
		protected function update(e:Event):void {
			dialogCtrl.update();
			dialogView.teks.text = dialogCtrl.getTeks();
		}		
		
		private function dialogViewClick(e:MouseEvent):void {
			dialogCtrl.onMouseClick();			
		}
	}

}