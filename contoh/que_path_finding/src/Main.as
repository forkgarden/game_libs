package
{
	import fg.bmps.BmpBLoader;
	import fg.mazes.Maze;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import fg.bmps.BmpLoader;
	import flash.text.TextFormatAlign;
	import fg.ui.Teks;
	import fg.path_finder_helper.PFHelper;
	import fg.pathfinders.PathFinder;

	[Frame(factoryClass="Preloader")]
	public class Main extends Sprite 
	{
		
		protected const TILE_SIZE:int = 32;
		
		protected var bmpLoader:BmpBLoader;
		protected var maze:Maze;
		protected var halLoading:Sprite;
		protected var karakter:Bitmap;
		protected var pfHelper:PFHelper;
		protected var pathFinder:PathFinder;
		protected var targets:Vector.<Object> = new Vector.<Object>();
		protected var targetCont:Sprite;
		protected var targetActive:Object;
		
		public function Main() 
		{
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}

		private function init(e:Event = null):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			// entry point
			
			addEventListener(MouseEvent.CLICK, mapOnClick, false, 0, true);
			
			//buat halaman loading
			halLoadingCreate();
			addChild(halLoading);
			
			bmpLoader = new BmpBLoader();
			bmpLoader.reg('char', 'char.png');
			bmpLoader.reg('jalan', 'jalan.png');
			bmpLoader.reg('rumput', 'rumput.png');
			bmpLoader.reg('target', 'target.png');
			bmpLoader.onLoadCallBack = bmpOnLoaded;
			bmpLoader.load();
		}		
		
		//fungsi ini untuk membuat tampilan loading
		protected function halLoadingCreate():void {
			var teks:Teks = new Teks();
			
			halLoading = new Sprite();
			halLoading.addChild(teks);
			
			teks.text = 'Loading ...';
			teks.width = this.stage.stageWidth;
			teks.format.align = TextFormatAlign.CENTER;
			teks.height = teks.textHeight + 4;
			teks.y = this.stage.stageHeight / 2 - teks.height / 2;
			teks.formatApply();
		}
		
		/**
		 * Fungsi ini dipanggil saat gambar
		 * selesai di load
		 */
		protected function bmpOnLoaded():void {
			//bersihkan layar
			removeChildren();
			
			//buat maze
			//6 column, 4 baris, ukuran ruangan 3x3
			maze = new Maze();
			maze.create(6, 4, 3);
			maze.drawWImg(this, bmpLoader.getBmpById('jalan').bmp.bitmapData, bmpLoader.getBmpById('rumput').bmp.bitmapData);
			
			//buat karakter
			karakter = bmpLoader.getBmpById('char').bmp;
			addChild(karakter);
			karakter.x = TILE_SIZE;
			karakter.y = TILE_SIZE;
			
			//inisialisasi Path Finding Helper
			pfHelper = new PFHelper();
			pfHelper.checkCanMoveToPos = checkBlock;
			
			//inisialisasi Path Finding
			pathFinder = new PathFinder();
			pathFinder.maxNodes = 1000;
			pathFinder.checkCanMoveToPos = checkBlock;
			
			targetCont = new Sprite();
			addChild(targetCont);
			
			this.addEventListener(Event.ENTER_FRAME, update, false, 0, true);			
		}
		
		protected function redrawTarget():void {
			var obj:Object;
			var spr:Sprite;
			var bmp:Bitmap;
			
			targetCont.removeChildren();
			
			for each (obj in targets) {
				bmp = new Bitmap(bmpLoader.getBmpById('target').bmp.bitmapData);
				bmp.x = obj.tileX * TILE_SIZE;
				bmp.y = obj.tileY * TILE_SIZE;
				targetCont.addChild(bmp);
			}
			
			if (targetActive) {
				bmp = new Bitmap(bmpLoader.getBmpById('target').bmp.bitmapData);
				bmp.x = targetActive.tileX * TILE_SIZE;
				bmp.y = targetActive.tileY * TILE_SIZE;
				targetCont.addChild(bmp);
			}
		}
		
		protected function update(evt:Event):void {
			var obj:Object;
			var ar:Array;
			
			//update karakter posisition
			if (pfHelper.aktif) {
				pfHelper.update();
				karakter.x = pfHelper.pos.x;
				karakter.y = pfHelper.pos.y;
			}
			else {
				if (targets.length > 0) {
					targetActive = targets.shift();
					
					ar = pathFinder.find(getTilePosX(), getTilePosY(), targetActive.tileX, targetActive.tileY);
					
					if (ar.length > 0) {
						pfHelper.start(ar);
					}
					
					redrawTarget();
				}
				else {
					targetActive = null;
					redrawTarget();
				}
			}
			
		}
		
		/**
		 * 
		 * check apakah tile bisa dilewati
		 * tile bisa dilewati bila tidak ada tembok
		 * 
		 * @param	i
		 * @param	j
		 * @return
		 * 
		 */
		protected function checkBlock(i:int, j:int):Boolean {			
			if (maze.map[i][j] > 0) return false;
			
			return true;
		}		
		
		/**
		 * Hitung posisi x karakter pada unit tile.
		 * 
		 * @return
		 */
		protected function getTilePosX():int{
			return Math.floor((karakter.x) / TILE_SIZE);
		}
		
		/**
		 * Hitung posisi vertical karakter pada unit tile
		 * @return
		 */
		protected function getTilePosY():int{
			return Math.floor(karakter.y / TILE_SIZE);
		}		
		
		/**
		 * Fungsi ini dipanggil saat map di click
		 * 
		 * @param	evt
		 */
		protected function mapOnClick(evt:MouseEvent):void {
			var tileX:Number;
			var tileY:Number;
			var ar:Array;
			
			if (targets.length < 5) {
				tileX = Math.floor(stage.mouseX / TILE_SIZE);
				tileY = Math.floor(stage.mouseY / TILE_SIZE);
				
				targets.push({tileX:tileX, tileY:tileY});
				
				redrawTarget();
			}
		}
		
		
	}

}