package
{
	import fg.bmps.BmpLoader;
	import fg.path_finder_helper.PFHelper;
	import fg.pathfinders.PathFinder;
	import fg.tmxs.Tmx;
	import fg.ui.Teks;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextFormatAlign;

	[Frame(factoryClass="Preloader")]
	public class MainPFSpeed extends Sprite 
	{
		
		protected var tmx:Tmx = new Tmx();
		protected var pfHelper:PFHelper;
		protected var pathFinder:PathFinder;
		protected var char:Sprite;
		protected var bmpLoader:BmpLoader;
		protected var halLoading:Sprite;

		public function MainPFSpeed() 
		{
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		/**
		 * Fungsi utama
		 * 
		 * @param	e
		 */
		protected function init(e:Event = null):void {
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			//buat halaman loading
			halLoadingCreate();
			addChild(halLoading);
			
			//inisialisasi tmx
			tmx = new Tmx();
			tmx.url = 'map.tmx';
			
			//saat tmx selesai di load panggil fungsi tmxLoaded
			tmx.onLoadCallBack = tmxLoaded;
			tmx.load();
		}
		
		//fungsi ini untuk membuat tampilan loading
		protected function halLoadingCreate():void {
			var teks:Teks = new Teks();
			
			halLoading = new Sprite();
			halLoading.addChild(teks);
			
			teks.text = 'Loading ...';
			teks.width = this.stage.stageWidth;
			teks.format.align = TextFormatAlign.CENTER;
			teks.height = teks.textHeight + 4;
			teks.y = this.stage.stageHeight / 2 - teks.height / 2;
			teks.formatApply();
		}		
		
		//fungsi ini dipanggil saat file tmx selesai di load
		protected function tmxLoaded():void {
			
			//meload gambar karakter
			bmpLoader = new BmpLoader();
			bmpLoader.url = 'charf02.png';
			
			//setelah gambar di load, panggil fungsi bmpOnLoaded
			bmpLoader.onLoadCallBack = bmpOnLoaded;
			bmpLoader.load();
		}
		
		//fungsi ini dipanggil saat gambar karakter selesai di load
		protected function bmpOnLoaded():void {
			
			removeChildren();
			
			//render map ke layar
			tmx.render(this);
			
			//buat karakter
			char = new Sprite();
			char.addChild(bmpLoader.bmp);
			addChild(char);
			
			//buat pfHelper
			pfHelper = new PFHelper();
			pfHelper.checkCanMoveToPos = checkBlock;
			
			//buat pathFinder
			pathFinder = new PathFinder();
			pathFinder.checkCanMoveToPos = checkBlock;
			pathFinder.maxNodes = 1000;
			
			this.addEventListener(Event.ENTER_FRAME, update, false, 0, true);
			this.stage.addEventListener(MouseEvent.CLICK, mapOnClick, false, 0, true);
		}
		
		//fungsi ini dipanggil oleh pfHelper dan pathFinder
		//untuk mengecek apakah suatu tile bisa dilalui atau tidak
		protected function checkBlock(i:int, j:int):Boolean {
			
			//bila posisi di luar layar, tidak dapat dilalui
			if (i < 0) return false;
			if (i >= tmx.width) return false;
			if (j < 0) return false;
			if (j >= tmx.height) return false;
			
			return true;
		}
		
		//fungsi ini dipanggil saat map di click
		public function mapOnClick(evt:MouseEvent):void {
			var ar:Array;
			var posX:Number;
			var posY:Number;
			var tileX:Number;
			var tileY:Number;
			
			//check apakah PathFinder Helper sedang aktif
			//bila tidak maka karakter boleh jalan
			if (pfHelper.aktif == false) {
				
				//hitung posisi tile karakter
				posX = Math.floor((char.x) / 32.0);
				posY = Math.floor((char.y) / 32.0);
				
				//hitung posisi tile yang di klik
				tileX = Math.floor(stage.mouseX / 32.0);
				tileY = Math.floor(stage.mouseY / 32.0);
				
				//cari jalan
				ar = pathFinder.find(posX, posY, tileX, tileY);
				
				//bila path ketemu
				if (ar.length > 0) { 
					aturKecepatan();
					pfHelper.start(ar);
				}
			}
		}
		
		/**
		 * Mengatur kecepatan dari karakter,
		 * Karakter akan bergerak lebih cepat bila di rumput
		 */
		protected function aturKecepatan():void {
			var tileX:Number;
			var tileY:Number;
			
			//hitung posisi tile tempat user berdiri
			tileX = Math.floor(char.x / 32);
			tileY = Math.floor(char.y / 32);
			
			//bila melewati ubin, kecepatan menurun
			if (tmx.getLayerByName('jalan').getTile(tileX, tileY).gid > 0) {
				pfHelper.stepCount = 12;
			}
			
			//bila tidak lewat di ubin
			//kecepatan normal
			else {
				pfHelper.stepCount = 3;
			}
		}
		
		protected function update(e:Event):void {
			if (pfHelper.aktif) {
				
				//bila posisi karakter sedang pas berada di tile
				//atur kecepatan berdasarkan tile
				if (pfHelper.onTile) {
					aturKecepatan();
				}
				
				pfHelper.update();
				
				//update posisi karakter sesuai dengan hasil perhitungan dari
				//pfHelper.
				char.x = pfHelper.pos.x;
				char.y = pfHelper.pos.y;
				
				trace('pos ' + char.x);
			}
		}
		
	}

}