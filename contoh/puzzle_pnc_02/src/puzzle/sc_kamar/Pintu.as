package puzzle.sc_kamar {
	import Main;
	import puzzle.Cerita;
	import puzzle.SceneObj;
	
	public class Pintu extends SceneObj {
		
		public function Pintu() {
			
		}
		
		public override function onAction():void {
			var cerita:Cerita;
			
			cerita = Main.inst.cerita;
			
			if (cerita.tahap == Cerita.AWAL) {
				Main.inst.dialog.start(
					[
						"Eh, pintunya kok dikunci!!!",
						"Aku taruh kuncinya dimana ya?",
						"Waduh aku gak bisa keluar nih!"
					], function ():void {
						Main.inst.cerita.tahap = Cerita.LEMARI_CHECK;
					});
			}
			else if (cerita.tahap == Cerita.PINTU_BUKA_KUNCI) {
				Main.inst.dialog.start(
					[
						"Selamat, kamu telah menyelesaikan permainan ini",
						"Sampai jumpa di tutorial berikutnya"
					], function ():void {
						Main.inst.cerita.tahap = 0;
						Main.inst.cerita.flPintuKunci = false;
					});
			}
			else {
				if (Main.inst.cerita.flPintuKunci) {
					Main.inst.dialog.start(["Pintunya dikunci, aku lupa kuncinya dimana"]);
				}
				else {
					//pindah ruangan
					
				}
			}
		}		
		
	}

}