package puzzle {
	
	public class Cerita {
		
		public static const AWAL:int   = 1;
		public static const PINTU_CHECK:int = 2;
		public static const LEMARI_CHECK:int = 3;
		public static const PINTU_BUKA_KUNCI:int = 4;
		
		protected var _tahap:int = 1;
		protected var _flPintuKunci:Boolean = true;
		
		//Class ini berfungsi untuk menyimpan state dari cerita
		//serta beberapa flag-flag yang berisi informasi tentang
		//status dari game
		public function Cerita() {
			_tahap = AWAL;
		}
		
		public function get tahap():int 
		{
			return _tahap;
		}
		
		public function set tahap(value:int):void 
		{
			_tahap = value;
		}
		
		public function get flPintuKunci():Boolean 
		{
			return _flPintuKunci;
		}
		
		public function set flPintuKunci(value:Boolean):void 
		{
			_flPintuKunci = value;
		}
		
	}

}