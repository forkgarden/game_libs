package puzzle {
	import Main;
	import fg.tmxs.TileObject;
	import flash.display.Sprite;
	import flash.geom.Point;

	public class SceneObj {
		
		protected var _view:Sprite;
		protected var _tmxObj:TileObject;
		protected var _posRef:Point = new Point();
		
		public function SceneObj() {
			
		}
		
		public function destroy():void {
			if (_view.parent) _view.parent.removeChild(_view);
			_view = null;
			_tmxObj = null;
			_posRef = null;
		}
		
		public function getViewIndex():Number {
			return _view.parent.getChildIndex(_view);
		}
		
		//ambil informasi dari map untuk ditambahkan ke Object
		//informasi itu antara lain adalah gambar dan target reference
		//tmxObj adalah object di map
		public function initFromTmx(tmxObj:TileObject):void {
			var refName:String;
			
			//ambil data view
			_view = tmxObj.sprite;
			
			if (tmxObj.hasProp('tx')) {
				_posRef.x = parseInt(tmxObj.getPropValueByName('tx'));
			}
			else {
				_posRef.x = 0;
			}
			
			if (tmxObj.hasProp('ty')) {
				_posRef.y = parseInt(tmxObj.getPropValueByName('ty'));
			}
			else {
				_posRef.y = 0;
			}
			
			_posRef.x *= Main.TILE_SIZE;
			_posRef.x += _view.x;
			_posRef.x = Math.floor(_posRef.x / Main.TILE_SIZE);
			
			_posRef.y *= Main.TILE_SIZE;
			_posRef.y += _view.y;
			_posRef.y = Math.floor(_posRef.y / Main.TILE_SIZE);
			
			//simpan reference dari tmxObj
			this._tmxObj = tmxObj;
		}
		
		public function onAction():void {
		}
		
		//hitung posisi tileX dari object
		public function getTilePosX():Number {
			var res:Number = 0;
			
			res = Math.floor(_view.x / Main.TILE_SIZE);
			
			return res;
		}
		
		public function getTilePosY():Number {
			var res:Number;
			
			res = _view.y;
			res += _view.height;
			res -= Main.TILE_SIZE;
			//res += Main.TILE_SIZE / 2;
			res /= Main.TILE_SIZE;
			res = Math.floor(res);
			
			return res;
		}
		
		public function get view():Sprite 
		{
			return _view;
		}
		
		public function set view(value:Sprite):void 
		{
			if (value == null) throw new Error("view is null");
			_view = value;
		}
		
		//public function get ref():SceneObj 
		//{
			//return _ref;
		//}
		
		public function get tmxObj():TileObject 
		{
			return _tmxObj;
		}
		
		public function get posRef():Point 
		{
			return _posRef;
		}
	}
}