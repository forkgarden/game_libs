package 
{
	import fg.path_finder_helper.PFHelper;
	import fg.pathfinders.PathFinder;
	import fg.tmxs.Tmx;
	import flash.display.Sprite;

	public class KarakterUtama extends Sprite
	{
		[Embed(source = "../bin/char.png")]
		private var Char:Class;
		
		private var pathFinder:PathFinder;
		private var pfHelper:PFHelper;
		private var _tmx:Tmx;
		
		public function KarakterUtama() 
		{
			addChild(new Char());
			
			//inisialisasi Path Finding Helper
			pfHelper = new PFHelper();
			pfHelper.checkCanMoveToPos = checkBlock;
			
			//inisialisasi Path Finding
			pathFinder = new PathFinder();
			pathFinder.maxNodes = 1000;
			pathFinder.checkCanMoveToPos = checkBlock;
			
			//aktifkan pergerakan diagonal
			pathFinder.diagonalMove = true;
		}
		
		private function getTilePosX():int{
			return Math.floor(x / MainTarget.TILE_SIZE);
		}
		
		private function getTilePosY():int{
			return Math.floor(y / MainTarget.TILE_SIZE);
		}
		
		public function jalanKePos(tx:int, ty:int):void {
			var ar:Array;
			
			if (pfHelper.aktif == false) {
				ar = pathFinder.find(getTilePosX(), getTilePosY(), tx, ty);
				
				if (ar.length > 0) {
					pfHelper.start(ar);
				}
			}
		}
		
		public function update():void {
			if (pfHelper.aktif) {
				pfHelper.update();
				x = pfHelper.pos.x;
				y = pfHelper.pos.y;
			}			
		}
		
		private function checkBlock(i:int, j:int):Boolean {
			var rumah:Rumah;
			
			if (i < 0) return false;
			if (i >= _tmx.width) return false;
			if (j < 0) return false;
			if (j >= _tmx.height) return false;
			
			if (_tmx.getLayerByName('block').getTile(i, j).gid > 0) return false;
			
			return true;
		}
		
		public function get tmx():Tmx 
		{
			return _tmx;
		}
		
		public function set tmx(value:Tmx):void 
		{
			_tmx = value;
		}
		
	}

}