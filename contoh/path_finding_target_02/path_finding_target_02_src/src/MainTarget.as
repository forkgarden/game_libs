package {
	
	import fg.tmxs.Tmx;
	import fg.ui.Teks;
	import flash.display.Sprite;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextFormatAlign;
	
	[Frame(factoryClass="Preloader")]
	public class MainTarget extends Sprite {
		
		private var karakter:KarakterUtama;
		private var halLoading:Sprite;
		private var _tmx:Tmx;
		
		public static const TILE_SIZE:int = 32;
		
		public function MainTarget() {
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		public function mapOnClick(evt:MouseEvent):void {
			var tileX:Number;
			var tileY:Number;
			
			tileX = Math.floor(stage.mouseX / TILE_SIZE);
			tileY = Math.floor(stage.mouseY / TILE_SIZE);
			
			karakter.jalanKePos(tileX, tileY);			
		}
		
		private function init(e:Event = null):void {
			removeEventListener(Event.ADDED_TO_STAGE, init);
			this.stage.scaleMode = StageScaleMode.SHOW_ALL;
			
			addEventListener(MouseEvent.CLICK, mapOnClick, false, 0, true);
			
			//buat halaman loading
			halLoadingCreate();
			addChild(halLoading);			
			
			//insialisasi TMX untuk meload map
			_tmx = new Tmx();
			_tmx.url = 'map.tmx';
			
			//setelah map di load panggil fungsi tmxLoaded
			_tmx.onLoadCallBack = tmxLoaded;
			_tmx.load();
		}
		
		private function halLoadingCreate():void {
			var teks:Teks = new Teks();
			
			halLoading = new Sprite();
			halLoading.addChild(teks);
			
			teks.text = 'Loading ...';
			teks.width = this.stage.stageWidth;
			teks.format.align = TextFormatAlign.CENTER;
			teks.height = teks.textHeight + 4;
			teks.y = this.stage.stageHeight / 2 - teks.height / 2;
			teks.formatApply();
		}
		
		private function tmxLoaded():void {			
			var rumah:Rumah;
			var teks:Teks;
			var border:Sprite;
			
			this.removeChildren();
			
			//render bagian base
			_tmx.getLayerByName('base').render(this);
			
			//buat object rumah
			Rumah.create('rumah1', _tmx.getLayerByName('object').getObjByName('rumah1'));
			Rumah.create('rumah2', _tmx.getLayerByName('object').getObjByName('rumah2'));
			Rumah.create('rumah3', _tmx.getLayerByName('object').getObjByName('rumah3'));
			
			for each (rumah in Rumah.lists) {
				addChild(rumah.view);
				rumah.view.addEventListener(MouseEvent.CLICK, rumahOnClick, false, 0, true);
			}
			
			karakter = new KarakterUtama();
			karakter.tmx = tmx;
			karakter.x = 320;
			karakter.y = 320;
			addChild(karakter);
			
			//border
			border = new Sprite();
			border.graphics.beginFill(0, .5);
			border.graphics.drawRect(0, 0, 640, 40);
			border.mouseEnabled = false;
			addChild(border);
			
			//teks
			teks = new Teks();
			teks.width = 640;
			teks.textColor = 0xffffff;
			teks.format.align = TextFormatAlign.CENTER;
			teks.formatApply();
			teks.mouseEnabled = false;
			teks.text = "Contoh penerapan path finding pada target object besar dan jumlah lebih dari satu\n Click pada rumah untuk memerintahkan karakter berjalan ke arah pintu";
			addChild(teks);
			
			this.addEventListener(Event.ENTER_FRAME, update, false, 0, true);	
		}
		
		private function rumahOnClick(evt:MouseEvent):void {
			var rumah:Rumah;
			
			evt.stopPropagation();
			
			rumah = Rumah.getObjByView(evt.currentTarget as Sprite);
			karakter.jalanKePos(rumah.posRefX, rumah.posRefY);
		}
		
		private function update(e:Event):void {
			karakter.update();
		}
		
		public function get tmx():Tmx 
		{
			return _tmx;
		}
		
	}
}