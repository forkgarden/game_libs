package fg.ui {
	import flash.text.TextField;
	import flash.text.TextFormat;
	/**
	 * ...
	 * @author 
	 */
	public class Teks extends TextField {
		
		protected var _format:TextFormat;
		
		public function Teks() {
			_format = getTextFormat();
			multiline = false;
			mouseEnabled = false;
			mouseWheelEnabled = false;
			tabEnabled = false;
			doubleClickEnabled = false;
			
			_format.font = 'Arial';
			_format.size = 12;
			formatApply();
		}
		
		public function destroy():void {
			_format = null;
		}
		
		public function formatApply():void {
			this.setTextFormat(_format);
			this.defaultTextFormat = _format;
		}
		
		public function get format():TextFormat {
			return _format;
		}
		
	}

}