package fg.pathfinders   {

	internal final class TNode {
		
		protected static const KANAN:int = 1;
		
		protected var _dist:int = 0;
		protected var _parentIdx:int = 0;
		protected var _parent:TNode;
		protected var _x:int = 0;
		protected var _y:int = 0;
		protected var _g:int = 0;
		protected var _idx:int = 0;
		protected var _open:Boolean = true;
		protected var _dir:int = KANAN;
		protected var _h:int = 0;
		
		public function TNode() {
			
		}
		
		public function destroy():void {
			_parent = null;
		}
		
		public function toStringRef():String {
			return "[" + _x + "-" + _y + "]";
		}
		
		public function getDistAStar(sx:int, sy:int, tx:int, ty:int):int {
			return ((Math.abs(_x - sx) + Math.abs(_y - sy))) + ((Math.abs(tx - _x) + Math.abs(ty - _y)) * 2);
		}
		
		public function getDistBF(sx:int, sy:int):int {
			return (Math.abs(_x - sx) + Math.abs(_y - sy));
		}
		
		public function getDistFast(tx:int, ty:int):int {
			return (Math.abs(tx - _x) + Math.abs(ty - _y));
		}
		
		public function getDist(idx:int, k:int, l:int):int {
			return 	(
				(idx + 1) + 
				(Math.abs(k - _x) + Math.abs(l - _y)) * 2
			)
		}
		
		public function get dist():int 
		{
			return _dist;
		}
		
		public function set dist(value:int):void 
		{
			_dist = value;
		}
		
		public function get parentIdx():int 
		{
			return _parentIdx;
		}
		
		public function set parentIdx(value:int):void 
		{
			_parentIdx = value;
		}
		
		public function get x():int 
		{
			return _x;
		}
		
		public function set x(value:int):void 
		{
			_x = value;
		}
		
		public function get y():int 
		{
			return _y;
		}
		
		public function set y(value:int):void 
		{
			_y = value;
		}
		
		public function get idx():int 
		{
			return _idx;
		}
		
		public function set idx(value:int):void 
		{
			_idx = value;
		}
		
		public function get open():Boolean 
		{
			return _open;
		}
		
		public function set open(value:Boolean):void 
		{
			_open = value;
		}
		
		public function get dir():int 
		{
			return _dir;
		}
		
		public function set dir(value:int):void 
		{
			_dir = value;
		}
		
		public function get g():int {
			return _g;
		}
		
		public function set g(value:int):void {
			_g = value;
		}
		
		public function get parent():TNode {
			return _parent;
		}
		
		public function set parent(value:TNode):void {
			_parent = value;
		}
		
		public function get h():int {
			return _h;
		}
		
		public function set h(value:int):void {
			_h = value;
		}
		
	}

}