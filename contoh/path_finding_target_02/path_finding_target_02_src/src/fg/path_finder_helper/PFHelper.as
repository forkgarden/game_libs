package fg.path_finder_helper {
	import flash.geom.Point;
	
	public class PFHelper {
		
		protected var dataPath:Array;
		protected var vel:Point = new Point(2, 2);
		protected var _gridWidth:Number = 32;
		protected var _gridHeight:Number = 32;
		protected var _stepCount:Number = 3;
		protected var _stepIdx:Number = 0;
		protected var dataIdx:int = 0;
		protected var _pos:Point = new Point();
		protected var posAwal:Point = new Point();
		protected var _positionOnGrid:Function;
		protected var _checkCanMoveToPos:Function;
		protected var _onUpdate:Function;
		protected var _dist:Point = new Point();
		protected var _onTile:Boolean = false;
		protected var state:int = 0;
		
		protected var rotAkhir:Number = 0;
		protected var rotAwal:Number = 0;
		protected var rotIdx:Number = 0;
		protected var _rotCount:Number = 0;
		protected var prevRot:Number = 0;
		protected var _rotation:Number = 0;
		protected var rotGap:Number = 0;
		protected var _rotSpeed:Number = 0;
		
		//status data
		protected var _finish:Boolean = false;
		protected var _stopped:Boolean = false;
		protected var _aktif:Boolean = false;
		
		protected static const ROTATE:int = 1;		
		protected static const JALAN:int = 2;
		
		protected static const RAD2ANGLE:Number = 180.0 / Math.PI;	
		protected static const ANGLE2RAD:Number = Math.PI / 180.0;
		
		public function PFHelper() {
			//addEventListener(Event.ENTER_FRAME, update, false, 0, true);
		}
		
		protected function reset():void {
			_aktif = false;
		}
		
		public function stop():void {
			_aktif = false;
			_stopped = true;
			_finish = true;
		}
		
		public function start(data:Array):void {
			var obj:Object;
			
			//trace('pf helper start ' + JSON.stringify(data));
			
			if (data.length == 0) {
				throw new Error();
			}
			
			this.dataPath = data;
			_aktif = true;
			_finish = false;
			_stopped = false;
			dataIdx = 0;
			
			obj = data[0];
			
			_pos.x = obj[0] * _gridWidth;
			_pos.y = obj[1] * _gridHeight;
			
			posAwal.x = _pos.x;
			posAwal.y = _pos.y;
			
			state = JALAN;
			
			dataIdx++;
			if (dataIdx >= dataPath.length) {
				stop();
			}
			else {
				dataNext();
			}
		}
		
		protected function dataNext():void {
			var obj:Object;
			
			obj = dataPath[dataIdx];
			
			if (_checkCanMoveToPos != null) {
				if (_checkCanMoveToPos(obj[0], obj[1]) == false) {
					stop();
					return;
				}
			}
			
			_dist.x = (obj[0] * _gridWidth - _pos.x);
			_dist.y = (obj[1] * _gridHeight - _pos.y);
			
			posAwal.x = _pos.x;
			posAwal.y = _pos.y;
			_stepIdx = 0;
			
			rotAkhir = Math.atan2((obj[1] * _gridHeight) - _pos.y, (obj[0] * _gridWidth) - _pos.x);
			rotAkhir *= RAD2ANGLE;
			if (rotAkhir < 0) rotAkhir = 360 + rotAkhir;
			rotAwal = prevRot;
			rotGap = getGap();
			_rotCount = Math.floor(Math.abs(rotGap / _rotSpeed));
			rotIdx = 0;
			
			if (_rotSpeed > 0 && (_rotCount > 0)) {
				state = ROTATE;
			}
			else {
				state = JALAN;
			}
			
			_onTile = true;		
		}
		
		protected function normalizeAngle(angle:Number):Number {
			while (angle >= 360) {
				angle -= 360;
			}
			
			while (angle < 0) {
				angle += 360;
			}
			
			return angle;
		}
		
		protected function getGap():Number {
			var gap:Number = 0;
			
			rotAkhir = normalizeAngle(rotAkhir);
			rotAwal = normalizeAngle(rotAwal);
			
			if (rotAkhir > rotAwal) {
				gap = rotAkhir - rotAwal;
				if (gap > 180) {
					gap = rotAwal + 360 - rotAkhir;
					gap *=-1;
				}
			}
			else {
				gap = rotAwal - rotAkhir;
				if (gap > 180) {
					gap = rotAkhir + 360 - rotAwal;
					gap *=-1;
				}
				
				gap *=-1;
			}
			
			//trace('get gap, rot akhir ' + rotAkhir + '/rot awal ' + rotAwal + '/gap ' + gap);
			
			return gap;
		}
		
		public function update():void {
			if (_aktif == false || _finish) {
				return;
			}
			
			_onTile = false;
			
			if (state == JALAN) {
				_stepIdx++;
				
				_pos.x = posAwal.x + (_stepIdx / _stepCount) * _dist.x;
				_pos.y = posAwal.y + (_stepIdx / _stepCount) * _dist.y;
				
				if (_stepIdx >= _stepCount) {
					
					dataIdx++;
					if (dataIdx >= dataPath.length) {
						stop();
						
						//_finish = true;
						//_aktif = false;
						//_stopped = false;
						return;
					}
					else {
						dataNext();
					}
				}				
			}
			else if (state == ROTATE) {
				_rotation = rotAwal + (rotIdx / _rotCount) * rotGap;
				
				rotIdx++;
				if (rotIdx > _rotCount) {
					state = JALAN;
					prevRot = _rotation;
					//trace('rot end');
				}
			}
			else {
				throw new Error();
			}
			
		}
		
		public function get gridWidth():Number {
			return _gridWidth;
		}
		
		public function set gridWidth(value:Number):void {
			_gridWidth = value;
		}
		
		public function get gridHeight():Number {
			return _gridHeight;
		}
		
		public function set gridHeight(value:Number):void {
			_gridHeight = value;
		}
		
		public function get stepCount():Number {
			return _stepCount;
		}
		
		public function set stepCount(value:Number):void {
			_stepCount = value;
		}
		
		public function get pos():Point {
			return _pos;
		}
		
		public function set pos(value:Point):void {
			_pos = value;
		}
		
		public function get positionOnGrid():Function {
			return _positionOnGrid;
		}
		
		public function set positionOnGrid(value:Function):void {
			_positionOnGrid = value;
		}
		
		public function get checkCanMoveToPos():Function {
			return _checkCanMoveToPos;
		}
		
		public function set checkCanMoveToPos(value:Function):void {
			_checkCanMoveToPos = value;
		}
		
		public function get onUpdate():Function {
			return _onUpdate;
		}
		
		public function set onUpdate(value:Function):void {
			_onUpdate = value;
		}
		
		public function get aktif():Boolean {
			return _aktif;
		}
		
		public function get dist():Point {
			return _dist;
		}
		
		public function set dist(value:Point):void {
			_dist = value;
		}
		
		public function get finish():Boolean {
			return _finish;
		}
		
		public function set finish(value:Boolean):void {
			_finish = value;
		}
		
		public function get stopped():Boolean {
			return _stopped;
		}
		
		public function get onTile():Boolean 
		{
			if (_onTile) {
				_onTile = false;
				return true;
			}
			
			return _onTile;
		}
		
		public function set onTile(value:Boolean):void 
		{
			_onTile = value;
		}
		
		public function get rotation():Number 
		{
			return _rotation;
		}
		
		public function get rotSpeed():Number 
		{
			return _rotSpeed;
		}
		
		public function set rotSpeed(value:Number):void 
		{
			_rotSpeed = value;
		}
		
	}

}