package fg.tmxs {
	import flash.display.Bitmap;
	import flash.display.DisplayObjectContainer;
	import flash.display.Sprite;
	import flash.geom.Point;

	public class Layer {
		
		public static const LYR_LAYER:String = 'layer';
		public static const LYR_IMAGE:String = 'imagelayer';
		public static const LYR_OBJECT:String = 'objectgroup';
		public static const LYR_PROP:String = 'properties';
		public static const LYR_TILESET:String = 'tileset';
		
		protected var _name:String = '';
		protected var _width:Number = 0;
		protected var _height:Number = 0;
		protected var _tiles:Vector.<TileMap> = new Vector.<TileMap>();
		protected var _objs:Vector.<TileObject> = new Vector.<TileObject>();
		protected var _type:String = '';
		protected var _tmx:Tmx;
		protected var _image:Image;
		protected var _offset:Point = new Point();
		protected var _visible:Boolean = true;
		protected var destroyed:Boolean = false;
		
		public function Layer() {
			
		}
		
		public function destroy():void {
			var tile:TileMap;
			var tileObj:TileObject;
			
			if (destroyed) return;
			destroyed = true;
			
			while (_objs.length > 0) {
				tileObj = _objs.pop();
				tileObj.destroy();
				tileObj = null;
			}
			
			while (_tiles.length > 0) {
				tile = _tiles.pop();
				tile.destroy();
				tile = null;
			}
			
			_image.destroy();
			
			_tmx = null;
			_image = null;
			_offset = null;
		}
		
		public function getObjByName(name:String):TileObject {
			var obj:TileObject;
			
			if (_type != LYR_OBJECT) {
				trace("WARN: This type of layer, doesn't support Object");
				return null;
			}
			
			for each (obj in objs) {
				if (obj.name == name) return obj;
			}
			
			trace('Object not found, name ' + name);
			return null;
		}
		
		public function getObjById(id:String):TileObject {
			var obj:TileObject;
			
			if (_type != LYR_OBJECT) {
				trace("WARN: This type of layer, doesn't support Object");
				return null;
			}
			
			for each (obj in objs) {
				if (obj.id == id) return obj;
			}
			
			trace('Object not found, id ' + id);
			return null;
		}
		
		protected function renderImage(cont:DisplayObjectContainer):void {
			_image.bmp.x = _offset.x;
			_image.bmp.y = _offset.y;
			cont.addChild(_image.bmp);
			trace(_image.bmp);
		}
		
		protected function objSwap(i:int, j:int):void {
			var tileObj:TileObject;
			
			tileObj = objs[i];
			objs[i] = objs[j];
			objs[j] = tileObj;
		}
		
		public function objSort():void {
			var obj:TileObject;
			var obj2:TileObject;
			var i:int;
			var j:int;
			
			if (_type == LYR_OBJECT) {
				for (i = 0; i < objs.length - 1; i++) {
					for (j = i + 1; j < objs.length; j++) {
						if (objs[i].y > objs[j].y) {
							objSwap(i, j);
						}
					}
				}
			}
		}
		
		protected function renderObject(cont:DisplayObjectContainer):void {
			var obj:TileObject;
			var spr:Sprite;
			
			for each (obj in objs) {
				spr = obj.sprite;
				spr.x = obj.x;
				spr.y = obj.y;
				spr.y -= spr.height;
				cont.addChild(spr);
			}
		}
		
		protected function renderLayer(cont:DisplayObjectContainer):void {
			var i:int;
			var j:int;
			var tile:TileMap;
			var bmp:Bitmap;
			
			trace('render layer ' + type + '/' + _name);
			
			for (j = 0; j < _height; j++) {
				for (i = 0; i < _width; i++) {
					tile = getTile(i, j);
					if (tile.gid > 0) {
						bmp = new Bitmap(_tmx.getBmpDataByGid(tile.gid));
						bmp.x = i * _tmx.tw;
						bmp.y = j * _tmx.th;
						bmp.y -= bmp.height;
						bmp.y += _tmx.th;
						cont.addChild(bmp);
					}
				}
			}			
		}
		
		public function render(cont:DisplayObjectContainer):void {
			var contSpr:Sprite = new Sprite();
			
			if (_type == LYR_LAYER) {
				renderLayer(contSpr);
			}
			else if (_type == LYR_OBJECT) {
				renderObject(contSpr);
			}
			else if (_type == LYR_IMAGE) {
				renderImage(contSpr);
			}
			else {
				throw new Error('');
			}
			
			cont.addChild(contSpr);
		}
		
		public function getTile(x:Number, y:Number):TileMap {
			var idx:Number;
			
			if (_type != LYR_LAYER) {
				trace("WARNING: this layer doeesn't support tile");
				return null;
			}
			
			idx = y * _width + x;
			
			if (idx < _tiles.length) return _tiles[idx];
			
			return null;
		}
		
		public function get name():String {
			return _name;
		}
		
		public function set name(value:String):void {
			_name = value;
		}
		
		public function get width():Number {
			return _width;
		}
		
		public function set width(value:Number):void {
			_width = value;
		}
		
		public function get height():Number {
			return _height;
		}
		
		public function set height(value:Number):void {
			_height = value;
		}
		
		public function get tiles():Vector.<TileMap> {
			return _tiles;
		}
		
		public function set tiles(value:Vector.<TileMap>):void {
			_tiles = value;
		}
		
		public function get type():String {
			return _type;
		}
		
		public function set type(value:String):void {
			_type = value;
		}
		
		public function get tmx():Tmx {
			return _tmx;
		}
		
		public function set tmx(value:Tmx):void {
			_tmx = value;
		}
		
		public function get objs():Vector.<TileObject> {
			return _objs;
		}
		
		public function set objs(value:Vector.<TileObject>):void {
			_objs = value;
		}
		
		public function get image():Image {
			return _image;
		}
		
		public function set image(value:Image):void {
			_image = value;
		}
		
		public function get offset():Point {
			return _offset;
		}
		
		public function set offset(value:Point):void {
			_offset = value;
		}
		
		public function get visible():Boolean {
			return _visible;
		}
		
		public function set visible(value:Boolean):void {
			_visible = value;
		}
		
	}

}