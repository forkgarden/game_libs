package fg.tmxs {
	/**
	 * ...
	 * @author 
	 */
	public class Property {
		
		protected var _name:String = '';
		protected var _type:String = '';
		protected var _value:String = '';
		
		public function Property() {
			
		}
		
		public function destroy():void {
			
		}
		
		public function get name():String {
			return _name;
		}
		
		public function set name(value:String):void {
			_name = value;
		}
		
		public function get type():String {
			return _type;
		}
		
		public function set type(value:String):void {
			_type = value;
		}
		
		public function get value():String {
			return _value;
		}
		
		public function set value(value:String):void {
			_value = value;
		}
		
	}

}