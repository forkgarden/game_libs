package fg.tmxs {
	import flash.display.BitmapData;
	/**
	 * ...
	 * @author 
	 */
	public class BmpData {
		
		protected var _bmpData:BitmapData;
		protected var _id:Number = 0;
		protected var destroyed:Boolean = false;
		
		public function BmpData() {
			
		}
		
		public function destroy():void {
			if (destroyed) return;
			destroyed = true;
			
			_bmpData.dispose();
			_bmpData = null;
		}
		
		public function get bmpData():BitmapData {
			return _bmpData;
		}
		
		public function set bmpData(value:BitmapData):void {
			_bmpData = value;
		}
		
		public function get id():Number {
			return _id;
		}
		
		public function set id(value:Number):void {
			_id = value;
		}
		
	}

}