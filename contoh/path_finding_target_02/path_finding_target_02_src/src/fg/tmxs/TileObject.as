package fg.tmxs {
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Sprite;

	public class TileObject {
		
		protected var _id:String = '';
		protected var _gid:Number = 0;
		protected var _x:Number = 0;
		protected var _y:Number = 0;
		protected var _width:Number = 0;
		protected var _height:Number = 0;
		protected var _layer:Layer;
		protected var _properties:Vector.<Property> = new Vector.<Property>();
		protected var _rotation:Number = 0;
		protected var _bitmapData:BitmapData;
		protected var _tmx:Tmx;
		protected var _name:String = '';
		protected var _bmp:Bitmap;
		protected var _sprite:Sprite;
		protected var destroyed:Boolean = false;
		
		public function TileObject() {
			
		}
		
		public function destroy():void {
			var prop:Property;
			
			if (destroyed) return;
			destroyed = true;
			
			while (_properties.length > 0) {
				prop = _properties.pop();
				prop.destroy();
			}
			
			if (_bitmapData != null) _bitmapData.dispose();
			_bitmapData = null;
			
			if (_bmp.bitmapData) _bmp.bitmapData.dispose();
			_bmp = null;
			_sprite = null;
			_layer = null;
			_tmx = null;
		}
		
		public function hasProp(name:String):Boolean {
			var prop:Property;
			
			for each (prop in _properties) {
				if (prop.name == name) {
					return true;
				}
			}
			
			return false;
		}
		
		public function getPropValueByName(name:String):String {
			var prop:Property;
			
			for each (prop in _properties) {
				if (prop.name == name) {
					return prop.value;
				}
			}
			
			return null;
		}
		
		protected function validate(n:Object):void {
			if (n == null) throw new Error();
			if (n is Number && isNaN(n as Number)) throw new Error();
		}
		
		public function get id():String {
			return _id;
		}
		
		public function set id(value:String):void {
			_id = value;
		}
		
		public function get x():Number {
			return _x;
		}
		
		public function set x(value:Number):void {
			_x = value;
		}
		
		public function get y():Number {
			return _y;
		}
		
		public function set y(value:Number):void {
			_y = value;
		}
		
		public function get width():Number {
			return _width;
		}
		
		public function set width(value:Number):void {
			validate(value);
			_width = value;
		}
		
		public function get height():Number {
			return _height;
		}
		
		public function set height(value:Number):void {
			validate(value);
			_height = value;
		}
		
		public function get layer():Layer {
			return _layer;
		}
		
		public function set layer(value:Layer):void {
			validate(value);
			_layer = value;
		}
		
		public function get gid():Number {
			return _gid;
		}
		
		public function set gid(value:Number):void {
			validate(value);
			_gid = value;
		}
		
		public function get rotation():Number {
			return _rotation;
		}
		
		public function set rotation(value:Number):void {
			_rotation = value;
		}
		
		public function get tmx():Tmx {
			return _tmx;
		}
		
		public function set tmx(value:Tmx):void {
			_tmx = value;
		}
		
		public function get bitmapData():BitmapData {
			return _tmx.getBmpDataByGid(_gid);
		}
		
		public function get name():String {
			return _name;
		}
		
		public function set name(value:String):void {
			_name = value;
		}
		
		public function get bmp():Bitmap {
			if (_bmp) return _bmp;
			
			_bmp = new Bitmap(_tmx.getBmpDataByGid(_gid));
			return _bmp;
		}
		
		public function set bmp(value:Bitmap):void {
			_bmp = value;
		}
		
		public function get sprite():Sprite {
			if (_sprite) return _sprite;
			
			_sprite = new Sprite();
			_sprite.addChild(bmp);
			
			_sprite.x =  _x;
			_sprite.y = _y;
			
			//normalize
			_sprite.y -= _sprite.height;
			
			return _sprite;
		}
		
		public function set sprite(value:Sprite):void {
			_sprite = value;
		}
		
		public function get properties():Vector.<Property> 
		{
			return _properties;
		}
		
		public function set properties(value:Vector.<Property>):void 
		{
			_properties = value;
		}
		
	}

}