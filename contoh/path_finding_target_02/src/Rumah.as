package
{
	import MainTarget;
	import fg.tmxs.TileObject;
	import flash.display.Sprite;
	import flash.geom.Point;
	
	public class Rumah extends Sprite
	{
		
		protected var _id:String;
		protected var _posRefX:int;
		protected var _PosRefY:int;
		protected var _view:Sprite;
		protected static var _lists:Vector.<Rumah> = new Vector.<Rumah>();
		
		public function Rumah()
		{
		
		}
		
		public static function getObjByView(view:Sprite):Rumah {
			var rumah:Rumah;
			
			for each (rumah in _lists) {
				if (rumah.view == view) return rumah;
			}
			
			return null;
		}
		
		public static function create(id:String, obj:TileObject):void {
			var rumah:Rumah;
			
			rumah = new Rumah();
			rumah.id = id;
			rumah.initFromTmapObj(obj);
			
			_lists.push(rumah);
		}
		
		public function initFromTmapObj(obj:TileObject):void {
			//set sprite
			_view = obj.sprite;
			
			_view.mouseEnabled = true;
			_view.mouseChildren = false;			
			
			//hitung posisi reference dari rumah
			//hitung posisi x
			_posRefX = parseFloat(obj.getPropValueByName('targetx'));
			
			//rubah unit dari tile ke pixel
			_posRefX *= MainTarget.TILE_SIZE;
			
			//tambahkan dengan posisi rumah sebenarnya
			_posRefX += _view.x;
			
			//rubah unit kembali ke tile
			_posRefX = Math.floor(_posRefX / MainTarget.TILE_SIZE);
			
			//posisi y
			_PosRefY = parseFloat(obj.getPropValueByName('targety'));
			_PosRefY *= MainTarget.TILE_SIZE;
			_PosRefY += _view.y;
			_PosRefY = Math.floor(_PosRefY / MainTarget.TILE_SIZE);
			
			trace('init from obj ' + _posRefX + '/' + _PosRefY);
		}
		
		public function get posRefX():int
		{
			return _posRefX;
		}
		
		public function get posRefY():int 
		{
			return _PosRefY;
		}
		
		public function get view():Sprite 
		{
			return _view;
		}
		
		public function get id():String 
		{
			return _id;
		}
		
		public function set id(value:String):void 
		{
			_id = value;
		}
		
		static public function get lists():Vector.<Rumah> 
		{
			return _lists;
		}
		
		public function set posRefX(value:int):void 
		{
			_posRefX = value;
		}
		
		public function set posRefY(value:int):void 
		{
			_PosRefY = value;
		}
	
	}

}