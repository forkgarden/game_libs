package 
{
	import fg.mazes.Maze;
	import flash.geom.Point;
	public class RandomPeti 
	{
		protected var acak:AcakKartu;
		protected var jmlPeti:int = 10;
		protected var _poss:Vector.<Point> = new Vector.<Point>();
		protected var jmlKolom:int = 0;
		
		public function RandomPeti() 
		{
			var i:int;
			var pos:Point;
			var jml:int;
			
			jmlKolom = Main.inst.mapHandler.maze.width;
			jml = Main.inst.mapHandler.maze.width * Main.inst.mapHandler.maze.height;
			
			acak = new AcakKartu();
			acak.count = jml;
			
			for (i = 0; i < jmlPeti; i++) {
				pos = new Point();
				getPos(pos);
				_poss.push(pos);
			}
			
			//trace(JSON.stringify(_poss));
			
		}
		
		protected function getPos(pos:Point):void {
			var idx:int;
			
			while (true) {
				idx = acak.getNumber();
				idxToPos(pos, idx);
				if (validPos(pos)) {
					return;
				}
			}
		}
		
		protected function posSama(p1:Point, p2:Point):Boolean {
			if (p1.x != p2.x) return false;
			if (p1.y != p2.y) return false;
			
			return true;
		}
		
		protected function validPos(p:Point):Boolean {
			var p2:Point;
			var map:Maze;
			
			map = Main.inst.mapHandler.maze;
			
			if (p.x >= map.width) return false;
			if (p.y >= map.height) return false;
			
			if (Main.inst.mapHandler.maze.map[p.x][p.y] > 0) return false;
			
			for each (p2 in _poss) {
				if (posSama(p2, p)) return false;
			}
			
			return true;
		}
		
		protected function idxToPos(p:Point, idx:int):void {
			p.x = idx % jmlKolom;
			p.y = Math.floor(idx / jmlKolom);
		}
		
		public function get poss():Vector.<Point> 
		{
			return _poss;
		}
		
		
	}

}