package 
{
	import fg.ui.Teks;
	import fg.ui.Tombol;
	import fg.ui.VLayout;
	import flash.display.Sprite;

	public class Soal extends Sprite
	{
		protected var soalTxt:Teks;
		protected var jawabs:Vector.<Tombol>;
		protected var vlayout:VLayout;
		protected var jawabBenar:int = 0;
		protected var jawabUser:int = 0;
		protected var overlay:Sprite;
		
		public function Soal() 
		{
			var tombol:Tombol;
			
			overlay = new Sprite();
			
			vlayout = new VLayout();
			
			soalTxt = new Teks();
			soalTxt.text = 'Teks Soal';
			vlayout.addItem(soalTxt);
			
			jawabs = new Vector.<Tombol>();
			
			addJawaban('jawab 1');
			addJawaban('jawab 2');
			addJawaban('jawab 3');
			addJawaban('jawab 4');
			
			addChild(vlayout);
		}
		
		public function show():void {
			
		}
		
		protected function addJawaban(label:String):void {
			var tombol:Tombol;
			
			tombol = new Tombol();
			tombol.onClick = tombolOnClick;
			tombol.label = label;
			jawabs.push(tombol);
			vlayout.addItem(tombol);			
		}
		
		protected function getTombolIdx(tombol:Tombol):int {
			var i:int;
			
			for (i = 0; i < jawabs.length; i++) {
				if (jawabs[i] == tombol) return i;
			}
			
			throw new Error();
			return null;
		}
		
		protected function tombolOnClick(tombol:Tombol):void {
			jawabUser = getTombolIdx(tombol);
			this.visible = false;
		}
		
	}

}