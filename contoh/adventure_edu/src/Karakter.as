package 
{
	import SceneObj;
	import fg.path_finder_helper.PFHelper;
	import fg.pathfinders.PathFinder;
	import flash.display.Bitmap;
	import flash.events.MouseEvent;

	public class Karakter extends SceneObj {
		
		protected var inst:Karakter;
		protected var pf:PathFinder;
		protected var pfh:PFHelper;
		protected var targetObj:SceneObj;
		
		public function Karakter() {
			if (inst) throw new Error();
			inst = this;
			
			pf = new PathFinder();
			pf.maxNodes = 100;
			pf.checkCanMoveToPos = checkTile;
			pf.checkSampai = pfCheckSampai;
			
			pfh = new PFHelper();
			pfh.gridWidth = Main.TILE_SIZE;
			pfh.gridHeight = Main.TILE_SIZE;
			pfh.checkCanMoveToPos = checkTile;
			
			//this.graphics.beginFill(0x0000ff, 1);
			//this.graphics.drawCircle(Main.TILE_SIZE/2, Main.TILE_SIZE/2, Main.TILE_SIZE/2);
			//this.graphics.endFill();
			addChild(new Bitmap(Main.inst.bmp.getBmpById('char').bmp.bitmapData));
		}
		
		protected function pfCheckSampai(ix:int, jx:int):Boolean {
			//trace ('pf check sampai ' + targetObj);
			
			if (targetObj) {
				//trace('check sampai');
				if (ix == targetObj.tilePos.x) {
					if (Math.abs(jx - targetObj.tilePos.y) == 1) return true;
				}
				
				if (jx == targetObj.tilePos.y) {
					if (Math.abs(ix - targetObj.tilePos.x) == 1) return true;
				}
				
				//trace('pf check sampai target gagal, pos ' + targetObj.tilePos + '/i ' + ix + '/jx ' + jx);
			}
			
			return false;
		}
		
		public function petiOnClick(peti:Peti):void {
			var ar:Array;
			var tileX:Number;
			var tileY:Number;
			
			if (peti == null) throw new Error();
			
			trace('peti click ' + peti);
			
			peti.setTilePos();
			tileX = peti.tilePos.x;
			tileY = peti.tilePos.y;
			
			jalan(tileX, tileY, peti);
		}
		
		protected function jalan(i:int, j:int, obj:SceneObj):void {
			var ar:Array;
			
			if (pfh.aktif == false) {
				setTilePos();
				
				targetObj = obj;
				ar = pf.find(_tilePos.x, _tilePos.y, i, j);
				
				//bila path ketemu
				if (ar.length > 0) { 
					pfh.start(ar);
					return;
				}
			}
			
			targetObj = null;
		}
		
		public function mapOnClick(evt:MouseEvent):void {
			var tileX:Number;
			var tileY:Number;
			
			tileX = Math.floor((stage.mouseX + Main.inst.contRect.x) / Main.TILE_SIZE);
			tileY = Math.floor((stage.mouseY + Main.inst.contRect.y) / Main.TILE_SIZE);
			
			jalan(tileX, tileY, null);
		}
		
		protected function checkTile(i:int, j:int):Boolean {
			if (i < 0) return false;
			if (j < 0) return false;
			if (i >= Main.inst.mapHandler.maze.width) return false;
			if (j >= Main.inst.mapHandler.maze.height) return false;
			
			if (Main.inst.mapHandler.maze.map[i][j] > 0) return false;
			
			var peti:Peti;
			for each (peti in Main.inst.petiLists) {
				peti.setTilePos();
				if (peti.tilePos.x == i) {
					if (peti.tilePos.y == j) {
						return false;
					}
				}
			}
			
			return true;
		}
		
		protected function checkSampaiTarget(obj:SceneObj):Boolean {
			obj.setTilePos();
			setTilePos();
			
			if (obj is Peti) {
				if (_tilePos.x == obj.tilePos.x) {
					if (Math.abs(_tilePos.y - obj.tilePos.y) == 1) return true;
				}
				
				if (_tilePos.y == obj.tilePos.y) {
					if (Math.abs(_tilePos.x - obj.tilePos.x) == 1) return true;
				}
			}
			
			return false;
		}
		
		public function update():void {
			if (pfh.aktif) {
				pfh.update();
				
				this.x = pfh.pos.x;
				this.y = pfh.pos.y;
				
				if (pfh.finish) {
					if (targetObj) {
						if (checkSampaiTarget(targetObj)) {
							trace('sudah sampai');
							Main.inst.cont.addChild(Main.inst.soal);
						}
						else {
							trace('belum sampai');
						}
					}
				}
			}
		}
		
	}

}