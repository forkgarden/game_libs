package  {
	import flash.display.Sprite;
	import flash.geom.Point;
	import Main;

	public class SceneObj extends Sprite {
		
		protected var _view:Sprite;
		protected var _ref:SceneObj;
		protected var _tilePos:Point = new Point();
		
		public function SceneObj() {
			
		}
		
		public function onAction():void {
		}
		
		public function inRange2(ix:int, jx:int):Boolean {
			//TODO
			return false;
		}
		
		public function posisiKarakterDiPinggirObj():Boolean {
			//TODO:
			return true;
		}
		
		public function setTilePos():void {
			_tilePos.x = Math.floor(x / Main.TILE_SIZE);
			_tilePos.y = Math.floor(y / Main.TILE_SIZE);
		}
		
		public function get tilePos():Point 
		{
			return _tilePos;
		}
	}
}