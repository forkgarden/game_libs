package
{
	import fg.bmps.BmpBLoader;
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.ui.Multitouch;
	import flash.ui.MultitouchInputMode;
	
	public class Main extends Sprite 
	{
		
		public static const TILE_SIZE:int = 64;
		
		public static const C_WIDTH:Number = 800;
		public static const C_HEIGHT:Number = 480;
		
		protected static var _inst:Main;
		
		protected var _mapHandler:Map;
		protected var _karakter:Karakter;
		protected var _cont:Sprite = new Sprite();
		protected var cam:Camera;
		protected var _contRect:Rectangle;
		protected var randPeti:RandomPeti;
		protected var _petiLists:Vector.<Peti>;
		protected var _soal:Soal;
		protected var _bmp:BmpBLoader;
		
		public function Main() 
		{
			stage.scaleMode = StageScaleMode.NO_SCALE;
			stage.align = StageAlign.TOP_LEFT;
			stage.addEventListener(Event.DEACTIVATE, deactivate);
			
			// touch or gesture?
			Multitouch.inputMode = MultitouchInputMode.TOUCH_POINT;
			
			// Entry point
			// New to AIR? Please read *carefully* the readme.txt files!
			_inst = this;
			
			addChild(_cont);
			
			_bmp = new BmpBLoader();
			_bmp.reg("box", "images/box.png");
			_bmp.reg("peti", "images/peti.png");
			_bmp.reg("rumput", "images/rumput.png");
			_bmp.reg("char", "images/depan01.png");
			_bmp.onLoadCallBack = bmpOnLoad;
			_bmp.load();
		}
		
		protected function bmpOnLoad():void {
			_mapHandler = new Map();
			_mapHandler.mouseEnabled = false;
			_mapHandler.mouseChildren = false;
			_cont.addChild(_mapHandler);
			
			_karakter = new Karakter();
			_karakter.x = TILE_SIZE;
			_karakter.y = TILE_SIZE;
			_karakter.mouseEnabled = false;
			_karakter.mouseChildren = false;
			_cont.addChild(_karakter);
			
			_soal = new Soal();
			//addChild(_soal);
			
			cam = new Camera();
			_contRect = new Rectangle(0, 0, Main.C_WIDTH, Main.C_HEIGHT);
			
			initPeti();
			
			stage.addEventListener(MouseEvent.CLICK, mapOnClick, false, 0, true);
			addEventListener(Event.ENTER_FRAME, update, false, 0, true);			
		}
		
		protected function initPeti():void {
			var p:Point;
			var peti:Peti;
			
			_petiLists = new Vector.<Peti>();
			randPeti = new RandomPeti();
			
			for each (p in randPeti.poss) {
				peti = new Peti();
				peti.x = p.x * TILE_SIZE;
				peti.y = p.y * TILE_SIZE;
				peti.addEventListener(MouseEvent.CLICK, petiOnClick, false, 0, true);
				_petiLists.push(peti);
				_cont.addChild(peti);
			}
		}
		
		protected function petiOnClick(evt:MouseEvent):void {
			evt.stopPropagation();
			
			_karakter.petiOnClick(evt.currentTarget as Peti);
		}
		
		protected function update(e:Event):void {
			_karakter.update();
			cam.update();
			
			_contRect.x = cam.pos.x;
			_contRect.y = cam.pos.y;
			_cont.scrollRect = _contRect;
			
		}
		
		protected function mapOnClick(e:MouseEvent):void {
			_karakter.mapOnClick(e);
		}
		
		private function deactivate(e:Event):void 
		{
			// make sure the app behaves well (or exits) when in background
			//NativeApplication.nativeApplication.exit();
		}
		
		public function get mapHandler():Map 
		{
			return _mapHandler;
		}
		
		public function set mapHandler(value:Map):void 
		{
			_mapHandler = value;
		}
		
		static public function get inst():Main 
		{
			return _inst;
		}
		
		public function get cont():Sprite 
		{
			return _cont;
		}
		
		public function set cont(value:Sprite):void 
		{
			_cont = value;
		}
		
		public function get karakter():Karakter 
		{
			return _karakter;
		}
		
		public function get contRect():Rectangle 
		{
			return _contRect;
		}
		
		public function get petiLists():Vector.<Peti> 
		{
			return _petiLists;
		}
		
		public function get soal():Soal 
		{
			return _soal;
		}
		
		public function set soal(value:Soal):void 
		{
			_soal = value;
		}
		
		public function get bmp():BmpBLoader 
		{
			return _bmp;
		}
		
	}
	
}