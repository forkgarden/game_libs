package 
{
	import fg.mazes.Maze;
	import flash.display.Sprite;

	public class Map extends Sprite {
		
		public static const MAPW:int = 8;
		public static const MAPH:int = 8;
		public static const ROOM_SIZE:int = 5;
		
		protected var _maze:Maze;
		
		public function Map() 
		{
			_maze = new Maze();
			_maze.create(MAPW, MAPH, ROOM_SIZE);
			_maze.drawWImg(
				this, 
				Main.inst.bmp.getBmpById('box').bmp.bitmapData, 
				Main.inst.bmp.getBmpById('rumput').bmp.bitmapData, 
				Main.TILE_SIZE
			);
		}
		
		public function get maze():Maze 
		{
			return _maze;
		}
		
	}

}