package 
{
	import flash.geom.Point;
	public class Camera 
	{
		protected var _pos:Point = new Point();
		protected var max:Point = new Point();
		protected var min:Point = new Point();
		protected var targetPos:Point = new Point();
		protected var smooth:Boolean = false;
		
		public function Camera() 
		{			
			
		}
		
		public function update():void {
			var gap:Number = 0;
			
			targetPos.x = Main.inst.karakter.x - Main.C_WIDTH / 2;
			targetPos.y = Main.inst.karakter.y - Main.C_HEIGHT / 2;
			
			if (smooth) {
				gap = targetPos.x - pos.x;
				if (Math.abs(gap) > 1) {
					gap *= .1;
					pos.x += gap;
				}
				
				gap = targetPos.y - pos.y;
				if (Math.abs(gap) > 1) {
					gap *= .1;
					pos.y += gap;
				}
			}
			else {
				pos.x = targetPos.x;
				pos.y = targetPos.y;
			}
			
			
			
		}
		
		public function get pos():Point 
		{
			return _pos;
		}
		
	}

}