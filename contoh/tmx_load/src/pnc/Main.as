package pnc {
	
	import fg.tmxs.Tmx;
	import fg.ui.Teks;
	import flash.display.Sprite;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.text.TextFormatAlign;
	
	[Frame(factoryClass="Preloader")]
	public class Main extends Sprite {
		
		protected var tmx:Tmx = new Tmx();
		protected var halLoading:Sprite;
		
		public function Main() {
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		//fungsi ini untuk membuat tampilan loading
		protected function halLoadingCreate():void {
			var teks:Teks = new Teks();
			
			halLoading = new Sprite();
			halLoading.addChild(teks);
			
			teks.text = 'Loading ...';
			teks.width = this.stage.stageWidth;
			teks.format.align = TextFormatAlign.CENTER;
			teks.height = teks.textHeight + 4;
			teks.y = this.stage.stageHeight / 2 - teks.height / 2;
			teks.formatApply();
		}
		
		protected function init(e:Event = null):void {
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			stage.scaleMode = StageScaleMode.SHOW_ALL;
			
			//buat halaman loading
			halLoadingCreate();
			addChild(halLoading);
			
			//insialisasi TMX untuk meload map
			tmx = new Tmx();
			tmx.url = 'map.tmx';
			
			//setelah map di load panggil fungsi tmxLoaded
			tmx.onLoadCallBack = tmxLoaded;
			tmx.load();
		}
		
		//fungsi ini dipanggil saat map selesai di load
		protected function tmxLoaded():void {
			//bersihkan layar
			this.removeChildren();
			
			//render tmx;
			tmx.render(this);
		}		
	}
}