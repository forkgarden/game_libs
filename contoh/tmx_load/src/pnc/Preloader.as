package pnc
{
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.text.TextField;
	import flash.utils.getDefinitionByName;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	
	/**
	 * ...
	 * @author test
	 */
	public class Preloader extends MovieClip {
		
		protected var loading_txt:TextField;
		protected var format:TextFormat;
		
		public function Preloader() 
		{
			if (stage) {
				stage.scaleMode = StageScaleMode.NO_SCALE;
				stage.align = StageAlign.TOP_LEFT;
			}
			addEventListener(Event.ENTER_FRAME, checkFrame);
			loaderInfo.addEventListener(ProgressEvent.PROGRESS, progress);
			loaderInfo.addEventListener(IOErrorEvent.IO_ERROR, ioError);
			
			// TODO show loader
			loading_txt = new TextField();
			loading_txt.text = 'Loading ...';
			loading_txt.x = 0;
			loading_txt.width = 640;
			loading_txt.y = 240;
			format = loading_txt.getTextFormat();
			format.align = TextFormatAlign.CENTER;
			loading_txt.setTextFormat(format);
			addChild(loading_txt);
		}
		
		private function ioError(e:IOErrorEvent):void 
		{
			trace(e.text);
		}
		
		private function progress(e:ProgressEvent):void 
		{
			// TODO update loader
		}
		
		private function checkFrame(e:Event):void 
		{
			if (currentFrame == totalFrames) 
			{
				stop();
				loadingFinished();
			}
		}
		
		private function loadingFinished():void 
		{
			removeEventListener(Event.ENTER_FRAME, checkFrame);
			loaderInfo.removeEventListener(ProgressEvent.PROGRESS, progress);
			loaderInfo.removeEventListener(IOErrorEvent.IO_ERROR, ioError);
			
			// TODO hide loader
			removeChild(loading_txt);
			startup();
		}
		
		private function startup():void 
		{
			var mainClass:Class = getDefinitionByName("pnc.Main") as Class;
			addChild(new mainClass() as DisplayObject);
		}
		
	}
	
}