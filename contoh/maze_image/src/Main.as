package
{
	import fg.mazes.Maze;
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;	

	[Frame(factoryClass="Preloader")]
	public class Main extends Sprite 
	{
		
		[Embed(source = "../bin/jalan.png")]
		protected var ImgJalan:Class;
		
		[Embed(source = "../bin/rumput.png")]
		protected var ImgRumput:Class;
		
		protected var maze:Maze;
		
		public function Main() 
		{
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}

		private function init(e:Event = null):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			// entry point
			
			this.stage.addEventListener(MouseEvent.CLICK, stageOnClick, false, 0, true);
			mazeCreate();
		}
		
		protected function mazeCreate():void {
			var imgJalan:Bitmap = new ImgJalan();
			var imgRumput:Bitmap = new ImgRumput();
			
			maze = new Maze();
			maze.create(6, 4, 3);
			maze.drawWImg(this, imgJalan.bitmapData, imgRumput.bitmapData);
		}
		
		/**
		 * Fungsi ini dipanggil saat map di click
		 * 
		 * @param	evt
		 */
		protected function stageOnClick(evt:MouseEvent):void {
			mazeCreate();
		}
	}

}