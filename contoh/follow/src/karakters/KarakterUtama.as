package karakters {
	import Main;
	import flash.display.Sprite;
	import karakters.Karakter;

	/*
	 * Karakter Utama
	 */
	public class KarakterUtama extends Karakter {
		
		[Embed(source = "../../bin/char.png")]
		private var ImgChar:Class;
				
		public function KarakterUtama() {
			addChild(new ImgChar());
		}
		
	}

}