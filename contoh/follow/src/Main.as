package{
	import fg.bmps.BmpBLoader;
	import fg.tmxs.Tmx;
	import flash.display.Sprite;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import karakters.KarakterUtama;
	import karakters.Meong;

	[Frame(factoryClass="Preloader")]
	public class Main extends Sprite {
		
		public static const TILE_SIZE:Number = 32;
		public static const MAP_WIDTH:Number = 20;
		public static const MAP_HEIGHT:Number = 15;
		
		protected static var _inst:Main;
		protected var _bmpBLoader:BmpBLoader;
		
		protected var _tmx:Tmx;
		protected var krUtama:KarakterUtama;
		protected var meong:Meong;
		
		public function Main() {
			_inst = this;
			
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}

		protected function init(e:Event = null):void {
			removeEventListener(Event.ADDED_TO_STAGE, init);
			this.stage.scaleMode = StageScaleMode.SHOW_ALL;
			
			//meload peta
			_tmx = new Tmx();
			_tmx.url = "map.tmx";
			_tmx.load();
			
			//Setelah selesai di load, panggil fungsi tmxLoaded
			_tmx.onLoadCallBack = tmxLoaded;
		}
		
		protected function tmxLoaded():void {
			//gambar map ke layar
			_tmx.render(this);
			
			//buat karakter utama
			krUtama = new KarakterUtama();
			krUtama.x = 32;
			krUtama.y = 32;
			addChild(krUtama);
			
			meong = new Meong();
			meong.karakterUtama = krUtama;
			meong.x = 320;
			meong.y = 64;
			addChild(meong);
			
			//add listener untuk mouse-event dan enter-frame event.
			this.stage.addEventListener(MouseEvent.CLICK, stageOnClick, false, 0, true);
			addEventListener(Event.ENTER_FRAME, update);			
		}
		
		//fungsi ini dipanggil saat stage di click
		protected function stageOnClick(e:MouseEvent):void {
			var posX:Number = 0;
			var posY:Number = 0;
				
			//hitung posisi tile yang di click
			posX = Math.floor(e.stageX / TILE_SIZE);
			posY = Math.floor(e.stageY / TILE_SIZE);
			
			krUtama.jalanKePosisi(posX, posY);
		}
		
		protected function update(e:Event):void {
			krUtama.update();
			meong.update();
		}
		
		static public function get inst():Main 
		{
			return _inst;
		}
		
		public function get tmx():Tmx 
		{
			return _tmx;
		}
	}

}