package sceneObj {
	import Main;
	import flash.display.Sprite;
	import sceneObj.SceneObj;

	/*
	 * Karakter Utama
	 */
	public class KarakterUtama extends SceneObj {
				
		public function KarakterUtama() {
			addChild(Main.inst.bmpBLoader.getBmpById('krkUtama').bmp);
		}
		
	}

}