package sceneObj {
	import fg.path_finder_helper.PFHelper;
	import fg.pathfinders.PathFinder;
	import fg.tmxs.TileObject;
	import flash.display.Sprite;
	import flash.geom.Point;
	import Main;

	public class SceneObj extends Sprite {
		
		protected var _tmxObj:TileObject;
		protected var _ref:SceneObj;
		protected var pathFinder:PathFinder;
		protected var pathFinderHelper:PFHelper;
		protected var _tilePos:Point;
		
		public function SceneObj() {
			
			//inisialisasi PathFinder
			pathFinder = new PathFinder();
			pathFinder.diagonalMove = true;
			
			//panggil fungsi checkCanMoveToPos untuk mengecek tile
			//apakah bisa dilalui atau tidak
			pathFinder.checkCanMoveToPos = checkCanMoveToPos;
			
			//inisialisasi PFHelper
			pathFinderHelper = new PFHelper();
			pathFinderHelper.gridWidth = Main.TILE_SIZE;
			pathFinderHelper.gridHeight = Main.TILE_SIZE;
			pathFinderHelper.stepCount = 4;
			
			//panggil fungsi checkCanMoveToPos untuk mengecek tile
			//apakah bisa dilalui atau tidak
			pathFinderHelper.checkCanMoveToPos = checkCanMoveToPos;
			
			_tilePos = new Point();
		}
		
		//fungsi ini dipanggil oleh pathFinder dan pathFinderHelper
		//untuk mengecek apakah sebuah tile bisa dilewati atau tidak
		protected function checkCanMoveToPos(i:int, j:int):Boolean {
			
			//bila posisi di luar map, maka tidk bisa dilewati
			if (i < 0) return false;
			if (i >= Main.MAP_WIDTH) return false;
			if (j < 0) return false;
			if (j >= Main.MAP_HEIGHT) return false;
			
			//bila posisinya ternyata adalah non-walkable, maka tidak bisa
			//dilewati
			//informasi mengenai non-walkable tile ada di layer block dari file tmx
			if (Main.inst.tmx.getLayerByName("obj").getTile(i, j).gid > 0) return false;
			
			return true;
		}
				
		
		//fungsi ini untuk menghandle karakter saat akan jalan
		//ke posisi tetentu di map
		public function jalanKePosisi(ix:int, jx:int):void {
			var ar:Array;
			
			//trace('jalan ke posisi ' + ix + '/' + jx);
			
			//bila karakter sedang tidak jalan
			if (pathFinderHelper.aktif == false) {
				
				//cari jalur terpendek
				ar = pathFinder.find(
							getTilePosX(),  
							getTilePosY(), 
							ix, 
							jx
						);
				
				//bila ada jalur
				//mulai jalan
				if (ar.length > 0) {
					pathFinderHelper.start(ar);
				}
			}
		}		
		public function update():void {
			if (pathFinderHelper.aktif == false) return;
			
			pathFinderHelper.update();
			
			//update posisi
			x = pathFinderHelper.pos.x;
			y = pathFinderHelper.pos.y;			
		}
		
		//hitung posisi tileX dari object
		public function getTilePosX():Number {
			var res:Number = 0;
			
			res = Math.floor(x / Main.TILE_SIZE);
			
			return res;
		}
		
		//TODO: refactoring
		public function inRange2(ix:int, jx:int):Boolean {
			var i:int;
			var j:int;
			var w:int;
			var h:int;
			var px:int;
			var py:int;
			
			w = parseInt(_tmxObj.getPropValueByName("w"), 10);
			h = parseInt(_tmxObj.getPropValueByName("h"), 10);
			
			for (i = 0; i < w; i++) {
				for (j = 0; j < h; j++) {
					px = getTilePosX() + i;
					py = getTilePosY() - j;
					if (Math.abs(ix - px) == 1) {
						if (Math.abs(jx - py) == 0) {
							return true;
						}
					}
					else if (Math.abs(ix - px) == 0) {
						if (Math.abs(jx - py) == 1) {
							return true;
						}
					}
					trace('p ' + px + '-' + py);
				}
			}
			
			trace("test in range failed, w " + w + '/h ' + h + '/ix ' + ix + '/jx ' + jx + '/gridpos ' + getTilePosX() + '-' + getTilePosY());
			return false;
		}
		
		public function getTilePosY():Number {
			var res:Number;
			
			res = Math.floor(y / Main.TILE_SIZE);
			
			return res;
		}
		
		public function get ref():SceneObj 
		{
			return _ref;
		}
		
		public function get tmxObj():TileObject 
		{
			return _tmxObj;
		}
		
		public function get tilePos():Point 
		{
			_tilePos.x = getTilePosX();
			_tilePos.y = getTilePosY();
			return _tilePos;
		}
	}
}