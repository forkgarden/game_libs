package sceneObj 
{
	import sceneObj.SceneObj;
	public class Meong extends SceneObj
	{
		protected var _karakterUtama:KarakterUtama;
		
		public function Meong() 
		{
			addChild(Main.inst.bmpBLoader.getBmpById('meong').bmp);
			pathFinder.checkSampai = checkSampai;
			pathFinder.diagonalMove = false;
			pathFinder.maxNodes = 50;
		}
		
		/**
		 * Check apakah pathfinder boleh berhenti mencari jalan
		 * karena posisi sudah dekat
		 * 
		 * @param	ix posisi horizontal sekarang
		 * @param	jx posisi vertical sekarang
		 * @return
		 */
		protected function checkSampai(ix:Number, jx:Number):Boolean {
			var jarakX:Number;
			var jarakY:Number;
			var jarak:Number;
			
			jarakX = Math.abs(_karakterUtama.tilePos.x - ix);
			jarakY = Math.abs(_karakterUtama.tilePos.y - jx);
			jarak = Math.sqrt(jarakX * jarakX + jarakY * jarakY);
			
			return jarak <= 3;
		}
		
		/**
		 * higunt jarak dari posisi Meong ke karakter utama
		 * @return
		 */
		public function jarakKrUtama():Number {
			var jarakX:Number;
			var jarakY:Number;
			var jarak:Number;
			
			jarakX = Math.abs(_karakterUtama.tilePos.x - tilePos.x);
			jarakY = Math.abs(_karakterUtama.tilePos.y - tilePos.y);
			jarak = Math.sqrt(jarakX * jarakX + jarakY * jarakY);
			
			return jarak;
		}
		
		public override function update():void {
			if (pathFinderHelper.aktif) {
				pathFinderHelper.update();
				
				//update posisi
				x = pathFinderHelper.pos.x;
				y = pathFinderHelper.pos.y;				
			}
			else {
				//check apakah jarak ke karakter utama terlalu jauh
				//bila ya, jalan ke posisi karakter utama
				if (jarakKrUtama() > 3) {
					jalanKePosisi(_karakterUtama.tilePos.x, _karakterUtama.tilePos.y);
				}
			}
		}
		
		public function set karakterUtama(value:KarakterUtama):void 
		{
			_karakterUtama = value;
		}
		
	}

}