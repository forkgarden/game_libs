package pnc {
	import pnc.bullets.Bullet;
	import pnc.units.Unit;
	/**
	 * ...
	 * @author 
	 */
	public class Collider {
		protected var _bulletLists:Vector.<Bullet>;
		protected var _unitLists:Vector.<Unit>;
		
		public function Collider() {
			
		}
		
		protected function valid(bullet:Bullet, unit:Unit):Boolean {
			if (unit.view.parent == null) return false;
			if (bullet.group == unit.group) return false;
			if (bullet.view.hitTestObject(unit.view) == false) return false;
			
			return true;
		}
		
		protected function collideUnit(bullet:Bullet):void {
			var unit:Unit;
			
			for each (unit in _unitLists) {
				if (valid(bullet, unit)) { 
					unit.hp -= bullet.power;
					bullet.view.parent.removeChild(bullet.view);
					//throw new Error();
				}
			}
			
		}
		
		public function collide():void {
			var bullet:Bullet;
			var unit:Unit;
			
			//bullet vs enemy
			for each (bullet in bulletLists) {
				if (bullet.view.parent) {
					collideUnit(bullet);
				}
			}
			
		}
		
		public function get unitLists():Vector.<Unit> {
			return _unitLists;
		}
		
		public function set unitLists(value:Vector.<Unit>):void {
			_unitLists = value;
		}
		
		public function get bulletLists():Vector.<Bullet> {
			return _bulletLists;
		}
		
		public function set bulletLists(value:Vector.<Bullet>):void {
			_bulletLists = value;
		}
		
	}

}