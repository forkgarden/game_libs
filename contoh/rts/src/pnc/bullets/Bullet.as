package pnc.bullets {
	import flash.display.Sprite;
	import flash.geom.Point;
	import pnc.Main;
	/**
	 * ...
	 * @author 
	 */
	public class Bullet {
		
		//public static const GR_HERO:String = 'hero';
		//public static const GR_ENEMY:String = 'enemy';
		
		public static const TY_HERO:String = 'hero';
		public static const TY_CRIT:String = 'crit';
		
		protected var _view:Sprite;
		protected var _vel:Point = new Point(2, 2);
		protected var _speed:Number = 8;
		protected var _type:String = '';
		protected var _group:String = '';
		protected var _power:Number = 10;
		
		public function Bullet() {
			_view = new Sprite();
			_view.graphics.beginFill(0xff0000);
			_view.graphics.drawCircle(0, 0, 4);
			_view.graphics.endFill();
		}
		
		protected function isOutside():Boolean {
			if (_view.x < 0) return true;
			if (_view.x > Main.MZ_WIDTH * Main.GRID_WIDTH) return true;
			if (_view.y < 0) return true;
			if (_view.y > Main.MZ_HEIGHT * Main.GRID_HEIGHT) return true;
			
			return false;
		}
		
		public function update():void {
			if (_view.parent) {
				_view.x += _vel.x;
				_view.y += _vel.y;
				
				//outside screen
				if (isOutside()) {
					_view.parent.removeChild(_view);
				}
			}
		}
		
		public function get view():Sprite {
			return _view;
		}
		
		public function get vel():Point {
			return _vel;
		}
		
		public function get speed():Number {
			return _speed;
		}
		
		public function get type():String {
			return _type;
		}
		
		public function get group():String {
			return _group;
		}
		
		public function get power():Number {
			return _power;
		}
		
		public function set group(value:String):void {
			_group = value;
		}
		
	}

}