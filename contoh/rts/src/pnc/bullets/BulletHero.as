package pnc.bullets {
	import pnc.Main;
	
	public class BulletHero extends Bullet{
		
		public function BulletHero() {
			_type = TY_HERO;
			_group = Main.GROUP_HERO;
			_power = 10;
		}
		
	}

}