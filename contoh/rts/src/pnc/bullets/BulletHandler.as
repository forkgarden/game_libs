package pnc.bullets {
	import pnc.bullets.Bullet;

	public class BulletHandler {
		
		protected var _bulletLists:Vector.<Bullet> = new Vector.<Bullet>();
		
		public function BulletHandler() {
			
		}
		
		public function getBullet(ty:String):Bullet {
			var bullet:Bullet;
			
			for each (bullet in _bulletLists) {
				if (bullet.view.parent == null) {
					if (bullet.type == ty) return bullet;
				}
			}
			
			return null;
		}
		
		public function update():void {
			var bullet:Bullet;
			
			for each (bullet in _bulletLists) {
				bullet.update();
			}
		}
		
		public function get bulletLists():Vector.<Bullet> {
			return _bulletLists;
		}
		
		public function set bulletLists(value:Vector.<Bullet>):void {
			_bulletLists = value;
		}
		
	}

}