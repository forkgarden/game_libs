package pnc {
	import flash.geom.Point;
	import pnc.units.Unit;

	public class Cmd {
		
		public static const CMD_SERANG:String = 'serang';
		public static const CMD_JALAN:String = 'jalan';
		
		protected var _pos:Point = new Point();
		protected var _type:String = CMD_JALAN;
		protected var _active:Boolean = false;
		protected var _target:pnc.units.Unit;
		
		public function Cmd() {
			
		}
		
		public function reset():void {
			_type = '';
			_active = false;
			_target = null;
		}
		
		public function get pos():Point {
			return _pos;
		}
		
		public function set pos(value:Point):void {
			_pos = value;
		}
		
		public function get type():String {
			return _type;
		}
		
		public function set type(value:String):void {
			_type = value;
		}
		
		public function get active():Boolean {
			return _active;
		}
		
		public function set active(value:Boolean):void {
			_active = value;
		}
		
		public function get target():pnc.units.Unit {
			return _target;
		}
		
		public function set target(value:pnc.units.Unit):void {
			_target = value;
		}
		
	}

}