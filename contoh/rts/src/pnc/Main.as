package pnc{
	import fg.debugs.Debugger;
	import fg.mazes.Maze;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import pnc.bullets.BulletHandler;
	import pnc.Cmd;
	import pnc.units.Critter;
	import pnc.units.Hero;
	import pnc.units.UnitHandler;
	
	public class Main extends Sprite {
		
		public static const MZ_WIDTH:int = 16;
		public static const MZ_HEIGHT:int = 16;
		
		public static const GRID_WIDTH:int = 32;
		public static const GRID_HEIGHT:int = 32;
		
		public static const GROUP_HERO:String = 'group_hero';
		public static const GROUP_ENEMY:String = 'group_enemy';
		
		protected static var _inst:Main;
		
		protected var _maze:Maze = new Maze();
		protected var _hero:pnc.units.Hero = new pnc.units.Hero();
		protected var crit:pnc.units.Critter = new pnc.units.Critter();
		protected var debugger:Debugger;
		protected var _bulletH:BulletHandler = new BulletHandler();
		protected var _unitH:UnitHandler = new UnitHandler();
		protected var collider:Collider = new Collider();
		
		public function Main() {
			_inst = this;
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event = null):void {
			removeEventListener(Event.ADDED_TO_STAGE, init);
			addEventListener(Event.ENTER_FRAME, update, false, 0, true);
			
			// entry point
			_maze.create(16, 16, 4);
			_maze.draw(this, 32);
			hero.view.x = 32;
			hero.view.y = 32;
			addChild(hero.view);
			unitH.unitLists.push(hero);
			
			crit = new pnc.units.Critter();
			crit.view.x = 7 * GRID_WIDTH;
			crit.view.y = 7 * GRID_HEIGHT;
			addChild(crit.view);
			unitH.unitLists.push(crit);
			
			collider.bulletLists = _bulletH.bulletLists;
			collider.unitLists = unitH.unitLists;
			
			stage.addEventListener(MouseEvent.CLICK, stageClick, false, 0, true);
			
			addChild(Debugger.getInst());
		}
		
		protected function update(e:Event):void {
			_unitH.update();
			_bulletH.update();
			collider.collide();
			
			Debugger.getInst().addLine('', true);
			Debugger.getInst().addLine('hero state ' + hero.state.activeRec.id);
			Debugger.getInst().addLine('hero state main ' + hero.state.active.id);
			Debugger.getInst().addLine('pf helper, aktif ' + hero.pfHelper.aktif + '/stopped ' + hero.pfHelper.stopped + '/finish ' + hero.pfHelper.finish);
			Debugger.getInst().addLine('pf helper ' + hero.pfHelper.pos.x + '/' + hero.pfHelper.pos.y);
			Debugger.getInst().addLine('target ' + hero.target);
			Debugger.getInst().addLine('target in range ' + (hero.target?hero.targetInRange():false));
			Debugger.getInst().addLine('weapon ready ' + hero.weapon.ready + '/ctr ' + hero.weapon.ctr.ctr);
			Debugger.getInst().addLine('');
			
		}
		
		protected function stageClick(e:MouseEvent):void {
			hero.cmd.active = true;
			hero.cmd.type = pnc.Cmd.CMD_JALAN;
			hero.cmd.pos.x = e.stageX / GRID_WIDTH;
			hero.cmd.pos.y = e.stageY / GRID_HEIGHT;
		}
		
		public function get maze():Maze {
			return _maze;
		}
		
		public static function get inst():Main {
			return _inst;
		}
		
		public function get hero():pnc.units.Hero {
			return _hero;
		}
		
		public function get bulletH():BulletHandler {
			return _bulletH;
		}
		
		public function get unitH():UnitHandler {
			return _unitH;
		}
		
		public function set unitH(value:UnitHandler):void {
			_unitH = value;
		}
		
	}
	
}