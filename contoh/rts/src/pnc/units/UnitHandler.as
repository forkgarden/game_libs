package pnc.units {
	import pnc.units.Unit;

	public class UnitHandler {
		
		protected var _unitLists:Vector.<Unit> = new Vector.<Unit>();
		
		public function UnitHandler() {
			
		}
		
		public function update():void {
			var unit:Unit;
			
			for each (unit in _unitLists) {
				if (unit.view.parent) unit.update();
			}
		}
		
		public function get unitLists():Vector.<Unit> {
			return _unitLists;
		}
		
		public function set unitLists(value:Vector.<Unit>):void {
			_unitLists = value;
		}
		
	}

}