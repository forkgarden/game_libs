package pnc.units {
	import fg.hstates.State;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import pnc.Cmd;
	import pnc.Main;
	
	public class Critter extends Unit {
		
		public function Critter() {
			_view = new Sprite();
			
			_view.graphics.beginFill(0xff0000);
			_view.graphics.drawCircle(16, 16, 8);
			_view.graphics.endFill();
			
			//click area
			_view.graphics.beginFill(0, 0);
			_view.graphics.drawRect(0, 0, 32, 32);
			_view.graphics.endFill();
			
			_view.buttonMode = true;
			_view.useHandCursor = true;
			
			pathFinder.checkCanMoveToPos = checkCanMoveToTile;
			
			_pfHelper.checkCanMoveToPos = checkCanMoveToTile;
			_pfHelper.stepCount = 32;
			
			_group = Main.GROUP_ENEMY;
			
			_state = new State();
			_state.adds([ST_IDLE, ST_JALAN]);
			_state.select(ST_IDLE).setAsActiveState();
			
			_view.addEventListener(MouseEvent.CLICK, onClick, false, 0, true);			
		}
		
		protected function onClick(e:MouseEvent):void {
			var hero:Hero;
			
			e.stopPropagation();
			
			hero = Main.inst.hero;
			
			hero.cmd.active = true;
			hero.cmd.target = this;
			hero.cmd.type = Cmd.CMD_SERANG;
			
			trace('critter click');
		}
		
		public override function update():void {
			var ar:Array = [];
			var posX:Number;
			var posY:Number;
			
			_pfHelper.update();
			
			if (_state.activeRec.id == Unit.ST_IDLE) {
				//get random pos
				posX = Math.floor(Math.random() * Main.MZ_WIDTH);
				posY = Math.floor(Math.random() * Main.MZ_HEIGHT);
				
				gridPosSet();
				
				ar = pathFinder.find(_gridPos.x, _gridPos.y, posX, posY);
				
				if (ar.length > 0) {
					_pfHelper.start(ar);
					_state.select(ST_JALAN).setAsActiveState();
				}
				
			}
			else if (_state.activeRec.id == Unit.ST_JALAN) {
				if (_pfHelper.aktif) {
					_pfHelper.update();
					_view.x = _pfHelper.pos.x;
					_view.y = _pfHelper.pos.y;
					
					if (_pfHelper.finish) {
						_state.select(ST_IDLE).setAsActiveState();
					}
				}
			}
			else {
				throw new Error();
			}
		}
		
	}

}