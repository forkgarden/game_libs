package pnc.units {
	import fg.hstates.State;
	import fg.hstates.StateManager;
	import flash.display.Sprite;
	import pnc.Cmd;
	import pnc.Main;
	import pnc.units.Unit;
	import pnc.weapons.WeaponHero;
	
	public class Hero extends Unit {
		
		protected var _cmd:Cmd = new Cmd();
		protected var _target:Unit;
		
		public function Hero() {
			_view = new Sprite();
			_view.graphics.beginFill(0x0000ff);
			_view.graphics.drawCircle(16, 16, 16);
			_view.graphics.endFill();
			_view.mouseEnabled = false;
			
			pathFinder.checkCanMoveToPos = checkCanMoveToTile;
			_pfHelper.checkCanMoveToPos = checkCanMoveToTile;
			_pfHelper.stepCount = 4;
			
			_weapon = new WeaponHero();
			_weapon.owner = this;
			
			_weaponOffset.x = 16;
			_weaponOffset.y = 16;
			
			_posTembak.x = 16;
			_posTembak.y = 16;
			
			_state = new State();
			_state.adds([ST_IDLE, ST_JALAN, ST_SERANG]);
			_state.select(ST_SERANG).adds([ST_KEJAR, ST_TEMBAK]);
			_state.select(ST_IDLE).setAsActiveState();
			
			_group = Main.GROUP_HERO;
		}
		
		public function targetInRange():Boolean {
			return geom.getJarak(
						_view.x + _weaponOffset.x, 
						_view.y + _weaponOffset.y, 
						_target.view.x + _target.posTembak.x, 
						_target.view.y + _target.posTembak.y) < _weapon.range;
		}
		
		protected function weaponOnReady():void {
			if (_target) {
				if (targetInRange()) {
					_weapon.shoot(_target);
				}
				else if (isOnGrid() && _pfHelper.aktif == false) {
					//kejar lagi
					//_state = ST_IDLE;
					_cmd.active = true;
					_cmd.target = target;
					trace('kejar lagi');
				}
			}
		}
		
		/*
		protected function walkOnGrid():void {
			if (_target && targetInRange()) {
				pfHelper.stop();
			}
			
			if (_cmd.active) {
				pfHelper.stop();
				return;
			}
		}
		*/
		
		/*
		protected function walkEnd():void {
			if (_state == ST_KEJAR) {
				//musuh mati
				if (_target.view.parent == null) {
					_state = ST_IDLE;
					_target = null;
					return;
				}
				else {
					//musuh in range
					//musuh masih hidup
					//musuh jauh
					//_state = ST_IDLE;
					_cmd.active = true;
					_cmd.target = _target;
					_cmd.type = Cmd.CMD_SERANG;
				}
				
			}
			else {
				_state.child(ST_IDLE).setAsActiveState();
			}
			
		}
		*/
		
		protected function receiveCommand():void {
			var ar:Array;
			
			_pfHelper.stop();
			_target = null;
			
			if (_cmd.type == Cmd.CMD_SERANG) {
				
				_target = _cmd.target;
				_target.gridPosSet();
				gridPosSet();
				
				ar = pathFinder.find(_gridPos.x, _gridPos.y, _target.gridPos.x, _target.gridPos.y);
				if (ar.length > 0) {
					_state.select(ST_SERANG).select(ST_KEJAR).setAsActiveState();
					_pfHelper.start(ar);
					_cmd.reset();
				}
				
			}
			else if (_cmd.type == Cmd.CMD_JALAN) {
				gridPosSet();
				
				ar = pathFinder.find(_gridPos.x, _gridPos.y, _cmd.pos.x, _cmd.pos.y);
				if (ar.length > 0) {
					_pfHelper.start(ar);
					_state.select(ST_JALAN).setAsActiveState();
					_target = null;
				}
				
				_cmd.reset();
			}
		}
		
		protected function aiIdleUpdate():void {
			if (_cmd.active && isOnGrid()) {
				receiveCommand();
			}
		}
		
		protected function jalanUpdate():void {
			if (_pfHelper.aktif) {
				_pfHelper.update();
				
				_view.x = _pfHelper.pos.x;
				_view.y = _pfHelper.pos.y;	
				
				if (_pfHelper.finish) {
					_state.select(ST_IDLE).setAsActiveState();
				}
			}
		}
		
		protected function aiJalanUpdate():void {
			jalanUpdate();
			
			//interrupt command
			if (_cmd.active && isOnGrid()) {
				receiveCommand();
			}
		}
		
		protected function aiSerangUpdate():void {
			var ar:Array;
			
			if (_state.active.id != ST_SERANG) throw new Error();
			
			if (_state.activeRec.id == ST_KEJAR) {
				if (_pfHelper.aktif) {
					_pfHelper.update();
					
					_view.x = _pfHelper.pos.x;
					_view.y = _pfHelper.pos.y;	
				}
				else {
					if (_target && (targetInRange() == false)) {
						gridPosSet();
						_target.gridPosSet();
						ar = pathFinder.find(_gridPos.x, _gridPos.y, _target.gridPos.x, _target.gridPos.y);
						if (ar.length > 0) {
							_state.select(ST_SERANG).select(ST_KEJAR).setAsActiveState();
							_pfHelper.start(ar);
						}						
					}
				}
				
				//enemy on range
				if (isOnGrid() && targetInRange()) {
					_state.select(ST_SERANG).select(ST_TEMBAK).setAsActiveState();
					_pfHelper.stop();
				}
			}
			else if (_state.activeRec.id == ST_TEMBAK) {
				
				if (_weapon.ready) {
					if (_target && targetInRange()) {
						_weapon.shoot(_target);
					}
				}
				
				//target pindah dari range tembakan
				if (_target && targetInRange() == false) {
					gridPosSet();
					_target.gridPosSet();
					ar = pathFinder.find(_gridPos.x, _gridPos.y, _target.gridPos.x, _target.gridPos.y);
					if (ar.length > 0) {
						_state.select(ST_SERANG).select(ST_KEJAR).setAsActiveState();
						_pfHelper.start(ar);
					}
				}
				
				//target menghilang
				if (_target.view.parent == null) {
					_state.select(ST_IDLE).setAsActiveState();
				}
				
			}
			else {
				throw new Error();
			}
			
			//interupt command
			if (_cmd.active && isOnGrid()) {
				receiveCommand();
			}
		}
		
		public override function update():void {
			var ar:Array = [];
			
			_weapon.update();
			
			if (_state.active.id == Unit.ST_IDLE) {
				aiIdleUpdate();
			}
			else if (_state.active.id == Unit.ST_JALAN) {
				aiJalanUpdate();
			}
			else if (_state.active.id == Unit.ST_SERANG) {
				aiSerangUpdate();
			}
			
			
		}
		
		public function get cmd():Cmd {
			return _cmd;
		}
		
		public function set cmd(value:Cmd):void {
			_cmd = value;
		}
		
		public function get target():Unit {
			return _target;
		}
		
		public function set target(value:Unit):void {
			_target = value;
		}
		
	}

}