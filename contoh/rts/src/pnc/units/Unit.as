package pnc.units {
	import fg.geoms.Geom;
	import fg.hstates.State;
	import fg.path_finder_helper.PFHelper;
	import fg.pathfinders.PathFinder;
	import flash.display.Sprite;
	import flash.geom.Point;
	import pnc.Main;
	import pnc.weapons.Weapon;
	
	public class Unit {
		
		public static const ST_IDLE:String = 'idle';
		public static const ST_JALAN:String = 'jalan';
		public static const ST_KEJAR:String = 'kejar';
		public static const ST_SERANG:String = 'serang';		
		public static const ST_TEMBAK:String = 'tembak';		
		
		protected var pathFinder:PathFinder = new PathFinder();
		protected var _pfHelper:PFHelper = new PFHelper();
		protected var _group:String = '';
		protected var _view:Sprite;
		protected var _state:State;
		protected var targetJalanPos:Point = new Point();
		protected var _gridPos:Point = new Point();
		protected var _weapon:Weapon;
		protected var _hp:Number = 0;
		protected var geom:Geom = new Geom();
		protected var _weaponOffset:Point = new Point();
		protected var _posTembak:Point = new Point();
		protected var posTengah:Point = new Point(16, 16);
		
		protected var clicArea:Sprite;
		protected var hitArea:Sprite;
		
		public function Unit() {
			
		}
		
		public function update():void {
			
		}
		
		protected function isOnGrid():Boolean {
			if (Math.floor(_view.x) % Main.GRID_WIDTH != 0) return false;
			if (Math.floor(_view.y) % Main.GRID_HEIGHT != 0) return false;
			
			return true;
		}
		
		public function gridPosSet():void {
			_gridPos.x = Math.floor((_view.x + (posTengah.x)) / pnc.Main.GRID_WIDTH);
			_gridPos.y = Math.floor((_view.y + (posTengah.y)) / pnc.Main.GRID_HEIGHT);
		}
		
		protected function checkCanMoveToTile(i:int, j:int):Boolean {
			if (i >= Main.inst.maze.map.length - 1) return false;
			if (i <= 0) return false;
			if (j >= Main.inst.maze.map[i].length - 1) return false;
			if (j <= 0) return false;
			
			if (Main.inst.maze.map[i][j] > 0) return false;
			
			return true;
		}
		
		/*
		protected function checkWallExists(i:int, j:int):Boolean {
			if (i >= pnc.Main.inst.maze.map.length - 1) return true;
			if (i <= 0) return true;
			if (j >= pnc.Main.inst.maze.map[i].length - 1) return true;
			if (j <= 0) return true;
			if (pnc.Main.inst.maze.map[i][j] == 1) return true;
			
			return false;
		}	
		*/
		
		public function get view():Sprite {
			return _view;
		}
		
		public function get gridPos():Point {
			return _gridPos;
		}
		
		public function get weapon():Weapon {
			return _weapon;
		}
		
		public function get hp():Number {
			return _hp;
		}
		
		public function get weaponOffset():Point {
			return _weaponOffset;
		}
		
		public function get posTembak():Point {
			return _posTembak;
		}
		
		public function get group():String {
			return _group;
		}
		
		public function set group(value:String):void {
			_group = value;
		}
		
		public function set hp(value:Number):void {
			_hp = value;
		}
		
		public function get state():State {
			return _state;
		}
		
		public function get pfHelper():PFHelper {
			return _pfHelper;
		}
		
	}

}