package pnc.weapons {
	import pnc.bullets.Bullet;
	import pnc.bullets.BulletHero;
	import fg.geoms.Geom;
	import pnc.units.Unit;
	import pnc.Main;
	
	public class WeaponHero extends Weapon {
		
		public function WeaponHero() {
			_range = 32 * 3;
			_rate = 20;
		}
		
		public override function update():void {
			super.update();
		}
		
		public override function shoot(target:Unit):void {
			var bullet:Bullet;
			var angle:Number;
			
			bullet = Main.inst.bulletH.getBullet(Bullet.TY_HERO);
			if (bullet == null) {
				bullet = new BulletHero();
				Main.inst.bulletH.bulletLists.push(bullet);
			}
			
			bullet.group = Main.GROUP_HERO;
			
			angle = geom.getAngle(target.view.y - _owner.view.y, target.view.x - _owner.view.x);
			
			bullet.vel.x = bullet.speed * Math.cos(angle * Geom.ANGLE2RAD);
			bullet.vel.y = bullet.speed * Math.sin(angle * Geom.ANGLE2RAD);
			
			bullet.view.x = _owner.view.x + _owner.weaponOffset.x;
			bullet.view.y = _owner.view.y + _owner.weaponOffset.y;
			
			Main.inst.addChild(bullet.view);
			
			_ready = false;
			_ctr.start();
		}
				
	}

}