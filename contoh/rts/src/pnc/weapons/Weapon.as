package pnc.weapons {
	import fg.counters.Counter;
	import fg.geoms.Geom;
	import pnc.units.Unit;

	public class Weapon {
		protected var _power:Number = 10;
		protected var _owner:Unit;
		protected var geom:Geom = new Geom();
		protected var _range:Number = 32 * 3;
		protected var _rate:Number = 20;
		protected var _ready:Boolean = false;
		protected var _ctr:Counter = new Counter();
		
		public function Weapon() {
			_ctr.start();
		}
		
		public function shoot(target:Unit):void {
			
		}
		
		public function update():void {
			_ctr.update();
			if (_ctr.ctr > _rate) {
				_ready = true;
			}
		}
		
		public function get power():Number {
			return _power;
		}
		
		public function get owner():Unit {
			return _owner;
		}
		
		public function set owner(value:Unit):void {
			_owner = value;
		}
		
		public function get range():Number {
			return _range;
		}
		
		public function get rate():Number {
			return _rate;
		}
		
		public function get ready():Boolean {
			return _ready;
		}
		
		public function get ctr():Counter {
			return _ctr;
		}
		
	}

}