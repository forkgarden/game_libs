/*
 * 
 * */

package
{
	import fg.kotaks.Garis;
	import fg.kotaks.Grid;
	import fg.kotaks.Kotak;
	import fg.kotaks.Line2Line;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.filesystem.FileStream;
	import flash.geom.Rectangle;
	import fg.kotaks.Asynch;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.utils.ByteArray;
	import flash.display.PNGEncoderOptions;
	
	public class Main extends Sprite 
	{	
		
		[Embed(source="../bin/knight.png")]
		private var ImgRabbit:Class;
		
		protected var grid:Grid;
		protected var grid2:Grid;
		protected var line:Line2Line;
		protected var garis:Garis;
		protected var kotak:Kotak;
		protected var kotak2:Kotak;
		protected var cont:Sprite = new Sprite();
		protected var cont2:Sprite = new Sprite();
		protected var bmpTarget:Bitmap = new Bitmap(new BitmapData(400, 200));
		protected var imgSrc:Bitmap;
		protected var cols:Number = 3;
		protected var rows:Number = 5;
		protected var kotakpl:Number = 100;
		
		public function Main() 
		{
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		protected function init(e:Event = null):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			grid2Test();
			//gridTest();
		}
		
		protected function grid2Test():void {
			
			imgSrc = (new ImgRabbit())
			addChild(imgSrc);
			
			cont = new Sprite();
			addChild(cont);
			
			grid = new Grid();
			grid.setColRow(cols, rows);
			grid.cont = cont;
			grid.init();
			grid.dotSetPosAwal(imgSrc.width, imgSrc.height);
			grid.updatePos();
			//grid.onUpdate = gridOnUpdate;
			
			kotak = new Kotak();
			kotak2 = new Kotak();
			
			bmpTarget = new Bitmap(new BitmapData(kotakpl * cols - 1, kotakpl * rows - 1, true));
			
			cont2 = new Sprite();
			cont2.x = 400;
			cont2.addChild(bmpTarget);
			//addChild(cont2);
			
			grid2 = new Grid();
			grid2.setColRow(cols, rows);
			grid2.cont = cont2;
			grid2.init();			
			grid2.dotSetPosAwal(bmpTarget.width, bmpTarget.height);
			grid2.updatePos();
			
			stage.addEventListener(KeyboardEvent.KEY_DOWN, keyOnDown, false, 0, true);
		}
		
		protected function keyOnDown(e:KeyboardEvent):void {
			trace (e.keyCode);
			
			//s
			if (e.keyCode == 83) {
				save(bmpTarget, 'test');
			}
		}
		
		protected function save(bmpCrp:Bitmap, namaFile:String):void {
			var file:File;
			var fileStream:FileStream;
			var bytes:ByteArray;
			
			grid2Draw();
			
			file = File.desktopDirectory;
			file = file.resolvePath('.\\dir\\' + namaFile + '.png');
			trace('file loc: ' + file.nativePath);
			
			fileStream = new FileStream();
			fileStream.open(file, FileMode.WRITE);
			
			bytes = new ByteArray();
			bmpCrp.bitmapData.encode(new Rectangle(0, 0, bmpCrp.width, bmpCrp.height), new PNGEncoderOptions(), bytes);
			
			fileStream.writeBytes(bytes);
			fileStream.close();
		}
		
		protected function kotakDraw(i:Number, j:Number):void {
			var max:Number;
			var kotak:Kotak;
			var kotak2:Kotak;
			
			//Asynch.getInst().add(function ():void {
				//trace('kotak draw ' + i + '/' + j);
				kotak = grid.setKotak2(i, j);
				kotak2 = grid2.setKotak2(i, j);
				max = Math.max(kotak.getMax(), kotak2.getMax());
				kotak2.copyPixel(imgSrc.bitmapData, bmpTarget.bitmapData, kotak, max, max);
			//})
		}
		
		protected function grid2Draw():void {
			var i:int;
			var j:int;

			bmpTarget.bitmapData.fillRect(new Rectangle(0, 0, bmpTarget.width, bmpTarget.height), 0xffffffff);
			
			Kotak.clearKotak();
			for (i = 0; i < cols - 1; i++) {
				for (j = 0; j < rows - 1; j++) {
					kotakDraw(i, j);
				}
			}
		}
		
		protected function gridOnUpdate(grid:Grid):void {
			var i:int;
			var j:int;
			
			grid2Draw();
		}
		
		protected function gridTest():void {
			grid = new Grid();
			grid.setColRow(3, 3);
			grid.cont = this;
			grid.init();
			
			kotak = new Kotak();
			grid.setKotak(kotak, 0, 0);
		}
		
		protected function garisTest():void {
			garis = new Garis();
			garis.setDefDot();
			garis.setRandomPos(new Rectangle(0, 0, 800, 600));
			garis.gambarManual(this.graphics);
			addChild(garis.p1);
			addChild(garis.p2);
			
			this.stage.addEventListener(MouseEvent.CLICK, stageOnClickGaris);			
		}
		
		protected function kotakTest():void {
			kotak = new Kotak();
			kotak.setPos(new Rectangle(10, 10, 300, 200));
			kotak.p22.x = 400;
			kotak.p22.y = 300;
			kotak.gambar(this.graphics, 10,10);
			this.stage.addEventListener(MouseEvent.CLICK, stageOnClickKotak);
		}
		
		protected function stageOnClickKotak(e:MouseEvent):void {
		}
		
		protected function stageOnClickGaris(e:MouseEvent):void {
			this.graphics.clear();
			garis.setRandomPos(new Rectangle(0, 0, 800, 600));
			garis.gambarManual(this.graphics, 10);
		}
		
	}
}