package fg.kotaks 
{
	import fg.kotaks.Dot;
	import flash.display.Graphics;
	import flash.display.Sprite;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	public class Garis 
	{
		protected var _p1:fg.kotaks.Dot;
		protected var _p2:fg.kotaks.Dot;
		protected var pTengah:Point;
		protected var _debugCont:Sprite;
		protected var helper:Helper;
		protected var _garisM:GarisM;
		
		protected static var _lists:Vector.<Garis>;
		
		public function destroy(destroyDot:Boolean = true):void {
			if (destroyDot) {
				_p1.destroy();
				_p2.destroy();
			}
			_p1 = null;
			_p2 = null;
			pTengah = null;
		}
		
		public function Garis() 
		{
			_lists = new Vector.<Garis>();
			pTengah = new Point();
			helper = new Helper();
			_garisM = new GarisM();
		}
		
		public function gambarManual(g:Graphics, titikJml:Number = 0):void {
			var i:int;
			var idx:Number;
			var p:Point = new Point();
			
			if (titikJml == 0) {
				titikJml = helper.jarak2Point(_p1.pos, _p2.pos);
			}
			
			g.beginFill(0);
			g.lineStyle(0);
			for (i = 0; i < titikJml; i++) {
				idx = i / titikJml;
				helper.getInterpolatePos(idx, _p1.pos, _p2.pos, p);
				g.drawCircle(p.x, p.y, 1);
			}
			g.endFill();
		}
		
		//TODO: garis
		public function setRandomPos(rect:Rectangle):void {
			_p1.randomPos(rect);
			_p2.randomPos(rect);
		}
		
		//TODO: garis
		public function dotTerdekat(garis:Garis):Dot {
			var jarak1:Number;
			var jarak2:Number;
			
			jarak1 = helper.jarak2Point(garis.point2Line(_p1.pos), _p1.pos);
			jarak2 = helper.jarak2Point(garis.point2Line(_p2.pos), _p2.pos);
			
			if (jarak1 < jarak2) {
				return _p1;
			}
			else {
				return _p2;
			}
		}
		
		//TODO: helper
		public function point2Line(pC:Point, maxIteration:Number=10):Point {
			return point2Line2(_p1.x, _p1.y, _p2.x, _p2.y, pC, maxIteration);
		}
		
		//TODO: helper
		public function intersect(garis:Garis):Point {
			var jarak1:Number;
			var jarak2:Number;
			var taliCopy1:Garis = copy();
			var taliCopy2:Garis = garis.copy();
			var i:int;
			var res:Point = new Point();
			
			for (i = 0; i < 10; i++) {
				taliCopy1.potong(taliCopy1.dotLain(taliCopy1.dotTerdekat(garis)));
			}
			
			res.x = taliCopy1.p1.x;
			res.y = taliCopy1.p1.y;
			
			taliCopy1.destroy();
			taliCopy2.destroy();
			
			return res;
		}
		
		//TODO: helper
		protected function point2Line2(Ax:Number, Ay:Number, Bx:Number, By:Number, pC:Point, maxIteration:Number = 10):Point { 
			var pDekatX:Number = 0; 
			var pDekatY:Number = 0; 
			var pTengahX:Number = 0; 
			var pTengahY:Number = 0; 
			var pRes:Point = new Point();
			var ctr:Number = 0;
			
			while (true) {
				pDekatX = Bx; 
				pDekatY = By; 
				
				if (helper.jarak(pC.x - Ax, pC.y - Ay) <= helper.jarak(pC.x - Bx, pC.y - By)) {
					pDekatX = Ax;
					pDekatY = Ay;
				} 
				
				if (ctr < maxIteration) {
					pTengahX = Ax + (Bx - Ax) / 2.0; 
					pTengahY = Ay + (By - Ay) / 2.0; 
					
					//recursive
					Ax = pDekatX;
					Ay = pDekatY;
					
					Bx = pTengahX;
					By = pTengahY;
					
					ctr++;
				}
				else { 
					pRes.x = pDekatX; 
					pRes.y = pDekatY; 
					break;
				} 
			}
			
			return pRes;
		}
		
		public function copy():Garis {
			var tali:Garis = new Garis();
		
			tali.setDefDot();
			
			tali.p1.x = _p1.pos.x;
			tali.p1.y = _p1.pos.y;
			
			tali.p2.x = _p2.pos.x;
			tali.p2.y = _p2.pos.y;
			
			return tali;
		}
		
		//TODO: helper
		public function potong(p:Dot):void {
			var pTengah:Point = getCenter();
			
			if (p == _p1) {
				_p1.x = pTengah.x;
				_p1.y = pTengah.y;
			}
			else if (p == _p2) {
				_p2.x = pTengah.x;
				_p2.y = pTengah.y;
			}
			else {
				throw new Error();
			}
			
			pTengah = null;
			
		}
		
		public function dotSetColor(clr:uint):void {
			_p1.clr = clr;
			_p2.clr = clr;
		}
		
		public function setDefDot():void {
			_p1 = new Dot();
			_p2 = new Dot();
		}
		
		//TODO: hapus
		public function dotLain(dot:Dot):Dot {
			if (dot == _p1) return _p2;
			if (dot == _p2) return _p1;
			return null;
		}
		
		//TODO: helper
		public function getCenter():Point {
			var pTengah:Point = new Point();
			
			pTengah.x = _p1.x + (_p2.x - _p1.x) / 2;
			pTengah.y = _p1.y + (_p2.y - _p1.y) / 2;
			
			return pTengah;
		}
		
		public function debug():String {
			var str:String = '';
			
			str += "dot1 pos " + _p1.pos + "\n";
			str += "dot2 pos " + _p2.pos + "\n";
			
			return str;
		}
		
		public function get p1():Dot 
		{
			return _p1;
		}
		
		public function get p2():Dot 
		{
			return _p2;
		}
		
		static public function get lists():Vector.<Garis> 
		{
			return _lists;
		}
		
		public function set p1(value:fg.kotaks.Dot):void 
		{
			_p1 = value;
		}
		
		public function set p2(value:fg.kotaks.Dot):void 
		{
			_p2 = value;
		}
		
		public function get garisM():GarisM 
		{
			return _garisM;
		}
		
		public function set garisM(value:GarisM):void 
		{
			_garisM = value;
		}
		
		public function render(cont:Sprite):void {
			cont.graphics.lineStyle(3);
			cont.graphics.moveTo(p1.x, p1.y);
			cont.graphics.lineTo(p2.x, p2.y);
		}

		public function update():void {
		}
		
	}

}