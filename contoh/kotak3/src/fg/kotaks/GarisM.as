package fg.kotaks 
{
	import flash.geom.Point;
	
	public class GarisM 
	{
		protected var helper:Helper;
		protected var _dot1:Point;
		protected var _dot2:Point;
		
		
		public function destroy(destroyDot:Boolean = true):void {
			_dot1 = null;
			_dot2 = null;
		}
		
		public function GarisM() 
		{
			helper = new Helper();
		}
		
		public function getPanjang():Number {
			return helper.jarak2Point(_dot1, _dot2);
		}
		
		public function dotTerdekat(garis:GarisM):Point {
			var jarak1:Number;
			var jarak2:Number;
			
			jarak1 = helper.jarak2Point(garis.point2Line(_dot1), _dot1);
			jarak2 = helper.jarak2Point(garis.point2Line(_dot2), _dot2);
			
			if (jarak1 < jarak2) {
				return _dot1;
			}
			else {
				return _dot2;
			}
		}
		
		public function point2Line(pC:Point, maxIteration:Number = 10):Point {
			return point2Line2(_dot1.x, _dot1.y, _dot2.x, _dot2.y, pC, maxIteration);
		}
		
		public function dotLain(dot:Point):Point {
			if (dot == _dot1) return _dot2;
			if (dot == _dot2) return _dot1;
			throw new Error();
		}
		
		public function intersect(garis:GarisM):Point {
			var jarak1:Number;
			var jarak2:Number;
			var taliCopy1:GarisM = copy();
			var taliCopy2:GarisM = garis.copy();
			var i:int;
			var resP:Point = new Point();
			
			for (i = 0; i < 10; i++) {
				taliCopy1.potong(taliCopy1.dotLain(taliCopy1.dotTerdekat(garis)));
			}
			
			resP.x = taliCopy1.dot1.x;
			resP.y = taliCopy1.dot1.y;
			
			taliCopy1.destroy();
			taliCopy2.destroy();
			
			return resP;
		}
		
		protected function point2Line2(Ax:Number, Ay:Number, Bx:Number, By:Number, pC:Point, maxIteration:Number = 10):Point { 
			var pDekatX:Number = 0; 
			var pDekatY:Number = 0; 
			var pTengahX:Number = 0; 
			var pTengahY:Number = 0; 
			var ctr:Number = 0;
			var resP:Point = new Point();
			
			while (true) {
				pDekatX = Bx; 
				pDekatY = By; 
				
				if (helper.jarakPerbandingan(pC.x - Ax, pC.y - Ay) <= helper.jarakPerbandingan(pC.x - Bx, pC.y - By)) {
					pDekatX = Ax;
					pDekatY = Ay;
				} 
				
				if (ctr < maxIteration) {
					pTengahX = Ax + (Bx - Ax) / 2.0; 
					pTengahY = Ay + (By - Ay) / 2.0; 
					
					//recursive
					Ax = pDekatX;
					Ay = pDekatY;
					
					Bx = pTengahX;
					By = pTengahY;
					
					ctr++;
				}
				else { 
					resP.x = pDekatX; 
					resP.y = pDekatY; 
					break;
				} 
			}
			
			return resP;
		}
		
		public function copy():GarisM {
			var garis:GarisM = new GarisM();
			
			garis.dot1.x = _dot1.x;
			garis.dot1.y = _dot1.y;
			
			garis.dot2.x = _dot2.x;
			garis.dot2.y = _dot2.y;
			
			return garis;
		}
		
		public function potong(inputPoint:Point):void {
			var pTengah:Point = getPointTengah();
			
			if (inputPoint == _dot1) {
				_dot1.x = pTengah.x;
				_dot1.y = pTengah.y;
			}
			else if (inputPoint == _dot2) {
				_dot2.x = pTengah.x;
				_dot2.y = pTengah.y;
			}
			else {
				throw new Error();
			}
		}
		
		public function getPointTengah():Point {
			var resP:Point = new Point();
			
			resP.x = _dot1.x + (_dot2.x - _dot1.x) / 2;
			resP.y = _dot1.y + (_dot2.y - _dot1.y) / 2;
			
			return resP;
		}
			
		public function get dot1():Point 
		{
			return _dot1;
		}
		
		public function get dot2():Point 
		{
			return _dot2;
		}
		
	}
}