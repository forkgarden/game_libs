package fg.kotaks 
{
	import fg.inters.Move;
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.geom.Point;
	import fg.debugs.Debugger;
	import flash.geom.Rectangle;
	
	public class Dot extends Sprite
	{
		protected static var _lists:Vector.<Dot>;
		
		public static const STATIC:int = 1;
		public static const HORIZONTAL:int = 2;
		public static const VERTICAL:int = 3;
		public static const FREE:int = 4;
		
		protected var _taliHor:Garis;
		protected var _taliVer:Garis;
		protected var _type:int = 0;
		protected var _move:Move;
		protected var _posRel:Number = 0;
		protected var _index:Number = 0;
		protected var posDown:Point = new Point();
		protected var _posIdx:Point = new Point();
		protected var posMove:Point = new Point();
		protected var posLine:Point = new Point();
		protected var _pos:Point = new Point();
		protected var p:Point = new Point();
		protected var _mouseUpCallBack:Function;
		
		protected var _clr:uint = 0;
		
		protected var _dotLine:Sprite = new Sprite();
		
		public function Dot() 
		{
			draw();
		}
		
		public function destroy():void {
			posDown = null;
			_posIdx = null;
			posMove = null;
			posLine = null;
			_pos = null;
			_mouseUpCallBack = null;
			_taliHor = null;
			_taliVer = null;
			if (_move) _move.destroy();
			_dotLine = null;
			removeChildren();
		}
		
		public function randomPos(rect:Rectangle):void {
			x = Math.random() * rect.width + rect.x;
			y = Math.random() * rect.height + rect.y;
		}
		
		public function setMove(stage:Stage):void {
			if (_type == STATIC) {
				
			}
			else {
				_move = new Move(this, stage);
				_move.callBackMouseDown = mouseOnDown;
				_move.callBackMouseUp = mouseOnUp;
				_move.callBackMove = mouseOnMove;
			}
		}
		
		protected function mouseOnUp():void {
			_mouseUpCallBack();
		}
		
		protected function mouseOnDown():void {
			//posDown.x = this.x;
			//posDown.y = this.y;
		}
		
		protected function mouseOnMove(dx:Number, dy:Number):void {
			var easing:Number = .01;
			var pTali:Number = 0;
			var pDot:Number = 0;
			
			if (_type == STATIC) {
				this.x = posDown.x;
				this.y = posDown.y;
			}
			else if (_type == VERTICAL) {
				posMove.x = this.x;
				posMove.y = this.y;
				
				point2Line(_taliVer.p1.x, _taliVer.p1.y, _taliVer.p2.x, _taliVer.p2.y, posMove, posLine);
				
				_dotLine.x = posLine.x;
				_dotLine.y = posLine.y;
				
				//update index
				pDot = jarak(posLine, _taliVer.p1.x, _taliVer.p1.y);
				
				p.x = _taliVer.p1.x;
				p.y = _taliVer.p1.y;
				
				pTali = jarak(p, _taliVer.p2.x, _taliVer.p2.y);
				
				_posRel = pDot / pTali;
				
				if (_posRel < 0) _posRel = 0;
				if (_posRel > 1) _posRel = 1;
				
				aturPos();
			}
			else if (_type == HORIZONTAL) {
				posMove.x = this.x;
				posMove.y = this.y;
				
				point2Line(_taliHor.p1.x, _taliHor.p1.y, _taliHor.p2.x, _taliHor.p2.y, posMove, posLine);
				
				_dotLine.x = posLine.x;
				_dotLine.y = posLine.y;
				
				//update index
				pDot = jarak(posLine, _taliHor.p1.x, _taliHor.p1.y);
				
				p.x = _taliHor.p1.x;
				p.y = _taliHor.p1.y;
				
				pTali = jarak(p, _taliHor.p2.x, _taliHor.p2.y);
				
				_posRel = pDot / pTali;
				
				if (_posRel < 0) _posRel = 0;
				if (_posRel > 1) _posRel = 1;
				
				aturPos();
			}
			else if (_type == FREE) {
				
			}
			else {
				throw new Error();
			}
			
		}
		
		protected function posToAxisX():void {
			var lenX:Number = 0;
			var posX:Number = 0;
		}
		
		public function draw():void {
			
			this.graphics.clear();
			this.graphics.beginFill(_clr);
			this.graphics.lineStyle(1, 0xffffff);
			this.graphics.drawRect( -5, -5, 10, 10);
			this.graphics.endFill();
			
			//debug
			_dotLine = new Sprite();
			_dotLine.graphics.beginFill(0);
			_dotLine.graphics.lineStyle(1, _clr);
			_dotLine.graphics.drawCircle(0, 0, 5);
			_dotLine.mouseEnabled = false;
			_dotLine.mouseChildren = false;
		}
		
		public static function create():Dot {
			var dot:Dot = new Dot();
			
			if (_lists == null) _lists = new Vector.<Dot>();
			
			_lists.push(dot);
			
			return dot;
		}
		
		public static function addToScreen(cont:Sprite):void {
			var dot:Dot;
			
			for each (dot in _lists) {
				cont.addChild(dot);
			}
		}
		
		protected function jarak(p1:Point, p2X:Number, p2Y:Number):Number {
			return Math.sqrt(Math.pow(p1.x - p2X, 2) + Math.pow(p1.y - p2Y, 2));
		} 		
		
		protected function point2Line(Ax:Number, Ay:Number, Bx:Number, By:Number, pC:Point, pRes:Point, ctr:Number = 0):void { 
			var pDekatX:Number = 0;
			var pDekatY:Number = 0;
			var pTengahX:Number = 0;
			var pTengahY:Number = 0;
		   
			pDekatX = Bx;
			pDekatY = By;
		   
			if (jarak(pC, Ax, Ay) <= jarak(pC, Bx, By)) {
				pDekatX = Ax;
				pDekatY = Ay;
			}
		   
			if (ctr < 10) {
				pTengahX = Ax + (Bx - Ax) / 2.0;
				pTengahY = Ay + (By - Ay) / 2.0;
				point2Line(pDekatX, pDekatY, pTengahX, pTengahY, pC, pRes, ctr + 1);
			}
			else {
				pRes.x = pDekatX;
				pRes.y = pDekatY;
			}
		} 		
		
		public function aturPos():void {
			var p:Point;
			
			if (_type == FREE) {
				
			}
			else if (_type == HORIZONTAL) {
				x = taliHor.p1.x;
				y = taliHor.p1.y;
				
				x += _posRel * (taliHor.p2.x - taliHor.p1.x);
				y += _posRel * (taliHor.p2.y - taliHor.p1.y);
			}
			else if (_type == VERTICAL) {
				x = taliVer.p1.x;
				y = taliVer.p1.y;
				
				x += _posRel * (taliVer.p2.x - taliVer.p1.x);
				y += _posRel * (taliVer.p2.y - taliVer.p1.y);
			}
			else if (_type == STATIC) {
				p = _taliHor.intersect(_taliVer);
				x = p.x;
				y = p.y;
			}
			else {
				throw new Error();
			}
		}		
		
		public function update():void {
			
		}
		
		//static public function get lists():Vector.<Dot> 
		//{
			//return _lists;
		//}
		
		//static public function set lists(value:Vector.<Dot>):void 
		//{
			//_lists = value;
		//}
		
		public function get taliHor():Garis 
		{
			return _taliHor;
		}
		
		public function set taliHor(value:Garis):void 
		{
			_taliHor = value;
		}
		
		public function get taliVer():Garis 
		{
			return _taliVer;
		}
		
		public function set taliVer(value:Garis):void 
		{
			_taliVer = value;
		}
		
		public function get type():int 
		{
			return _type;
		}
		
		public function set type(value:int):void 
		{
			_type = value;
		}
		
		public function get move():Move 
		{
			return _move;
		}
		
		public function set move(value:Move):void 
		{
			_move = value;
		}
		
		public function get index():Number 
		{
			return _index;
		}
		
		public function set index(value:Number):void 
		{
			_index = value;
		}
		
		public function get posIdx():Point 
		{
			return _posIdx;
		}
		
		public function set posIdx(value:Point):void 
		{
			_posIdx = value;
		}
		
		public function get posRel():Number 
		{
			return _posRel;
		}
		
		public function set posRel(value:Number):void 
		{
			_posRel = value;
		}
		
		public function get dotLine():Sprite 
		{
			return _dotLine;
		}
		
		public function get mouseUpCallBack():Function 
		{
			return _mouseUpCallBack;
		}
		
		public function set mouseUpCallBack(value:Function):void 
		{
			_mouseUpCallBack = value;
		}
		
		public function get pos():Point 
		{
			_pos.x = x;
			_pos.y = y;
			return _pos;
		}
		
		public function get clr():uint 
		{
			return _clr;
		}
		
		public function set clr(value:uint):void 
		{
			_clr = value;
			draw();
		}
		
	}

}