package fg.kotaks 
{
	import flash.geom.Point;

	public class Helper 
	{
		protected var _pCenter:Point = new Point();
		protected var pRes:Point = new Point();
		protected var pSrc:Point = new Point();
		
		public function Helper() 
		{
			
		}
		
		public function jarakPerbandingan(dx:Number, dy:Number):Number {
			return Math.pow(dx, 2) + Math.pow(dy, 2);
		}
		
		public function jarak(dx:Number, dy:Number):Number {
			return Math.sqrt(jarakPerbandingan(dx, dy));
		}
		
		public function jarak2Point(p1:Point, p2:Point):Number {
			return jarak(p2.x - p1.x, p2.y - p1.y);
		}
		
		public function getInterpolatePos(idx:Number, pAwal:Point, pAkhir:Point, resP:Point):void {
			resP.x = pAwal.x + (pAkhir.x - pAwal.x) * idx;
			resP.y = pAwal.y + (pAkhir.y - pAwal.y) * idx;
		}		
		
	}

}