package fg.kotaks 
{
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.xml.XMLNode;
	
	public class Grid 
	{
		
		protected var _cont:Sprite;
		protected var taliAtas:Garis;
		protected var cols:Vector.<Garis>;
		protected var rows:Vector.<Garis>;
		protected var dotCount:Number;
		protected var colJml:Number = 2;
		protected var rowJml:Number = 2;
		protected var dots:Vector.<Dot>;
		
		protected var _onUpdate:Function;
		
		public function Grid() 
		{
			dotCount = colJml * rowJml;
			dots = new Vector.<Dot>();
		}
		
		public function setColRow(col:Number, row:Number):void {
			colJml = col;
			rowJml = row;
			dotCount = colJml * rowJml;
		}
		
		public function setKotak(kotak:Kotak, ix:Number, jx:Number):void {
			var dot:Dot;
			
			dot = dotGetByPos(ix, jx);
			kotak.p11.copyFrom(dot.pos);
			
			dot = dotGetByPos(ix + 1, jx);
			kotak.p21.copyFrom(dot.pos);
			
			dot = dotGetByPos(ix, jx + 1);
			kotak.p12.copyFrom(dot.pos);
			
			dot = dotGetByPos(ix + 1, jx + 1);
			kotak.p22.copyFrom(dot.pos);
		}
		
		public function setKotak2(ix:Number, jx:Number):Kotak {
			var dot:Dot;
			var kotak:Kotak;
			
			kotak = Kotak.create();
			
			dot = dotGetByPos(ix, jx);
			kotak.p11.copyFrom(dot.pos);
			
			dot = dotGetByPos(ix + 1, jx);
			kotak.p21.copyFrom(dot.pos);
			
			dot = dotGetByPos(ix, jx + 1);
			kotak.p12.copyFrom(dot.pos);
			
			dot = dotGetByPos(ix + 1, jx + 1);
			kotak.p22.copyFrom(dot.pos);
			
			return kotak;
		}
		
		public function destroy():void {
			
		}
		
		public function init():void {
			var i:int;
			var dot:Dot;
			var col:Number;
			var row:Number;
			var tali:Garis;
			
			cols = new Vector.<Garis>();
			rows = new Vector.<Garis>();			
			
			for (i = 0; i < dotCount; i++) {
				dots.push(Dot.create());
			}
			
			//TODO: angka 3 dibuat rumus
			for (i = 0; i < colJml; i++) {
				cols.push(new Garis());
			}
			
			for (i = 0; i < rowJml; i++) {
				rows.push(new Garis());
			}
			
			for (i = 0; i < dotCount; i++) {
				dot = dots[i];
				dot.index = i;
				
				col = i % cols.length;
				row = Math.floor(i / cols.length);
				
				dot.posIdx.x = col;
				dot.posIdx.y = row;
				dot.taliHor = rows[row];
				dot.taliVer = cols[col];
				dot.type = dotGetType(col, row);
				dot.setMove(_cont.stage);
				dot.mouseUpCallBack = dotOnMouseUp;
				
				dotSetAxisIndex(dot);
			}
			
			dotSetTaliVer();
			dotSetTaliHor();
			dotSetPosAwal();
			updatePos();
			
			for each (dot in dots) {
				_cont.addChild(dot);
			}
		}
		
		protected function dotOnMouseUp():void {
			updatePos();
			if (_onUpdate != null) _onUpdate(this);
		}
		
		public function updatePos():void {
			var dot:Dot;
			
			//vertical
			for each (dot in dots) {
				if (dot.type == Dot.VERTICAL) {
					dot.aturPos();
				}
			}
			
			//horizontal
			for each (dot in dots) {
				if (dot.type == Dot.HORIZONTAL) {
					dot.aturPos();
				}
			}
			
			//static
			for each (dot in dots) {
				if (dot.type == Dot.STATIC) {
					dot.aturPos();
				}
			}
		}
		
		protected function dotSetPos(index:Number):void {
			var dot:Dot;
			
			dot = dots[index];
			dot.aturPos();
		}
		
		public function dotSetPosAwal(p:Number = 100, l:Number = 100):void {
			var dot:Dot;
			
			dot = dotGetByPos(0, 0);
			dot.x = 0;
			dot.y = 0;
			
			dot = dotGetByPos(cols.length - 1, 0);
			dot.x = p;
			dot.y = 0;
			
			dot = dotGetByPos(0, rows.length - 1);
			dot.x = 0;
			dot.y = l;
			
			dot = dotGetByPos(cols.length - 1, rows.length - 1);
			dot.x = p;
			dot.y = l;
		}
		
		/**
		 * mendefinisikan tali vertikal
		 * dari atas ke bawah
		 * 
		*/		
		protected function dotSetTaliVer():void {
			var i:int;
			var tali:Garis;
			
			for (i = 0; i < cols.length; i++) {
				tali = cols[i];
				tali.p1 = dots[i];		//tali p1 adalah dot di atas
				tali.p2 = dots[i + (colJml * (rowJml - 1))];	//TODO: 6 harus dibuat rumus
			}
		}
		
		/**
		 * Mendefinisikan tali horizontal
		 */
		protected function dotSetTaliHor():void {
			var i:int;
			var tali:Garis;
			
			for (i = 0; i < rows.length; i++) {
				tali = rows[i];
				tali.p1 = dots[colJml * i];
				tali.p2 = dots[colJml * i + colJml - 1];
			}
		}
		
		protected function dotSetAxisIndex(dot:Dot):void {
			var dotColIdx:Number;
			var dotRowIdx:Number;
			var colCount:Number = cols.length;
			
			dotColIdx = dot.index % cols.length;
			dotRowIdx = Math.floor(dot.index / cols.length);
			
			if (dot.type == Dot.FREE) {
				
			}
			else if (dot.type == Dot.HORIZONTAL || (dot.type == Dot.STATIC)) {
				dot.posRel = dotColIdx / (cols.length - 1); 
			} 
			else if (dot.type == Dot.VERTICAL) {
				dot.posRel = dotRowIdx / (rows.length - 1);
			}
			else {
				throw new Error();
			}
			
			//trace('dot set type ' + dot.type);
		}
		
		protected function dotGetType(i:int, j:int):int {
			//kiri
			if (i == 0) {
				if (j == 0) {
					return Dot.FREE;
				}
				else if (j > 0) {
					if (j == rows.length - 1) {
						return Dot.FREE;
					}
					else if (j < rows.length - 1) {
						return Dot.VERTICAL;
					}
				}
			}
			else if (i == cols.length - 1) {
				if (j == 0) {
					return Dot.FREE;
				}
				else if (j == rows.length - 1) {
					return Dot.FREE;
				}
				else if (j < rows.length - 1) {
					return Dot.VERTICAL;
				}
			}
			else if (i > 0) {
				if (j == 0) {
					return Dot.HORIZONTAL;
				}
				else if (j == rows.length - 1) {
					return Dot.HORIZONTAL;
				}
				else if (j < rows.length - 1) {
					return Dot.STATIC;
				}
			}
			
			throw new Error();
			return 0;
		}
		
		protected function dotGetByPos(i:int, j:int):Dot {
			var dot:Dot;
			
			for each (dot in dots) {
				if (dot.posIdx.x == i) {
					if (dot.posIdx.y == j) {
						return dot;
					}
				}
			}
			
			throw new Error();
			return null;
		}
		
		public function get cont():Sprite 
		{
			return _cont;
		}
		
		public function set cont(value:Sprite):void 
		{
			_cont = value;
		}
		
		public function get onUpdate():Function 
		{
			return _onUpdate;
		}
		
		public function set onUpdate(value:Function):void 
		{
			_onUpdate = value;
		}
	}
}