package fg.kotaks 
{
	import flash.display.BitmapData;
	import flash.display.Graphics;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	public class Kotak 
	{
		public static const P_IDLE:int = 0;
		public static const P_UPDATE:int = 1;
		
		protected var pState:int = 0;
		protected var pX:Number = 0;
		protected var pY:Number = 0;
		protected var pXMax:Number = 0;
		protected var pYMax:Number = 0;
		protected var pCtr:Number = 0;
		protected var pSrcKotak:Kotak;
		protected var pSrcBmpData:BitmapData;
		protected var pTargetBmpData:BitmapData;
		
		protected var _p11:Point;
		protected var _p21:Point;
		protected var _p12:Point;
		protected var _p22:Point;
		protected var helper:Helper;
		
		protected var p1:Point = new Point();
		protected var p2:Point = new Point();
		protected var p3:Point = new Point();		
		
		protected static var lists:Vector.<Kotak> = new Vector.<Kotak>();
		
		public function Kotak() 
		{
			_p11 = new Point();
			_p21 = new Point();
			_p12 = new Point();
			_p22 = new Point();
			
			helper = new Helper();
		}
		
		public static function clearKotak():void {
			var kotak:Kotak;
			
			while (lists.length > 0) {
				kotak = lists.pop();
				kotak.destroy();
			}
		}
		
		public static function create():Kotak {
			var kotak:Kotak = new Kotak();
			lists.push(kotak);
			return kotak;
		}
		
		public function destroy():void {
			pState = 0;
			pSrcKotak = null;
			pSrcBmpData = null;
			pTargetBmpData = null;
			helper = null;
			_p11 = null;
			_p12 = null;
			_p21 = null;
			_p22 = null;
		}
		
		public function getMax():Number {
			var res:Number;
			
			res = helper.jarak2Point(_p11, _p12);
			res = Math.max(res, helper.jarak2Point(_p11, _p21));
			res = Math.max(res, helper.jarak2Point(_p11, _p22));
			
			res = Math.max(res, helper.jarak2Point(_p12, _p21));
			res = Math.max(res, helper.jarak2Point(_p12, _p22));
			
			res = Math.max(res, helper.jarak2Point(_p21, _p22));
			
			return res;
		}
		
		public function startDrawAsynch(srcBmpData:BitmapData, targetBmpData:BitmapData, panjang:Number, lebar:Number):void {
			pState = P_UPDATE;
			pSrcBmpData = pSrcBmpData;
			pTargetBmpData = targetBmpData;
			pXMax = panjang;
			pYMax = lebar;
			pCtr = 0;
			targetBmpData.fillRect(new Rectangle(0, 0, targetBmpData.width, targetBmpData.height), 0xffffffff);
		}
		
		public function update():void {
			if (pState == P_UPDATE) {
				copyPixelAsynch();
			}
		}
		
		public function setPos(rect:Rectangle):void {
			_p11.x = rect.x;
			_p11.y = rect.y;
			
			_p21.x = rect.x + rect.width;
			_p21.y = rect.y;
			
			_p12.x = rect.x;
			_p12.y = rect.y + rect.height;
			
			_p22.x = rect.x + rect.width;
			_p22.y = rect.y + rect.height;
		}
		
		public function getColor(g:BitmapData, idxX:Number, idxY:Number):uint {
			convertPos(idxX, idxY, p3);
			return g.getPixel32(p3.x, p3.y);
		}
		
		protected function convertPos(i:Number, j:Number, res:Point):void {
			helper.getInterpolatePos(i, _p11, _p21, p1);
			helper.getInterpolatePos(i, _p12, _p22, p2);
			helper.getInterpolatePos(j, p1, p2, res);
		}
		
		public function copyPixelAsynch():void {
			
		}
			
		public function copyPixel(src:BitmapData, target:BitmapData, srcKotak:Kotak, panjang:Number, lebar:Number):void {
			var i:int;
			var j:int;
			var clr:uint;
			
			for (i = 0; i <= panjang; i++) {
				for (j = 0; j <= lebar; j++) {
					clr = srcKotak.getColor(src, i / panjang, j / lebar);
					convertPos(i/panjang, j/lebar, p3);
					target.setPixel32(p3.x, p3.y, clr);
				}
			}
		}
		
		public function gambar(g:Graphics, panjang:Number, lebar:Number, clr:uint = 0, radius:Number = 1):void {
			var i:int;
			var j:int;
			
			g.beginFill(0);
			g.lineStyle(0);
			for (i = 0; i <= panjang; i++) {
				for (j = 0; j <= lebar; j++) {
					convertPos(i/panjang, j/lebar, p3);
					g.drawCircle(p3.x, p3.y, 1);
				}
			}
			g.endFill();
		}
		
		public function get p11():Point 
		{
			return _p11;
		}
		
		public function get p21():Point 
		{
			return _p21;
		}
		
		public function get p12():Point 
		{
			return _p12;
		}
		
		public function get p22():Point 
		{
			return _p22;
		}
		
	}

}