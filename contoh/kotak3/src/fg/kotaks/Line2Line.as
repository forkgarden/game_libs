package fg.kotaks 
{
	import fg.debugs.Debugger;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.Point;
	import flash.geom.Rectangle;

	public class Line2Line extends Sprite
	{
		protected var garis1:Garis;
		protected var garis2:Garis;
		protected var dot:Dot;
		
		public function Line2Line() 
		{
			
		}
		
		protected function mouseOnUp():void {
			var p:Point;
			var i:int = 0;
			
			this.graphics.clear();
			
			garis1.dotSetColor(0xff);
			p = garis1.intersect(garis2);
			
			dot.x = p.x;
			dot.y = p.y;
			dot.clr = 0xff0000;
			addChild(dot);
			
			garis1.render(this);
			garis2.render(this);
			
			debug();
		}
		
		protected function debug():void {
			Debugger.getInst().addLine("tali1", true);
			Debugger.getInst().addLine(garis1.debug());
			Debugger.getInst().addLine("tali2");
			Debugger.getInst().addLine(garis2.debug());
			Debugger.getInst().addLine("dot pos " + dot.pos);			
		}
		
		protected function taliRandomPos(tali:Garis):void {
			tali.p1.x = Math.random() * 800;
			tali.p1.y = Math.random() * 600;			
			
			tali.p2.x = Math.random() * 800;
			tali.p2.y = Math.random() * 600;
		}
		
		public function init():void {
			//lineH = new LineHelper();
			
			garis1 = new Garis();
			garis1.p1 = new Dot();
			garis1.p2 = new Dot();
			garis1.p1.setMove(this.stage);
			garis1.p2.setMove(this.stage);
			garis1.p1.mouseUpCallBack = mouseOnUp;
			garis1.p2.mouseUpCallBack = mouseOnUp;
			addChild(garis1.p1);
			addChild(garis1.p2);
			
			garis2 = new Garis();
			garis2.p1 = new Dot();
			garis2.p2 = new Dot();
			garis2.p1.setMove(this.stage);
			garis2.p2.setMove(this.stage);
			garis2.p1.mouseUpCallBack = mouseOnUp;
			garis2.p2.mouseUpCallBack = mouseOnUp;
			addChild(garis2.p1);
			addChild(garis2.p2);
			
			garis1.setRandomPos(new Rectangle(0,0, 800, 600));
			garis2.setRandomPos(new Rectangle(0,0, 800, 600));
			
			dot = new Dot();
			dot.mouseEnabled = false;
			dot.mouseChildren = false;
			addChild(dot);
			dot.parent.setChildIndex(dot, 0);
			
			garis1.dotSetColor(0xff);			
			
			garis1.render(this);
			garis2.render(this);	
			
			addEventListener(Event.ENTER_FRAME, update);
		}
		
		protected function update(e:Event):void {
			
		}
		
		//public function get debugCont():Sprite 
		//{
			//return _debugCont;
		//}
		
		//public function set debugCont(value:Sprite):void 
		//{
			//_debugCont = value;
		//}
	}
}