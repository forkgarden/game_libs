package fg.kotaks 
{
	import flash.display.Sprite;
	import flash.events.Event;
	/**
	 * ...
	 * @author test
	 */
	public class Asynch extends Sprite
	{
		
		private static var inst:Asynch;
		protected var funcs:Vector.<Function> = new Vector.<Function>();
		
		public function Asynch() 
		{
			if (inst) throw new Error();
			
			addEventListener(Event.ENTER_FRAME, update);
		}
		
		protected function update(e:Event):void {
			var f:Function;
			
			if (funcs.length > 0) {
				f = funcs.shift();
				f();
			}
		}
		
		public function add(f:Function):void {
			funcs.push(f);
		}
		
		public static function getInst():Asynch	{
			if (inst) return inst;
			
			inst = new Asynch();
			return inst;
		}
		
	}

}