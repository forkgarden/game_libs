package 
{
	import flash.display.Bitmap;
	import flash.geom.Point;
	/**
	 * ...
	 * @author test
	 */
	public class Camera 
	{
		
		protected var _karakter:Bitmap;
		protected var jarakMin:Point = new Point(320, 240);
		protected var _pos:Point = new Point();
		protected var smooth:Boolean = true;
		
		public function Camera() 
		{
			
		}
		
		public function update():void {
			
			if (smooth) {
				_pos.x += ((_karakter.x - jarakMin.x) - _pos.x) * .1;
				_pos.y += ((_karakter.y - jarakMin.y) - _pos.y) * .1;
			}
			else {
				_pos.x = _karakter.x - jarakMin.x;
				_pos.y = _karakter.y - jarakMin.y;
			}
		}
		
		public function set karakter(value:Bitmap):void 
		{
			_karakter = value;
		}
		
		public function get pos():Point 
		{
			return _pos;
		}
		
	}

}