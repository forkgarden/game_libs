package{
	import fg.bmps.BmpBLoader;
	import fg.bmps.BmpLoader;
	import fg.tekstypes.TypeWritter;
	import fg.ui.Teks;
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextFormatAlign;
	
	[Frame(factoryClass = "Preloader")]
	public class Main extends Sprite {
		
		protected var hals:Vector.<Halaman>;
		protected var bmp:BmpBLoader;
		protected var halIdx:int = 0;
		protected var loading_txt:Teks;
		protected var loadingScreen:Sprite;
		protected var ketik:TypeWritter;
		
		protected var backCont:Sprite;
		protected var teksCerita:Teks;
		protected var panahKanan:Sprite;
		protected var panahKiri:Sprite;
		
		public function Main() {
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		//fungsi awal
		protected function init(e:Event = null):void {
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			//inisialisasi TypeWritter
			//set kecepatan .5 agar pergerakannya lambat
			ketik = new TypeWritter();
			ketik.speed = .5;
			
			//membuat halaman loading
			halLoadingCreate();
			
			//inisilisasi gambar
			bmpInit();
			
			//inisialisasi data halaman
			halInit();
			
			//inisialisasi layout
			layoutInit();
			
			addEventListener(Event.ENTER_FRAME, update, false, 0, true);
		}
		
		//membuat halaman loading
		protected function halLoadingCreate():void {
			loadingScreen = new Sprite();
			
			loading_txt = new Teks();
			loading_txt.format.align = TextFormatAlign.CENTER;
			loading_txt.width = 800;
			loading_txt.text = 'Meload Gambar ';
			loading_txt.formatApply();
			loading_txt.height = loading_txt.textHeight + 4;
			loading_txt.y = 300 - loading_txt.height / 2;
			loadingScreen.addChild(loading_txt);
			
			addChild(loadingScreen);
		}
		
		//meload semua gambar background
		//setelah selesai akan memanggil fungsi bmpOnLoad
		protected function bmpInit():void {
			bmp = new BmpBLoader();
			bmp.reg('hal01', 'imgs/hal01.png');
			bmp.reg('hal02', 'imgs/hal02.png');
			bmp.reg('hal03', 'imgs/hal03.png');
			bmp.reg('hal04', 'imgs/hal04.png');
			bmp.reg('hal05', 'imgs/hal05.png');
			bmp.reg('hal06', 'imgs/hal06.png');
			bmp.reg('hal07', 'imgs/hal07.png');
			bmp.reg('hal08', 'imgs/hal08.png');
			bmp.reg('hal09', 'imgs/hal09.png');
			bmp.reg('hal10', 'imgs/hal10.png');
			bmp.reg('panah_kanan', 'imgs/panah_kanan.png');
			bmp.reg('panah_kiri', 'imgs/panah_kiri.png');
			bmp.onLoadCallBack = bmpOnLoad;
			bmp.load();			
		}		
		
		//fungsi ini akan dipanggil bila semua gambar 
		//selesai di load
		protected function bmpOnLoad():void {
			//hapus loading screen
			removeChild(loadingScreen);
			
			//tambahkan gambar panah pada object panahKanan
			panahKanan.addChild(bmp.getBmpById('panah_kanan').bmp);
			
			//tambahkan gambar panah kiri pada object panahKiri
			panahKiri.addChild(bmp.getBmpById('panah_kiri').bmp);
			
			//halaman index diisi dengan 0
			//menandai awal halaman
			halIdx = 0;
			
			//panggil fungsi gantiHalaman untuk memulai.
			gantiHalaman(halIdx);
		}		
		
		//inisialisasi data halaman
		//semua data disimpan dalam object Halaman
		//semua object disimpan dalam vector hals.
		protected function halInit():void {
			var hal:Halaman;
			
			//buat vector untuk menyimpan semua halaman
			hals = new Vector.<Halaman>();
			
			//buat object Halaman untuk menyimpan data tiap halaman
			hal = new Halaman();
			hal.teks = '';
			hal.gbrId = 'hal01';
			hals.push(hal);
			
			hal = new Halaman();
			hal.teks = 'Amr bin Jamuh adalah seroang pembesar di Madinah. Akan tetapi, ia masih kafir. Amr bin Jamuh masih menyembah berhala.';
			hal.gbrId = 'hal02';
			hals.push(hal);
			
			hal = new Halaman();
			hal.teks = 'Ia mempunyai berhala bernama manat. Berhala itu diletakkannya dalam tempat khusus. Ia pun sering bersujud dan meminta berkah kepada berhala itu.';
			hal.gbrId = 'hal03';
			hals.push(hal);
			
			hal = new Halaman();
			hal.teks = 'Anak - anaknya pun sedih. Mereka tidak mau ayah mereka terus dalam kekafirannya. Mereka kemudian menyusun sebuah rencana.';
			hal.gbrId = 'hal04';
			hals.push(hal);
			
			hal = new Halaman();
			hal.teks = 'Suatu ketika Amr bin Jamuh meletakkan pedangnya pada berhala Ketika Amr bin Jamuh pergi, anak - anaknya mengambil pedang tersebut dan menyembunyikannya.';
			hal.gbrId = 'hal05';
			hals.push(hal);
			
			hal = new Halaman();
			hal.teks = 'Ketika tahu pedangnya hilang. Amr bin Jamuh pun marah karena berhala tersebut tidak mampu membela dirinya sendiri sehingga pedangnya hilang.';
			hal.gbrId = 'hal06';
			hals.push(hal);
			
			hal = new Halaman();
			hal.teks = 'Tak lama kemudian Amr bin Jamuh pergi keluar. Kesempatan itu tidak disia - siakan oleh anak - anaknya Mereka membuang berhala tersebut ke tempat sampah.';
			hal.gbrId = 'hal07';
			hals.push(hal);
			
			hal = new Halaman();
			hal.teks = 'Ketika pulang, ia pun mencari - cari berhalanya. Ia sangat marah ketika tahu berhalanya ada di tempat sampah. Kemudian ia bersihkan dan dikembalikannya lagi ke tempatnya semula.';
			hal.gbrId = 'hal08';
			hals.push(hal);
			
			hal = new Halaman();
			hal.teks = 'Melihat ayahnya belum juga sadar dari kekafirannya, anak - anak Amr bin Jamuh tidak putus asa. Mereka mengikatkan berhala tersebut dengan bangkai anjing dan melemparkannya ke tempat pembuangan sampah.';
			hal.gbrId = 'hal09';
			hals.push(hal);
			
			hal = new Halaman();
			hal.teks = 'Akhirnya Amr bin Jamuh sadar, berhala tidak dapat menolong dirinya sendiri. Bagaimana mungkin berhala dapat menolong manusia? Amr bin Jamuh pun kemudian mengucapkan syahadat. Semoga Allah meridhai Amr bin Jamuh beserta keluarganya.';
			hal.gbrId = 'hal10';
			hals.push(hal);
		}		
		
		//membuat layout
		protected function layoutInit():void {
			//background container, berisi gambar-gambar background dari tiap halaman
			backCont = new Sprite();
			addChild(backCont);
			
			//teks berjalan
			teksCerita = new Teks();
			teksCerita.width = 800;
			teksCerita.format.align = TextFormatAlign.CENTER;
			teksCerita.multiline = true;
			teksCerita.wordWrap = true;
			teksCerita.format.size = 18;
			teksCerita.formatApply();
			addChild(teksCerita);
			
			//membuat object panah
			//saat dibuat panah kanan dan kiri masih berupa kontainer dan
			//belum ada gambarnya
			
			//panah kanan
			panahKanan = new Sprite();
			panahKanan.addEventListener(MouseEvent.CLICK, kananOnClick, false, 0, true);
			addChild(panahKanan);
			
			//panah kiri
			panahKiri = new Sprite();
			panahKiri.addEventListener(MouseEvent.CLICK, kiriOnClick, false, 0, true);
			addChild(panahKiri);
			
			panahKanan.y = 566 - 128 - 8;
			panahKanan.x = 800 - 8 - 64;
			
			panahKiri.y = panahKanan.y;
			panahKiri.x = panahKanan.x - 6 - 64;
			panahKiri.visible = false;
		}
		
		//panah kanan di klik
		//bila belum halaman terakhir
		//pindah halaman
		protected function kananOnClick(e:MouseEvent):void {
			if (halIdx < hals.length - 1) {
				halIdx++;
				gantiHalaman(halIdx);
			}
		}
		
		//panah kiri di klik
		//bila belum halaman pertama
		//pindah halaman
		protected function kiriOnClick(E:MouseEvent):void {
			if (halIdx > 0) {
				halIdx--;
				gantiHalaman(halIdx);
			}
		}
		
		//dipanggil saat enterframe
		protected function update(e:Event):void {
			//update tulisan berjalan
			ketik.update();
			
			teksCerita.text = ketik.teksOutput;
			
			//atur posisi supaya ke tengah
			teksCerita.y = 80 - teksCerita.textHeight / 2;
			
			//update loadingScreen jika masih dalam proses loading
			if (loadingScreen.parent) {
				loading_txt.text = 'Meload Gambar ' + bmp.loadedBitmapCount() + '/' +  bmp.bmpTotal() + ' -- ' + Math.floor((bmp.bmpCr.byteLoaded / bmp.bmpCr.byteTotal) * 100);
			}
		}
		
		//fungsi ini untuk menangani pergantian halaman
		protected function gantiHalaman(idx:int):void {
			var bmpLoader:BmpLoader
			var idGambar:String;
			var halamanAktif:Halaman
			var gambarBack:Bitmap;
			
			//hapus background
			backCont.removeChildren();
			
			//dapatkan background baru
			halamanAktif = hals[idx];
			idGambar = halamanAktif.gbrId;
			bmpLoader = bmp.getBmpById(idGambar);
			gambarBack = bmpLoader.bmp;
			
			//isi dengan background baru			
			backCont.addChild(gambarBack);
			
			//aktifkan lagi tulisan berjalan dengan data baru
			ketik.teksInput = hals[idx].teks;
			ketik.start();
			
			//update panah sesuai halaman
			if (idx == 0) {
				panahKiri.visible = false;
				panahKanan.visible = true;
			}
			else if (idx <= hals.length - 2) {
				panahKiri.visible = true;
				panahKanan.visible = true;
			}
			else {
				panahKiri.visible = true;
				panahKanan.visible = false;
			}
		}
		
	}
	
}