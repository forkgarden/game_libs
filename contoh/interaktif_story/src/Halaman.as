package {
	
	//Class ini berfungsi untuk menyimpan data dari tiap halaman
	public class Halaman {
		
		protected var _teks:String = '';
		protected var _gbrId:String = '';
		
		public function Halaman() {
			
		}
		
		public function get teks():String {
			return _teks;
		}
		
		public function set teks(value:String):void {
			_teks = value;
		}
		
		public function get gbrId():String {
			return _gbrId;
		}
		
		public function set gbrId(value:String):void {
			_gbrId = value;
		}
		
	}

}