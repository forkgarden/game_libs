package {
	
	import fg.bmps.BmpLoader;
	import fg.path_finder_helper.PFHelper;
	import fg.pathfinders.PathFinder;
	import fg.tmxs.Property;
	import fg.tmxs.TileObject;
	import fg.tmxs.Tmx;
	import fg.ui.Teks;
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextFormatAlign;
	
	[Frame(factoryClass="Preloader")]
	public class MainTarget extends Sprite {
		
		[Embed(source = "../bin/char.png")]
		protected var CharImg:Class;
		
		protected var tmx:Tmx = new Tmx();
		protected var pfHelper:PFHelper;
		protected var pathFinder:PathFinder;
		protected var char:Bitmap;
		protected var bmpLoader:BmpLoader;
		protected var rumah:Sprite;
		protected var rumahPosX:int;
		protected var rumahPosY:int;
		protected var halLoading:Sprite;
		
		public static const TILE_SIZE:int = 32;
		
		public function MainTarget() {
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		//Mencari posisi x dari karakter dalam satuan tile
		protected function getTilePosX():int{
			return Math.floor((char.x) / TILE_SIZE);
		}
		
		//Mencari posisi y dari karakter dalam satuan tile
		protected function getTilePosY():int{
			return Math.floor((char.y) / TILE_SIZE);
		}
		
		//fungsi ini dipanggil saat map di click
		public function mapOnClick(evt:MouseEvent):void {
			var ar:Array;
			var tujuanX:Number;
			var tujuanY:Number;
			
			//check apakah PathFinder Helper sedang aktif
			//bila tidak maka karakter boleh jalan
			if (pfHelper.aktif == false) {
				
				//check apakah mengklik rumah
				if (evt.target == rumah) {
					tujuanX = rumahPosX;
					tujuanY = rumahPosY;
				}
				else {
					
					//hitung posisi tile yang di klik
					tujuanX = Math.floor(stage.mouseX / TILE_SIZE);
					tujuanY = Math.floor(stage.mouseY / TILE_SIZE);
				}
				
				ar = pathFinder.find(getTilePosX(), getTilePosY(), tujuanX, tujuanY);
				
				//bila path ketemu
				if (ar.length > 0) { 
					pfHelper.start(ar);
				}
				else {
					trace('path finder failed ' + ar.length);
				}
			}
			else {
				trace('pfhelper aktif ' + pfHelper.aktif);
			}
		}
		
		protected function init(e:Event = null):void {
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			this.stage.scaleMode = StageScaleMode.SHOW_ALL;
			
			addEventListener(MouseEvent.CLICK, mapOnClick, false, 0, true);
			
			//buat halaman loading
			halLoadingCreate();
			addChild(halLoading);			
			
			//insialisasi TMX untuk meload map
			tmx = new Tmx();
			tmx.url = 'map.tmx';
			
			//setelah map di load panggil fungsi tmxLoaded
			tmx.onLoadCallBack = tmxLoaded;
			tmx.load();
		}
		
		//fungsi ini untuk membuat tampilan loading
		protected function halLoadingCreate():void {
			var teks:Teks = new Teks();
			
			halLoading = new Sprite();
			halLoading.addChild(teks);
			
			teks.text = 'Loading ...';
			teks.width = this.stage.stageWidth;
			teks.format.align = TextFormatAlign.CENTER;
			teks.height = teks.textHeight + 4;
			teks.y = this.stage.stageHeight / 2 - teks.height / 2;
			teks.formatApply();
		}
		
		//fungsi ini dipanggil saat map selesai di load
		protected function tmxLoaded():void {
			var rumahObj:TileObject;
			var prop:Property;
			
			this.removeChildren();
			
			//render tmx
			//render bagian base
			tmx.getLayerByName('base').render(this);
			
			//buat object rumah
			rumahObj = tmx.getLayerByName('object').getObjByName('rumah');
			rumah = rumahObj.sprite;
			rumah.mouseEnabled = true;
			addChild(rumah);
			
			//hitung posisi reference dari rumah
			//hitung posisi x
			rumahPosX = parseFloat(rumahObj.getPropValueByName('tx'));
			
			//rubah ke unit pixel
			rumahPosX *= TILE_SIZE;
			
			//tambahkan dengan posisi rumah sebenarnya
			//untuk mendapatkan posisi absolute
			rumahPosX += rumah.x;
			
			//rubah ke unit tile
			rumahPosX = Math.floor(rumahPosX / TILE_SIZE);
			
			//posisi y
			rumahPosY = parseFloat(rumahObj.getPropValueByName('ty'));
			rumahPosY *= TILE_SIZE;
			rumahPosY += rumah.y;
			rumahPosY = Math.floor(rumahPosY / TILE_SIZE);
			
			//inisialisasi karakter
			char = new CharImg();
			char.x = 320;
			char.y = 320;
			addChild(char);
			
			//inisialisasi Path Finding Helper
			pfHelper = new PFHelper();
			pfHelper.checkCanMoveToPos = checkBlock;
			
			//inisialisasi Path Finding
			pathFinder = new PathFinder();
			pathFinder.maxNodes = 1000;
			pathFinder.checkCanMoveToPos = checkBlock;
			
			//aktifkan pergerakan diagonal
			pathFinder.diagonalMove = true;
			
			this.addEventListener(Event.ENTER_FRAME, update, false, 0, true);			
		}
		
		//check apakah tile bisa dilewati
		protected function checkBlock(i:int, j:int):Boolean {
			if (i < 0) return false;
			if (i >= tmx.width) return false;
			if (j < 0) return false;
			if (j >= tmx.height) return false;
			
			//check object
			if (tmx.getLayerByName('block').getTile(i, j).gid > 0) return false;
			
			return true;
		}
		
		protected function update(e:Event):void {
			
			//bila pfHelper aktif, jalankan karakter
			if (pfHelper.aktif) {
				pfHelper.update();
				char.x = pfHelper.pos.x;
				char.y = pfHelper.pos.y;
			}
		}
		
	}
}