package
{
	import fg.bmps.BmpLoader;
	import fg.path_finder_helper.PFHelper;
	import fg.pathfinders.PathFinder;
	import fg.tmxs.Tmx;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import fg.ui.Teks;
	import flash.text.TextFormatAlign;

	[Frame(factoryClass="Preloader")]
	public class MainPFRotation extends Sprite 
	{
		
		protected var tmx:Tmx = new Tmx();
		protected var pfHelper:PFHelper;
		protected var pathFinder:PathFinder;
		protected var tank:Sprite;
		protected var bmpLoader:BmpLoader;
		protected var halLoading:Sprite;

		public function MainPFRotation() 
		{
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		//fungsi awal
		protected function init(e:Event = null):void {
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			//buat halaman loading
			halLoadingCreate();
			addChild(halLoading);
			
			//inisialisasi tmx
			tmx = new Tmx();
			tmx.url = 'map.tmx';
			
			//setelah map di load panggil fungsi tmxLoaded
			tmx.onLoadCallBack = tmxLoaded;
			tmx.load();
		}
		
		//fungsi ini untuk membuat tampilan loading
		protected function halLoadingCreate():void {
			var teks:Teks = new Teks();
			
			halLoading = new Sprite();
			halLoading.addChild(teks);
			
			teks.text = 'Loading ...';
			teks.width = this.stage.stageWidth;
			teks.format.align = TextFormatAlign.CENTER;
			teks.height = teks.textHeight + 4;
			teks.y = this.stage.stageHeight / 2 - teks.height / 2;
			teks.formatApply();
		}
		
		//fungsi ini dipanggil saat map selesai di load
		protected function tmxLoaded():void {
			
			//inisialisasi BmpLoader
			//untuk meload gambar tank
			bmpLoader = new BmpLoader();
			bmpLoader.url = 'tank.png';
			
			//setelah gambar di load, panggil fungsi bmpOnLoaded
			bmpLoader.onLoadCallBack = bmpOnLoaded;
			bmpLoader.load();
		}
		
		//fungsi ini dipanggil saat gambar tank selesai di load
		protected function bmpOnLoaded():void {
			removeChildren();
			
			//render map
			tmx.render(this);
			
			//buat tank
			tank = new Sprite();
			
			//posisi gambar di taruh agak ke kiri atas
			//agar bisa dirotasi dengan benar
			bmpLoader.bmp.x =-16;
			bmpLoader.bmp.y =-16;
			tank.addChild(bmpLoader.bmp);
			
			//posisi tank di geser 16 px
			//agar pas di grid
			tank.x = 64 + 16;
			tank.y = 64 + 16;
			
			//rotasi awal 0
			tank.rotation = 0;
			addChild(tank);
			
			//inisialisasi PFHelper
			//untuk membantu menggerakkan tank
			//melalui jalan yang dibuat oleh PathFinder
			pfHelper = new PFHelper();
			pfHelper.checkCanMoveToPos = checkBlock;
			
			//set kecepatan sudut 10 derajat
			pfHelper.rotSpeed = 10;
			
			//inisialisasi PathFinder
			pathFinder = new PathFinder();
			pathFinder.checkCanMoveToPos = checkBlock;
			pathFinder.maxNodes = 1000;
			
			this.addEventListener(Event.ENTER_FRAME, update, false, 0, true);
			this.stage.addEventListener(MouseEvent.CLICK, mapOnClick, false, 0, true);
		}
		
		//fungsi ini dipanggil oleh PFHelper dan PathFinder
		//untuk mengecek apakah tile bisa dilewati atau tidak
		protected function checkBlock(i:int, j:int):Boolean {
			
			//bila posisi di luar layar, tidak boleh jalan
			if (i < 0) return false;
			if (i >= tmx.width) return false;
			if (j < 0) return false;
			if (j >= tmx.height) return false;
			
			//bila di atas rumput, tidak boleh jalan
			if (tmx.getLayerByName('jalan').getTile(i, j).gid == 0) return false;
			
			return true;
		}
		
		//fungsi ini dipanggil saat map di click
		public function mapOnClick(evt:MouseEvent):void {
			var ar:Array;
			var posX:Number;
			var posY:Number;
			var tileX:Number;
			var tileY:Number;
			
			//check apakah PathFinder Helper sedang aktif
			//bila tidak maka karakter boleh jalan
			if (pfHelper.aktif == false) {
				
				//hitung posisi tile karakter
				posX = Math.floor((tank.x) / 32.0);
				posY = Math.floor((tank.y) / 32.0);
				
				//hitung posisi tile yang di klik
				tileX = Math.floor(stage.mouseX / 32.0);
				tileY = Math.floor(stage.mouseY / 32.0);
				
				//cari jalan
				ar = pathFinder.find(posX, posY, tileX, tileY);
				
				//bila path ketemu, jalan
				if (ar.length > 0) { 
					pfHelper.start(ar);
				}
			}
		}		
		
		//fungsi ini dipanggil saat ENTER_FRAME
		protected function update(e:Event):void {
			if (pfHelper.aktif) {
				pfHelper.update();
				
				//update posisi
				tank.x = pfHelper.pos.x + 16;
				tank.y = pfHelper.pos.y + 16;
				
				//update rotasi
				tank.rotation = pfHelper.rotation;
			}
		}
		
	}

}