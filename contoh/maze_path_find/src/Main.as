package
{
	import fg.bmps.BmpBLoader;
	import fg.mazes.Maze;
	import fg.path_finder_helper.PFHelper;
	import fg.pathfinders.PathFinder;
	import fg.ui.Teks;
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextFormatAlign;

	[Frame(factoryClass="Preloader")]
	public class Main extends Sprite 
	{
		
		protected const TILE_SIZE:int = 32;
		
		protected var bmpLoader:BmpBLoader;
		protected var maze:Maze;
		protected var halLoading:Sprite;
		protected var karakter:Bitmap;
		protected var pfHelper:PFHelper;
		protected var pathFinder:PathFinder;		
		
		public function Main() 
		{
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}

		private function init(e:Event = null):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			this.stage.scaleMode = StageScaleMode.SHOW_ALL;
			
			addEventListener(MouseEvent.CLICK, mapOnClick, false, 0, true);
			
			//buat halaman loading
			halLoadingCreate();
			addChild(halLoading);
			
			bmpLoader = new BmpBLoader();
			bmpLoader.reg('char', 'char.png');
			bmpLoader.reg('jalan', 'jalan.png');
			bmpLoader.reg('rumput', 'rumput.png');
			bmpLoader.onLoadCallBack = bmpOnLoaded;
			bmpLoader.load();
		}		
		
		//fungsi ini untuk membuat tampilan loading
		protected function halLoadingCreate():void {
			var teks:Teks = new Teks();
			
			halLoading = new Sprite();
			halLoading.addChild(teks);
			
			teks.text = 'Loading ...';
			teks.width = this.stage.stageWidth;
			teks.format.align = TextFormatAlign.CENTER;
			teks.height = teks.textHeight + 4;
			teks.y = this.stage.stageHeight / 2 - teks.height / 2;
			teks.formatApply();
		}
		
		/**
		 * Fungsi ini dipanggil saat gambar
		 * selesai di load
		 */
		protected function bmpOnLoaded():void {
			//bersihkan layar
			removeChildren();
			
			//buat maze
			//6 column, 4 baris, ukuran ruangan 3x3
			maze = new Maze();
			maze.create(6, 4, 3);
			maze.drawWImg(this, bmpLoader.getBmpById('jalan').bmp.bitmapData, bmpLoader.getBmpById('rumput').bmp.bitmapData);
			
			//buat karakter
			karakter = bmpLoader.getBmpById('char').bmp;
			addChild(karakter);
			karakter.x = TILE_SIZE;
			karakter.y = TILE_SIZE;
			
			//inisialisasi Path Finding Helper
			pfHelper = new PFHelper();
			pfHelper.checkCanMoveToPos = checkBlock;
			
			//inisialisasi Path Finding
			pathFinder = new PathFinder();
			pathFinder.maxNodes = 1000;
			pathFinder.checkCanMoveToPos = checkBlock;
			
			this.addEventListener(Event.ENTER_FRAME, update, false, 0, true);			
		}
		
		protected function update(evt:Event):void {
			
			//update karakter posisition
			if (pfHelper.aktif) {
				pfHelper.update();
				karakter.x = pfHelper.pos.x;
				karakter.y = pfHelper.pos.y;
			}
			
		}
		
		/**
		 * 
		 * check apakah tile bisa dilewati
		 * tile bisa dilewati bila tidak ada tembok
		 * 
		 * @param	i
		 * @param	j
		 * @return
		 * 
		 */
		protected function checkBlock(i:int, j:int):Boolean {			
			if (maze.map[i][j] > 0) return false;
			
			return true;
		}		
		
		/**
		 * Hitung posisi x karakter pada unit tile.
		 * 
		 * @return
		 */
		protected function getTilePosX():int{
			return Math.floor((karakter.x) / TILE_SIZE);
		}
		
		/**
		 * Hitung posisi vertical karakter pada unit tile
		 * @return
		 */
		protected function getTilePosY():int{
			return Math.floor(karakter.y / TILE_SIZE);
		}		
		
		/**
		 * Fungsi ini dipanggil saat map di click
		 * 
		 * @param	evt
		 */
		protected function mapOnClick(evt:MouseEvent):void {
			var tileX:Number;
			var tileY:Number;
			var ar:Array;
			
			//check apakah PathFinder Helper sedang aktif
			//bila tidak maka karakter boleh jalan
			if (pfHelper.aktif == false) {
				
				//hitung posisi tile yang di klik
				tileX = Math.floor(stage.mouseX / TILE_SIZE);
				tileY = Math.floor(stage.mouseY / TILE_SIZE);
				
				ar = pathFinder.find(getTilePosX(), getTilePosY(), tileX, tileY);
				
				//bila path ketemu
				if (ar.length > 0) {
					pfHelper.start(ar);
				}
			}
			
		}
		
	}

}