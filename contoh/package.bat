::default
rmdir %target_name% /s /q
rmdir %target_dir% /s /q
rmdir %server_dir% /s /q
mkdir %target_dir% || goto error
xcopy %src_dir% %target_dir% /i /s /y || goto error

echo dependency
for %%d in (%dep%) do (

	if "%%d" == "pf"   xcopy ..\..\pathfinder\src\fg\pathfinders %target_dir%\src\fg\pathfinders /s /i /y
	if %errorlevel% neq 0 goto error
	
	if "%%d" == "pfh"  xcopy ..\..\path_finder_helper\src\fg\path_finder_helper %target_dir%\src\fg\path_finder_helper\ /s /i
	if %errorlevel% neq 0 goto error
	
	if "%%d" == "maz"  xcopy ..\..\mazeLib02\src\fg\mazes %target_dir%\src\fg\mazes\ /s /i
	if %errorlevel% neq 0 goto error
	
	if "%%d" == "bmp"  xcopy ..\..\BmpHandler\src\fg\bmps %target_dir%\src\fg\bmps\ /s /i
	if %errorlevel% neq 0 goto error
	
	if "%%d" == "tmx"  xcopy ..\..\tmx\src\fg\tmxs %target_dir%\src\fg\tmxs\ /s /i 
	if %errorlevel% neq 0 goto error
	
	if "%%d" == "ktk"  xcopy ..\..\teks_type\src\fg\tekstypes %target_dir%\src\fg\tekstypes /s /i
	if %errorlevel% neq 0 goto error
	
	if "%%d" == "dlg"  xcopy ..\..\dialog\src\fg\dialogs %target_dir%\src\fg\dialogs /s /i
	if %errorlevel% neq 0 goto error
	
	:: ui dec
	if "%%d" == "ui"  mkdir %target_dir%\src\fg\ui
	if %errorlevel% neq 0 goto error
	
	:: ui
	if "%%d" == "tek"  copy ..\..\ui\src\fg\ui\Teks.as		%target_dir%\src\fg\ui\Teks.as /y
	if %errorlevel% neq 0 goto error
	
	if "%%d" == "pnl"  copy ..\..\ui\src\fg\ui\Panel3D.as	%target_dir%\src\fg\ui\Panel3D.as /y
	if %errorlevel% neq 0 goto error
	
	if "%%d" == "inp"  copy ..\..\ui\src\fg\ui\Input.as	%target_dir%\src\fg\ui\Input.as /y
	if %errorlevel% neq 0 goto error
	
)

::clean up
echo clean up
del %target_dir%\*.bat || goto error

::serve copy
echo copy to server
mkdir %server_dir%\ || goto error
xcopy %src_dir%\bin %server_dir% /i /s /y || goto error

echo clean up server
del %server_dir%\js\*.* /q || goto error
rmdir %server_dir%\js\ /s /q || goto error
del %server_dir%\expressInstall.swf || goto error
del %server_dir%\index.html

::zipping
echo zipping
move %target_dir% .
%zip_app%\7za a %target_name%.zip .\%target_name%\* || goto error

echo moving zip to server
move %target_name%.zip %zip_dir%

echo success with no error
goto :end

:error
echo ERROR %errorlevel%

:end
echo end

pause