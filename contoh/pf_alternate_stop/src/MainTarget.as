package {
	
	import fg.bmps.BmpLoader;
	import fg.path_finder_helper.PFHelper;
	import fg.pathfinders.PathFinder;
	import fg.tmxs.TileObject;
	import fg.tmxs.Tmx;
	import fg.ui.Teks;
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextFormatAlign;
	
	[Frame(factoryClass="Preloader")]
	public class MainTarget extends Sprite {
		
		protected var tmx:Tmx = new Tmx();
		protected var pfHelper:PFHelper;
		protected var pathFinder:PathFinder;
		protected var char:Bitmap;
		protected var bmpLoader:BmpLoader;
		protected var meong:Sprite;
		protected var meongPosTileX:int;
		protected var meongPosTileY:int;
		protected var halLoading:Sprite;
		
		public static const TILE_SIZE:int = 32;
		
		public function MainTarget() {
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		/**
		 * menghitung posisi tile x dari karakter 
		 * @return
		 */
		protected function getTilePosX():int{
			return Math.floor((char.x) / 32.0);
		}
		
		/**
		 * menghitung posisi tile y dari karakter
		 * @return
		 */
		protected function getTilePosY():int{
			return Math.floor((char.y) / 32.0);
		}
		
		//fungsi ini dipanggil saat map di click
		public function mapOnClick(evt:MouseEvent):void {
			var ar:Array;
			var tileX:Number;
			var tileY:Number;
			
			//check apakah PathFinder Helper sedang aktif
			//bila tidak maka karakter boleh jalan
			if (pfHelper.aktif == false) {
				
				//check apakah mengklik kucing
				if (evt.target == meong) {
					
					trace('click rumah ' + meongPosTileX + '/' + meongPosTileY);
					//hitung posisi tile yang di klik
					tileX = meongPosTileX;
					tileY = meongPosTileY;
					pathFinder.checkSampai = pfCheckSampai;
				}
				else {
					
					//hitung posisi tile yang di klik
					tileX = Math.floor(stage.mouseX / 32.0);
					tileY = Math.floor(stage.mouseY / 32.0);
					pathFinder.checkSampai = null;
				}
				
				trace('click pos ' + tileX + '/' + tileY);
				
				ar = pathFinder.find(getTilePosX(), getTilePosY(), tileX, tileY);
				
				//bila path ketemu
				if (ar.length > 0) { 
					pfHelper.start(ar);
				}
				else {
					trace('path finder failed ' + ar.length);
				}
			}
			else {
				trace('pfhelper aktif ' + pfHelper.aktif);
			}
		}
		
		protected function init(e:Event = null):void {
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			addEventListener(MouseEvent.CLICK, mapOnClick, false, 0, true);
			
			//buat halaman loading
			halLoadingCreate();
			addChild(halLoading);
			
			//insialisasi TMX untuk meload map
			tmx = new Tmx();
			tmx.url = 'map.tmx';
			
			//setelah map di load panggil fungsi tmxLoaded
			tmx.onLoadCallBack = tmxLoaded;
			tmx.load();
		}
		
		//fungsi ini untuk membuat tampilan loading
		protected function halLoadingCreate():void {
			var teks:Teks = new Teks();
			
			halLoading = new Sprite();
			halLoading.addChild(teks);
			
			teks.text = 'Loading ...';
			teks.width = this.stage.stageWidth;
			teks.format.align = TextFormatAlign.CENTER;
			teks.height = teks.textHeight + 4;
			teks.y = this.stage.stageHeight / 2 - teks.height / 2;
			teks.formatApply();
		}
		
		//fungsi ini dipanggil saat map selesai di load
		protected function tmxLoaded():void {			
			//inisialisasi bmpLoader untuk meload gambar karakter
			bmpLoader = new BmpLoader();
			bmpLoader.url = 'char.png';
			
			//setelah gambar di load panggil fungsi bmpOnLoaded
			bmpLoader.onLoadCallBack = bmpOnLoaded;
			bmpLoader.load();
		}
		
		//fungsi ini dipanggil saat gambar karakter selesai di load
		protected function bmpOnLoaded():void {
			var meongObj:TileObject;
			
			this.removeChildren();
			
			//render tmx
			//render bagian base
			tmx.getLayerByName('base').render(this);
			
			//buat object rumah
			meongObj = tmx.getLayerByName('object').getObjByName('meong');
			meong = meongObj.sprite;
			meong.mouseEnabled = true;
			meongPosTileX = Math.floor(meong.x / TILE_SIZE);
			meongPosTileY = Math.floor(meong.y / TILE_SIZE);
			addChild(meong);
			
			//inisialisasi karakter
			char = bmpLoader.bmp;
			char.x = 320;
			char.y = 320;
			addChild(char);
			
			//inisialisasi Path Finding Helper
			pfHelper = new PFHelper();
			pfHelper.checkCanMoveToPos = checkBlock;
			
			//inisialisasi Path Finding
			pathFinder = new PathFinder();
			pathFinder.checkSampai = pfCheckSampai;
			pathFinder.checkCanMoveToPos = checkBlock;
			
			//aktifkan pergerakan diagonal
			pathFinder.diagonalMove = true;
			
			this.addEventListener(Event.ENTER_FRAME, update, false, 0, true);			
		}
		
		/**
		 * Check apakah karakter sudah bisa berhenti
		 * Kondisi bisa berhenti bila posisi karakter sudah
		 * dekat dengan meong.
		 * 
		 * @param	i
		 * @param	j
		 * @return
		 */
		protected function pfCheckSampai(i:int, j:int):Boolean {
			
			if (Math.abs(meongPosTileX - i) < 5) {
				if (Math.abs(meongPosTileY - j) < 5) {
					return true;
				}
			}
			
			return false;
		}
		
		//check apakah tile bisa dilewati
		protected function checkBlock(i:int, j:int):Boolean {
			//tile di luar layar tidak bisa dilewati
			if (i < 0) return false;
			if (i >= tmx.width) return false;
			if (j < 0) return false;
			if (j >= tmx.height) return false;
			
			//check object
			if (i == meongPosTileX) {
				if (j == meongPosTileY) {
					return false;
				}
			}
			
			return true;
		}
		
		protected function update(e:Event):void {
			
			//bila pfHelper aktif, jalankan karakter
			if (pfHelper.aktif) {
				pfHelper.update();
				char.x = pfHelper.pos.x;
				char.y = pfHelper.pos.y;
			}
		}
		
	}
}