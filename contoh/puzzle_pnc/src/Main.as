/* prerequisite
 * 
 * Dialog
 * target with reference
 * 
 * Class included
 * 
 * 
 * 
 * */

package{
	import fg.tmxs.Tmx;
	import fg.ui.Teks;
	import flash.display.Sprite;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextFormatAlign;
	import puzzle.Cerita;
	import puzzle.Dialog;
	import puzzle.KarakterUtama;
	import puzzle.SceneObj;
	import puzzle.SceneRpg;
	import puzzle.sc_kamar.Kamar;

	[Frame(factoryClass="Preloader")]
	public class Main extends Sprite {
		
		public static const MAP_WIDTH:int = 9;
		public static const MAP_HEIGHT:int = 6;
		
		public static const TILE_SIZE:Number = 48;
		
		protected static var _inst:Main;
		
		protected var _tmx:Tmx;
		protected var _krUtama:KarakterUtama;
		protected var sceneObjLists:Vector.<SceneObj> = new Vector.<SceneObj>();
		protected var _cerita:Cerita;
		protected var _dialog:Dialog;
		protected var halLoading:Sprite;
		protected var _sceneCr:SceneRpg;
		
		public function Main() {
			_inst = this;
			
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}

		protected function init(e:Event = null):void {
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			stage.scaleMode = StageScaleMode.SHOW_ALL;
			
			_sceneCr = new Kamar();
			_sceneCr.readyCallBack = sceneOnReady;
			_sceneCr.init();
			
			//buat halaman loading
			halLoadingCreate();
			addChild(halLoading);	
			
			//membuat dialog
			_dialog = new Dialog();
			_dialog.visible = false;
			addChild(_dialog);
			
			//membuat handler untuk cerita
			//untuk menyimpan state
			_cerita = new Cerita();
		}
		
		protected function sceneOnReady(scene:SceneRpg):void {
			halLoading.visible = false;
			
			addChild(scene);
			
			_krUtama = new KarakterUtama();
			_krUtama.initFromTmx(scene.tmx.getLayerByName('object').getObjByName("karakter"));
			
			this.stage.addEventListener(MouseEvent.CLICK, stageOnClick, false, 0, true);
			addEventListener(Event.ENTER_FRAME, update);
			
			//
			_dialog.start(
				[
					"Selamat datang di tutorial puzzle point and click",
					"Ini adalah contoh sederhana dari game puzzle point and click",
					"Click pada layar untuk mulai main"
				]
			);			
		}
		
		//fungsi ini untuk membuat tampilan loading
		protected function halLoadingCreate():void {
			var teks:Teks = new Teks();
			
			halLoading = new Sprite();
			halLoading.addChild(teks);
			
			teks.text = 'Loading ...';
			teks.width = this.stage.stageWidth;
			teks.format.align = TextFormatAlign.CENTER;
			teks.height = teks.textHeight + 4;
			teks.y = this.stage.stageHeight / 2 - teks.height / 2;
			teks.formatApply();
		}		
		
		//fungsi ini dipanggil saat stage di click
		protected function stageOnClick(e:MouseEvent):void {
			var posX:Number = 0;
			var posY:Number = 0;
			
			//check apakah karakter utama ada
			if (_krUtama) {
				
				//hitung posisi tile yang di click
				posX = Math.floor(e.stageX / TILE_SIZE);
				posY = Math.floor(e.stageY / TILE_SIZE);
				
				//perintahkan karakter utama untuk jalan ke posisi 
				//yang diklik 
				_krUtama.jalanKePosisi(posX, posY);
			}
		}
		
		protected function update(e:Event):void {
			_krUtama.update();
			_sceneCr.update();
		}
		
		static public function get inst():Main 
		{
			return _inst;
		}
		
		public function get cerita():Cerita 
		{
			return _cerita;
		}
		
		public function get dialog():Dialog 
		{
			return _dialog;
		}
		
		public function get tmx():Tmx 
		{
			return _tmx;
		}
		
		public function get krUtama():KarakterUtama 
		{
			return _krUtama;
		}
		
		public function get sceneCr():SceneRpg 
		{
			return _sceneCr;
		}
	}
}