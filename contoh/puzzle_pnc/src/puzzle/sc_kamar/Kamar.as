package puzzle.sc_kamar 
{
	import fg.tmxs.TileObject;
	import puzzle.SceneRpg;
	import puzzle.sc_kamar.Kasur;
	import puzzle.sc_kamar.Lemari1;
	import puzzle.sc_kamar.Pintu;

	public class Kamar extends SceneRpg
	{
		
		public function Kamar() 
		{
			super();
			_mapUrl = "map.tmx";
		}
		
		protected override function tmxLoaded():void {
			var tileObj:TileObject;
			
			//gambar map ke layar
			//layer yang digambar adalah layer "back" dan "object"
			_tmx.getLayerByName("back").render(this);
			_tmx.getLayerByName("object").render(this);
			
			//membuat object lemari dan pintu
			sceneObjCreate("lemari1", Lemari1);
			sceneObjCreate("pintu", Pintu);
			sceneObjCreate("kasur", Kasur);
			
			super.tmxLoaded();
		}		
		
	}
}