package puzzle.sc_kamar {
	import Main;
	import puzzle.Cerita;
	import puzzle.SceneObj;
	
	public class Lemari1 extends SceneObj {
		
		public function Lemari1() {
			
		}
		
		public override function onAction():void {
			var cerita:Cerita;
			
			cerita = Main.inst.cerita;
			
			if (cerita.tahap == Cerita.LEMARI_CHECK) {
				Main.inst.dialog.start(
					[
						"Hmm... Sepertinya aku ingat pernah menyimpan kunci disini",
						"Wah, ini dia, aku lupa menyimpan kunci pintu disini",
						"Aku bisa membuka pintu sekarang"
					], function ():void {
						Main.inst.cerita.tahap = Cerita.PINTU_BUKA_KUNCI;
					});
			}
			else {
				Main.inst.dialog.start(["Ini lemari pakaian"]);
			}
		}
		
	}

}