package puzzle 
{
	import fg.tmxs.Tmx;
	import flash.display.Sprite;
	import flash.events.MouseEvent;

	public class SceneRpg extends Sprite
	{
		
		protected var objs:Vector.<SceneObj>;
		protected var _tmx:Tmx;
		protected var _readyCallBack:Function;
		protected var _mapUrl:String = '';
		
		public function SceneRpg() 
		{
			_tmx = new Tmx();
			objs = new Vector.<SceneObj>();
		}
		
		protected function getIndex(obj:SceneObj):Number {
			return obj.view.parent.getChildIndex(obj.view);
		}
		
		protected function swap(obj1:SceneObj, obj2:SceneObj):void {
			if (obj1.view.parent == null) return;
			if (obj2.view.parent == null) return;
			if (obj1.view.parent != obj2.view.parent) return;
			
			if (obj1.view.y > obj2.view.y) {
				if (obj1.getViewIndex() > obj2.getViewIndex()) {
					return;
				}
			}
			else {
				if (obj1.getViewIndex() < obj2.getViewIndex()) {
					return;
				}
			}
			
			obj1.view.parent.swapChildren(obj1.view, obj2.view);
		}
		
		protected function sort():void {
			var obj:SceneObj;
			var i:int;
			
			for (i = 0; i < objs.length - 2; i++) {
				swap(objs[i], objs[i + 1]);
			}
		}
		
		public function update():void {
			sort();
		}		
		
		public function init():void {
			_tmx.url = _mapUrl;
			_tmx.load();
			_tmx.onLoadCallBack = tmxLoaded;
		}
		
		protected function tmxLoaded():void {
			if (_readyCallBack != null) _readyCallBack(this);
		}		
		
		//Fungsi ini untuk membuat object-object di scene
		//parameter
		//name: nama dari object yang di set di file tmx
		//class: nama Class dari object sebagai handler
		//class harus bertipe SceneObj
		protected function sceneObjCreate(name:String, classObj:Class):void {
			var sceneObj:SceneObj;
			
			sceneObj = new classObj();
			
			//ambil gambar dari tmx untuk tampilan dari object
			sceneObj.initFromTmx(_tmx.getLayerByName("object").getObjByName(name));
			
			//set listener
			sceneObj.view.addEventListener(MouseEvent.CLICK, sceneObjClick);
			
			//simpan object di vector
			objs.push(sceneObj);
		}
		
		//fungsi ini dipanggil saat object di click
		protected function sceneObjClick(evt:MouseEvent):void {
			var spr:Sprite;
			var obj:SceneObj;
			
			evt.stopPropagation();
			
			spr = evt.currentTarget as Sprite;
			obj = getObjByView(spr);
			
			//perintahkan karakter utama jalan ke object
			Main.inst.krUtama.jalanKeObject(obj);
		}
		
		//mendapatkan object berdasarkan viewnya
		protected function getObjByView(view:Sprite):SceneObj {
			var sObj:SceneObj;
			
			for each (sObj in objs) {
				if (sObj.view == view) return sObj
			}
			
			return null;
		}		
		
		
		public function get readyCallBack():Function 
		{
			return _readyCallBack;
		}
		
		public function set readyCallBack(value:Function):void 
		{
			_readyCallBack = value;
		}
		
		public function get tmx():Tmx 
		{
			return _tmx;
		}
		
		public function set tmx(value:Tmx):void 
		{
			_tmx = value;
		}
		
		public function get mapUrl():String 
		{
			return _mapUrl;
		}
		
		public function set mapUrl(value:String):void 
		{
			_mapUrl = value;
		}
		
		
	}

}