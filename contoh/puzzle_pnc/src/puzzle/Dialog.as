package puzzle {
	import fg.dialogs.DialogController;
	import fg.ui.Teks;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	public class Dialog extends Sprite {
		
		protected var overlay:Sprite;
		protected var teksBack:Sprite;
		protected var teks:Teks;
		protected var _finishCallBack:Function;
		protected var dialogCtrl:DialogController;
		
		//Class ini berfungsi untuk membuat dialog
		public function Dialog() {
			
			dialogCtrl = new DialogController();
			dialogCtrl.finishCallBack = dialogOnFinish;
			
			//buat layout
			
			//buat background
			overlay = new Sprite();
			overlay.graphics.beginFill(0, 0);
			overlay.graphics.drawRect(0, 0, 432, 288);
			overlay.graphics.endFill();
			addChild(overlay);
			
			//background teks
			teksBack = new Sprite();
			teksBack.graphics.beginFill(0, .5);
			teksBack.graphics.drawRect(0,0,432, 150);
			teksBack.graphics.endFill();
			teksBack.x = 0;
			teksBack.y = 288 - 150;
			addChild(teksBack);
			
			//teks
			teks = new Teks();
			teks.format.bold = true;
			teks.format.size = 18;
			teks.x = 8;
			teks.y = teksBack.y + 8;
			teks.height = teksBack.height - 16;
			teks.width = teksBack.width - 16;
			teks.textColor = 0;
			teks.wordWrap = true;
			teks.formatApply();
			addChild(teks);
			
			addEventListener(Event.ENTER_FRAME, update, false, 0, true);
			addEventListener(MouseEvent.CLICK, onMouseClick, false, 0, true);
		}
		
		protected function dialogOnFinish():void {
			visible = false;
			if (_finishCallBack != null) _finishCallBack();
		}
		
		//fungsi ini dipanggil saat dialog di click
		protected function onMouseClick(evt:MouseEvent):void {
			evt.stopPropagation();
			dialogCtrl.onMouseClick();
		}
		
		//update dialog, tampilkan teks
		protected function update(e:Event):void {
			dialogCtrl.update();
			teks.text = dialogCtrl.getTeks();
		}
		
		//fungsi ini dipanggil saat memulai dialog
		public function start(data:Array, callBack:Function = null):void {
			_finishCallBack = callBack;
			visible = true;	
			teks.text = '';
			
			//tampilkan dialog ke paling depan
			parent.setChildIndex(this, parent.numChildren - 1);
			
			dialogCtrl.start(data);
		}
		
		public function get finishCallBack():Function {
			return _finishCallBack;
		}
		
		public function set finishCallBack(value:Function):void {
			_finishCallBack = value;
		}
		
	}

}