package puzzle {
	import Main;
	import fg.path_finder_helper.PFHelper;
	import fg.pathfinders.PathFinder;
	import puzzle.SceneObj;

	/*
	 * Karakter Utama
	 * 
	 */
	public class KarakterUtama extends SceneObj {
		
		protected var pathFinder:PathFinder;
		protected var pathFinderHelper:PFHelper;
		protected var targetSceneObj:SceneObj;
		
		public function KarakterUtama() {
			
			//inisialisasi PathFinder
			pathFinder = new PathFinder();
			pathFinder.diagonalMove = true;
			
			//panggil fungsi checkCanMoveToPos untuk mengecek tile
			//apakah bisa dilalui atau tidak
			pathFinder.checkCanMoveToPos = checkCanMoveToPos;
			
			//inisialisasi PFHelper
			pathFinderHelper = new PFHelper();
			pathFinderHelper.gridWidth = Main.TILE_SIZE;
			pathFinderHelper.gridHeight = Main.TILE_SIZE;
			pathFinderHelper.stepCount = 4;
			
			//panggil fungsi checkCanMoveToPos untuk mengecek tile
			//apakah bisa dilalui atau tidak
			pathFinderHelper.checkCanMoveToPos = checkCanMoveToPos;
		}
		
		//fungsi ini untuk mengecek apakah karakter
		//benar-benar sampai ke tujuan
		//karakter dianggap sampai ke tempat tujuan apabila
		//posisinya sudah bersebelahan dengan target
		protected function checkSampaiTarget(obj:SceneObj):Boolean {
			if (getTilePosX() == obj.posRef.x) {
				if (getTilePosY() == obj.posRef.y) {
					return true;
				}
			}
			
			return false;
		}
		
		//fungsi ini untuk mengupdate karakter tiap kali ada event ENTER_FRAME
		public function update():void {
			var target:SceneObj;
			
			if (pathFinderHelper.aktif == false) return;
			
			pathFinderHelper.update();
			
			//update posisi
			_view.x = pathFinderHelper.pos.x;
			_view.y = pathFinderHelper.pos.y;
			
			//check apakah pathFinderHelper sudah selesai
			//dan ada target yang dituju
			if (pathFinderHelper.finish && targetSceneObj) {
				
				//check apakah karakter benar-benar sudah sampai 
				//target
				if (checkSampaiTarget(targetSceneObj)) {
					//aktifkan RPG Event
					targetSceneObj.onAction();
					targetSceneObj = null;
				}
			}
		}
		
		//fungsi ini dipanggil oleh pathFinder dan pathFinderHelper
		//untuk mengecek apakah sebuah tile bisa dilewati atau tidak
		protected function checkCanMoveToPos(i:int, j:int):Boolean {
			var sceneObj:SceneObj;
			
			//bila posisi di luar map, maka tidk bisa dilewati
			if (i < 0) return false;
			if (i >= Main.MAP_WIDTH) return false;
			if (j < 0) return false;
			if (j >= Main.MAP_HEIGHT) return false;
			
			//bila posisinya ternyata adalah non-walkable, maka tidak bisa
			//dilewati
			//informasi mengenai non-walkable tile ada di layer block dari file tmx
			if (Main.inst.sceneCr.tmx.getLayerByName("block").getTile(i, j).gid > 0) return false;
			
			
			return true;
		}
		
		public function setPosFromGrid(i:int, j:int):void {
			
		}
		
		//fungsi ini untuk menghandle karakter saat akan jalan
		//ke posisi tetentu di map
		public function jalanKePosisi(ix:int, jx:int):void {
			var ar:Array;
			
			trace('jalan ke posisi ' + ix + '/' + jx);
			
			//bila karakter sedang tidak jalan
			if (pathFinderHelper.aktif == false) {
				
				//cari jalur terpendek
				ar = pathFinder.find(
							getTilePosX(),  
							getTilePosY(), 
							ix, 
							jx
						);
				
				//bila ada jalur
				//mulai jalan
				if (ar.length > 0) {
					pathFinderHelper.start(ar);
				}
			}
			
		}
		
		//fungsi ini menangani saat karakter hendak jalan
		//ke object tertentu.
		public function jalanKeObject(obj:SceneObj):void {
			targetSceneObj = obj;
			
			if (checkSampaiTarget(obj)) {
				obj.onAction();
				targetSceneObj = null;							
				return;
			}
			
			trace('jalan ke object, pos ref ' + obj.posRef + '/pos obj ' + obj.getTilePosX() + '-' + obj.getTilePosY());
			jalanKePosisi(obj.posRef.x, obj.posRef.y);
		}
	}

}