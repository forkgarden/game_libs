package
{
	import fg.bmps.BmpLoader;
	import fg.path_finder_helper.PFHelper;
	import fg.pathfinders.PathFinder;
	import fg.tmxs.Tmx;
	import fg.ui.Teks;
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextFormatAlign;

	[Frame(factoryClass="Preloader")]
	public class MainPFCost extends Sprite 
	{
		
		protected var tmx:Tmx = new Tmx();
		protected var pfHelper:PFHelper;
		protected var pathFinder:PathFinder;
		protected var char:Bitmap;
		protected var bmpLoader:BmpLoader;
		protected var halLoading:Sprite;

		public function MainPFCost() 
		{
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		//fungsi ini adalah fungsi yang pertama kali dipanggil
		protected function init(e:Event = null):void {
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			//buat halaman loading
			halLoadingCreate();
			addChild(halLoading);			
			
			//inisialisasi Tmx untuk meload map
			tmx = new Tmx();
			tmx.url = 'map.tmx';
			
			//saat map selesai di load, panggil fungsi tmxLoaded
			tmx.onLoadCallBack = tmxLoaded;
			tmx.load();
		}
		
		//fungsi ini untuk membuat tampilan loading
		protected function halLoadingCreate():void {
			var teks:Teks = new Teks();
			
			halLoading = new Sprite();
			halLoading.addChild(teks);
			
			teks.text = 'Loading ...';
			teks.width = this.stage.stageWidth;
			teks.format.align = TextFormatAlign.CENTER;
			teks.height = teks.textHeight + 4;
			teks.y = this.stage.stageHeight / 2 - teks.height / 2;
			teks.formatApply();
		}
		
		//fungsi ini dipanggil saat map selesai di load
		protected function tmxLoaded():void {
			//inisialisasi BmpLoader
			//untuk meload gambar tank
			bmpLoader = new BmpLoader();
			bmpLoader.url = 'charf02.png';
			
			//setelah gambar di load, panggil fungsi bmpOnLoaded
			bmpLoader.onLoadCallBack = bmpOnLoaded;
			bmpLoader.load();
		}
		
		//fungsi ini dipanggil saat file gambar selesai di load
		protected function bmpOnLoaded():void {
			
			removeChildren();
			
			//render map
			tmx.render(this);
			
			//load character
			char = bmpLoader.bmp;
			char.x = 32;
			char.y = 32;
			addChild(char);
			
			//inisialisasi path finding helper
			pfHelper = new PFHelper();
			
			//panggil fungsi checkBlock untuk mengecek tile
			//apakah bisa dilalui atau tidak
			pfHelper.checkCanMoveToPos = checkBlock;
			
			//inisialisasi pathfinder
			pathFinder = new PathFinder();
			
			//aktifkan pergerakan diagonal
			pathFinder.diagonalMove = true;
			
			//panggil fungsi checkBlock untuk mengecek tile
			//apakah bisa dilalui atau tidak
			pathFinder.checkCanMoveToPos = checkBlock;
			
			//aktifkan penghitungan cost tile
			//pathFinder akan memanggil fungsi checkCost 
			//untuk menentukan apakah jalan itu dilalui atau tidak
			//berdasarkan harganya
			pathFinder.getCost = checkCost;
			
			pathFinder.maxNodes = 1000;
			
			this.addEventListener(Event.ENTER_FRAME, update, false, 0, true);
			this.stage.addEventListener(MouseEvent.CLICK, mapOnClick, false, 0, true);
		}		
		
		//fungsi ini dipanggil saat map di click
		public function mapOnClick(evt:MouseEvent):void {
			var ar:Array;
			var posX:Number;
			var posY:Number;
			var tileX:Number;
			var tileY:Number;
			
			//check apakah karakter masih jalan
			//bila iya, maka perintah tidak dilaksanakan
			if (pfHelper.aktif == false) {
				
				//hitung posisi tile karakter
				posX = Math.floor((char.x) / 32.0);
				posY = Math.floor((char.y) / 32.0);
				
				//hitung posisi tile yang di klik
				tileX = Math.floor(stage.mouseX / 32.0);
				tileY = Math.floor(stage.mouseY / 32.0);				
				
				//cari jalur terpendek
				//jalur yang dicari adalah jalur terpendek
				//dengan harga terendah
				ar = pathFinder.find(posX, posY, tileX, tileY);
				
				//bila ketemu maka mulai jalan
				if (ar.length > 0) { 
					pfHelper.start(ar);
				}
			}
		}
		
		//hitung harga tiap tile
		protected function checkCost(i:int, j:int):Number {
			
			//bila tilenya ubin, maka harganya diturunin
			//karakter akan memilih ubin daripada rumput
			//dengan perbandingan 1 : 10
			//artinya jalan di ubin 10 tile lebih dipilih dari 1 tile jalan di rumput
			if (tmx.getLayerByName('jalan').getTile(i, j).gid > 0) {
				return 1;
			}
			
			return 10;
		}
		
		//fungsi ini dipanggil oleh pathfinder dan PFHelper
		//untuk mengecek apaka tile bisa dilewati atau tidak
		protected function checkBlock(i:int, j:int):Boolean {
			
			//bila posisi di luar map maka tidak boleh dilewati
			if (i < 0) return false;
			if (i >= tmx.width) return false;
			if (j < 0) return false;
			if (j >= tmx.height) return false;
			
			//selain itu boleh dilewati
			return true;
		}
		
		//fungsi ini dipanggil saat event ENTER_FRAME
		protected function update(e:Event):void {
			if (pfHelper.aktif) {
				pfHelper.update();
				char.x = pfHelper.pos.x;
				char.y = pfHelper.pos.y;
			}
		}
		
	}

}