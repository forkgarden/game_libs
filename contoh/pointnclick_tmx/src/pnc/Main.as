package pnc {
	
	import fg.path_finder_helper.PFHelper;
	import fg.pathfinders.PathFinder;
	import fg.tmxs.Tmx;
	import fg.ui.Teks;
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextFormatAlign;
	
	[Frame(factoryClass="Preloader")]
	public class Main extends Sprite {
		
		
		[Embed(source = "../../bin/char.png")]
		protected var KarakterImg:Class;
		
		protected var tmx:Tmx = new Tmx();
		protected var pfHelper:PFHelper;
		protected var pathFinder:PathFinder;
		protected var char:Bitmap;
		protected var halLoading:Sprite;
		
		public function Main() {
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		//fungsi ini dipanggil saat map di click
		public function mapOnClick(evt:MouseEvent):void {
			var ar:Array;
			var posX:Number;
			var posY:Number;
			var tileX:Number;
			var tileY:Number;
			
			//check apakah PathFinder Helper sedang aktif
			//bila tidak maka karakter boleh jalan
			if (pfHelper.aktif == false) {
				
				//hitung posisi tile karakter
				posX = Math.floor((char.x) / 32.0);
				posY = Math.floor((char.y) / 32.0);
				
				//hitung posisi tile yang di klik
				tileX = Math.floor(stage.mouseX / 32.0);
				tileY = Math.floor(stage.mouseY / 32.0);
				
				ar = pathFinder.find(posX, posY, tileX, tileY);
				
				//bila path ketemu
				if (ar.length > 0) { 
					pfHelper.start(ar);
				}
				else {
					trace('path finder failed ' + ar.length);
				}
			}
			else {
				trace('pfhelper aktif ' + pfHelper.aktif);
			}
		}
		
		//fungsi ini untuk membuat tampilan loading
		protected function halLoadingCreate():void {
			var teks:Teks = new Teks();
			
			halLoading = new Sprite();
			halLoading.addChild(teks);
			
			teks.text = 'Loading ...';
			teks.width = this.stage.stageWidth;
			teks.format.align = TextFormatAlign.CENTER;
			teks.height = teks.textHeight + 4;
			teks.y = this.stage.stageHeight / 2 - teks.height / 2;
			teks.formatApply();
		}
		
		protected function init(e:Event = null):void {
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			this.stage.scaleMode = StageScaleMode.SHOW_ALL;
			
			addEventListener(MouseEvent.CLICK, mapOnClick, false, 0, true);
			
			//buat halaman loading
			halLoadingCreate();
			addChild(halLoading);
			
			//insialisasi TMX untuk meload map
			tmx = new Tmx();
			tmx.url = 'map.tmx';
			
			//setelah map di load panggil fungsi tmxLoaded
			tmx.onLoadCallBack = tmxLoaded;
			tmx.load();
		}
		
		//fungsi ini dipanggil saat map selesai di load
		protected function tmxLoaded():void {
			//bersihkan layar
			this.removeChildren();
			
			//render tmx;
			tmx.render(this);
			
			//inisialisasi karakter
			char = new KarakterImg();
			char.x = 32;
			char.y = 32;
			addChild(char);
			
			//inisialisasi Path Finding Helper
			pfHelper = new PFHelper();
			pfHelper.checkCanMoveToPos = checkBlock;
			
			//inisialisasi Path Finding
			pathFinder = new PathFinder();
			
			//aktifkan pergerakan diagonal
			pathFinder.diagonalMove = true;
			pathFinder.checkCanMoveToPos = checkBlock;
			pathFinder.maxNodes = 1000;
			
			this.addEventListener(Event.ENTER_FRAME, update, false, 0, true);	
		}
		
		//check apakah tile bisa dilewati
		protected function checkBlock(i:int, j:int):Boolean {
			//tile di luar layar tidak bisa dilewati
			if (i <= 0) return false;
			if (i >= tmx.width) return false;
			if (j <= 0) return false;
			if (j >= tmx.height) return false;
			
			//tile yang berada di layer object
			//tidak bisa dilewati
			if (tmx.getLayerByName('obj').getTile(i, j).gid > 0) return false;
			
			return true;
		}
		
		protected function update(e:Event):void {
			
			//bila PathFinderHelper aktif, jalankan karakter
			if (pfHelper.aktif) {
				pfHelper.update();
				char.x = pfHelper.pos.x;
				char.y = pfHelper.pos.y;
			}
		}
		
	}
}