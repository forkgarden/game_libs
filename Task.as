package fg {
	import flash.display.Sprite;
	import flash.events.Event;
	
	public class Task extends Sprite {
		
		private static const WAIT:int = 1;
		private static const RUN:int = 2;
		private static const FINISH:int = 3;
		
		private var _state:int = 0;
		private var ctr:int = 0;
		private var ctrWait:int = 0;
		private var _command:Function;
		private var _valueInt:int = 0;
		
		private static var tasks:Vector.<Task> = new Vector.<Task>();
		
		public function Task() {
		}
		
		public static function create():Task {
			var task:Task = new Task();
			tasks.push(task);
			
			return task;
		}
		
		private static function taskClear():void {
			var task:Task;
			var i:int;
			
			for (i = 0; i < tasks.length; i++) {
				task = tasks[i];
				if (task.state == FINISH) {
					tasks.splice(i, 1);
				}
			}
		}
		
		public function start(wait:int, cmd:Function): void {
			ctr = 0;
			ctrWait = wait;
			_state = WAIT;
			_valueInt = valueInt;
			
			_command = cmd;
			addEventListener(Event.ENTER_FRAME, update);
			//trace('task started ' + _command);
		}
		
		public function stop():void {
			_state = 0;
		}
		
		public function update(e:Event):void {
			var tempCmd:Function;
			
			if (_state == WAIT) {
				ctr++;
				if (ctr >= ctrWait) {
					if (_command != null) {
						tempCmd = _command;
						_command = null;
						removeEventListener(Event.ENTER_FRAME, update);
						tempCmd();
					}
					_state = FINISH;
					taskClear();
					
				}
			}
		}
		
		public function get command():Function {
			return _command;
		}
		
		public function set command(value:Function):void {
			_command = value;
		}
		
		public function get valueInt():int {
			return _valueInt;
		}
		
		public function set valueInt(value:int):void {
			_valueInt = value;
		}
		
		public function get state():int {
			return _state;
		}
		
	}

}