package{
	import fg.scrollRect.ScrollerRect;
	import flash.display.Sprite;
	import flash.events.Event;
	import fg.debugs.Debugger;
	
	/**
	 * ...
	 * @author 
	 */
	public class MainScroller extends Sprite {
		
		protected var spr2:Sprite;
		protected var spr:Sprite;
		protected var scroll:ScrollerRect;
		
		public function MainScroller() {
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event = null):void {
			removeEventListener(Event.ADDED_TO_STAGE, init);
			// entry point
			
			spr = new Sprite();
			spr.x = 300;
			addChild(spr);
			
			spr2 = new Sprite();
			spr2.graphics.beginFill(0x333333);
			spr2.graphics.drawRect(0, 0, 100, 500);
			spr2.graphics.endFill();
			spr2.graphics.lineStyle(3);
			spr2.graphics.moveTo(0, 0);
			spr2.graphics.lineTo(100, 500);
			spr2.x = 0;
			spr.addChild(spr2);
			
			addChild(Debugger.getInst());
			Debugger.getInst().addLine('test');
			
			scroll = new ScrollerRect();
			scroll.view = spr;
			scroll.maskRect.width = 100;
			scroll.maskRect.height = 200;
			scroll.lockX = true;
			scroll.setup();
			
		}
		
	}
	
}