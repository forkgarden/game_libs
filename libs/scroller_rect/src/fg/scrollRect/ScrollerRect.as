﻿package fg.scrollRect {
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import fg.debugs.Debugger;
	
	public class ScrollerRect extends Sprite {
		
		protected static var _mouseIsMoving:Boolean = false;
		
		protected var mouseDown:Boolean = false;
		protected var mouseLastPos:Point = new Point();
		protected var mouseSpeed:Point = new Point();
		protected var point:Point = new Point();
		
		protected var _view:Sprite;
		protected var _lockX:Boolean = false;
		protected var _lockY:Boolean = false;
		protected var viewLastPos:Point = new Point();
		//protected var viewLastFramePos:Point = new Point();
		protected var _guide:MovieClip;
		protected var _bound:Rectangle = new Rectangle();
		protected var _lewatBatasAtas:Boolean = false;
		protected var _lewatBatasBawah:Boolean = false;
		protected var _bounce:Boolean = true;
		protected var _flDebug:Boolean = true;
		protected var _flMouseChildren:Boolean = true;
		protected var _maskRect:Rectangle = new Rectangle();
		protected var _viewHeight:Number = 0;
		protected var rect2:Rectangle = new Rectangle(0, 0, 1000000, 1000000);
		protected var posRectLast:Point = new Point();
		protected var speedLast:Number = 0;
		
		protected var graphicTouch:Sprite;
		protected var _viewSpr:Sprite;
		
		public function ScrollerRect() {
			
		}
		
		public function setup():void {
			_view.addEventListener(MouseEvent.MOUSE_DOWN, viewMouseDown, false, 0, true);
			_view.mouseChildren = _flMouseChildren;
			
			_lewatBatasAtas = false;
			_lewatBatasBawah = false;
			_mouseIsMoving = false;
			
			if (_view.stage) {
				stageSetup();
			}
			else {
				_view.addEventListener(Event.ADDED_TO_STAGE, viewOnAddedToStage, false, 0, true);
			}
			
			mouseSpeed.x = 0;
			mouseSpeed.y = 0;
			
			//viewLastFramePos.y = view.y;
			//viewLastFramePos.x = view.x;
			
			mouseDown = false;
			
			trace(_view.height);
			
			_view.scrollRect = _maskRect;
			
			trace(viewGetHeight());
			
			addEventListener(Event.ENTER_FRAME, update, false, 0, true);
		}
		
		public function getPercentage():Number {
			var max:Number;
			var cr:Number;
			var res:Number;
			
			max = viewGetHeight() - _maskRect.height;
			res = _maskRect.y / max;
			
			//res = 1 - (cr / max);
			//if (res < 0) return 0;
			//if (res > 1) return 1;
			
			return res;
		}
		
		protected function viewOnAddedToStage(e:Event):void {
			_view.removeEventListener(Event.ADDED_TO_STAGE, viewOnAddedToStage);
			stageSetup();
		}
		
		protected function stageSetup():void {
			_view.stage.addEventListener(MouseEvent.MOUSE_UP, mouseUp, false, 0, true);
			_view.stage.addEventListener(MouseEvent.MOUSE_MOVE, mouseMove, false, 0, true);
		}
		
		protected function viewMouseDown(e:MouseEvent):void {
			if (_guide) _guide.visible = false;
			
			mouseDown = true;
			_mouseIsMoving = false;
			
			mouseLastPos.x = e.stageX;
			mouseLastPos.y = e.stageY;
			
			viewLastPos.y = _maskRect.y;
			viewLastPos.x = _maskRect.x;
			
			/*
			_bound.x = _maskRect.x;
			_bound.y = _maskRect.y;
			_bound.width = _maskRect.width;
			_bound.height = _maskRect.height;			
			*/
		}
		
		protected function mouseMove(e:MouseEvent):void {
			var deltaY:Number;
			var deltaX:Number;
			
			if (mouseDown) {
				
				deltaX = e.stageX - mouseLastPos.x;
				deltaY = e.stageY - mouseLastPos.y;
				
				if (Math.max(Math.abs(deltaX), Math.abs(deltaY)) > 16) {
					_mouseIsMoving = true;
				}
				
				if (_lockX == false) {
					//_view.x = viewLastPos.x + deltaX;
					_maskRect.x = viewLastPos.x + deltaX;
				}
				
				if (_lockY == false) {
					//_view.y = viewLastPos.y + deltaY;
					_maskRect.y = viewLastPos.y - deltaY;
				}
				
				_view.scrollRect = _maskRect;
				
			}
			
		}
		
		public function destroy():void {
			removeEventListener(Event.ENTER_FRAME, update);
		}
		
		protected function mouseUp(e:MouseEvent):void {
			mouseDown = false;
		}
		
		protected function viewGetWidth():Number {
			var i:int;
			var width:Number = 0;
			var disp:DisplayObject;
			
			for (i = 0; i < view.numChildren; i++) {
				disp = view.getChildAt(i);
				if (disp.visible) {
					if (disp.x + disp.width > width) {
						width = disp.x + disp.width;
					}
				}
			}
			return width;
		}
		
		protected function viewGetHeight():Number {
			var i:int;
			var hRes:Number = 0;
			var disp:DisplayObject;
			
			//view.scrollRect = null;
			//hRes = view.height;
			
			for (i = 0; i < view.numChildren; i++) {
				disp = view.getChildAt(i);
				if (disp.y + disp.height > hRes) {
					hRes = disp.y + disp.height;
				}
			}
			
			//view.scrollRect = _maskRect;
			
			return hRes;
		}
		
		protected function updateBatasKiri():void {
			var gapX:Number;
			
			if (_lockX) return;
			
			//kanan
			gapX = 0 - _maskRect.x;
			
			//kanan
			if (gapX > 0) {
				gapX *= .5;
				if (Math.abs(gapX) < 1.0) {
					gapX = 0;
				}
				
				_maskRect.x -= gapX;
				//viewLastFramePos.x = _maskRect.x;
			}
			
		}
		
		protected function updateBatasBawah():void {
			var gapY:Number;
			
			if (_maskRect.height < viewGetHeight()) {
				if (_maskRect.y + _maskRect.height > viewGetHeight()) {
					gapY = _maskRect.y + _maskRect.height - viewGetHeight();
					
					if (gapY > 0) {
						if (Math.abs(gapY) < 1.0) {
							_maskRect.y = viewGetHeight() - _maskRect.height;
							gapY = 0;
							_view.scrollRect = _maskRect;
						}
						else {
							gapY *= .5;
							_maskRect.y -= gapY;
							_view.scrollRect = _maskRect;
						}
					}
					
				}
			}
			
			return;
		}
		
		protected function updateBatasKanan():void {
			var gapX:Number;
			
			if (_lockX) return;
			
			if (viewGetWidth() < _bound.width) {
				gapX = view.x - _bound.x;
				if (gapX > 0) {
					if (Math.abs(gapX) < 1.0) {
						view.x = _bound.x;
					}
					else {
						gapX *= .5;
						view.x -= gapX;
					}
				}
				if (gapX <= 0) {
					if (Math.abs(gapX) < 1.0) {
						view.x = _bound.x;
					}
					else {
						gapX *= .5;
						view.x -= gapX;
					}
				}
			}
			else {
				gapX = (view.x + viewGetWidth()) - (_bound.x + _bound.width);
				if (gapX < 0) {
					gapX *= .5;
					if (Math.abs(gapX) < 1) {
						gapX = 0;
					}
					
					view.x -= gapX;
					//viewLastFramePos.x = view.x;
				}
			}
		}
		
		protected function updateBatasAtas():void {
			var gapY:Number;
			
			gapY = 0 - _maskRect.y;
			
			if (gapY > 0) {
				if (gapY > 1) {
					_lewatBatasAtas = true;
				}
				
				gapY *= .5;
				if (Math.abs(gapY) < 1) {
					gapY = 0;
					_maskRect.y = 0;
				}
				
				_maskRect.y += gapY;
				_view.scrollRect = _maskRect;
				
				//viewLastFramePos.y = _view.y;
			}
			
		}
		
		protected function debug():void {
			if (_flDebug == false) return;
			
			Debugger.getInst().addLine("", true);
			//Debugger.getInst().addLine("view y " + _view.y, false);
			//Debugger.getInst().addLine("view x " + _view.x, false);
			Debugger.getInst().addLine("_mask x y " + _maskRect.x + "/" + _maskRect.y, false);
			//Debugger.getInst().addLine("_mask dim " + _maskRect.width + "/" + _maskRect.height, false);
			//Debugger.getInst().addLine("_bound x y " + _bound.x + '/' + _bound.y, false);
			//Debugger.getInst().addLine("_view height/get height " + view.height + ' - ' + viewGetHeight(), false);
			//Debugger.getInst().addLine("_lewat atas " + _lewatBatasAtas);
			//Debugger.getInst().addLine("_lewat bawah " + (_maskRect.y + _maskRect.height > viewGetHeight()));
			Debugger.getInst().addLine("_mouse speed " + Math.floor(mouseSpeed.x) + "/" + Math.floor(mouseSpeed.y));
			//Debugger.getInst().addLine("mouse is moving " + _mouseIsMoving);
			//Debugger.getInst().addLine("mouseDown " + mouseDown);
			Debugger.getInst().addLine("perc " + getPercentage());
		}
		
		public function update(e:Event):void { 
			var gapY:Number = 0;
			var gapX:Number = 0;
			var speed:Number;
			
			debug();

			if (mouseDown == false) {
				
				//update speed
				if (Math.abs(mouseSpeed.x) > 0) {
					mouseSpeed.x *= .5;
					if (Math.abs(mouseSpeed.x) < 1.0) mouseSpeed.x = 0;
					_maskRect.x += mouseSpeed.x;
					_view.scrollRect = _maskRect;
				}
				
				if (Math.abs(mouseSpeed.y) > 0) {
					mouseSpeed.y *= .5;
					if (Math.abs(mouseSpeed.y) < 1.0) mouseSpeed.y = 0;
					_maskRect.y += mouseSpeed.y;
					_view.scrollRect = _maskRect;
				}
				
				//atas
				updateBatasAtas();
				updateBatasKiri();
				updateBatasBawah();
				updateBatasKanan();
			}
			
			mouseSpeed.y = (mouseSpeed.y + (_maskRect.y - posRectLast.y)) * .75;
			posRectLast.y = _maskRect.y;
			
			mouseSpeed.x = (mouseSpeed.x + (_maskRect.x - posRectLast.x)) * .75;
			posRectLast.x = _maskRect.x;
			
			//debug();
		}
		
		public function get view():Sprite {
			return _view;
		}
		
		public function set view(value:Sprite):void {
			if (value == null) throw new Error();
			_view = value;
			_viewHeight = view.height;
		}
		
		public function get lockX():Boolean {
			return _lockX;
		}
		
		public function set lockX(value:Boolean):void {
			_lockX = value;
		}
		
		public function get lockY():Boolean {
			return _lockY;
		}
		
		public function set lockY(value:Boolean):void {
			_lockY = value;
		}
		
		public function get guide():MovieClip 
		{
			return _guide;
		}
		
		public function set guide(value:MovieClip):void 
		{
			_guide = value;
		}
		
		public function get bound():Rectangle 
		{
			return _bound;
		}
		
		public function set bound(value:Rectangle):void 
		{
			_bound = value;
		}
		
		public static function get mouseIsMoving():Boolean 
		{
			return _mouseIsMoving;
		}
		
		public static function set mouseIsMoving(value:Boolean):void 
		{
			_mouseIsMoving = value;
		}
		
		public function get lewatBatasAtas():Boolean {
			return _lewatBatasAtas;
		}
		
		public function get bounce():Boolean {
			return _bounce;
		}
		
		public function set bounce(value:Boolean):void {
			_bounce = value;
		}
		
		public function get lewatBatasBawah():Boolean {
			return _lewatBatasBawah;
		}
		
		public function get flDebug():Boolean {
			return _flDebug;
		}
		
		public function set flDebug(value:Boolean):void {
			_flDebug = value;
		}
		
		public function get flMouseChildren():Boolean {
			return _flMouseChildren;
		}
		
		public function set flMouseChildren(value:Boolean):void {
			_flMouseChildren = value;
		}
		
		public function get maskRect():Rectangle {
			return _maskRect;
		}
		
	}
}