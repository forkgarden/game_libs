package fg.geoms{
	
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.display.Sprite;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	
	public class Geom {
				
		public static const ANGLE2RAD:Number = Math.PI / 180.0;
		public static const RAD2ANGLE:Number = 180.0 / Math.PI;
		
		private var _sin:Array = new Array();
		private var _cos:Array = new Array();
		
		public function Geom() {
			var i:int;
			
			for (i = 0; i < 360; i++ ) {
				_sin.push(Math.sin(i * ANGLE2RAD));
				_cos.push(Math.cos(i * ANGLE2RAD));
			}			
		}
		
		public function rotateRel(rotation:Number, posX:Number, posY:Number, res:Point):void {
			var rot:Number;
			
			rot = rotation;
			rot = rotToAngle(rot);
			rot = rot * ANGLE2RAD;
			
			res.x = posX * Math.cos(rot) - posY * Math.sin(rot);
			res.y = posX * Math.sin(rot) + posY * Math.cos(rot);
		}
		
		public function getGlobalCoordinate(spr:DisplayObjectContainer, posX:Number, posY:Number, targetSprite:DisplayObjectContainer, res:Point):void {
			var rot:Number = 0;
			var resTemp:Point = new Point();
			var sprite:Sprite;
			var rect:Rectangle;
			
			//trace('get global coord: ' + spr + '/' + targetSprite);
			if (spr == null) return;
			
			if (targetSprite == spr) {
				res.x = posX;
				res.y = posY;
				
			} else if (spr.parent && (spr.parent is DisplayObjectContainer)) {
				res.x = posX * spr.scaleX;
				res.y = posY * spr.scaleY;
				
				resTemp.x = res.x;
				resTemp.y = res.y;
				
				rot = spr.rotation * RAD2ANGLE;
				rot = rotToAngle(rot);
				rot = rot * ANGLE2RAD;
				
				res.x = res.x * Math.cos(rot) - res.y * Math.sin(rot);
				res.y = resTemp.x * Math.sin(rot) + res.y * Math.cos(rot);
				
				res.x += spr.x;
				res.y += spr.y;
				
				try {
					if (spr["clipRect"] != null) {
						rect = spr["clipRect"];
						if (rect) {
							res.x -= rect.x;
							res.y -= rect.y;
							trace('rect available: ' + rect);
						}
					}
				} catch (e:Error) {
					
				}
				
				getGlobalCoordinate(spr.parent, res.x, res.y, targetSprite, res);
			} else {
				throw new Error();
			}
		}
		
		public function getAngle(dy:Number, dx:Number):Number { 
			var angle:Number;
			
			return Math.atan2(dy, dx) * RAD2ANGLE;
		}
		
		public function sudutInvers(sudut:Number):Number {
		
			if (sudut > 180) {
				sudut = -180 + (sudut - 180);
			}
			
			if (sudut < -180) {
				sudut = 180 - (sudut+ 180);
			}
			
			if (sudut > 0 && sudut <= 180) {
				return 180 - sudut;
			}
			else if (sudut <= 0 && sudut > -180) {
				return -180 - sudut;
			}
			else {
				throw new Error();
			}
		}
		
		public function interpolateAngle(ta:Number, a:Number, minAngle:Number, maxAngle:Number):Number { 
			var res:Number = 0;
			
			ta = rotToAngle(ta);
			a = rotToAngle(a);
			
			if (ta > a) {
				if (Math.abs(ta - a) > 180) {
					res = -(a + 360 - ta);
				} else {
					res = ta - a;
				}
			} 
			else if (ta < a) {
				if (Math.abs(a - ta) > 180) {
					res = (ta + 360 - a)
				} 
				else {
					res = ta - a;
				}
			}
			else {
				return 0;
			}
			
			//clam res
			if (Math.abs(res) <= minAngle) return 0;
			if (Math.abs(res) >= maxAngle) {
				if (res > 0) return maxAngle;
				if (res < 0) return -maxAngle;
			}
			
			return res;
		}
		
		public function getJarak(ax:Number, ay:Number, bx:Number, by:Number):Number {
			return Math.sqrt(Math.pow(bx - ax, 2) + Math.pow(by - ay, 2));
		}
		
		public function rotToAngle(n:Number):Number {
			n = n % 360;
			
			if (n < 0) {
				return 360 + n;
			}
			
			return n;
		}
		
	}

}