package fg.debugs {
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.filters.DropShadowFilter;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	public class Debugger extends Sprite {
		
		public static var inst:Debugger;
		
		private var _watch:TextField;
		private var _str:String = '';
		private var _mouseLine:Boolean = true;
		private var mouseSpr:Sprite = new Sprite();
		private var mouseText:TextField;
		
		public function Debugger() {
			inst = this;
			watchSetup();
			
			this.mouseEnabled = false;
			this.doubleClickEnabled = false;
			this.tabEnabled = false;
			
			mouseText = new TextField();
			mouseText.mouseEnabled = false;
			mouseText.tabEnabled = false;
			mouseText.doubleClickEnabled = false;
			mouseText.mouseWheelEnabled = false;
			mouseSpr.addChild(mouseText);
			
			addChild(mouseSpr);
			
			this.mouseEnabled = false;
			this.mouseChildren = false;
			
			addEventListener(Event.ENTER_FRAME, update, false, 0, true);
		}
		
		public static function getInst():Debugger {
			if (inst) return inst;
			
			inst = new Debugger();
			return inst;
		}
		
		private function update(e:Event): void {
			if (parent) parent.setChildIndex(this, parent.numChildren - 1);
		}
		
		public function addLine(str:String, clear:Boolean = false):void {
			if (clear) _str = '';
			_str += str + '\n';
			
			watchWrite();
		}
		
		public function watchWrite(str:String = null):void {
			if (str == null) {
				_watch.text = this._str;
			} else {
				_watch.text = str;
			}
			
			_watch.scrollV = _watch.numLines;
			
			if (this.parent) this.parent.setChildIndex(this, this.parent.numChildren - 1);
		}
		
		private function watchSetup():void {
			var format:TextFormat;
			
			_watch = new TextField();
			_watch.doubleClickEnabled = false;
			_watch.mouseEnabled = false;
			_watch.mouseWheelEnabled = false;
			_watch.tabEnabled = false;
			_watch.selectable = false;
			_watch.width = 800;
			_watch.height = 800;
			_watch.textColor = 0x0; // 0xffffff;
			_watch.textColor = 0xffffff;
			_watch.border = false;
			_watch.filters = [new DropShadowFilter(1,45,0,1,0,0,1)];
			
			format = _watch.getTextFormat();
			format.font = "Arial";
			format.size = 18;
			_watch.setTextFormat(format);
			_watch.defaultTextFormat = format;
			addChild(_watch);
		}
		
		public function get mouseLine():Boolean {
			return _mouseLine;
		}
		
		public function set mouseLine(value:Boolean):void {
			_mouseLine = value;
		}
	}
	
}