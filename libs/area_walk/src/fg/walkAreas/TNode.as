package fg.walkAreas   {

	public final class TNode {
		
		protected var _parentIdx:int = 0;
		protected var _parent:TNode;
		protected var _x:int = 0;
		protected var _y:int = 0;
		protected var _g:int = 0;
		protected var _idx:int = 0;
		protected var _open:Boolean = true;
		protected var free:Boolean = false;
		
		protected static var lists:Vector.<TNode> = new Vector.<TNode>();
		
		public function TNode() {
			
		}
		
		public static function release(nodeP:TNode):void {
			var node:TNode;
			
			for each (node in lists) {
				if (node == nodeP) {
					node.free = true;
					return;
				}
			}
			
			throw new Error('nodeP not found' + nodeP.idx);
		}
		
		public static function getTNode():TNode {
			var node:TNode;
			
			for each (node in lists) {
				if (node.free) {
					node.reset();
					throw new Error();
					return node;
				}
			}
			
			node = new TNode();
			lists.push(node);
			return node;
		}
		
		public function reset():void {
			_parentIdx = 0;
			_parent = null;
			_open = true;
			free = false;
		}
		
		public function destroy():void {
			_parent = null;
		}
		
		public function toStringRef():String {
			return "[" + _x + "-" + _y + "]";
		}
		
		public function get parentIdx():int 
		{
			return _parentIdx;
		}
		
		public function set parentIdx(value:int):void 
		{
			_parentIdx = value;
		}
		
		public function get x():int 
		{
			return _x;
		}
		
		public function set x(value:int):void 
		{
			_x = value;
		}
		
		public function get y():int 
		{
			return _y;
		}
		
		public function set y(value:int):void 
		{
			_y = value;
		}
		
		public function get idx():int 
		{
			return _idx;
		}
		
		public function set idx(value:int):void 
		{
			_idx = value;
		}
		
		public function get open():Boolean 
		{
			return _open;
		}
		
		public function set open(value:Boolean):void 
		{
			_open = value;
		}
		
		public function get g():int {
			return _g;
		}
		
		public function set g(value:int):void {
			_g = value;
		}
		
		public function get parent():TNode {
			return _parent;
		}
		
		public function set parent(value:TNode):void {
			_parent = value;
		}
		
	}

}