package fg.walkAreas   {
	
	import fg.walkAreas.TNode;
	
	public final class WalkArea {
		
		protected var _maxCost:Number = 0;
		
		protected var _openNodes:Vector.<TNode>;
		protected var _checkCanMoveToPos:Function;
		protected var _maxNodes:int = 100;
		protected var _diagonalMove:Boolean = false;
		protected var _getCost:Function;
		
		public function WalkArea() {
			_openNodes = new Vector.<TNode>();
		}
		
		//TODO: Pooling
		protected function nodeCreate(parent:TNode, i:int, j:int, nodes:Vector.<TNode>):void {
			var node:TNode;
			
			node = TNode.getTNode();
			node.x = i;
			node.y = j;
			node.open = true;
			node.idx = nodes.length;
			
			if (parent) {
				node.parent = parent;
				node.parentIdx = parent.idx
				node.g = parent.g + 1;
			}
			else {
				node.parent = null;
				node.parentIdx = -1;
				node.g = 0;
			}
			
			if (_getCost != null) node.g += _getCost();
			nodes.push(node);
			_openNodes.push(node);
			node = null;
		}
		
		protected function resToArray(res:Vector.<TNode>):Array {
			var ar:Array = [];
			var node:TNode;
			
			for each (node in res) {
				ar.push([node.x, node.y]);
			}
			
			return ar;
		}
		
		public function search(sx:int, sy:int):Array {
			var res:Vector.<TNode> = new Vector.<TNode>();
			
			findArea(sx, sy, res);
			
			trace('finish ' + res);
			
			return resToArray(res);
		}
		
		protected function findArea(sourceX:int, sourceY:int, res:Vector.<TNode>):void { 
			var node:TNode;
			var i:int;
			var maxLen:int;
			var nodeCr:TNode;
			var len:int;
			
			while (res.length > 0) {
				node = res.pop();
				TNode.release(node);
				node = null;
			}
			
			while (_openNodes.length > 0) {
				_openNodes.pop();
			}
			
			//create the first node
			nodeCreate(null, sourceX, sourceY, res);
			
			//cari node terdekat
			while (true) {
				maxLen = _maxCost;
				nodeCr = null;
				len = _openNodes.length - 1;
				
				//cari open nodes
				//dengan jarak terdekat ke sumber
				//sekaligus hapus yang sudah tidak dipakai
				
				//trace('cari open node yang ada, length ' + _openNodes.length);
				for (i = len; i >= 0; i--) {
					node = _openNodes[i];
					if (node.open) {
						if (node.g < maxLen) {
							nodeCr = node;
							maxLen = node.g;
						}
					} else {
						//trace('gagal, open ' + node.open + '/g ' + node.g + '/maxLen ' + maxLen);
						_openNodes.splice(i, 1);
					}
				}
				
				if (nodeCr) {
					nodeCr.open = false;
					
					//up
					if (nodePosPossible(nodeCr.x, nodeCr.y - 1, res)) {
						nodeCreate(nodeCr, nodeCr.x, nodeCr.y - 1, res);
					}
					
					//right
					if (nodePosPossible(nodeCr.x + 1, nodeCr.y, res)) {
						nodeCreate(nodeCr, nodeCr.x + 1, nodeCr.y, res);
					}
					
					//down
					if (nodePosPossible(nodeCr.x, nodeCr.y + 1, res)) { 
						nodeCreate(nodeCr, nodeCr.x, nodeCr.y + 1, res);
					}
					
					//left
					if (nodePosPossible(nodeCr.x - 1, nodeCr.y, res)) {
						nodeCreate(nodeCr, nodeCr.x - 1, nodeCr.y, res);
					}
					
					//TODO: diagonal
					if (_diagonalMove) {
						//atas kiri
						if (nodePosPossible(nodeCr.x - 1, nodeCr.y - 1, res)) {
							nodeCreate(nodeCr, nodeCr.x - 1, nodeCr.y - 1, res);
						}
						
						//atas kanan
						if (nodePosPossible(nodeCr.x + 1, nodeCr.y - 1, res)) {
							nodeCreate(nodeCr, nodeCr.x + 1, nodeCr.y - 1, res);
						}
						
						//bawah kanan
						if (nodePosPossible(nodeCr.x + 1, nodeCr.y + 1, res)) {
							nodeCreate(nodeCr, nodeCr.x + 1, nodeCr.y + 1, res);
						}
						
						//bawah kiri
						if (nodePosPossible(nodeCr.x - 1, nodeCr.y + 1, res)) {
							nodeCreate(nodeCr, nodeCr.x - 1, nodeCr.y + 1, res);
						}
					}
				}
				else {
					trace('no more nodes can be expanded');
					return;
				}
			}
			
			trace('path find failed no reason');
			return;
		}
		
		protected function nodePosPossible(ix:int, jx:int, nodes:Vector.<TNode>):Boolean {
			var node:TNode;
			var block:Boolean;
			
			//check duplicate dengan node yang sudah ada
			for each (node in nodes) {
				if (node.x == ix && node.y == jx) {
					return false;
				}
			}
			
			//tidak ter block
			if (_checkCanMoveToPos(ix, jx) == false) return false;
			
			return true;
		}
		
		public function get checkCanMoveToPos():Function {
			return _checkCanMoveToPos;
		}
		
		public function set checkCanMoveToPos(value:Function):void {
			_checkCanMoveToPos = value;
		}
		
		public function get maxNodes():int {
			return _maxNodes;
		}
		
		public function set maxNodes(value:int):void {
			_maxNodes = value;
		}
		
		public function get diagonalMove():Boolean 
		{
			return _diagonalMove;
		}
		
		public function set diagonalMove(value:Boolean):void 
		{
			_diagonalMove = value;
		}
		
		public function get getCost():Function 
		{
			return _getCost;
		}
		
		public function set getCost(value:Function):void 
		{
			_getCost = value;
		}
		
		public function get maxCost():Number 
		{
			return _maxCost;
		}
		
		public function set maxCost(value:Number):void 
		{
			_maxCost = value;
		}
		
	}

}