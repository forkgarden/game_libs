package
{
	import fg.walkAreas.WalkArea;
	import flash.display.Sprite;
	import flash.events.Event;
	
	public class MainAreaWalk extends Sprite 
	{
		protected var walkArea:WalkArea = new WalkArea();
		
		public function MainAreaWalk() 
		{
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event = null):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			// entry point
			var res:Array;
			var obj:Array;
			var spr:Sprite;
			
			walkArea.maxCost = 5;
			walkArea.checkCanMoveToPos = walkAreaCheck;
			res = walkArea.search(7, 7);
			trace(JSON.stringify(res));
			
			for each (obj in res) {
				trace(JSON.stringify(obj));
				
				spr = new Sprite();
				spr.graphics.beginFill(0x0000ff, .5);
				spr.graphics.drawRect(0, 0, 25, 25);
				spr.graphics.endFill();
				spr.x = obj[0] * 25;
				spr.y = obj[1] * 25;
				addChild(spr);
			}
		}
		
		protected function walkAreaCheck(i:int, j:int):Boolean {
			if (i < 0) return false;
			if (j < 0) return false;
			if (i > 30) return false;
			if (j > 30) return false;
			
			if (i == 5 && j == 5) return false;
			
			return true;
		}
		
	}
	
}