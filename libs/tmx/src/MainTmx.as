package{
	import adobe.utils.XMLUI;
	import fg.tmxs.Tmx;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.events.Event;
	
	public class MainTmx extends Sprite {
		
		private var tmx:Tmx;
		
		public function MainTmx() {
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event = null):void {
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			// entry point
			tmx = new Tmx();
			tmx.url = 'test2.tmx';
			tmx.onLoadCallBack = tmxOnLoaded;
			tmx.load();
		}
		
		protected function tmxOnLoaded():void {
			trace('complete');
			
			tmx.render(this.stage);
			
			/*
			trace(tmx.getLayerByName('layer1').getTile(0, 0).bitmapData);
			trace(tmx.getLayerByName('layer1').objs);
			trace(tmx.getLayerByName('object_layer_1').getObjById('1').properties[0].name);
			trace(tmx.layers[0].getTile(0,0).bitmapData);
			*/
			
		}
		
	}
	
}