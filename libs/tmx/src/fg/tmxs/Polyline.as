package fg.tmxs {
	import flash.geom.Point;
	/**
	 * ...
	 * @author 
	 */
	public class Polyline {
		
		protected var _point:Point = new Point();
		
		public function Polyline() {
			
		}
		
		public function get point():Point {
			return _point;
		}
		
		public function set point(value:Point):void {
			_point = value;
		}
		
	}

}