package fg.tmxs {
	import flash.display.BitmapData;
	/**
	 * ...
	 * @author 
	 */
	internal class TileMap {
		protected var _gid:Number = 0;
		protected var _tmx:Tmx;
		protected var _tileSet:TileSet;
		protected var _bitmapData:BitmapData;
		protected var destroyed:Boolean = false;
		
		public function TileMap() {
			
		}
		
		public function destroy():void {
			if (destroyed) return;
			destroyed = true;
			
			if (_bitmapData) _bitmapData.dispose();
			_bitmapData = null;
			_tileSet = null;
			_tmx = null;
		}
		
		public function hasTile():Boolean {
			return _gid > 0;
		}
		
		public function get gid():Number {
			return _gid;
		}
		
		public function set gid(value:Number):void {
			_gid = value;
		}
		
		public function get tmx():Tmx {
			return _tmx;
		}
		
		public function set tmx(value:Tmx):void {
			_tmx = value;
		}
		
		public function get tileSet():TileSet {
			return _tileSet;
		}
		
		public function set tileSet(value:TileSet):void {
			_tileSet = value;
		}
		
		public function get bitmapData():BitmapData {
			return _tmx.getBmpDataByGid(_gid);
		}
		
	}

}