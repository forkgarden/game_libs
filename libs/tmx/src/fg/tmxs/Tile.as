package fg.tmxs {
	/**
	 * ...
	 * @author 
	 */
	public class Tile {
		
		protected var _id:Number = 0;
		protected var _image:Image;
		protected var _onLoadCallBack:Function;
		protected var destroyed:Boolean = false;
		
		public function Tile() {
			
		}
		
		public function destroy():void {
			if (destroyed) return;
			destroyed = true;
			
			_image.destroy();
			_image = null;
			_onLoadCallBack = null;
		}
		
		public function load():void {
			_image.load();
		}
		
		protected function imgOnLoad():void {
			_onLoadCallBack();
		}
		
		public function get image():Image {
			return _image;
		}
		
		public function set image(value:Image):void {
			_image = value;
			_image.onLoadCallBack = imgOnLoad;
		}
		
		public function get onLoadCallBack():Function {
			return _onLoadCallBack;
		}
		
		public function set onLoadCallBack(value:Function):void {
			_onLoadCallBack = value;
		}
		
		public function get id():Number {
			return _id;
		}
		
		public function set id(value:Number):void {
			_id = value;
		}
		
	}

}