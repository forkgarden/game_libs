package fg.tmxs {
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Loader;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.net.URLRequest;
	
	/**
	 * ...
	 * @author 
	 */
	public class Image {
		
		protected var _source:String = '';
		protected var _width:Number = 0;
		protected var _height:Number = 0;
		protected var _tw:Number = 0;
		protected var _th:Number = 0;
		protected var loader:Loader;
		protected var _bmp:Bitmap;
		protected var _onLoadCallBack:Function;
		protected var _tile:Tile;
		protected var _tileSet:TileSet;
		protected var _layer:Layer;
		protected var destroyed:Boolean = false;
		
		public function Image() {
			
		}
		
		public function destroy():void {
			if (destroyed) return;
			destroyed = true;
			
			loader = null;
			_onLoadCallBack = null;
			_bmp = null;
			_tile = null;
			_tileSet = null;
			_layer = null;
		}
		
		public function getBitmapDataSpriteSheet(gid:Number, tileSet:TileSet):BitmapData {
			var localGid:Number = gid - tileSet.firstGid;
			var x:Number;
			var y:Number;
			var rect:Rectangle;
			var bmpData:BitmapData = new BitmapData(tileSet.tileWidth, tileSet.tileHeight);
			
			x = (localGid % tileSet.columns) * (tileSet.tileWidth + tileSet.spacing);
			y = Math.floor(localGid / tileSet.columns) * (tileSet.tileHeight + tileSet.spacing);
			rect = new Rectangle(x, y, tileSet.tileWidth, tileSet.tileHeight);
			rect.x += tileSet.margin;
			rect.y += tileSet.margin;
			
			bmpData.copyPixels(_bmp.bitmapData, rect, new Point(0, 0));
			return bmpData;
		}
		
		public function load():void {
			
			if (_bmp) _onLoadCallBack();
			loader = new Loader();
			loader.contentLoaderInfo.addEventListener(Event.COMPLETE, imgOnLoad, false, 0, true);
			loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, imgOnError, false, 0, true);
			loader.load(new URLRequest(_source));
			
			//trace('load image ' + _source);
			
		}
		
		protected function imgOnLoad(e:Event):void {
			
			_bmp = e.currentTarget.content as Bitmap;
			
			if (_tile) {
				_tw = _bmp.width;
				_th = _bmp.height;
			}
			else if (_tileSet) {
				_tw = _tileSet.tileWidth;
				_th = _tileSet.tileHeight;
			}
			else {
				_tw = _bmp.width;
				_th = _bmp.height;
			}
			
			_onLoadCallBack();
		}
		
		protected function imgOnError(e:IOErrorEvent):void {
			trace('_url ' + _source);
			trace(e.text);
		}
		
		public function get source():String {
			return _source;
		}
		
		public function set source(value:String):void {
			if (value == null) throw new Error();
			if (value == '') throw new Error();
			_source = value;
		}
		
		public function get width():Number {
			return _width;
		}
		
		public function set width(value:Number):void {
			_width = value;
		}
		
		public function get height():Number {
			return _height;
		}
		
		public function set height(value:Number):void {
			_height = value;
		}
		
		public function get onLoadCallBack():Function {
			return _onLoadCallBack;
		}
		
		public function set onLoadCallBack(value:Function):void {
			_onLoadCallBack = value;
		}
		
		public function get bmp():Bitmap {
			return _bmp;
		}
		
		public function set bmp(value:Bitmap):void {
			_bmp = value;
		}
		
		public function get tw():Number {
			return _tw;
		}
		
		public function set tw(value:Number):void {
			_tw = value;
		}
		
		public function get th():Number {
			return _th;
		}
		
		public function set th(value:Number):void {
			_th = value;
		}
		
		public function get tile():Tile {
			return _tile;
		}
		
		public function set tile(value:Tile):void {
			_tile = value;
		}
		
		public function get tileSet():TileSet {
			return _tileSet;
		}
		
		public function set tileSet(value:TileSet):void {
			_tileSet = value;
		}
		
		public function get layer():Layer {
			return _layer;
		}
		
		public function set layer(value:Layer):void {
			_layer = value;
		}
		
	}

}