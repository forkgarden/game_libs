package fg.tmxs {
	import fg.tmxs.Image;
	import flash.display.BitmapData;
	
	internal class TileSet {
		
		protected var _firstGid:Number = 0;
		protected var _name:String = '';
		protected var _tileWidth:Number = 0;
		protected var _tileHeight:Number = 0;
		protected var _spacing:Number = 0;
		protected var _margin:Number = 0;
		protected var _tileCount:Number = 0;
		protected var _columns:Number = 0;
		protected var _image:Image;
		protected var _tiles:Vector.<Tile> = new Vector.<Tile>();
		protected var _onLoadCallBack:Function;
		protected var _loadComplete:Boolean = false;
		protected var destroyed:Boolean = false;
		
		public function TileSet() {
			
		}
		
		public function destroy():void {
			var tile:Tile;
			
			if (destroyed) return;
			destroyed = true;
			
			if (_image) _image.destroy();
			_image = null;
			_onLoadCallBack = null;
			
			while (_tiles.length > 0) {
				tile = _tiles.pop();
				tile.destroy();
				tile = null;
			}
		}
		
		public function getImage(id:Number):BitmapData {
			var tile:Tile;
			var res:Tile;
			
			if (_image) {
				return _image.getBitmapDataSpriteSheet(id, this);
			}
			
			for each (tile in _tiles) {
				if (tile.id == (id - _firstGid)) res = tile;
			}
			
			if (res) {
				return res.image.bmp.bitmapData;
			}
			else {
				trace("TileSet get Image failed, id " + id);
				return new BitmapData(1, 1);
			}
			
		}
		
		/*
		public function getImage(id:Number, bmpData:BitmapData):void {
			var tile:Tile;
			var res:Tile;
			
			if (_image) {
				_image.getBitmapData2(id, this, bmpData);
				return;
			}
			
			for each (tile in _tiles) {
				if (tile.id == (_firstGid - id)) res = tile;
			}
			
			res.image.getBitmapData2(id, this, bmpData);
		}
		*/
		
		protected function imgOnLoad():void {
			var tile:Tile;
			
			for each (tile in _tiles) {
				tile.onLoadCallBack = imgOnLoad;
				if (tile.image && tile.image.bmp == null) {
					tile.load();
					return;
				}
			}
			
			_loadComplete = true;
			_onLoadCallBack();
		}
		
		public function load():void {
			
			if (_loadComplete) _onLoadCallBack();
			
			if (_image != null) {
				_image.load();
			}
			else {
				imgOnLoad();
			}
		}
		
		public function get firstGid():Number {
			return _firstGid;
		}
		
		public function set firstGid(value:Number):void {
			_firstGid = value;
		}
		
		public function get name():String {
			return _name;
		}
		
		public function set name(value:String):void {
			_name = value;
		}
		
		public function get tileWidth():Number {
			return _tileWidth;
		}
		
		public function set tileWidth(value:Number):void {
			_tileWidth = value;
		}
		
		public function get tileHeight():Number {
			return _tileHeight;
		}
		
		public function set tileHeight(value:Number):void {
			_tileHeight = value;
		}
		
		public function get spacing():Number {
			return _spacing;
		}
		
		public function set spacing(value:Number):void {
			_spacing = value;
		}
		
		public function get margin():Number {
			return _margin;
		}
		
		public function set margin(value:Number):void {
			_margin = value;
		}
		
		public function get tileCount():Number {
			return _tileCount;
		}
		
		public function set tileCount(value:Number):void {
			_tileCount = value;
		}
		
		public function get columns():Number {
			return _columns;
		}
		
		public function set columns(value:Number):void {
			_columns = value;
		}
		
		public function get image():Image {
			return _image;
		}
		
		public function set image(value:Image):void {
			_image = value;
			_image.onLoadCallBack = imgOnLoad;
		}
		
		public function get tiles():Vector.<Tile> {
			return _tiles;
		}
		
		public function set tiles(value:Vector.<Tile>):void {
			_tiles = value;
		}
		
		public function get onLoadCallBack():Function {
			return _onLoadCallBack;
		}
		
		public function set onLoadCallBack(value:Function):void {
			_onLoadCallBack = value;
		}
		
		public function get loadComplete():Boolean {
			return _loadComplete;
		}
		
		public function set loadComplete(value:Boolean):void {
			_loadComplete = value;
		}
		
	}

}