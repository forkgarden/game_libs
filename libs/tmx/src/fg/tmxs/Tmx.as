package fg.tmxs {
	import fg.tmxs.Image;
	import fg.tmxs.Tile;
	import fg.tmxs.TileSet;
	import flash.display.BitmapData;
	import flash.display.DisplayObjectContainer;
	import flash.events.Event;
	import flash.events.HTTPStatusEvent;
	import flash.events.IOErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;

	public class Tmx {
		
		protected var urlLoader:URLLoader;
		protected var xml:XML;
		protected var _url:String;
		protected var _tilesets:Vector.<TileSet> = new Vector.<TileSet>();
		protected var _layers:Vector.<Layer> = new Vector.<Layer>
		protected var _onLoadCallBack:Function;
		protected var _tw:Number = 0;
		protected var _th:Number = 0;
		protected var _tileBmpDatas:Vector.<BmpData> = new Vector.<BmpData>();
		protected var _version:String = '';
		protected var _orientation:String = '';
		protected var _renderorder:String = '';
		protected var _width:Number = 0;
		protected var _height:Number = 0;
		protected var hexidelength:Number = 0;
		protected var staggeraxis:String = '';
		protected var staggerindex:String = '';
		protected var backgroundcolor:String = '';
		
		public function Tmx() {
			
		}
		
		public function destroy():void {
			var tileSet:TileSet;
			var layer:Layer;
			var bmpData:BmpData;
			
			xml = null;
			urlLoader = null;
			_onLoadCallBack = null;
			
			while (_tilesets.length > 0) {
				tileSet = _tilesets.pop();
				tileSet.destroy();
				tileSet = null;
			}
			
			while (_layers.length > 0) {
				layer = _layers.pop();
				layer.destroy();
				layer = null;
			}
			
			while (_tileBmpDatas.length > 0) {
				bmpData = _tileBmpDatas.pop();
				bmpData.destroy();
				bmpData = null;
			}
			
		}
		
		protected function getTileSet(gid:Number):TileSet {
			var tileSet:TileSet;
			var res:TileSet;
			
			for each (tileSet in _tilesets) {
				if (tileSet.firstGid <= gid) res = tileSet;
			}
			
			return res;
		}
		
		public function render(cont:DisplayObjectContainer):void {
			var layer:Layer;
			
			for each (layer in _layers) {
				layer.render(cont);
			}
		}
		
		/*
		public function render2(cont:DisplayObjectContainer):void {
			var layer:Layer;
			var i:int;
			var j:int;
			var tile:TileMap;
			var bmp:Bitmap = new Bitmap();
			
			for each (layer in _layers) {
				for (j = 0; j < layer.height; j++) {
					for (i = 0; i < layer.width; i++) {
						tile = layer.getTile(i, j);
						if (tile.gid > 0) {
							bmp = new Bitmap();
							bmp.bitmapData = getBmpData(i, j, layer);
							bmp.x = i * tw;
							bmp.y = j * th;
							cont.addChild(bmp);
						}
					}
				}
			}
			
		}
		*/
		
		protected function getBmpDataFromPool(gid:Number):BmpData {
			var bmpData:BmpData;
			
			for each (bmpData in _tileBmpDatas) {
				if (bmpData.id == gid) {
					return bmpData;
				}
			}
			
			return null;
		}
		
		public function getLayerByName(name:String):Layer {
			var layer:Layer;
			
			for each (layer in _layers) {
				if (layer.name == name) return layer;
			}
			
			trace('Layer not found for name ' + name);
			return null;
		}
		
		public function getBmpDataByGid(gid:Number):BitmapData {
			var bmpDataObj:BmpData;
			var bmpData:BitmapData;
			var tileSet:TileSet;
			
			bmpDataObj = getBmpDataFromPool(gid);
			
			if (bmpDataObj) return bmpDataObj.bmpData;
			
			tileSet = getTileSet(gid);
			if (tileSet) {
				bmpData = getTileSet(gid).getImage(gid);
				bmpDataObj = new BmpData();
				bmpDataObj.id = gid;
				bmpDataObj.bmpData = bmpData;
				_tileBmpDatas.push(bmpDataObj);
				
				return bmpData;
			}
			else {
				trace('tileset not found, gid ' + gid);
			}
			
			return new BitmapData(1,1);
		}
		
		/*
		public function getBmpData(x:Number, y:Number, layer:Layer):BitmapData {
			var tile:TileMap;
			var tileSet:TileSet;
			var bmpData:BitmapData
			var bmpDataObj:BmpData;
			
			tile = layer.getTile(x, y);
			if (tile.gid == 0) {
				return new BitmapData(1, 1);
			}
			
			return getBmpDataByGid(tile.gid);
		}
		*/
		
		public function load():void {
			trace('load ' + _url);
			
			urlLoader = new URLLoader();
			urlLoader.addEventListener(Event.COMPLETE, onLoaded, false, 0, true);
			urlLoader.addEventListener(IOErrorEvent.IO_ERROR, onLoadError, false, 0, true);
			urlLoader.addEventListener(HTTPStatusEvent.HTTP_STATUS, onHttpStatus);
			
			urlLoader.load(new URLRequest(_url));
			
			trace(urlLoader);
		}
		
		protected function loadImage():void {
			var tileset:TileSet;
			var layer:Layer;
			
			for each (tileset in tilesets) {
				if (tileset.loadComplete == false) {
					tileset.load();
					return;
				}
			}
			
			//load image
			for each (layer in _layers) {
				if (layer.type == Layer.LYR_IMAGE) {
					if (layer.image.bmp == null) {
						layer.image.load();
						layer.image.onLoadCallBack = loadImage;
						return;
					}
				}
			}
			
			_onLoadCallBack();
		}
		
		protected function onHttpStatus(e:HTTPStatusEvent):void {
			
		}
		
		protected function onLoadError(e:IOErrorEvent):void {
			trace(e.text);
		}
		
		protected function checkSupport():void {
		}
		
		protected function onLoaded(e:Event):void {
			var xmlList:XMLList;
			var xmlListTile:XMLList;
			var xmlList2:XMLList;
			var xmlTileSet:XML;
			var tileSet:TileSet;
			var xml2:XML;
			var xml3:XML;
			var xml4:XML;
			var image:Image;
			var tile:Tile;
			var tileMap:TileMap;
			var layer:Layer;
			
			xml = new XML(e.currentTarget.data + '');
			
			_tw = xml.@tilewidth;
			_th = xml.@tileheight;
			_width = xml.@width;
			_height = xml.@height;
			_orientation = xml.@orientation;
			if (_orientation != 'orthogonal') trace('WARNING: orientation is not supported: ' + _orientation);
			
			//get tile set
			xmlList = xml.tileset;
			for each (xmlTileSet in xmlList) {
				tileSet = new TileSet();
				tileSet.firstGid = xmlTileSet.@firstgid;
				tileSet.columns = xmlTileSet.@columns;
				tileSet.margin = xmlTileSet.@margin;
				tileSet.spacing = xmlTileSet.@spacing;
				tileSet.tileCount = xmlTileSet.@tilecount;
				tileSet.tileHeight = xmlTileSet.@tileheight;
				tileSet.tileWidth = xmlTileSet.@tilewidth;
				tileSet.onLoadCallBack = loadImage;
				_tilesets.push(tileSet);
				
				//contain image
				xml3 = xmlTileSet.image[0]
				if (xml3) {
					image = new Image();
					image.source = xml3.@source;
					image.height = xml3.@height;
					image.width = xml3.@width;
					image.tileSet = tileSet;
					tileSet.image = image;
				}
				
				//contain tile 
				xmlListTile = xmlTileSet.tile;
				for each (xml3 in xmlListTile) {

					tile = new Tile();
					tile.id = xml3.@id;
					tileSet.tiles.push(tile);
					
					//contain image
					if (xml3.image[0]) {
						image = new Image();
						image.height = xml3.image.@height;
						image.width = xml3.image.@width;
						image.source = xml3.image.@source;
						image.tile = tile;
						tile.image = image;
					}
				}
				
			}
			
			getLayerData(xml);
			loadImage();
		}
		
		protected function getLayerData(xml:XML):void {
			var xmlLists:XMLList;
			var xml2:XML;
			var layer:Layer;
			var xmlList2:XMLList;
			var xml3:XML;
			var tileMap:TileMap;
			var img:Image;
			
			for each (xml2 in xml.children()) {
				
				if (xml2.name() == Layer.LYR_LAYER) {
					layer = new Layer();
					layer.type = xml2.name();
					layer.tmx = this;
					layer.name = xml2.@name;
					layer.width = xml2.@width;
					layer.height = xml2.@height;
					_layers.push(layer);
					
					xmlList2 = xml2.data.tile;
					
					for each (xml3 in xmlList2) {
						tileMap = new TileMap();
						tileMap.tmx = this;
						tileMap.gid = xml3.@gid;
						layer.tiles.push(tileMap);
					}
				}
				
				else if (xml2.name() == Layer.LYR_OBJECT) {
					layer = new Layer();
					layer.type = xml2.name();
					layer.tmx = this;
					layer.name = xml2.@name;
					_layers.push(layer);
					getObj(xml2, layer);
					layer.objSort();
				}
				
				else if (xml2.name() == Layer.LYR_IMAGE) {
					layer = new Layer();
					layer.type = xml2.name();
					layer.offset.x = xml2.@offsetx;
					layer.offset.y = xml2.@offsety;
					img = new Image();
					img.source = xml2.image.@source;
					img.width = xml2.image.@width;
					img.height = xml2.image.@height;
					img.layer = layer;
					layer.image = img;
					layers.push(layer);
				}
				
				else if (xml2.name() == Layer.LYR_PROP) {
					
				}
				else if (xml2.name() == Layer.LYR_TILESET) {
					
				}
				else {
					throw new Error('name undefined ' + xml2.name());
				}
			}
		}
		
		protected function getProp(xml:XML):Vector.<Property> {
			var xml2:XML;
			var vec:Vector.<Property> = new Vector.<Property>();
			var prop:Property;
			
			//trace('get prop ' + xml);
			
			for each(xml2 in xml.properties.children()) {
				prop = new Property();
				prop.name = xml2.@name;
				prop.type = xml2.@type;
				prop.value = xml2.@value;
				vec.push(prop);
				
				//trace('crete prop ' + JSON.stringify(prop));
			}
			
			return vec;
		}
		
		protected function getObj(xml:XML, layer:Layer):void {
			var xml2:XML;
			var obj:TileObject;
			
			for each (xml2 in xml.children()) {
				if (xml2.name() == 'object') {
					obj = new TileObject;
					obj.name = xml2.@name;
					obj.gid = xml2.@gid;
					obj.height = xml2.@height;
					obj.id = xml2.@id;
					obj.width = xml2.@width;
					obj.x = xml2.@x;
					obj.y = xml2.@y;
					obj.layer = layer;
					obj.tmx = this;
					obj.properties = getProp(xml2);
					layer.objs.push(obj);
				}
				else {
					throw new Error();
				}
			}
		}
		
		public function get url():String {
			return _url;
		}
		
		public function set url(value:String):void {
			_url = value;
		}
		
		public function get layers():Vector.<Layer> {
			return _layers;
		}
		
		//public function set layers(value:Vector.<Layer>):void {
			//_layers = value;
		//}
		
		public function get tilesets():Vector.<TileSet> {
			return _tilesets;
		}
		
		//public function set tilesets(value:Vector.<TileSet>):void {
			//_tilesets = value;
		//}
		
		public function get onLoadCallBack():Function {
			return _onLoadCallBack;
		}
		
		public function set onLoadCallBack(value:Function):void {
			_onLoadCallBack = value;
		}
		
		public function get tileBmpDatas():Vector.<BmpData> {
			return _tileBmpDatas;
		}
		
		public function get tw():Number {
			return _tw;
		}
		
		//public function set tw(value:Number):void {
			//_tw = value;
		//}
		
		public function get th():Number {
			return _th;
		}
		
		//public function set th(value:Number):void {
			//_th = value;
		//}
		
		public function get orientation():String {
			return _orientation;
		}
		
		//public function set orientation(value:String):void {
			//_orientation = value;
		//}
		
		public function get renderorder():String {
			return _renderorder;
		}
		
		public function get width():Number {
			return _width;
		}
		
		public function get height():Number {
			return _height;
		}
		
		//public function set renderorder(value:String):void {
			//_renderorder = value;
		//}
		
	}

}