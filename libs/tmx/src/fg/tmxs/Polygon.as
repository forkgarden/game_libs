package fg.tmxs {
	import flash.geom.Point;
	/**
	 * ...
	 * @author 
	 */
	public class Polygon {
		
		protected var _points:Vector.<Point> = new Vector.<Point>();
		
		public function Polygon() {
			
		}
				
		public function get points():Vector.<Point> {
			return _points;
		}
		
		public function set points(value:Vector.<Point>):void {
			_points = value;
		}
		
	}

}