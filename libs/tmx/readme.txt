Tmx adalah sebuah Class untuk membaca file .tmx yang dihasilkan oleh Tiled.
Tiled adalah sebuah applikasi gratis untuk membuat map.
Tiled memiliki banyak fitur untuk membuat map mulai dari yang 2d biasa hingga isometric.

Tidak semua fitur dari Tiled di support oleh Class ini.
Saat ini Tmx masih dalam tahap pengembangan.
Saya ingin nantinya bisa membaca semua fitur yang ada di Tiled.

Berikut adalah beberapa syarat agar file .tmx bisa dibaca oleh Class ini.
orientation: orthogonal / 2d (isometric dan lainnya masih dalam pengembangan)
tile layer format: XML (base64 compression dan csv belum ada)
external tileset data tidak di support.

Untuk saat ini Tmx sudah cukup untuk digunakan dalam pembuatan game 2D sederhana.

Cara penggunaannya cukup mudah

tmx = new Tmx();
tmx.url = 'test2.tmx';
tmx.onLoadCallBack = tmxOnLoaded;
tmx.load();

Tmx menggunakan callBack untuk mengetahui apakah file selesai di load atau belum.
Saat file ".tmx" selesai di load, anda bisa langsung menggunakannya.

protected function tmxOnLoaded():void {
	trace('complete');
	tmx.render(this.stage);
}

Berikut adalah beberapa contoh operasi yang bisa dilakukan oleh Tmx.

Merender semua layer ke Stage 
tmx.render(stage);

Mendapatkan data semua layer
tmx.layers

Mendapatkan layer ke 0 atau dengan nama tertentu
tmx.layers[0];
tmx.getLayerByName('layer1')

Merender layer tertentu ke stage
tmx.layers[0].render(stage);
tmx.getLayerByName('layer1').render(stage);

Mendapatkan data bitmapdata dari tile posisi 0, 0 dari layer ke 0
tmx.layers[0].getTile(0, 0).bitmapData

Mendapatkan data object dari layer dan mendapatkan properti serta namanya
tmx.getLayerByName('object_layer_1').getObjById('1').properties[0].name;

Masih banyak lagi operasi yang bisa dilakukan, Anda bisa mengeksplore sendiri.

Terima kasih sudah datang dan membaca