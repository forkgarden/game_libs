package fg.counters {
	/**
	 * ...
	 * @author 
	 */
	public class Counter {
		
		protected var _ctr:Number = 0;
		protected var _active:Boolean = false;
		
		public function Counter() {
			
		}
		
		public function stop():void {
			_ctr = 0;
			_active = false;
		}
		
		public function start():void {
			_active = true;
			ctr = 0;
		}
		
		public function update():void {
			if (active == false) return;
			ctr++;
		}
		
		public function get ctr():Number {
			return _ctr;
		}
		
		public function set ctr(value:Number):void {
			_ctr = value;
		}
		
		public function get active():Boolean {
			return _active;
		}
		
		public function set active(value:Boolean):void {
			_active = value;
		}
		
	}

}