package{
	import fg.path_finder_helper.PFHelper;
	import flash.display.Sprite;
	import flash.events.Event;
	
	public class MainPfHelper extends Sprite {
		
		protected var pfHelper:PFHelper;
		protected var spr:Sprite = new Sprite();
		
		public function MainPfHelper() {
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event = null):void {
			var i:int;
			var j:int;
			
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			// entry point
			spr = new Sprite();
			spr.graphics.beginFill(0, 1);
			spr.graphics.drawCircle(0, 0, 5);
			spr.graphics.endFill();
			spr.x = 16;
			spr.y = 32 + 16;
			addChild(spr);
			
			pfHelper = new PFHelper();
			pfHelper.start([
				[0,1],
				[1,1],
				[2,1],
				[2,2],
				[2,3]
			]);
			
			this.graphics.lineStyle(1, 0);
			for (i = 0; i < 5; i++) {
				for (j = 0; j < 5; j++) {
					this.graphics.moveTo(i * 32, 0);
					this.graphics.lineTo(i * 32, 32 * 4);
					this.graphics.moveTo(0, j * 32);
					this.graphics.lineTo(32 * 4, j * 32);
				}
			}
			
			addEventListener(Event.ENTER_FRAME, update);
		}
		
		protected function update(e:Event):void {
			if (pfHelper.aktif) {
				pfHelper.update();
				spr.x = pfHelper.pos.x + 16;
				spr.y = pfHelper.pos.y + 16;
			}
		}
		
	}
	
}