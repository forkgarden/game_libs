package fg.dialogs {
	import fg.tekstypes.TypeWritter;
	
	public class DialogController {
		protected var ketik:TypeWritter;
		protected var teksIdx:int = 0;
		protected var teksAr:Array = [];
		protected var _finishCallBack:Function;
		protected var _changeCallBack:Function;
		
		public function DialogController() {
			ketik = new TypeWritter();
		}
		
		//fungsi ini dipanggil saat dialog di click
		public function onMouseClick():void {
			//bila ketik sudah finish
			//ambil data dialog selanjutnya
			if (ketik.flFinish) {
				teksIdx++;
				
				//bila semua teks sudah ditampilkan
				//sembunyikan dialog
				//panggil _finishCallBack (bila ada)
				if (teksIdx >= teksAr.length) {
					if (_finishCallBack != null) _finishCallBack();
				}
				
				//tampilkan teks berikutnya
				else {
					if (_changeCallBack != null) _changeCallBack(teksIdx);
					ketik.teksInput = teksAr[teksIdx];
					ketik.start();
				}
			}
			
			//bila ketik belum finish, skip
			//agar bisa teks langsung selesai
			else {
				ketik.skip();
			}
		}
		
		//update dialog, tampilkan teks
		public function update():void {
			ketik.update();
		}
		
		public function getTeks():String {
			return ketik.teksOutput;
		}
		
		//fungsi ini dipanggil saat memulai dialog
		public function start(data:Array):void {
			
			//index teks dari data
			teksIdx = 0;
			
			//isi teks untuk teks yang berjalan 
			//diambil dari data
			ketik.teksInput = data[0];
			
			//simpan data teks untuk ditampilkan
			teksAr = data;
			
			//setelah dialog ditampilkan semuanya, panggil fungsi callBack
			ketik.start();
			
			if (_changeCallBack != null) _changeCallBack(teksIdx);
		}
		
		public function get finishCallBack():Function {
			return _finishCallBack;
		}
		
		public function set finishCallBack(value:Function):void {
			_finishCallBack = value;
		}
		
		public function get changeCallBack():Function 
		{
			return _changeCallBack;
		}
		
		public function set changeCallBack(value:Function):void 
		{
			_changeCallBack = value;
		}
		
	}

}