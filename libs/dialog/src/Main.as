package{
	import flash.display.Sprite;
	import flash.events.Event;

	[Frame(factoryClass="Preloader")]
	public class Main extends Sprite {
		
		public function Main() {
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}

		protected function init(e:Event = null):void {
			
		}
	}

}