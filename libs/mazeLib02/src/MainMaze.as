package {
	import fg.mazes.Maze;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	public class MainMaze extends Sprite {
		
		protected var maze:Maze = new Maze();
		
		public function MainMaze():void {
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		protected function init(e:Event = null):void {
			removeEventListener(Event.ADDED_TO_STAGE, init);
			stage.addEventListener(MouseEvent.CLICK, onClick);
			
			mazeCreate();
		}
		
		protected function mazeCreate():void {
			maze.create(5, 4, 2, false);
			maze.draw(this, 32);
			maze.debug();
		}
		
		protected function onClick(e:MouseEvent):void {
			mazeCreate();
		}
	}
	
}