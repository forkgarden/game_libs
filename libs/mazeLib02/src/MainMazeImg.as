package {
	import fg.mazes.Maze;
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	public class MainMazeImg extends Sprite {
		
		[Embed(source = "../lib/rumput.png")]
		protected var Rumput:Class;
		
		[Embed(source="../lib/tile.png")]
		protected var Tembok:Class;
		
		protected var maze:Maze = new Maze();
		protected var rumput:Bitmap = new Rumput();
		protected var tembok:Bitmap = new Tembok();
		
		protected var sprGambar:Sprite = new Sprite();
		
		public function MainMazeImg():void {
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		protected function init(e:Event = null):void {
			removeEventListener(Event.ADDED_TO_STAGE, init);
			stage.addEventListener(MouseEvent.CLICK, onClick);
			
			mazeCreate();
			
			addChild(sprGambar);
		}
		
		protected function mazeCreate():void {
			maze.create(4, 4, 2, false);
			
			maze.drawWImg(this, tembok.bitmapData, rumput.bitmapData);
			
			addChild(sprGambar);
			sprGambar.graphics.clear();
		}
		
		protected function onClick(e:MouseEvent):void {
			mazeCreate();
		}
		
	}
	
}