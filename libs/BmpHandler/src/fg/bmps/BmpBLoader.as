package fg.bmps {
	import fg.bmps.BmpLoader;
	
	public class BmpBLoader {
		
		protected var list:Vector.<BmpLoader> = new Vector.<BmpLoader>();
		protected var _onLoadCallBack:Function;
		protected var _bmpCr:BmpLoader;
		
		public function BmpBLoader() {
			
		}
		
		public function getBmpById(id:String):BmpLoader {
			var bmp:BmpLoader;
			
			for each (bmp in list) {
				if (bmp.id == id) return bmp;
			}
			
			return null;
		}
		
		public function bmpTotal():Number {
			return list.length;
		}
		
		public function loadedBitmapCount():Number {
			var n:Number = 0;
			var bmp:BmpLoader;
			
			for each (bmp in list) {
				if (bmp.flLoaded) n++;
			}
			
			return n;
		}
		
		protected function checkAllLoaded():Boolean {
			var bmp:BmpLoader;
			
			for each (bmp in list) {
				if (bmp.flLoaded == false) return false;
			}
			
			return true;
		}
		
		protected function bmpOnLoad():void {
			
			if (checkAllLoaded()) {
				_onLoadCallBack();
			}
			else {
				load();
			}
		}
		
		public function load():void {
			var bmp:BmpLoader;
			
			for each (bmp in list) {
				if (bmp.flLoaded == false) {
					_bmpCr = bmp;
					bmp.load();
					return;
				}
			}
			
			_onLoadCallBack();
		}
		
		public function reg(id:String, url:String):void {
			var bmp:BmpLoader = new BmpLoader();
			
			bmp.id = id;
			bmp.url = url;
			bmp.onLoadCallBack = bmpOnLoad;
			
			if (getBmpById(id) != null) throw new Error('duplicate id');
			
			list.push(bmp);
		}
		
		public function get onLoadCallBack():Function {
			return _onLoadCallBack;
		}
		
		public function set onLoadCallBack(value:Function):void {
			_onLoadCallBack = value;
		}
		
		public function get bmpCr():BmpLoader {
			return _bmpCr;
		}
		
	}

}