package fg.bmps {
	import flash.display.Bitmap;
	import flash.display.Loader;
	import flash.display.LoaderInfo;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.net.URLRequest;

	public class BmpLoader {
		protected var loader:Loader
		protected var _bmp:Bitmap;
		protected var _onLoadCallBack:Function;
		protected var _onErrorCallBack:Function;
		protected var _id:String;
		protected var _flLoaded:Boolean = false;
		protected var _url:String;
		protected var _byteLoaded:Number = 1;
		protected var _byteTotal:Number = 1;
		
		public function BmpLoader() {
			
		}
		
		public function load():void {
			trace('load ' + _url);
			
			if (_bmp) {
				_flLoaded = true;
				if (_onLoadCallBack != null) _onLoadCallBack();
			}
			
			loader = new Loader();
			loader.contentLoaderInfo.addEventListener(Event.COMPLETE, onLoad, false, 0, true);
			loader.contentLoaderInfo.addEventListener(ProgressEvent.PROGRESS, loadOnProgress, false, 0, true);
			loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, onError, false, 0, true);
			loader.load(new URLRequest(_url));
		}
		
		protected function loadOnProgress(e:ProgressEvent):void {
			_byteLoaded = e.bytesLoaded;
			_byteTotal = e.bytesTotal;
		}
		
		protected function onError(e:IOErrorEvent):void {
			if (_onErrorCallBack != null) _onErrorCallBack();
			trace(e.text);
		}
		
		protected function onLoad(e:Event):void {
			_bmp = (e.currentTarget as LoaderInfo).content as Bitmap;
			_flLoaded = true;
			if (_onLoadCallBack != null) _onLoadCallBack();
		}
		
		public function get bmp():Bitmap {
			return _bmp;
		}
		
		public function get onLoadCallBack():Function {
			return _onLoadCallBack;
		}
		
		public function set onLoadCallBack(value:Function):void {
			_onLoadCallBack = value;
		}
		
		public function get flLoaded():Boolean {
			return _flLoaded;
		}
		
		public function get id():String {
			return _id;
		}
		
		public function set id(value:String):void {
			_id = value;
		}
		
		public function get url():String {
			return _url;
		}
		
		public function set url(value:String):void {
			_url = value;
		}
		
		public function get byteLoaded():Number {
			return _byteLoaded;
		}
		
		public function get byteTotal():Number {
			return _byteTotal;
		}
		
		public function get onErrorCallBack():Function 
		{
			return _onErrorCallBack;
		}
		
		public function set onErrorCallBack(value:Function):void 
		{
			_onErrorCallBack = value;
		}
		
	}

}