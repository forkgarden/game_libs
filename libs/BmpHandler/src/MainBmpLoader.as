package{
	import fg.bmps.BmpBLoader;
	import flash.display.Sprite;
	import flash.events.Event;
	
	/**
	 * ...
	 * @author 
	 */
	public class MainBmpLoader extends Sprite {
		
		protected var bmp:BmpBLoader = new BmpBLoader();
		
		
		public function MainBmpLoader() {
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event = null):void {
			removeEventListener(Event.ADDED_TO_STAGE, init);
			// entry point
			
			bmp.reg('img1', 'tile/chr01.png');
			bmp.reg('img2', 'tile/chr02.png');
			bmp.reg('img3', 'tile/chr03.png');
			bmp.reg('img4', 'tile/chr04.png');
			bmp.reg('img5', 'tile/chr05.png');
			
			bmp.onLoadCallBack = bmpOnLoad;
			bmp.load();
		}
		
		protected function bmpOnLoad():void {
			addChild(bmp.getBmpById('img1').bmp);
			addChild(bmp.getBmpById('img5').bmp);
		}
		
	}
	
}