package 
{
	import fg.inters.MoveStage;
	import flash.display.Sprite;
	import mx.core.FlexSprite;
	/**
	 * ...
	 * @author test
	 */
	public class StageMoveTest extends Sprite
	{
		protected var spr:Sprite;
		protected var spr2:Sprite;
		protected var move:MoveStage;
		
		public function StageMoveTest() 
		{
			
			spr = new Sprite();
			spr.x = 100;
			spr.y = 100;
			spr.rotation = 30;
			spr.scaleX = 1.5;
			spr.scaleY = 1;
			
			addChild(spr);
			
			spr2 = new Sprite();
			spr2.graphics.beginFill(0, .5);
			spr2.graphics.drawRect(0, 0, 100, 100);
			spr2.graphics.endFill();
			spr.addChild(spr2);
			
			move = new MoveStage();
			move.view = spr2;
			move.start();
			
		}
		
	}

}