package fg.inters {
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.display.Stage;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	public class MoveContainer extends BaseInter {
		
		protected var _callBackMove:Function;
		protected var _moveTarget:MovieClip;
		
		public function MoveContainer(view:DisplayObject, moveTarget:MovieClip, stageRef:Stage) {
			trace("view " + view);
			trace("move target " + moveTarget);
			trace("stage ref " + stageRef);
			
			this._view = view;
			this.stageRef = stageRef;
			_moveTarget = moveTarget;
			_view.addEventListener(MouseEvent.MOUSE_DOWN, mouseDown);
		}
		
		protected override function mouseMove(e:MouseEvent):void {
			if (mouseIsDown) {
				
				viewStagePos.x = viewLastPos.x + (e.stageX - mouseLastPos.x);
				viewStagePos.y = viewLastPos.y + (e.stageY - mouseLastPos.y);
				pRes = _moveTarget.parent.globalToLocal(viewStagePos);
				
				_moveTarget.x = pRes.x;
				_moveTarget.y = pRes.y;
				
				//_moveTarget.x = viewLastPos.x + (e.stageX - mouseLastPos.x); // * Main.inst.ratio;
				//_moveTarget.y = viewLastPos.y + (e.stageY - mouseLastPos.y); // * Main.inst.ratio;
			}
		}
		
		protected override function mouseDown(e:MouseEvent):void {
			e.stopPropagation();
			
			mouseIsDown = true;
			
			mouseLastPos.x = e.stageX;
			mouseLastPos.y = e.stageY;
			
			pRes = _moveTarget.parent.localToGlobal(new Point(_moveTarget.x, _moveTarget.y));
			
			viewLastPos.x = pRes.x;
			viewLastPos.y = pRes.y;	
			
			//viewLastPos.x = _moveTarget.x;
			//viewLastPos.y = _moveTarget.y;
			
			//trace('mouse down');
			//trace('target ' + e.currentTarget.name);
			//trace('mouse pos ' + e.stageX + '/' + e.stageY);
			//trace('');
			
			view.stage.addEventListener(MouseEvent.MOUSE_MOVE, mouseMove);
			view.stage.addEventListener(MouseEvent.MOUSE_UP, mouseUp);
		}
		
		protected override function mouseUp(e:MouseEvent):void {
			mouseIsDown = false;
			
			view.stage.removeEventListener(MouseEvent.MOUSE_MOVE, mouseMove);
			view.stage.removeEventListener(MouseEvent.MOUSE_UP, mouseUp);
		}
		
		public function get callBackMove():Function {
			return _callBackMove;
		}
		
		public function set callBackMove(value:Function):void {
			_callBackMove = value;
		}
		
		public function get moveTarget():MovieClip {
			return _moveTarget;
		}
		
		public function set moveTarget(value:MovieClip):void {
			_moveTarget = value;
		}
	}
	
}