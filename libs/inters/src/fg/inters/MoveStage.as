package fg.inters {
	import flash.display.DisplayObject;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	public class MoveStage {
		
		protected var viewLastPos:Point = new Point();
		protected var viewStagePos:Point = new Point();
		
		protected var mouseLastPos:Point = new Point();
		protected var _view:DisplayObject;
		protected var pRes:Point = new Point();
		protected var p:Point = new Point();
		protected var _scale:Boolean = true;
		
		protected var _lockX:Boolean = false;
		protected var _lockY:Boolean = false;
		protected var _callBackMove:Function;
		protected var _callBackMouseDown:Function;
		protected var _callBackMouseUp:Function;
		
		public function MoveStage() {
		}
		
		public function start():void {
			_view.stage.removeEventListener(MouseEvent.MOUSE_DOWN, mouseDown);
			_view.stage.addEventListener(MouseEvent.MOUSE_DOWN, mouseDown);
		}
		
		protected function mouseUp(e:MouseEvent):void {
			_view.stage.removeEventListener(MouseEvent.MOUSE_MOVE, mouseMove);
			_view.stage.removeEventListener(MouseEvent.MOUSE_UP, mouseUp);
			
			if (_callBackMouseUp != null) _callBackMouseUp();
		}
		
		public function stop():void {
			_view.stage.removeEventListener(MouseEvent.MOUSE_DOWN, mouseDown);
			_view = null;
		}
		
		protected function mouseMove(e:MouseEvent):void {
			var dx:Number;
			var dy:Number;
			
			dx = e.stageX - mouseLastPos.x;
			dy = e.stageY - mouseLastPos.y;
			
			viewStagePos.x = viewLastPos.x + (e.stageX - mouseLastPos.x);
			viewStagePos.y = viewLastPos.y + (e.stageY - mouseLastPos.y);
			
			pRes = _view.parent.globalToLocal(viewStagePos);
			//pRes.x = viewStagePos.x;
			//pRes.y = viewStagePos.y;
			
			if (_lockX == false) _view.x = pRes.x;
			if (_lockY == false) _view.y = pRes.y;
			
			if (_callBackMove) {
				_callBackMove(dx, dy);
			}
		}
		
		protected function mouseDown(e:MouseEvent):void {
			
			mouseLastPos.x = e.stageX;
			mouseLastPos.y = e.stageY;
			
			pRes = _view.parent.localToGlobal(new Point(_view.x, _view.y));
			
			viewLastPos.x = pRes.x;
			viewLastPos.y = pRes.y;
			
			_view.stage.addEventListener(MouseEvent.MOUSE_MOVE, mouseMove);
			_view.stage.addEventListener(MouseEvent.MOUSE_UP, mouseUp);
			
			if (_callBackMouseDown) _callBackMouseDown();
		}
		
		public function get lockX():Boolean {
			return _lockX;
		}
		
		public function set lockX(value:Boolean):void {
			_lockX = value;
		}
		
		public function get callBackMove():Function {
			return _callBackMove;
		}
		
		public function set callBackMove(value:Function):void {
			_callBackMove = value;
		}
		
		public function get lockY():Boolean {
			return _lockY;
		}
		
		public function set lockY(value:Boolean):void {
			_lockY = value;
		}
		
		public function get scale():Boolean {
			return _scale;
		}
		
		public function set scale(value:Boolean):void {
			_scale = value;
		}
		
		public function get callBackMouseUp():Function 
		{
			return _callBackMouseUp;
		}
		
		public function set callBackMouseUp(value:Function):void 
		{
			_callBackMouseUp = value;
		}
		
		public function get callBackMouseDown():Function 
		{
			return _callBackMouseDown;
		}
		
		public function set callBackMouseDown(value:Function):void 
		{
			_callBackMouseDown = value;
		}
		
		public function get view():DisplayObject 
		{
			return _view;
		}
		
		public function set view(value:DisplayObject):void 
		{
			_view = value;
		}
	}
	
}