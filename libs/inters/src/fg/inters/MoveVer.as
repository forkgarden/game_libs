package fg.inters {
	import flash.display.DisplayObject;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	public class MoveVer extends BaseInter {
		
		protected var _moveTarget:DisplayObject;
		protected var _panah:DisplayObject;
		
		public function MoveVer() {

		}
		
		protected override function mouseUp(e:MouseEvent):void {
			mouseIsDown = false;
			
			//adjust position
			_panah.x = _moveTarget.x;
			_panah.y = _moveTarget.y;
			
			view.stage.removeEventListener(MouseEvent.MOUSE_MOVE, mouseMove);
			view.stage.removeEventListener(MouseEvent.MOUSE_UP, mouseUp);
		}
		
		protected override function mouseMove(e:MouseEvent):void {
			if (mouseIsDown) {
				
				viewStagePos.y = viewLastPos.y + (e.stageY - mouseLastPos.y);
				pRes = _moveTarget.parent.globalToLocal(viewStagePos);
				
				_moveTarget.y = pRes.y;
				_panah.y = _moveTarget.y;
			}
		}
		
		protected override function mouseDown(e:MouseEvent):void {
			e.stopPropagation();
			
			mouseIsDown = true;
			
			mouseLastPos.x = e.stageX;
			mouseLastPos.y = e.stageY;
			
			pRes = _moveTarget.parent.localToGlobal(new Point(_moveTarget.x, _moveTarget.y));
			
			viewLastPos.x = pRes.x;
			viewLastPos.y = pRes.y;
			
			trace('mouse down');
			trace('pres ' + pRes);
			trace('mouse last pos ' + mouseLastPos);
			
			view.stage.addEventListener(MouseEvent.MOUSE_MOVE, mouseMove);
			view.stage.addEventListener(MouseEvent.MOUSE_UP, mouseUp);
		}
		
		public function get moveTarget():DisplayObject {
			return _moveTarget;
		}
		
		public function set moveTarget(value:DisplayObject):void {
			_moveTarget = value;
		}		
		
		public override function set view(value:DisplayObject):void {
			_view = value;
			_view.addEventListener(MouseEvent.MOUSE_DOWN, mouseDown);
		}
		
		public function get panah():DisplayObject {
			return _panah;
		}
		
		public function set panah(value:DisplayObject):void {
			_panah = value;
		}
		
		public function destroy():void {
			if (_view) {
				_view.removeEventListener(MouseEvent.MOUSE_DOWN, mouseDown);
				_view.stage.removeEventListener(MouseEvent.MOUSE_MOVE, mouseMove);
				_view.stage.removeEventListener(MouseEvent.MOUSE_UP, mouseUp);
			}
			
			_view = null;
		}
		
	}

}