package fg.inters {
	import flash.display.DisplayObject;
	import flash.geom.Point;
	import flash.events.MouseEvent;
	import flash.display.Sprite;
	import flash.display.Stage;
	
	public class Scale extends BaseInter {
		
		private var oriScale:Point = new Point();
		private var oriDim:Point = new Point();
		
		public function Scale(view:DisplayObject, stageRef:Stage) {
			this._view = view;
			this.stageRef = stageRef;
			addListener();
		}
		
		protected override function mouseMove(e:MouseEvent):void {
			if (_active == false) return;
			
			if (mouseIsDown) {
				_view.scaleX = ((oriDim.x + (e.stageX - mouseLastPos.x) * 2) / oriDim.x) * oriScale.x;
				_view.scaleY = ((oriDim.y + (e.stageY - mouseLastPos.y) * 2) / oriDim.y) * oriScale.y;
			}
		}
		
		protected override function mouseDown(e:MouseEvent):void {
			if (_active == false) return;
			
			e.stopPropagation();
			super.mouseDown(e);
			
			mouseLastPos.x = e.stageX;
			mouseLastPos.y = e.stageY;
			
			oriScale.x = _view.scaleX;
			oriScale.y = _view.scaleY;
			
			oriDim.x = _view.width;
			oriDim.y = _view.height;
		}
		
	}

}