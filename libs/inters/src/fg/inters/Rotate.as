package fg.inters {
	import fg.Debugger;
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	public class Rotate extends BaseInter {
		
		protected var radToAngle:Number = 180.0 / Math.PI;
		protected var viewLastAngle:Number;
		protected var mouseLastAngle:Number;
		protected var stagePos:Point;
		protected var p:Point = new Point();
		
		public function Rotate(view:DisplayObject, stageRef:Stage) {
			this._view = view;
			this.stageRef = stageRef;
			addListener();
			trace('rotate created ' + view);
		}
		
		protected function getAngle(i:Number, j:Number):Number {
			var angle:Number;
			
			p.x = 0;
			p.y = 0;
			stagePos = _view.localToGlobal(p);
			
			angle = Math.atan2( - (j - stagePos.y), (i - stagePos.x));
			angle *= radToAngle;
			
			return IUtil.inst.angleAbs(angle);
		}
		
		protected override function mouseMove(e:MouseEvent):void {
			var angle:Number;
			var deltaAngle:Number;
			
			if (_active == false) return;
			
			if (mouseIsDown) {
				angle = getAngle(e.stageX, e.stageY);
				deltaAngle = IUtil.inst.angleGap(mouseLastAngle, angle);
				
				this._view.rotation = IUtil.inst.angleToRot(viewLastAngle + deltaAngle);
				
				var str:String;
				str  = 'Mouse Move: \n';
				str += 'delta anggle: ' + deltaAngle + '\n';
				str += 'mouse Angle: ' + angle + '\n';
				str += 'mouseLastAngle: ' + mouseLastAngle + '\n';
				Debugger.inst.watchWrite(str);
			} else {
				Debugger.inst.watchWrite('mouse is not pressed');
			}
		}
		
		protected override function mouseDown(e:MouseEvent):void {
			
			if (_active == false) {
				Debugger.inst.watchWrite('rotate is not active');
				return;
			} else {
			}
			
			e.stopPropagation();
			super.mouseDown(e);
			
			mouseLastPos.x = e.stageX;
			mouseLastPos.y = e.stageY;
			viewLastAngle = IUtil.inst.rotToAngle(_view.rotation);
			
			mouseLastAngle = getAngle(e.stageX, e.stageY);			
		}
		
	}

}