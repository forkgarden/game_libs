package fg.inters {
	import flash.display.DisplayObject;
	import flash.events.MouseEvent;
	import flash.display.Stage;
	import flash.geom.Point;
	import flash.display.Sprite;
	
	
	public class BaseInter {
		
		protected var viewLastPos:Point = new Point();
		protected var viewStagePos:Point = new Point();
		
		protected var mouseIsDown:Boolean = false;
		protected var mouseLastPos:Point = new Point();
		protected var _view:DisplayObject;
		protected var _active:Boolean = false;
		protected var stageRef:Stage;
		protected var pRes:Point = new Point();
		protected var p:Point = new Point();
		
		public function BaseInter() {
		
		}
		
		protected function addListener():void {
			this._view.addEventListener(MouseEvent.MOUSE_DOWN, mouseDown, false, 0, true);
			//_view.stage.addEventListener(MouseEvent.MOUSE_MOVE, mouseMove, false, 0, true);
			//_view.stage.addEventListener(MouseEvent.MOUSE_UP, mouseUp, false, 0, true);		
		}
		
		public function start():void {
			_active = true;
		}
		
		public function stop():void {
			_active = false;
		}
		
		public function removeListener():void {
			this._view.removeEventListener(MouseEvent.MOUSE_DOWN, mouseDown);
			stageRef.removeEventListener(MouseEvent.MOUSE_MOVE, mouseMove);
			stageRef.removeEventListener(MouseEvent.MOUSE_UP, mouseUp);		
		}
		
		protected function mouseUp(e:MouseEvent):void {
			mouseIsDown = false;
		}
		
		protected function mouseMove(e:MouseEvent):void {
		
		}
		
		protected function mouseDown(e:MouseEvent):void {
			var p:Point;
			
			if (_active) {
				e.stopPropagation();
				
				mouseLastPos.x = e.stageX;
				mouseLastPos.y = e.stageY;
				
				p = _view.localToGlobal(new Point(_view.x, _view.y));
				viewStagePos.x = p.x;
				viewStagePos.y = p.y;
				
				viewLastPos.x = p.x;
				viewLastPos.y = p.y;
			}
		}
		
		public function get view():DisplayObject {
			return _view;
		}
		
		public function set view(value:DisplayObject):void {
			_view = value;
		}
	}

}