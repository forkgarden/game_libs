package fg.inters {
	import flash.display.Bitmap;
	import flash.display.DisplayObject;
	import flash.display.SimpleButton;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.net.navigateToURL;
	import flash.net.URLRequest;
	
	public class IUtil {
		
		public static var inst:IUtil;
		
		public function IUtil() {
			if (inst) throw new Error();
			inst = this;
			init();
		}
		
		private function init():void {
			
		}
		
		public function angleAbs(n:Number):Number {
			if (n < 0) return 360 + n;
			return n;
		}
		
		public function angleGap(angle1:Number, angle2:Number):Number {
			var gap:Number;
			
			if (angle2 < angle1) {
				angle2 += 360;
			}
			
			gap = angle2 - angle1;
			if (gap > 180) {
				return -(angle1 + 360 - angle2);
			} else {
				return gap;
			}
		}
		
		public function angleToRot(n:Number):Number {
			n = n % 360;
			
			if (n < 0) {
				n = 360 + n;
			}
			
			if (n >= 0 && n < 180) {
				return -n;
			} else {
				return 360 - n;
			}
		}
		
		public function rotToAngle(n:Number):Number {
			if (n >= 0) {
				return 360 - n;
			} else {
				return -n;
			}
		}
		
	}

}