package fg.inters {
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.text.TextField;
	//import datas.Data;
	
	public class ScaleHor {
		
		protected var mouseIsDown:Boolean = false;
		protected var mouseLastPos:Point = new Point();
		private var viewLastScale:Point = new Point();
		private var _moveTarget:DisplayObject;
		private var _panah:MovieClip;
		private var _view:MovieClip;
		
		public function ScaleHor() {

		}
		
		private function mouseUp(e:MouseEvent):void {
			mouseIsDown = false;
			
			//adjust position
			_panah.x = _moveTarget.x;
			_panah.y = _moveTarget.y;
			
			view.stage.removeEventListener(MouseEvent.MOUSE_MOVE, mouseMove);
			view.stage.removeEventListener(MouseEvent.MOUSE_UP, mouseUp);
			//Data.getInst().pageCr.getElByDisp(_moveTarget).updateFromFla();
		}
		
		private function mouseMove(e:MouseEvent):void {
			var teks:TextField;
			
			if (mouseIsDown) {
				if (_moveTarget is TextField) {
					teks = _moveTarget as TextField;
					teks.width = viewLastScale.x + (e.stageX - mouseLastPos.x);
				}
				else {
					_moveTarget.scaleX = viewLastScale.x + (e.stageX - mouseLastPos.x) * .005;
					_panah.x = _moveTarget.x;
					_panah.y = _moveTarget.y;
				}
			}
		}
		
		private function mouseDown(e:MouseEvent):void {
			mouseIsDown = true;
				
			mouseLastPos.x = e.stageX;
			mouseLastPos.y = e.stageY;
			
			viewLastScale.x = _moveTarget.scaleX;
			viewLastScale.y = _moveTarget.scaleY;
			
			if (_moveTarget is TextField) {
				viewLastScale.x = (_moveTarget as TextField).width;
				viewLastScale.y = (_moveTarget as TextField).height;
			}
			
			view.stage.addEventListener(MouseEvent.MOUSE_MOVE, mouseMove, false, 0, true);
			view.stage.addEventListener(MouseEvent.MOUSE_UP, mouseUp, false, 0, true);			
		}	
		
		public function get view():MovieClip {
			return _view;
		}
		
		public function set view(value:MovieClip):void {
			_view = value;
			_view.addEventListener(MouseEvent.MOUSE_DOWN, mouseDown);
		}
		
		public function get panah():MovieClip {
			return _panah;
		}
		
		public function set panah(value:MovieClip):void {
			_panah = value;
		}
		
		public function get moveTarget():DisplayObject {
			return _moveTarget;
		}
		
		public function set moveTarget(value:DisplayObject):void {
			_moveTarget = value;
		}
		
		public function destroy():void {
			if (_view) {
				_view.removeEventListener(MouseEvent.MOUSE_DOWN, mouseDown);
				_view.stage.removeEventListener(MouseEvent.MOUSE_MOVE, mouseMove);
				_view.stage.removeEventListener(MouseEvent.MOUSE_UP, mouseUp);
			}
			
			_view = null;
			_panah = null;
			_moveTarget = null;
		}
		
	}

}