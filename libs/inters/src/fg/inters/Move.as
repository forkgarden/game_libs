package fg.inters {
	import flash.display.DisplayObject;
	import flash.display.Stage;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	public class Move {
		
		protected var viewLastPos:Point = new Point();
		protected var viewStagePos:Point = new Point();
		
		protected var mouseLastPos:Point = new Point();
		protected var _view:DisplayObject;
		protected var pRes:Point = new Point();
		protected var p:Point = new Point();
		protected var _scale:Boolean = true;
		
		protected var _lockX:Boolean = false;
		protected var _lockY:Boolean = false;
		protected var _callBackMove:Function;
		protected var _callBackMouseDown:Function;
		protected var _callBackMouseUp:Function;
		
		protected static var _isMoving:Boolean = false;
		
		public function Move(view:DisplayObject, stageRef:Stage) {
			this._view = view;
			this._view.addEventListener(MouseEvent.MOUSE_DOWN, mouseDown, false, 0, true);			
		}
		
		protected function mouseUp(e:MouseEvent):void {
			_view.stage.removeEventListener(MouseEvent.MOUSE_MOVE, mouseMove);
			_view.stage.removeEventListener(MouseEvent.MOUSE_UP, mouseUp);
			
			if (_callBackMouseUp != null) _callBackMouseUp();
		}
		
		public function destroy():void {
			pause();
			_view = null;
		}
		
		public function pause():void {
			if (null != _view) {
				_view.removeEventListener(MouseEvent.MOUSE_DOWN, mouseDown);
			}
		}
		
		public function resume():void {
			if (null != _view) {
				_view.removeEventListener(MouseEvent.MOUSE_DOWN, mouseDown);
				_view = null;
			}			
		}
		
		protected function mouseMove(e:MouseEvent):void {
			var dx:Number;
			var dy:Number;
			
			dx = e.stageX - mouseLastPos.x;
			dy = e.stageY - mouseLastPos.y;
			
			if (Math.abs(dx) > 8 || Math.abs(dy) > 8) {
				_isMoving = true;
			}
			
			viewStagePos.x = viewLastPos.x + (e.stageX - mouseLastPos.x);
			viewStagePos.y = viewLastPos.y + (e.stageY - mouseLastPos.y);
			
			if (_scale) {
				pRes = _view.parent.globalToLocal(viewStagePos);
			}
			else {
				pRes.x = viewStagePos.x;
				pRes.y = viewStagePos.y;
			}
			
			if (_lockX == false) _view.x = pRes.x;
			if (_lockY == false) _view.y = pRes.y;
			
			if (null != _callBackMove) {
				_callBackMove(dx, dy);
			}
		}
		
		protected function mouseDown(e:MouseEvent):void {
			e.stopPropagation();
			
			_isMoving = false;
			
			mouseLastPos.x = e.stageX;
			mouseLastPos.y = e.stageY;
			
			if (_scale) {
				pRes = _view.parent.localToGlobal(new Point(_view.x, _view.y));
				
				viewLastPos.x = pRes.x;
				viewLastPos.y = pRes.y;
			}
			else {
				viewLastPos.x = _view.x;
				viewLastPos.y = _view.y;				
			}
			
			_view.stage.addEventListener(MouseEvent.MOUSE_MOVE, mouseMove);
			_view.stage.addEventListener(MouseEvent.MOUSE_UP, mouseUp);
			
			if (_callBackMouseDown != null) _callBackMouseDown();
		}
		
		public function get lockX():Boolean {
			return _lockX;
		}
		
		public function set lockX(value:Boolean):void {
			_lockX = value;
		}
		
		public function get callBackMove():Function {
			return _callBackMove;
		}
		
		public function set callBackMove(value:Function):void {
			_callBackMove = value;
		}
		
		public function get lockY():Boolean {
			return _lockY;
		}
		
		public function set lockY(value:Boolean):void {
			_lockY = value;
		}
		
		public function get scale():Boolean {
			return _scale;
		}
		
		public function set scale(value:Boolean):void {
			_scale = value;
		}
		
		public function get callBackMouseUp():Function 
		{
			return _callBackMouseUp;
		}
		
		public function set callBackMouseUp(value:Function):void 
		{
			_callBackMouseUp = value;
		}
		
		public function get callBackMouseDown():Function 
		{
			return _callBackMouseDown;
		}
		
		public function set callBackMouseDown(value:Function):void 
		{
			_callBackMouseDown = value;
		}
		
		static public function get isMoving():Boolean 
		{
			return _isMoving;
		}
	}
	
}