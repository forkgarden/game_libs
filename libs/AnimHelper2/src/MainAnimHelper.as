package {
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.Event;
	import fg.anims.AnimFrame;
	
	public class MainAnimHelper extends Sprite {
		
		[Embed(source = "../lib/bot01.png")]
		protected var Frame1:Class;
		
		[Embed(source = "../lib/bot02.png")]
		protected var Frame2:Class;
		
		[Embed(source = "../lib/bot03.png")]
		protected var Frame3:Class;
		
		[Embed(source="../lib/bot04.png")]
		protected var Frame4:Class;
		
		protected var disps:Vector.<Bitmap> = new Vector.<Bitmap>();
		protected var anim:AnimFrame;
		
		public function MainAnimHelper() {
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		protected function init(e:Event = null):void {
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			// entry point
			anim = new AnimFrame();
			anim.addFrameAnim('jalan', [0, 1, 2, 3]);
			anim.addDisp(new Frame1());
			anim.addDisp(new Frame2());
			anim.addDisp(new Frame3());
			anim.addDisp(new Frame4());
			anim.animate('jalan', true);
			addChild(anim);
			
			addEventListener(Event.ENTER_FRAME, update, false, 0, true);
		}
		
		protected function update(e:Event):void {
			anim.update();
		}
		
	}
	
}