package fg.anims {
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import fg.anims.FrameArray;

	public class AnimFrame extends Sprite {
		
		protected var frames:Vector.<FrameArray> = new Vector.<FrameArray>();
		protected var _frameCr:FrameArray;
		protected var _disps:Vector.<DisplayObject> = new Vector.<DisplayObject>();
		
		public function AnimFrame() {
			 
		}
		
		public function addDisp(d:DisplayObject, i:int = 0, j:int = 0):void {
			d.x = i;
			d.y = j;
			_disps.push(d);
		}
		
		protected function getById(id:String):FrameArray {
			var frame:FrameArray;
			
			for each (frame in frames) {
				if (frame.id == id) return frame;
			}
			
			return null;
		}
		
		public function addFrameAnim(id:String, arr:Array):void {
			var frame:FrameArray;
			
			if (getById(id) == null) {
				frame = new FrameArray(id, arr);
				frames.push(frame);
			}
		}
		
		public function goToFrame(idx:int):void {
			if (_frameCr) _frameCr.stop();
			
			removeChildren();
			addChild(_disps[idx]);
		}
		
		public function animate(id:String = null, loop:Boolean = false):void {
			if (id == null) {
				_frameCr.start(loop);
			}
			
			_frameCr = getById(id);
			_frameCr.start(loop);
		}
		
		public function update():void {
			if (_frameCr) _frameCr.update();
			
			removeChildren();
			addChild(_disps[_frameCr.frame]);
		}
		
		public function get frameCr():FrameArray {
			return _frameCr;
		}
		
		public function get disps():Vector.<DisplayObject> {
			return _disps;
		}
		
	}

}