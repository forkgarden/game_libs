package fg.anims {
	 
	public class FrameArray {
		
		protected var _id:String;
		protected var _ar:Array;
		protected var _aktif:Boolean = false;
		protected var _idx:int = 0;
		protected var _loop:Boolean = false;
		protected var _finish:Boolean = false;
		protected var framePertama:Boolean = false;
		
		public function FrameArray(id:String, ar:Array) {
			_id = id;
			_ar = ar;
		}
		
		public function start(loop:Boolean = false):void {
			_aktif = true;
			_idx = 0;
			_finish = false;
			_loop = loop;
			framePertama = true;
		}
		
		public function stop():void {
			_aktif = false;
		}
		
		public function update():void {
			if (_aktif) {
				if (framePertama) {
					framePertama = false;
				}
				else {
					_idx++;
				}
				
				if (_idx >= _ar.length) {
					if (_loop) {
						_idx = 0;
					}
					else {
						_finish = true;
						_aktif = false;
					}
				}
			}
			
			//trace('update ' + _idx + '/loop ' + _loop);
		}
		
		public function get id():String {
			return _id;
		}
		
		public function get aktif():Boolean {
			return _aktif;
		}
		
		public function get frame():int {
			var res:int = 0;
			var idx2:int = _idx;
			
			if (idx2 < 0) idx2 = 0;
			if (idx2 >= _ar.length) idx2 = _ar.length - 1;
			
			return _ar[idx2];
		}
		
		public function get loop():Boolean {
			return _loop;
		}
		
		public function get finish():Boolean {
			return _finish;
		}
		
	}

}