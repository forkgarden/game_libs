package fg.hstates {
	
	public class State {
		protected var _lists:Vector.<State> = new Vector.<State>();
		protected var _id:String = '';
		protected var _active:State = null;
		protected var _parent:State;
		protected var _activeRec:State;
		
		public function State(id:String = '') {
			_id = id;
		}
		
		public function adds(ar:Array):void {
			var str:String;
			var state:State;
			
			for each (str in ar) {
				state = new State(str);
				state.parent = this;
				_lists.push(state);
			}
		}
		
		public function setAsActiveState():void {
			if (_parent) {
				_parent.active = this;
				_parent.setAsActiveState();
			}
		}
		
		//public function setActive(id:String):State {
			//_active = select(id);
			//return this;
		//}
		
		public function select(id:String):State {
			var state:State;
			
			for each (state in _lists) {
				if (state.id == id) return state;
			}
			
			return null;
		}
		
		public function get lists():Vector.<State> {
			return _lists;
		}
		
		public function get id():String {
			return _id;
		}
		
		public function get active():State {
			return _active;
		}
		
		public function set active(value:State):void {
			_active = value;
		}
		
		public function get parent():State {
			return _parent;
		}
		
		public function set parent(value:State):void {
			_parent = value;
		}
		
		public function get activeRec():State {
			var res:State;
			
			res = _active;
			
			if (_active.lists.length > 0) {
				res = _active.activeRec;
			}
			
			return res;
		}
				
	}

}