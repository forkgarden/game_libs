package fg.hstates {
	/**
	 * ...
	 * @author 
	 */
	public class StateManager {
		
		protected var _lists:Vector.<State> = new Vector.<State>();
		protected var _active:State;
		
		public function StateManager() {
			
		}
		
		public function adds(ar:Array):void {
			var str:String;
			var state:State;
			
			for each (str in ar) {
				state = new State(str);
				_lists.push(state);
			}
			
		}		
		
		public function select(id:String):State {
			var state:State;
			
			for each (state in _lists) {
				if (state.id == id) return state;
			}
			
			return null;
		}		
		
		public function setActive(id:String):StateManager {
			_active = select(id);
			return this;
		}
		
		public function get lists():Vector.<State> {
			return _lists;
		}
		
		public function set lists(value:Vector.<State>):void {
			_lists = value;
		}
		
		public function get active():State {
			return _active;
		}
		
		public function set active(value:State):void {
			_active = value;
		}
		
	}

}