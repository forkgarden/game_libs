package{
	import fg.hstates.State;
	import fg.hstates.StateManager;
	import flash.display.Sprite;
	import flash.events.Event;
	

	public class MainState extends Sprite {
		
		protected var state:StateManager;
		protected var state2:State;
		
		public function MainState() {
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event = null):void {
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			state2 = new State();
			state2.adds(['idle', 'jalan', 'serang']);
			state2.select('serang').adds(['kejar', 'serang']);
			state2.select('serang').select('serang').adds(['charge', 'tembak']);
			
			state2.select('serang').select('serang').select('charge').setAsActiveState();
			
			trace(state2.active.active.id);
			trace(state2.activeRec.id);
		}
		
	}
	
}