package fg {
	import flash.display.Sprite;
	import flash.events.Event;
	
	public class Easing extends Sprite {
		
		private static var easings:Vector.<Easing> = new Vector.<Easing>();
		
		private var src:Number;
		private var target:Number;
		private var _stepIdx:Number;
		private var easingParam:Number;
		private var stepCount:Number;
		private var callBack:Function;
		private var onFinish:Function;
		
		public function Easing() {
			addEventListener(Event.ENTER_FRAME, update);
		}
		
		public function start(src:Number, target:Number, stepCount:Number, easingParam:Number, onUpdate:Function, onFinish:Function):void {
			this.src = src;
			this.target = target;
			this.stepCount = stepCount;
			this._stepIdx = 0;
			this.callBack = onUpdate;
			this.onFinish = onFinish;
			this.easingParam = easingParam;
		}
		
		public static function create():Easing {
			var easing:Easing = new Easing();
			
			easings.push(easing);
			return easing;
		}
		
		public function update(e:Event):void {
			var easing:Easing;
			var len:int;
			var i:int;
			var temp:Function;
			
			if (_stepIdx <= stepCount) {
				
				if (callBack != null) callBack(getEasingValue(src, target, _stepIdx, stepCount, easingParam));
				
				if (_stepIdx == stepCount) {
					if (onFinish != null) {
						temp = onFinish;
						onFinish = null;
						temp();
						temp = null;
					}
				}
				
				_stepIdx++;
			}
			
			len = easings.length;
			for (i = len - 1; i >= 0; i--) {
				if (easings[i]._stepIdx == easings[i].stepCount) {
					easings.slice(i, 1);
				}
			}
			
		}
		
		public function getEasingValue(src:Number, target:Number, _stepIdx:Number, stepCount:Number, easingLevel:Number=1):Number {
			var stepVal:Number = 0;
			var easingVal:Number = 0;
			var _stepIdxAB:Number = 0;
			var dist:Number = 0;
			
			dist = target - src;
			stepVal = (target - src) / stepCount;
			_stepIdxAB = (_stepIdx * stepVal) / dist; //0 - 1
			
			easingVal = 1.0 - _stepIdxAB;
			easingVal *= easingLevel;
			
			return (_stepIdxAB - (_stepIdxAB * easingVal)) * dist;
		}
		
		public function get stepIdx():Number {
			return _stepIdx;
		}
		
	}

}