package{
	import fg.Easing;
	import flash.display.Sprite;
	import flash.events.Event;
	
	public class Main extends Sprite {
						
		public function Main() {
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event = null):void {
			removeEventListener(Event.ADDED_TO_STAGE, init);
				
			var spr:Sprite = new Sprite();
			var easing:Easing = new Easing();
			spr.graphics.beginFill(0);
			spr.graphics.drawCircle(0, 0, 20);
			spr.graphics.endFill();
			
			trace('get easing ' + easing.getEasingValue(500, 0, 1, 20, 1));
			
			addChild(spr);
			
			easing.start(
				0,
				500,
				20,
				1,
				function (n):void {
					spr.x = n;
					spr.y = 20;
				},
				function ():void {
					trace('finish');
					easing.start(0, 500, 20, -1, function (n):void {
						trace('on update ' + n);
						spr.x = 500 - n;
						spr.y = 20;
					}, function ():void {})
				});
		}
		
	}
	
}