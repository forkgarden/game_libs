package fg.base64s 
{
	
	public class Base64 
	{
		protected var base64Table:String = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';		
		
		public function Base64() 
		{
			
		}
		
		public function encode(str:String):String {
			var res:String;
			var bin:String;
			var mod3:Number;
			
			bin = strTobin(str, 8);
			mod3 = bin.length % 24;
			
			if (mod3 == 8) {
				bin += padStr('', 16);
			}
			else if (mod3 == 16) {
				bin += padStr('', 8);
			}
			
			res = binTo64(bin);
			
			if (mod3 == 8) {
				res = res.slice(0, res.length - 2) + "==";
			}
			else if (mod3 == 16) {
				res = res.slice(0, res.length - 1) + "=";
			}
			
			return res;
		}
		
		protected function binTo64(bin:String):String {
			var char6:String;
			var code:Number;
			var res:String = '';
			var char64:String = '';
			
			//trace('bin264 ' + bin);
			
			while (bin.length > 0) {
				char6 = bin.slice(0, 6);
				code = binToNum(char6);
				char64 = base64Table.charAt(code);
				res += char64;
				
				bin = bin.slice(6, int.MAX_VALUE);
				
				//trace('char6 ' + char6 + '/code ' + code + '/char64 ' + char64);
			}
			
			return res;
		}
		
		public function strTobin(str:String, pad:Number = 8):String {
			var res:String;
			var i:int;
			var code:Number = 0;
			var bin:String;
			
			res = '';
			for (i = 0; i < str.length; i++) {
				code = str.charCodeAt(i);
				bin = numTobin(code);
				res += padStr(bin, pad);
			}
			
			return res;
		}
		
		protected function padStr(str:String, len:Number = 8, pad:String = '0'):String {
			var res:String;
			
			res = str;
			while (res.length < len) {
				res = pad + res;
			}
			
			return res;
		}
		
		public function binToNum(bin:String):Number {
			var i:int;
			var res:Number = 0;
			
			for (i = bin.length - 1; i >= 0; i--) {
				
				res += (parseInt(bin.charAt(i)) * (Math.pow(2, (bin.length-1) - i)));
			}
			
			return res;
		}
		
		public function numTobin(num:Number):String {
			var hasilBagi:Number;
			var sisaBagi:Number;
			var res:String = '';
			
			if (num == 0) {
				return "0";
			}
			else if (num == 1) {
				return "1";
			}
			
			else {
				
				while (true) {
					hasilBagi = Math.floor(num / 2);
					sisaBagi = num % 2;
					
					res = sisaBagi + res;
					
					if (hasilBagi == 1) {
						return "1" + res;
					}
					else {
						num = hasilBagi;
					}
				}
				
			}
			
			throw new Error();
			return '';
		}		
		
		public function decode(str:String):String {
			var bin:String = '';
			var bin64:String = '';
			var char8:String;
			var code64:Number;
			var i:int;
			var res:String = '';
			var char:String = '';
			
			for (i = 0; i < str.length; i++) {
				
				code64 = base64Table.indexOf(str.charAt(i));
				if (str.charAt(i) == '=') code64 = 0;
				
				if (code64 >= 0) {
					bin64 = numTobin(code64);
					bin += padStr(bin64, 6);
				}
				
			}
			
			res = '';
			while (bin.length > 0) {
				char8 = bin.slice(0, 8);
				bin = bin.slice(8, int.MAX_VALUE);
				char = String.fromCharCode(binToNum(char8));
				res += char;
			}
			
			if (str.slice(str.length - 2, int.MAX_VALUE) == "==") {
				res = res.slice(0, res.length - 2);
			}
			else if (str.slice(str.length - 1, int.MAX_VALUE) == "=") {
				res = res.slice(0, res.length - 1);
			}
			
			
			return res;
		}
		
	}

}