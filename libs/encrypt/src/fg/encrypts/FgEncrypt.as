package fg.encrypts 
{
	import fg.base64s.Base64;

	public class FgEncrypt 
	{
		
		protected var base64:Base64;
		
		public function FgEncrypt() 
		{
			base64 = new Base64();
		}
		
		protected function strToArray(msg:String):Array {
			var i:int;
			var res:Array;
			
			res = [];
			for (i = 0; i < msg.length; i++) {
				res.push(msg.charCodeAt(i));
			}
			
			//trace('str to array, msg ' + msg + '/res ' + res);
			
			return res;
		}
		
		protected function padArray(array:Array, n:int):Array {
			var res:Array;
			var obj:Object;
			var i:int;
			
			res = [];
			for (i = 0; i < array.length; i++) {
				if (res.length < n) {
					res.push(array[i]);
				}
			}
			
			i = 0;
			while (res.length < n) {
				i++;
				res.push(res[i]);
			}
			
			return res;
		}
		
		public function decrypt(msg:String, key:String):String {
			var i:Number;
			var resAr:Array = [];
			var resStr:String;
			var msgDecoded:String;
			var msgAr:Array;
			var keyAr:Array;
			var char:String;
			
			//trace('msg 64 ' + msg);
			
			msgDecoded = base64.decode(msg);
			
			msgAr = strToArray(msgDecoded);
			keyAr = padArray(strToArray(key), msg.length);
			
			//trace('pesan 2 ' + msgAr);
			
			resAr = [];
			for (i = 0; i < msgAr.length; i++) {
				resAr.push(msgAr[i] ^ keyAr[i]);
			}
			
			//trace('pesan 1 ' + resAr);
			
			resStr = '';
			for (i = 0; i < resAr.length; i++) {
				resStr += String.fromCharCode(resAr[i]);
			}
			
			return resStr;
		}
		
		public function encrypt(msg:String, key:String):String {
			var i:Number;
			var resAr:Array = [];
			var resStr:String;
			var msgAr:Array;
			var keyAr:Array;
			var res64:String;
			
			msgAr = strToArray(msg);
			keyAr = padArray(strToArray(key), msg.length);
			
			//trace('pesan 1 ' + msgAr);
			
			
			for (i = 0; i < msgAr.length; i++) {
				resAr.push(msgAr[i] ^ keyAr[i]);
			}
			
			//trace('pesan 2 ' + resAr);
			
			resStr = '';
			for (i = 0; i < resAr.length; i++) {
				resStr += String.fromCharCode(resAr[i]);
			}
			
			res64 = base64.encode(resStr);
			
			//trace('b64 ' + res64);
			
			return res64;
		}
		
		protected function padStr(str:String, len:Number):String {
			var index:int = 0;
			
			while (str.length < len) {
				str += str.charCodeAt(index);
				index++;
			}
			
			return str;
		}
		
	}

}