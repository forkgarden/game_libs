package
{
	import fg.base64s.Base64;
	import fg.encrypts.FgEncrypt;
	import flash.display.Sprite;
	import flash.events.Event;
	
	public class MainEnc extends Sprite 
	{
		protected var enc:FgEncrypt;
		
		public function MainEnc() 
		{
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event = null):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			enc = new FgEncrypt();
			var b64:Base64;
			var res:String;
			var res64:String;
			var resDec:String;
			
			b64 = new Base64();
			
			res = 'ABCdf';
			res64 = b64.encode(res);
			resDec = b64.decode(res64);
			
			//trace('res ' + res);
			//trace('encode res ' + res64);
			//trace('decode ' + resDec + '/len ' + resDec.length);
			
			trace(enc.decrypt(enc.encrypt(res, '123'), '123'));			
		}
	}
	
}