package fg.ui 
{
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	//TODO: change so that this doesn't extends sprite
	public class VScrollBar extends Sprite
	{
		protected var tombol:Panel3D = new Panel3D();
		protected var bar:Panel3D = new Panel3D();
		protected var tombolPos:Point = new Point();
		protected var mousePos:Point = new Point();
		protected var _percent:Number = 0;
		protected var _mouseMoveCallBack:Function;
		
		public function VScrollBar() 
		{
			bar.reverse = true;
			tombol.view.addEventListener(MouseEvent.MOUSE_DOWN, tombolOnMouseDown, false, 0, true);
			
			tombol.width = (10);
			tombol.height = (10);
			bar.width = (5);
			bar.height = (100);
			
			addChild(bar.view);
			addChild(tombol.view);
			
			setWidth(10);
		}
		
		public function destroy():void {
			removeChildren();
			
			tombol.destroy();
			bar.destroy();
			_mouseMoveCallBack = null;
			
			tombol = null;
			bar = null;
			mousePos = null;
			tombolPos = null;
		}
		
		protected function tombolOnMouseDown(evt:MouseEvent):void {
			evt.stopPropagation();
			
			tombol.view.stage.addEventListener(MouseEvent.MOUSE_MOVE, tombolOnMouseMove, false, 0, true);
			tombol.view.stage.addEventListener(MouseEvent.MOUSE_UP, tombolOnMouseUp, false, 0, true);
			
			tombolPos.y = tombol.view.y;
			mousePos.y = evt.stageY;
		}
		
		protected function tombolOnMouseMove(evt:MouseEvent):void {
			evt.stopPropagation();
			
			tombol.view.y = tombolPos.y + (evt.stageY - mousePos.y);
			
			if (tombol.view.y > (bar.height - tombol.height)) {
				tombol.view.y = (bar.height - tombol.height);
			}
			
			if (tombol.view.y < 0) tombol.view.y = 0;
			
			_percent = (tombol.view.y / (bar.height - tombol.height));
			if (_mouseMoveCallBack != null) _mouseMoveCallBack();			
		}
		
		protected function tombolOnMouseUp(evt:MouseEvent):void {
			evt.stopPropagation();
			
			tombol.view.stage.removeEventListener(MouseEvent.MOUSE_MOVE, tombolOnMouseMove);
			tombol.view.stage.removeEventListener(MouseEvent.MOUSE_UP, tombolOnMouseUp);
		}
		
		public function setHeight(value:Number):void {
			bar.height = (value);
		}
		
		public function setWidth(value:Number):void {
			tombol.width = (value);
			bar.width = (value);
		}
		
		public function get percent():Number 
		{
			return _percent;
		}
		
		public function set percent(value:Number):void 
		{
			_percent = value;
		}
		
		public function get mouseMoveCallBack():Function 
		{
			return _mouseMoveCallBack;
		}
		
		public function set mouseMoveCallBack(value:Function):void 
		{
			_mouseMoveCallBack = value;
		}
	}

}