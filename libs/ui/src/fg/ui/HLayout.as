package fg.ui {
	import flash.display.DisplayObject;

	public class HLayout extends BaseLayout {
		
		public function HLayout() {
			super();
		}
		
		public override function refresh():void {
			var d:DisplayObject;
			var posX:int = 0;
			
			removeChildren();
			//flAddChildSafe = true;
			
			for each (d in items) {
				d.x = posX;
				d.y = 0;
				addChild(d);
				
				if (d is VLayout) {
					(d as VLayout).refresh();;
				}
				
				posX += d.width;
			}
			
			//flAddChildSafe = false;
		}
		
	}

}