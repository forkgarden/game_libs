package fg.ui {
	import flash.display.Sprite;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	/**
	 * ...
	 * @author test
	 */
	public class CTombol {
		
		private var panel3D:Panel3D;
		private var panel3DPencet:Panel3D;
		private var _view:Sprite;
		private var _clickCallBack:Function;
		private var _width:Number;
		private var _height:Number;
		
		public function CTombol() {
			panel3D = new Panel3D();
			panel3D.view.mouseEnabled = false;
			panel3DPencet = new Panel3D();
			panel3DPencet.setMode(1);
			panel3DPencet.view.mouseEnabled = false;
			
			_view = new Sprite();
			_view.mouseChildren = false;
			
			_view.addChild(panel3DPencet.view);
			_view.addChild(panel3D.view);
			
			_view.addEventListener(MouseEvent.CLICK, panelOnClick);
			_view.addEventListener(MouseEvent.MOUSE_DOWN, panelMouseDown);
			_view.addEventListener(MouseEvent.MOUSE_UP, panelMouseUp);
			
			width = 100;
			height = 32;
		}
		
		private function panelMouseUp(e:MouseEvent):void {
			panel3D.view.visible = true;
		}
		
		private function panelMouseDown(e:MouseEvent):void {
			panel3D.view.visible = false;
		}
		
		private function panelOnClick(e:MouseEvent):void {
			if (null != _clickCallBack) {
				_clickCallBack();
			}
		}
		
		public function get view():Sprite {
			return _view;
		}
		
		public function get width():Number {
			return _width;
		}
		
		public function set width(value:Number):void {
			_width = value;
			panel3D.width = value;
			panel3DPencet.width = value;
		}
		
		public function get height():Number {
			return _height;
		}
		
		public function set height(value:Number):void {
			_height = value;
			panel3D.height = value;
			panel3DPencet.height = value;
		}
		
		public function get clickCallBack():Function {
			return _clickCallBack;
		}
		
		public function set clickCallBack(value:Function):void {
			_clickCallBack = value;
		}
		
	}

}