package fg.ui 
{
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	/**
	 * ...
	 * @author test
	 */
	public class TreeView extends Sprite
	{
		protected var vLayout:VLayout;
		protected var hLayout:HLayout;
		protected var childLayout:VLayout;
		protected var _label:Teks;
		protected var tombol:Tombol;
		protected var _childs:Vector.<TreeView>;
		protected var back:Panel3D;
		protected var collapsed:Boolean = false;
		protected var _id:String = '';
		protected var _data:Object = {};
		protected var _parentTree:TreeView;
		
		protected static var _head:TreeView = null;
		protected static var _aktifTree:TreeView = null;
		
		public function TreeView(data:Object = null) 
		{
			if (_head == null) _head = this;
			
			tombol = new Tombol();
			tombol.label = '[+]';
			tombol.width = 40;
			tombol.onClick = tombolOnClick;
			
			hLayout = new HLayout();
			hLayout.addItem(tombol);
			hLayout.addItem(createLabel());
			
			childLayout = new VLayout();
			childLayout.visible = false;
			vLayout = new VLayout();
			vLayout.addItem(hLayout);
			vLayout.addItem(childLayout);
			childLayout.x = tombol.width;
			
			if (data) this._data = data;
			
			addChild(vLayout);
			
			_childs = new Vector.<TreeView>();
		}
		
		public static function create():TreeView {
			var res:TreeView;
			
			res = new TreeView();
			
			return res;
		}
		
		public override function removeChildren(beginIndex:int = 0, endIndex:int = 2147483647) : void {
			var t:TreeView;
			
			while (childs.length > 0) {
				t = childs.pop();
				t.destroy();
			}

		}
		
		public function destroy():void {
			var t:TreeView;
			
			if (_head) {
				_head = null;
			}
			
			tombol.destroy();
			hLayout.clear();
			vLayout.clear();
			back.destroy();
			
			while (_childs.length > 0) {
				t = _childs.pop();
				t.destroy();
			}
			
			removeChildren();
			tombol = null;
			hLayout = null;
			vLayout = null;
			back = null;
		}
		
		protected function tombolOnClick(tombol:Tombol):void {
			collapsed = !collapsed;
			TreeView.head.refresh();
		}
		
		public function open():void {
			collapsed = false;
			TreeView.head.refresh();
		}
		
		public function openRec():void {
			open();
			if (_parentTree) _parentTree.openRec();
		}
		
		public function refresh():void {
			var t:TreeView;
			
			if (collapsed) {
				childLayout.visible = false;
			}
			else {
				childLayout.visible = true;
				childLayout.clear();
				for each (t in _childs) {
					t.refresh();
					childLayout.addItem(t);
				}
			}
			vLayout.refresh();
			
			if (_childs.length == 0) {
				tombol.label = '';
			}
			else if (collapsed) {
				tombol.label = '[-]';
			}
			else {
				tombol.label = '[+]';
			}
			
		}
		
		protected function createLabel():Sprite {
			var spr:Sprite;
			
			spr = new Sprite();
			
			back = new Panel3D();
			back.width = 120;
			back.reverse = true;
			
			_label = new Teks();
			_label.width = 112;
			_label.x  = 4;
			_label.text = 'label';
			_label.height = label.textHeight + 4;
			
			_label.y = back.height / 2 - (_label.textHeight + 4) / 2;
			
			spr.addChild(back);
			spr.addChild(_label);
			
			return spr;
		}
		
		public function addItem(t:TreeView):void {
			_childs.push(t);
			_head.refresh();
			_head.refresh();
		}
		
		public function searchByView(view:DisplayObject):TreeView {
			var tChild:TreeView;
			var res:TreeView;
			
			if (_data.view == view) return this;
			
			for each (tChild in childs) {
				res = tChild.searchByView(view);
				if (res) return res;
			}
			
			return null;
		}
		
		//TODO:
		public function searchByLabel(label:String):TreeView {
			return null;
		}
		
		protected function searchNext(label:String, activeTree:TreeView):TreeView {
			var child:TreeView;
			var res:TreeView;
			
			//search this
			if (_label.text == label) return this;
			
			//search childs
			for each (child in _childs) {
				if (child.label.text == label) {
					res = child;
					return res;
				}
			}
			
			//search childs level 2
			for each (child in _childs) {
				res = child.searchByLabel(label);
				if (res) return res;
			}
			
			return null;
		}
		
		public function get label():Teks 
		{
			return _label;
		}
		
		public function get childs():Vector.<TreeView> 
		{
			return _childs;
		}
		
		static public function get head():TreeView 
		{
			return _head;
		}
		
		static public function get aktifTree():TreeView 
		{
			return _aktifTree;
		}
		
		public function get id():String 
		{
			return _id;
		}
		
		public function get data():Object 
		{
			return _data;
		}
		
		public function set parentTree(value:TreeView):void 
		{
			_parentTree = value;
		}
		
		
		
	}

}