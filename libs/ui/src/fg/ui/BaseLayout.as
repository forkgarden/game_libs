package fg.ui 
{
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	
	public class BaseLayout extends Sprite
	{
		
		protected var items:Vector.<DisplayObject>;
		protected var flAddChildSafe:Boolean = false;
		
		public function BaseLayout() 
		{
			items = new Vector.<DisplayObject>();			
		}
		
		public function addItem(d:DisplayObject):void {
			items.push(d);
			refresh();
		}
		
		public function refresh():void {
			
		}
		
		public function destroy():void {
			removeChildren();
			
			while (items.length > 0) items.pop();
		}
		
		public function clear():void {
			var d:DisplayObject;
			
			while (items.length > 0) {
				d = items.pop();
				if (d is BaseLayout) {
					(d as BaseLayout).clear();
				}
			}
			removeChildren();
		}		
		
		
		public function getItemTotalWidth():Number {
			var disp:DisplayObject;
			var res:Number = 0;
			
			for each (disp in items) {
				res += disp.width;
			}
			
			return res;
		}
		
		public function getItemTotalHeight():Number {
			var disp:DisplayObject;
			var res:Number = 0;
			
			for each (disp in items) {
				res += disp.height;
			}
			
			return res;
		}
		
		public function getItems():Vector.<DisplayObject> {
			return items;
		}
		
		//public function addItem(child:DisplayObject):void {
			//addChild(child);
		//}
		
		//public override function addChild(child:DisplayObject):DisplayObject {
			////trace('add child');
			//if (flAddChildSafe == false) throw new Error();
			//return super.addChild(child);
		//}
		
	}

}