package fg.ui 
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFieldType;
	
	public class Input extends Sprite {
		
		protected var _view:Sprite;
		protected var back:Panel3D;
		protected var _teks:Teks;
		protected var _defValue:String = '';
		protected var _onChange:Function;
		
		public function Input() 
		{			
			back = new Panel3D();
			back.mode = Panel3D.PANEL2;
			
			_teks = new Teks();
			_teks.type = TextFieldType.INPUT;
			_teks.mouseEnabled = true;
			_teks.addEventListener(Event.CHANGE, teksOnChange);
			
			width = 120;
			height = 33;
			
			addChild(back.view);
			addChild(_teks);
		}
		
		public function destroy():void {
			back.destroy();
			_teks.destroy();
			_onChange = null;
			removeChildren();
		}
		
		protected function teksOnChange(e:Event):void {
			if (_onChange != null) _onChange(_teks.text);
		}
		
		public override function set width(value:Number):void {
			back.width = value;
			_teks.width = value;
		}
		
		public override function set height(value:Number):void {
			back.height = value;
			_teks.y = back.height / 2 - (_teks.textHeight + 4) / 2;
			_teks.height = _teks.textHeight + 4;
		}
		
		public function get onChange():Function 
		{
			return _onChange;
		}
		
		public function set onChange(value:Function):void 
		{
			_onChange = value;
		}
		
		public function get teks():Teks 
		{
			return _teks;
		}
		
		public function get defValue():String 
		{
			return _defValue;
		}
		
		public function set defValue(value:String):void 
		{
			_defValue = value;
		}
		
	}

}