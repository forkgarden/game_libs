package fg.ui 
{
	import flash.geom.Point;
	/**
	 * ...
	 * @author test
	 */
	public class TransformData 
	{
		
		private var _position:Point = new Point();
		private var _width:Number = 0;
		private var _height:Number = 0;
		private var _scale:Point = new Point(1, 1);
		
		public function TransformData() 
		{
			
		}
		
		public function get position():Point 
		{
			return _position;
		}
		
		public function set position(value:Point):void 
		{
			_position = value;
		}
		
		public function get width():Number 
		{
			return _width;
		}
		
		public function set width(value:Number):void 
		{
			_width = value;
		}
		
		public function get height():Number 
		{
			return _height;
		}
		
		public function set height(value:Number):void 
		{
			_height = value;
		}
		
		public function get scale():Point 
		{
			return _scale;
		}
		
		public function set scale(value:Point):void 
		{
			_scale = value;
		}
		
	}

}