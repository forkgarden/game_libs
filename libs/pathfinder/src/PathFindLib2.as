package{
	import fg.mazes.Maze;
	import fg.pathfinders.PathFinder;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import fg.path_finder_helper.PFHelper;
	
	/**
	 * ...
	 * @author 
	 */
	public class PathFindLib2 extends Sprite {
		
		protected var pf:PathFinder = new PathFinder();
		protected var state:int = 1;
		protected var maze:Maze;
		protected var pfStart:Point = new Point();
		protected var pfAr:Array;
		protected var pfCont:Sprite;
		protected var pfHelper:PFHelper;
		protected var spr:Sprite;
		
		public function PathFindLib2() {
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event = null):void {
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			// entry point
			spr = new Sprite();
			spr.graphics.beginFill(0x00ff00, 1);
			spr.graphics.drawCircle(0, 0, 5);
			spr.graphics.endFill();
			addChild(spr);
			
			maze = new Maze();
			maze.create(10, 10, 2);
			maze.draw(this, 32);
			
			pf = new PathFinder();
			pf.checkCanMoveToPos = getWallStatus;
			
			pfHelper = new PFHelper();
			pfHelper.view = spr;
			
			pfCont = new Sprite();
			
			this.stage.addEventListener(MouseEvent.CLICK, stageOnClick, false, 0, true);
			addEventListener(Event.ENTER_FRAME, update);
		}
		
		protected function update(e:Event):void {
			pfHelper.update();
		}
		
		protected function getWallStatus(i:int, j:int):Boolean {
			return (maze.map[i][j] >= 1);
		}
		
		protected function pfDraw():void {
			var obj:Object;
			
			pfCont.graphics.clear();
			
			for each (obj in pfAr) {
				pfCont.graphics.beginFill(0x00ff00);
				pfCont.graphics.drawCircle(obj[0] * 32 + 16, obj[1] * 32 + 16, 5);
				pfCont.graphics.endFill();
			}
			
			addChild(pfCont);
		}
		
		protected function stageOnClick(e:MouseEvent):void {
			trace('stage on click, state ' + state);
			
			if (state == 1) { 
				pfStart.x = Math.floor(e.stageX / 32);
				pfStart.y = Math.floor(e.stageY / 32);
				state = 2;
				pfCont.graphics.clear();
			}
			else if (state == 2) {
				pfAr = pf.find(pfStart.x, pfStart.y, Math.floor(e.stageX / 32), Math.floor(e.stageY / 32));
				state = 1;
				//pfDraw();
				pfHelper.start(pfAr);
			}
			
		}
		
	}
	
}