package fg.pathfinders   {
	
	import flash.geom.Point;
	import fg.pathfinders.TNode;
	
	public final class PathFinder {
		
		protected static const KANAN:int = 1;
		protected static const KIRI:int  = 2;
		protected static const ATAS:int  = 3;
		protected static const BAWAH:int = 4;
		
		protected static const DIAGONAL:int = 6;
		
		public static const FAST:int = 1;
		public static const DIJKSTRA:int = 2;
		public static const ASTAR:int = 2;
		
		protected var _nodes:Vector.<TNode>;
		protected var _maxNodes:int = 1000;
		protected var _diagonalMove:Boolean = false;
		protected var _mode:int = ASTAR;
		protected var _checkCanMoveToPos:Function;
		protected var _getCost:Function;
		protected var _checkSampai:Function;
		protected var _debug:Boolean = false;
		
		public function PathFinder() {
			_nodes = new Vector.<TNode>();
		}
		
		protected function getNodeTerdekat(res:Vector.<TNode>, tx:int, ty:int):TNode {
			var node:TNode;
			var dist:int = 1000;
			var distTemp:int = 0;
			var nodeRes:TNode;
			
			for each (node in res) {
				distTemp = Math.abs(node.x - tx) + Math.abs(node.y - ty);
				if (distTemp < dist) {
					nodeRes = node;
					dist = distTemp;
				}
			}
			
			if (nodeRes.parentIdx == -1) nodeRes = null;
			
			return nodeRes;
		}
		
		//membuat path hasil pencarian
		//hasilnya disimpan di variable res
		//reskursif
		//dimulai dari node terdekat dengan target
		//di track terus hingga ke node dimana player berdiri
		protected function buildPathResult(node:TNode, res:Vector.<TNode>):void {
			var i:int = 0;
			var nodeTemp:TNode;
			var nodeParent:TNode;
			var len:int;
			
			//cari parent dari node yang sedang di check
			len = _nodes.length;
			for (i = 0; i < len; i++) { 
				nodeTemp = _nodes[i];
				if (nodeTemp.idx == node.parentIdx) {
					nodeParent = nodeTemp;
				}
			}
			
			//parent gak ada, node adalah node awal, return;
			if (nodeParent == null) return;
			
			//hasilnya di masukkan ke var res
			res.unshift(nodeParent);
			if (nodeParent.dir == 0) {
				return
			} else {
				buildPathResult(nodeParent, res);
			}
			
		}
		
		//TODO: manhattan mode
		protected function nodeCreate(parent:TNode, i:int, j:int, dir:int, targetX:int, targetY:int):TNode {
			var node:TNode;
			
			node = new TNode();
			node.x = i;
			node.y = j;
			node.open = true;
			node.idx = _nodes.length;
			
			if (parent) {
				node.parent = parent;
				node.parentIdx = parent.idx
				node.g = parent.g + 1;
			}
			else {
				node.parent = null;
				node.parentIdx = -1;
				node.g = 0;
			}
			
			node.h = Math.abs(targetX - i) + Math.abs(targetY - j);
			
			if (_mode == ASTAR) {
				node.dist = node.g + node.h;
			}
			else if (_mode == DIJKSTRA) {
				node.dist = node.g;
			}
			else {
				node.dist = node.h;
			}
			
			if (_getCost != null) node.dist += _getCost(node.x, node.y);
			
			node.dir = dir;
			
			return node;
		}
		
		//protected function drawPath(path:Vector.<TNode>):void {
			//var node:TNode;
			//var str:String = '';
			//
			//for each (node in path) {
				//str += '[' + node.x + '-' + node.y + ']-';
			//}
			//
		//}
		
		//protected function nodeToString(nv:Vector.<TNode>):String {
			//var node:TNode;
			//var str:String = "";
			//
			//for each (node in nv) {
				//str += node.toStringRef();
				//str += "-";
			//}
			//
			//return str.slice(0, str.length - 1);
		//}
		
		protected function resToArray(res:Vector.<TNode>):Array {
			var ar:Array = [];
			var node:TNode;
			
			for each (node in res) {
				ar.push([node.x, node.y, node.dir]);
			}
			
			return ar;
		}
		
		public function find(sx:int, sy:int, tx:int, ty:int):Array {
			var res:Vector.<TNode> = new Vector.<TNode>();
			var resAr:Array;

			while (_nodes.length > 0) {
				_nodes.pop();
			}
			
			//throw new Error();
			pathFind3(sx, sy, tx, ty, res);
			resAr = resToArray(res);
			
			while (res.length > 0) {
				res.pop();
			}
			
			return resAr;
		}
		
		protected function checkSampaiTujuan(i:int, j:int, tx:int, ty:int):Boolean {
			if ((i == tx) && (j == ty)) return true;
			
			if (_checkSampai != null) {
				if (_checkSampai(i, j)) {
					return true;
				}
			}
			
			return false;
		}
		
		protected function reset():void {
		
		}
		
		protected function getOpenNode():TNode {
			var i:int;
			var node:TNode;
			var maxLen:int;
			var nodeTemp:TNode;
			var len:int = 0;
			
			maxLen = 10000;
			
			len = _nodes.length - 1;
			for (i = len; i >= 0; i--) {
				node = _nodes[i];
				if (node.open) {
					if (node.dist < maxLen) {
						nodeTemp = node;
						maxLen = node.dist;
					}
				}
			}
			
			return nodeTemp;
		}
		
		protected function nodeOpenDiagonal(nodeCr:TNode, tx:int, ty:int):void {
			//atas kiri
			if (nodePosPossible(nodeCr.x - 1, nodeCr.y - 1, _nodes)) {
				_nodes.push(nodeCreate(nodeCr, nodeCr.x - 1, nodeCr.y - 1, DIAGONAL, tx, ty));
			}
			
			//atas kanan
			if (nodePosPossible(nodeCr.x + 1, nodeCr.y - 1, _nodes)) {
				_nodes.push(nodeCreate(nodeCr, nodeCr.x + 1, nodeCr.y - 1, DIAGONAL, tx, ty));
			}
			
			//bawah kanan
			if (nodePosPossible(nodeCr.x + 1, nodeCr.y + 1, _nodes)) {
				_nodes.push(nodeCreate(nodeCr, nodeCr.x + 1, nodeCr.y + 1, DIAGONAL, tx, ty));
			}
			
			//bawah kiri
			if (nodePosPossible(nodeCr.x - 1, nodeCr.y + 1, _nodes)) {
				_nodes.push(nodeCreate(nodeCr, nodeCr.x - 1, nodeCr.y + 1, DIAGONAL, tx, ty));
			}
		}
		
		protected function nodeOpen(nodeCr:TNode, tx:int, ty:int):void {
			//up
			if (nodePosPossible(nodeCr.x, nodeCr.y - 1, _nodes)) {
				_nodes.push(nodeCreate(nodeCr, nodeCr.x, nodeCr.y - 1, ATAS, tx, ty));
			}
			
			//right
			if (nodePosPossible(nodeCr.x + 1, nodeCr.y, _nodes)) {
				_nodes.push(nodeCreate(nodeCr, nodeCr.x + 1, nodeCr.y, KANAN, tx, ty));
			}
			
			//down
			if (nodePosPossible(nodeCr.x, nodeCr.y + 1, _nodes)) { 
				_nodes.push(nodeCreate(nodeCr, nodeCr.x, nodeCr.y + 1, BAWAH, tx, ty));
			}
			
			//left
			if (nodePosPossible(nodeCr.x - 1, nodeCr.y, _nodes)) {
				_nodes.push(nodeCreate(nodeCr, nodeCr.x - 1, nodeCr.y, KIRI, tx, ty));
			}
			
			if (_diagonalMove) {
				nodeOpenDiagonal(nodeCr, tx, ty);
			}
		}
		
		protected function pathFind3(sx:int, sy:int, tx:int, ty:int, res:Vector.<TNode>):void { 
			var nodeCr:TNode;
			
			reset();
			
			if ((sx == tx) && (sy == ty)) {
				if (_debug) trace("pf failed: target = start");
				return
			}
			
			//node pertama
			_nodes.push(nodeCreate(null, sx, sy, 0, tx, ty));
			
			while (true) {
				if ((_nodes.length >= _maxNodes)) {
					
					nodeCr = getNodeTerdekat(_nodes, tx, ty);
					
					if (nodeCr) {
						res.unshift(nodeCr);
						buildPathResult(nodeCr, res);
					}
					
					if (_debug) trace("pf failed: max nodes");
					return;
				}
				
				nodeCr = getOpenNode();
				if (nodeCr != null) {
					nodeCr.open = false;
					
					if (checkSampaiTujuan(nodeCr.x, nodeCr.y, tx, ty)) {
						res.unshift(nodeCr);
						buildPathResult(nodeCr, res);
						
						if (_debug) trace("pf success");
						return;
					}
					
					nodeOpen(nodeCr, tx, ty);
				}
				else {
					if (_debug) trace("no open nodes " + nodeCr);
					
					nodeCr = getNodeTerdekat(_nodes, tx, ty);
					
					if (nodeCr != null) {
						res.unshift(nodeCr);
						buildPathResult(nodeCr, res);
					}
					
					if (_debug) trace("pf failed no more open nodes");
					return;
				}
			}
			
			throw new Error();
			return;
		}
		
		protected function nodePosPossible(ix:int, jx:int, nodes:Vector.<TNode>):Boolean {
			var node:TNode;
			var block:Boolean;
			
			for each (node in nodes) {
				if (node.x == ix && node.y == jx) {
					return false;
				}
			}
			
			if (_checkCanMoveToPos(ix, jx) == false) return false;
			
			return true;
		}
		
		public function get checkCanMoveToPos():Function {
			return _checkCanMoveToPos;
		}
		
		public function set checkCanMoveToPos(value:Function):void {
			_checkCanMoveToPos = value;
		}
		
		public function get maxNodes():int {
			return _maxNodes;
		}
		
		public function set maxNodes(value:int):void {
			_maxNodes = value;
		}
		
		public function get diagonalMove():Boolean 
		{
			return _diagonalMove;
		}
		
		public function set diagonalMove(value:Boolean):void 
		{
			_diagonalMove = value;
		}
		
		public function get getCost():Function 
		{
			return _getCost;
		}
		
		public function set getCost(value:Function):void 
		{
			_getCost = value;
		}
		
		public function get checkSampai():Function 
		{
			return _checkSampai;
		}
		
		public function set checkSampai(value:Function):void 
		{
			_checkSampai = value;
		}
		
		public function get mode():int 
		{
			return _mode;
		}
		
		public function set mode(value:int):void 
		{
			_mode = value;
		}
		
		public function get debug():Boolean {
			return _debug;
		}
		
		public function set debug(value:Boolean):void {
			_debug = value;
		}
		
	}

}