package {
	import fg.pathfinders.PathFinder;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	public class MainPathFinder extends Sprite {
		
		protected var pf:PathFinder = new PathFinder();
		protected var state:int = 1;
		protected var pfStart:Point = new Point(1, 1);
		protected var pfAr:Array;
		protected var pfCont:Sprite;
		protected var spr:Sprite;		
		protected var map:Array;
		protected var mapCont:Sprite;
		
		public function MainPathFinder() {
			super();
			
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event = null):void {
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			pf = new PathFinder();
			pf.checkCanMoveToPos = getWallStatus;
			pfCont = new Sprite();
			
			map = [
				[1,1,1,1,1,1,1,1,1,1,1],
				[1,0,0,0,0,0,0,0,0,0,1],
				[1,0,1,1,1,1,1,0,0,0,1],
				[1,0,0,0,0,0,1,1,1,0,1],
				[1,0,1,1,1,1,1,0,0,0,1],
				[1,0,0,0,0,0,1,1,1,0,1],
				[1,0,1,1,1,1,1,0,0,0,1],
				[1,0,0,0,0,0,0,0,0,0,1],
				[1,1,1,1,1,1,1,1,1,1,1]
			];
			
			mapCont = new Sprite();
			mapDraw();
			addChild(mapCont);
			
			addChild(pfCont);
			
			getWallStatus(8, 1);
			
			this.stage.addEventListener(MouseEvent.CLICK, stageOnClick, false, 0, true);
		}
				
		protected function getWallStatus(i:int, j:int):Boolean {
			var res:Boolean = false;
			
			if (i < 0) res = true;
			if (j < 0) res = true;
			if (i >= map.length) res = true;
			if (j >= map[i].length) res = true;
			
			if (map[i][j] >= 1) res = true;
			
			return res;
		}
		
		protected function mapDraw():void {
			var i:int;
			var j:int;
			
			mapCont.graphics.clear();
			
			for (i = 0; i < map.length; i++) {
				for (j = 0; j < map[i].length; j++) {
					if (map[i][j]>0) {
						mapCont.graphics.beginFill(0);
						mapCont.graphics.drawRect(i * 32, j * 32, 31, 31);
						mapCont.graphics.endFill();
					}
				}
			}
			
		}
		
		protected function pfDraw():void {
			var obj:Object;
			
			pfCont.graphics.clear();
			
			for each (obj in pfAr) {
				pfCont.graphics.beginFill(0x00ff00);
				pfCont.graphics.drawCircle(obj[0] * 32 + 16, obj[1] * 32 + 16, 5);
				pfCont.graphics.endFill();
			}
			
			addChild(pfCont);
		}
		
		protected function stageOnClick(e:MouseEvent):void {
			var px:Number;
			var py:Number;
			
			px = Math.floor(e.stageX / 32);
			py = Math.floor(e.stageY / 32);
			
			trace('stage click ' + px + '/' + py);
			
			pfAr = pf.find(pfStart.x, pfStart.y, px, py);
			
			pfStart.x = px;
			pfStart.y = py;
			
			pfDraw();
			
		}		
		
	}

}