package{
	import fg.tekstypes.TypeWritter;
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	
	public class MainTeksKetik extends Sprite {
		
		[Embed(source = "../libs/back.jpg")]
		private var Back:Class;
		
		protected var teks:TextField;
		protected var teksType:TypeWritter;
		
		public function MainTeksKetik() {
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event = null):void {
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			backBuat();
			teksBuat();
			addEventListener(Event.ENTER_FRAME, update);
			
			stage.scaleMode = StageScaleMode.SHOW_ALL;
		}
		
		private function update(e:Event):void {
			teksType.update();
			teks.text = teksType.teksOutput;
		}
		
		protected function teksBuat():void {
			var teksFormat:TextFormat;
			var str:String = "";

			teks = new TextField();
			teks.width = stage.width - 64;
			teks.x = 32;
			teks.height = 480;
			teks.textColor = 0xffffff;
			teks.mouseEnabled = false;
			teks.tabEnabled = false;
			teks.mouseWheelEnabled = false;
			teks.doubleClickEnabled = false;
			teks.selectable = false;
			teks.wordWrap = true;
			teksFormat = teks.getTextFormat();
			teksFormat.font = 'arial';
			teksFormat.size = 14;
			teks.defaultTextFormat = teksFormat;
			addChild(teks);
			
			teksType = new TypeWritter();
			str = "Dengan nama Allah Yang Maha Pengasih, Maha Penyayang\n"+
					"Segala puji bagi Allah, Tuhan seluruh alam,\n"+
					"Yang Maha Pengasih, Maha Penyayang,\n"+
					"Pemilik hari pembalasan.\n"+
					"Hanya kepada Engkaulah kami menyembah \n"+
					"dan hanya kepada Engkaulah kami mohon pertolongan.\n"+
					"Tunjukilah kami jalan yang lurus\n"+
					"(yaitu) jalan orang - orang yang telah Engkau beri nikmat kepadanya\n"+
					"bukan (jalan) mereka yang dimurkai, dan bukan (pula jalan) mereka yang sesat.";
			teksType.teksInput = str;
			teksType.start();	
		}
		
		protected function backBuat():void {
			var spr:Sprite;
			var back:Bitmap;
			
			spr = new Sprite();
			back = new Back();
			spr.addChild(new Back());
			
			spr.width = 640;
			spr.height = 240;
			addChild(spr);
		}
		
	}
	
}