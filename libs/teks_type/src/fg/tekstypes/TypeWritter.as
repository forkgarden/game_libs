package fg.tekstypes
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.text.TextField;
	
	public class TypeWritter extends Sprite
	{
		
		protected var _teksInput:String = '';
		protected var _teksOutput:String = '';
		protected var _finishCallBack:Function;
		protected var _flFinish:Boolean;
		protected var ctr:Number = 0;
		protected var _active:Boolean = false;
		protected var _speed:Number = 1;
		
		public function TypeWritter()
		{
		}
		
		public function start():void
		{
			ctr = 0;
			_active = true;
			_flFinish = false;
		}
		
		public function skip():void
		{
			ctr = _teksInput.length;
		}
		
		public function update():void
		{
			if (!_active) return;
			
			_teksOutput = _teksInput.slice(0, ctr);
			
			ctr += _speed;
			if (ctr > _teksInput.length)
			{
				_active = false;
				_flFinish = true;
				if (_finishCallBack != null) _finishCallBack();
			}
		}
		
		public function get teksInput():String
		{
			return _teksInput;
		}
		
		public function set teksInput(value:String):void
		{
			_teksInput = value;
		}
		
		public function get teksOutput():String
		{
			return _teksOutput;
		}
		
		public function set teksOutput(value:String):void
		{
			_teksOutput = value;
		}
		
		public function get finishCallBack():Function
		{
			return _finishCallBack;
		}
		
		public function set finishCallBack(value:Function):void
		{
			_finishCallBack = value;
		}
		
		public function get speed():Number
		{
			return _speed;
		}
		
		public function set speed(value:Number):void
		{
			_speed = value;
		}
		
		public function get flFinish():Boolean 
		{
			if (_flFinish) {
				_flFinish = false;
				return true;
			}
			return _flFinish;
		}
	
	}
}