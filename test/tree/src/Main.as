package
{
	import fg.ui.TreeView;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	public class Main extends Sprite 
	{
		
		protected var tree:TreeView;
		protected var sprHead:Sprite;
		
		public function Main() 
		{
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event = null):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			// entry point
			tree = new TreeView();
			tree.removeChildren();
			addChild(tree);
			
			sprHead = spriteCreate(this, '1', 400, 300);
			spriteCreate(sprHead, '2', 30, 30);
			spriteCreate(sprHead, '3', 30, -30);
			spriteCreate(sprHead.getChildAt(0) as DisplayObjectContainer, '4', 15, 15);
			
			buildTreeFromCont(sprHead, tree);
		}
		
		protected function buildTreeFromCont(cont:DisplayObjectContainer, tree:TreeView):void {
			var disp:DisplayObject;
			var i:int;
			var t:TreeView;
			var dispC:DisplayObjectContainer;
			
			for (i = 0; i < cont.numChildren; i++) {
				disp = cont.getChildAt(i);
				t = new TreeView({view: disp});
				t.label.text = disp.name;
				t.parentTree = tree;
				tree.addItem(t);
				
				//recursif
				if (disp is DisplayObjectContainer) {
					buildTreeFromCont(disp as DisplayObjectContainer, t);
				}
			}
		}
		
		protected function sprOnClick(evt:MouseEvent):void {
			var t:TreeView;
			
			evt.stopPropagation();
			
			t = TreeView.head.searchByView(evt.currentTarget as DisplayObject);
			trace('spr on click');
			trace(t);
			trace((evt.currentTarget as DisplayObject).name);
			t.openRec();
		}
		
		protected function spriteCreate(cont:DisplayObjectContainer, id:String='', ix:int = 0, jx:int = 0):Sprite {
			var clr:uint = Math.floor(Math.random() * 0xffffff);
			var lineClr:uint = 0xffffff;
			var spr:Sprite;
			
			spr = new Sprite();
			spr.graphics.lineStyle(2, lineClr);
			spr.graphics.beginFill(clr);
			spr.graphics.drawCircle(20, 20, 20);
			spr.graphics.endFill();
			spr.name = id;
			spr.x = ix;
			spr.y = jx;
			spr.addEventListener(MouseEvent.CLICK, sprOnClick, false, 0, true);
			
			cont.addChild(spr);
			return spr;
		}
		
	}
	
}