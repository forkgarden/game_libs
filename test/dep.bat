if "%1" == "pf"   xcopy ..\..\pathfinder\src\fg\pathfinders                %2\src\fg\pathfinders /s
if "%1" == "pfh"  xcopy ..\..\path_finder_helper\src\fg\path_finder_helper %2\src\fg\path_finder_helper\ /s
if "%1" == "maz"  xcopy ..\..\mazeLib02\src\fg\mazes                       %2\src\fg\mazes\ /s
if "%1" == "bmp"  xcopy ..\..\BmpHandler\src\fg\bmps                       %2\src\fg\bmps\ /s
if "%1" == "tmx"  xcopy ..\..\tmx\src\fg\tmxs			                     %2\src\fg\tmxs\ /s
if "%1" == "ktk"  xcopy ..\..\ui\src\fg\tekstypes			         		 %2\src\fg\tekstypes /s

:: ui dec
if "%1" == "ui"  mkdir %2\src\fg\ui

:: ui
if "%1" == "tek"  copy ..\..\ui\src\fg\ui\Teks.as		%2\src\fg\ui\Teks.as
if "%1" == "pnl"  copy ..\..\ui\src\fg\ui\Panel3D.as	%2\src\fg\ui\Panel3D.as
if "%1" == "inp"  copy ..\..\ui\src\fg\ui\Input.as		%2\src\fg\ui\Input.as

::depedency tambahan
if "%1" == "inp"  copy ..\..\ui\src\fg\ui\Panel3D.as		%2\src\fg\ui\Panel3D.as