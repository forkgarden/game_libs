package
{
	import adobe.utils.CustomActions;
	import flash.display.Graphics;
	import flash.display.Sprite;
	import flash.events.AccelerometerEvent;
	import flash.events.Event;
	import flash.geom.Point;
	
	public class Main extends Sprite 
	{
		protected var dots:Vector.<Point> = new Vector.<Point>();
		
		public function Main() 
		{
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event = null):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			var dot:Point;
			
			// entry point
			dot = new Point();
			dot.x = 10;
			dot.y = 200;
			dots.push(dot);
			
			dot = new Point();
			dot.x = 100;
			dot.y = 10;
			dots.push(dot);
			
			dot = new Point();
			dot.x = 200
			dot.y = 200;
			dots.push(dot);
			
			dots = bagi(dots);
			render(dots);
			dots = bagi(dots);
			render(dots);
			dots = bagi(dots);
			render(dots);
			dots = bagi(dots);
			render(dots);
		}
		
		protected function getPointAtIdx(p1:Point, p2:Point, idx:Number):Point {
			var p:Point = new Point();
			p.x = p1.x + (p2.x - p1.x) * idx;
			p.y = p1.y + (p2.y - p1.y) * idx;
			
			return p;
		}
		
		protected function tengah(p1:Point,p2:Point):Point {
			return getPointAtIdx(p1, p2, .5);
		}
		
		protected function vectorAdd(v1:Vector.<Point>, v2:Vector.<Point>):void {
			var p:Point;
			
			for each (p in v2) {
				v1.push(p);
			}
		}
		
		
		protected function bagi(dots:Vector.<Point>):Vector.<Point> {
			var i:int;
			var res:Vector.<Point> = new Vector.<Point>();
			var bagi:Vector.<Point> = new Vector.<Point>();
			
			for (i = 0; i <= dots.length - 3; i++) {
				bagi = bagi2(dots.slice(i, i + 3));
				
				if (res.length == 0) {
					vectorAdd(res, bagi);
				}
				else {
					res.pop();
					vectorAdd(res, bagi.slice(2, int.MAX_VALUE));
				}
			}
			
			return res;
		}
		
		
		protected function bagi2(dots:Vector.<Point>):Vector.<Point> {
			var res:Vector.<Point> = new Vector.<Point>();
			var dot:Point;
			var idx:Number = 1 / 2;
			
			
			res.push(dots[0].clone());
			res.push(getPointAtIdx(dots[0], dots[1], idx));
			res.push(dots[1].clone());
			res.push(getPointAtIdx(dots[1], dots[2], 1-idx));
			res.push(dots[2].clone());
			
			dot = tengah(res[1], res[3]);
			dot = tengah(dot, res[2]);
			res[2].x = dot.x;
			res[2].y = dot.y;
			
			return res;
		}
		
		protected function render(dots:Vector.<Point>):void {
			var i:int;
			var g:Graphics = this.graphics;
			var p:Point;
			
			g.lineStyle(1);
			
			for (i = 0; i < dots.length-1; i++) {
				p = dots[i];
				g.moveTo(p.x, p.y);
				g.drawCircle(p.x, p.y, 2);
				g.moveTo(p.x, p.y);
				
				p = dots[i + 1];
				g.lineTo(p.x, p.y);
			}
			
			g.moveTo(p.x, p.y);
			g.drawCircle(p.x, p.y, 2);
		}
		
	}
	
}