package 
{
	import flash.geom.Point;
	/**
	 * ...
	 * @author test
	 */
	public class Line 
	{
		
		protected var _p1:Point = new Point();
		protected var _p2:Point = new Point();
		
		public function Line() 
		{
			
		}
		
		public function get p1():Point 
		{
			return _p1;
		}
		
		public function set p1(value:Point):void 
		{
			_p1 = value;
		}
		
		public function get p2():Point 
		{
			return _p2;
		}
		
		public function set p2(value:Point):void 
		{
			_p2 = value;
		}
		
	}

}