package objs.helper 
{
	import objs.ObjEdit;
	import ui.LabelPanel;

	public class LabelPanel2ObjEdit 
	{
		
		protected var labelPanel:LabelPanel;
		protected var objEdit:ObjEdit;
		
		public function LabelPanel2ObjEdit(label:LabelPanel, obj:ObjEdit) 
		{
			this.objEdit = obj;
			this.labelPanel = labelPanel;
		}
		
	}

}