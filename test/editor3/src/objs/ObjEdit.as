package objs
{
	import edits.TransformData;
	import fg.inters.Move;
	import fg.ui.Tombol;
	import fg.ui.VLayout;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.display.Graphics;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.utils.Dictionary;
	import panels.BasePanel;
	import ui.InputLabel;
	
	public class ObjEdit extends Sprite
	{
		public static const TY_PANEL:int = 1;
		public static const TY_SPRITE:int = 2;
		public static const TY_TEXT:int = 3;
		public static const TY_IMAGE:int = 4;
		public static const TY_PAGE:int = 5;
		public static const TY_BUTTON:int = 6;
		public static const TY_VSCROLL_LIST:int = 7;
		
		protected var move:Move;
		protected var _type:Number;
		protected var _childs:Vector.<ObjEdit>;
		protected var _transformData:TransformData;
		protected var _typeStr:String = '';
		protected var selectSprite:Sprite;
		protected var _parentObj:ObjEdit;
		protected var canHaveChild:Boolean = false;
		protected var namaDict:Dictionary;
		protected var strukturToRoot:Vector.<ObjEdit>;
		protected var strukturToRootView:Sprite;
		protected var _kunci:Boolean = false;
		
		protected var _id:String = '';
		
		protected static var id:Number = 0;
		protected static var lists:Vector.<ObjEdit> = new Vector.<ObjEdit>();
		
		public function ObjEdit()
		{
			if (_type != TY_PAGE) {
				move = new Move(this, MainEditor.inst.stage);
				move.callBackMove = onDrag;
				move.callBackMouseUp = onDragEnd;
			}
			
			addEventListener(MouseEvent.CLICK, viewOnClick);
			_transformData = new TransformData();
			_childs = new Vector.<ObjEdit>();
			strukturToRoot = new Vector.<ObjEdit>();
			
			selectSprite = new Sprite();
		}
		
		public function select():void {
			var g:Graphics;
			
			addChild(selectSprite);
			
			g = selectSprite.graphics;
			g.clear();
			g.lineStyle(3, 0xffff00);
			g.drawRect(0, 0, width, height);
		}
		
		public function deselect():void {
			var g:Graphics;
			var objEdit:ObjEdit;
			
			g = selectSprite.graphics;
			g.clear();
			
			for each (objEdit in _childs) {
				objEdit.deselect();
			}
		}
		
		public function debug():void {
			trace("x " + x + "/y " + y + "/type " + type);
		}
		
		protected function buildChild(obj):void {
			var objTombol:ObjTombol;
			
			if (obj.type == ObjEdit.TY_BUTTON) {
				objTombol = new ObjTombol();
				objTombol.fromObj(obj);
				addChild2(objTombol);
			}
			else if (obj is ObjHalaman) {
				
			}
			else if (obj is ObjImage) {
				
			}
			else if (obj is ObjText) {
				
			}
			else {
				throw new Error();
			}
		}
		
		public function updateFromTransformData():void {
			x = _transformData.x;
			y = _transformData.y;
			width = _transformData.width;
			height = _transformData.height;
		}
		
		protected function tabWrite(n:int=0):String {
			//TODO:
			return '';
		}
		
		public function getObjById(idParam:String):ObjEdit {
			var obj:ObjEdit;
			var res:ObjEdit;
			
			if (id == idParam) {
				return this;
			}
			
			for each (obj in _childs) {
				res = obj.getObjById(idParam);
				if (res) return res;
			}
			
			return null;
		}
		
		public static function createId():String {
			var id:int = 0;
			
			while (true) {
				id++;
				if (MainEditor.inst.halAktif.getObjById(id + '') == null) {
					return id + '';
				}
			}
			
			return null;
		}
		
		protected function onDragEnd():void {
			_transformData.x = x;
			_transformData.y = y;
		}
		
		protected function onDrag(dx:Number, dy:Number):void {
			
		}
		
		public function refresh():void {
			x = _transformData.x;
			y = _transformData.y;
			scaleX  = _transformData.scaleX;
			scaleY = _transformData.scaleY;
			rotation = _transformData.rotation;
		}
		
		public function changeProp(prop:String, value:String):Boolean
		{
			var tempId:String;
			
			if (prop == 'x') {
				this.x = parseInt(value);
				return true;
			}
			else if (prop == 'y') {
				this.y = parseInt(value);
				return true;
			}
			
			return false;
		}
		
		public function toObj():Object
		{
			return {};
		}
		
		public function fromObj(obj:Object):void
		{
			trace('load ' + obj);
		}
		
		public function removeChild2(child:ObjEdit):void {
			var i:int;
			var obj:ObjEdit;
			
			child.destroy();
			
			for (i = _childs.length - 1; i >= 0; i--) {
				obj = _childs[i];
				if (obj == child) {
					_childs.splice(i, 1);
					return;
				}
			}
			
			throw new Error('removechild2 child not found');
		}
		
		public function remove():void {
			if (_parentObj) {
				_parentObj.removeChild2(this);
			}
		}
		
		public function destroy():void
		{	
			var obj:ObjEdit;
			
			move.destroy();
			removeChildren();
			for each (obj in _childs) {
				obj.destroy();
			}
			if (parent) parent.removeChild(this);
			_parentObj = null;
		}
		
		public function addChild2(d:ObjEdit):void {
			if (d == null) throw new Error();
			if (canHaveChild == false) throw new Error();
			
			_childs.push(d);
			d.parentObj = this;
			MainEditor.inst.render();
		}
		
		protected function viewOnClick(evt:MouseEvent):void
		{
			evt.stopPropagation();
			if (Move.isMoving == false) {
				MainEditor.inst.activeObject = this;
				MainEditor.inst.halAktif.deselect();
				MainEditor.inst.activeObject.select();
			}
		}
		
		protected function setChildIdx(obj:ObjEdit, idx:int):void {
			var item:ObjEdit;
			var objIdx:int;
			
			objIdx = getChildIdx(obj);
			
			if (objIdx == idx) return;
			
			item = _childs[idx];
			_childs[idx] = obj;
			_childs[objIdx] = item;
		}
		
		protected function getChildIdx(obj:ObjEdit):int {
			var i:int;
			
			for (i = 0; i < _childs.length; i++) {
				if (obj == _childs[i]) return i;
			}
			
			throw new Error();
		}
		
		protected function panelAddCommonAction(panel:BasePanel):void {
			var idx:int;
			var ref:ObjEdit = this;
			
			panel.addTombol2('prop >', function(tbl):void {
				trace('edit on click');
				MainEditor.inst.panel.changePanel2(panelPropCreate());
				this.mouseEnabled = false;
			});			
			
			panel.addTombol2('hapus', function(tbl:Tombol):void {
				remove();
			});
			
			panel.addTombol2('kunci', function(tbl:Tombol):void {
				mouseEnabled = false;
				_kunci = true;
				deselect();
			});
			
			panel.addTombol2('geser atas', function(tbl:Tombol):void {
				idx = _parentObj.getChildIdx(ref);
				if (idx >0) {
					_parentObj.setChildIdx(ref, idx - 1);
					MainEditor.inst.render();
				}
			});
			
			panel.addTombol2('geser bawah', function(tbl:Tombol):void {
				idx = _parentObj.getChildIdx(ref);
				if (idx < _parentObj.childs.length - 1) {
					_parentObj.setChildIdx(ref, idx + 1);
					MainEditor.inst.render();
				}				
			});
		}
		
		public function unLock():void {
			var child:ObjEdit;
			
			mouseEnabled = true;
			_kunci = false;
			
			for each (child in _childs) {
				child.unLock();
			}
		}
		
		public function unHide():void {
			var child:ObjEdit;
			
			visible = true;
			for each (child in _childs) {
				child.unHide();
			}
		}
		
		protected function panelActiveCreate():BasePanel {
			var panel:BasePanel = new BasePanel();
			
			return panel;
		}
		
		protected function panelPropCreate():BasePanel {
			var panel:BasePanel;
			var input:InputLabel;
			
			panel = new BasePanel();
			
			input = new InputLabel('x', 'x');
			input.input.teks.text = this.x + '';
			input.onChange = changeProp;
			panel.addInput(input);
			
			input = InputLabel
				.create('y', 'y')
				.setText(this.y + '');
			input.onChange = changeProp;
			panel.addInput(input);
			
			input = InputLabel
				.create('width', 'width')
				.setText(this.width + '');
			input.onChange = changeProp;
			panel.addInput(input);
			
			input = InputLabel
				.create('height', 'height')
				.setText(this.height + '');
			input.onChange = changeProp;
			panel.addInput(input);
			
			return panel;
		}		
		
		public function get transformData():TransformData 
		{
			return _transformData;
		}
		
		public function set transformData(value:TransformData):void 
		{
			_transformData = value;
		}
		
		public function get childs():Vector.<ObjEdit> 
		{
			return _childs;
		}
		
		public function set childs(value:Vector.<ObjEdit>):void 
		{
			_childs = value;
		}
		
		public function get type():Number 
		{
			return _type;
		}
		
		public function set type(value:Number):void 
		{
			_type = value;
		}
		
		public function get id():String 
		{
			return _id;
		}
		
		public function set id(value:String):void 
		{
			_id = value;
		}
		
		public function get typeStr():String 
		{
			return _typeStr;
		}
		
		public function get parentObj():ObjEdit 
		{
			return _parentObj;
		}
		
		public function set parentObj(value:ObjEdit):void 
		{
			_parentObj = value;
		}
		
		public function get kunci():Boolean 
		{
			return _kunci;
		}
		
		public function set kunci(value:Boolean):void 
		{
			_kunci = value;
		}
	
	}

}