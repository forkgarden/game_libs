package objs 
{
	import flash.display.Sprite;

	public class ObjHalaman extends ObjEdit
	{
		protected var _flHead:Boolean = false;
		protected var _flPage:Boolean = false;
		
		public function ObjHalaman() 
		{
			super();
			refresh(32, 32);
			_type = ObjEdit.TY_PAGE;
			_typeStr = 'container';
			canHaveChild = true;
			move.destroy();
		}
		
		public function selectChild(obj:ObjEdit):void {
			
		}
		
		public override function fromObj(obj:Object):void {
			var ar:Array;
			
			//_transformData.fromObj(obj.transformData);
			
			ar = obj.childs as Array;
			for each (obj in ar) {
				buildChild(obj);
			}
			
			//updateFromTransformData();
			//trace('obj container from obj ' + JSON.stringify(obj));
		}
		
		//public override function updateFromTransformData():void {
			//x = _transformData.x;
			//y = _transformData.y;
		//}		
		
		public override function toObj():Object {
			var obj:Object = {};
			var objEdit:ObjEdit;
			var childData:Object;
			
			//obj.transformData = _transformData.toObj();
			obj.childs = [];
			for each (objEdit in _childs) {
				childData = objEdit.toObj();
				obj.childs.push(childData);
			}
			
			return obj;
		}
		
		protected function refresh(w:int, h:int):void {
			//graphics.beginFill(0xffff00, .3);
			//graphics.drawRect(0, 0, 32, 32);
			//graphics.endFill();
		}
		
		public function get flHead():Boolean 
		{
			return _flHead;
		}
		
		public function set flHead(value:Boolean):void 
		{
			_flHead = value;
		}
		

	}

}