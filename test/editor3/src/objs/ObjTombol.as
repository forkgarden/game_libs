package objs 
{
	import edits.ImgData;
	import fg.inters.Move;
	import fg.ui.Input;
	import fg.ui.Tombol;
	import flash.display.Bitmap;
	import flash.events.MouseEvent;
	import panels.BasePanel;
	import ui.InputLabel;
	import panels.MenuPanel;

	public class ObjTombol extends ObjEdit
	{
		protected var tombol:Tombol;
		
		public function ObjTombol() 
		{
			_type = ObjEdit.TY_BUTTON;
			tombol = new Tombol();
			tombol.label = "tombol";
			tombol.mouseEnabled = false;
			tombol.mouseChildren = false;
			addChild(tombol);
			
			this.mouseChildren = false;
			
			_id = ObjEdit.createId();
			_typeStr = 'tombol';
		}
		
		public override function changeProp(prop:String, value:String):Boolean
		{
			if (super.changeProp(prop,value)) {
				return true;
			}
			else if (prop == 'label') {
				tombol.setLabel(value);
				return true;
			}
			else {
				trace('prop unhandled' + prop);
			}
			
			return false;
		}
		
		public override function fromObj(obj:Object):void {
			trace("obj from obj" + JSON.stringify(obj));
			trace("tombol label " + tombol.label);
			
			_transformData.fromObj(obj.transformData);
			
			tombol.label = obj.label;
			updateFromTransformData();
		}
		
		public override function toObj():Object {
			var obj:Object = {};
			
			_transformData.x = x;
			_transformData.y = y;
			_transformData.width = width;
			_transformData.height = height;
			
			obj.transformData = _transformData.toObj();
			obj.label = tombol.label;
			obj.type = ObjEdit.TY_BUTTON;
			
			return obj;
		}
		
		protected override function viewOnClick(evt:MouseEvent):void
		{
			if (Move.isMoving == false) {
				evt.stopPropagation();
				MainEditor.inst.activeObject = this;
				
				MainEditor.inst.panel.changePanel2(panelActiveCreate());
			}
			
			super.viewOnClick(evt);
		}
		
		protected override function panelActiveCreate():BasePanel {
			var panel:BasePanel;
			var ref:ObjTombol = this;
			
			panel = super.panelActiveCreate();
						
			panel.addTombol2('event', function(tbl):void {
				//trace('edit on click');
				//MainEditor.inst.panel.changePanel2(objPropCreate());
				//this.mouseEnabled = false;
			});
			
			panel.addTombol2('...', function(tbl):void {
				trace('edit on click');
				MainEditor.inst.panel.changePanel2(new MenuPanel());
			});
			
			return panel;
		}
		
		protected override function panelPropCreate():BasePanel {
			var panel:BasePanel;
			var input:InputLabel;
			
			panel = super.panelPropCreate();
			
			input = InputLabel
				.create('label', 'label')
				.setText(tombol.label + '');
			input.onChange = changeProp;
			panel.addInput(input);		
			
			panel.addTombol2('<<', function(tbl):void {
				this.mouseEnabled = true;
				MainEditor.inst.panel.changePanel2(panelActiveCreate());
			});
			
			return panel;
		}
		
	}

}