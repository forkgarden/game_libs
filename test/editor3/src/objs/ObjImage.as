package objs 
{
	import edits.ImgData;
	import flash.display.Bitmap;
	/**
	 * ...
	 * @author test
	 */
	public class ObjImage extends ObjEdit
	{
		protected var bmp:Bitmap;
		protected var imgData:ImgData;
		protected var _bmpId:int = 0;
		
		public function ObjImage() 
		{
			_type = ObjEdit.TY_IMAGE;
			_typeStr = 'image';
		}
		
		public function setImgData(data:ImgData):void {
			this.imgData = data;
			bmp = new Bitmap(data.bmp.bitmapData);
			_bmpId = imgData.id;
			addChild(bmp);
		}
		
		public override function toObj():Object {
			var obj:Object = {};
			
			obj.transformData = _transformData.toObj();
			obj.bmpId = _bmpId; 
			return obj;
		}
		
		public override function fromObj(obj:Object):void {
			_transformData.fromObj(obj.transformData);
			setImgData(MainEditor.inst.imgCtrl.getImgDataById(obj.bmpId));
			updateFromTransformData();
		}
		
		public function get bmpId():int 
		{
			return _bmpId;
		}
		
		public function set bmpId(value:int):void 
		{
			_bmpId = value;
		}
		
	}

}