package objs 
{
	import fg.inters.Move;
	import fg.ui.Teks;
	import flash.events.MouseEvent;
	import panels.BasePanel;
	import panels.MenuPanel;
	import ui.InputLabel;

	public class ObjText extends ObjEdit
	{
		protected var _teks:Teks;
		
		public function ObjText() 
		{
			_type = ObjEdit.TY_TEXT;
			_teks = new Teks();
			_teks.text = 'text';
			_typeStr = 'teks';
			this.addChild(_teks);
			this.mouseChildren = false;
		}
		
		protected override function viewOnClick(evt:MouseEvent):void
		{
			if (Move.isMoving == false) {
				evt.stopPropagation();
				MainEditor.inst.panel.changePanel2(panelActiveCreate());
			}
			
			super.viewOnClick(evt);
		}	
		
		protected override function panelActiveCreate():BasePanel {
			var panel:BasePanel = new BasePanel();
						
			panel.addTombol2('prop >', function(tbl):void {
				trace('edit on click');
				MainEditor.inst.panel.changePanel2(panelPropCreate());
				this.mouseEnabled = false;
			});
			
			panel.addTombol2('event >', function(tbl):void {});
			
			panelAddCommonAction(panel);
			
			panel.addTombol2('<<<', function(tbl):void {
				trace('edit on click');
				MainEditor.inst.panel.changePanel2(new MenuPanel());
			});
			
			return panel;
		}
		
		public override function changeProp(prop:String, value:String):Boolean {
			
			trace('change prop ' + prop);
			
			if (super.changeProp(prop, value)) {
				return true;
			}
			else if (prop == "teks") {
				_teks.text = value;
			}
			else {
				trace('prop unhandled ' + prop);
			}
			
			return false;
			
		}
		
		protected override function panelPropCreate():BasePanel {
			var panel:BasePanel;
			var input:InputLabel;
			
			panel = super.panelPropCreate();
			
			input = new InputLabel('teks', 'teks');
			input.input.teks.text = _teks.text;
			input.onChange = function(prop:String, value:String):void {
				changeProp(prop, value);
			};
			panel.addInput(input);
			
			return panel;
		}
		
		public function get teks():Teks 
		{
			return _teks;
		}
		
		public override function toObj():Object {
			var obj:Object;
			
			obj.transformData = _transformData.toObj();
			obj.teks = _teks.text;
			
			return obj;
		}
			
		public override function fromObj(obj:Object):void {
			_transformData.fromObj(obj.transformData);
			_teks.text = obj.teks;
			updateFromTransformData();
		}
		
		
		
	}

}