package objs 
{
	import fg.inters.Move;
	import fg.ui.Tombol;
	import fg.ui.VLayout;
	import fg.ui.VScrollList;
	import flash.events.MouseEvent;
	import objs.helper.LabelPanel2ObjEdit;
	import panels.AddChildPanel;
	import panels.BasePanel;
	import panels.MenuPanel;
	import ui.LabelPanel;
	
	public class ObjVScrollList extends ObjEdit
	{
		protected var vScroll:VScrollList
		protected var items:Vector.<ObjEdit>;
		protected var itemLabels:Vector.<LabelPanel>;
		protected var item2Labels:Vector.<LabelPanel2ObjEdit>;
		protected var selectedItem:ObjEdit;
		
		public function ObjVScrollList() 
		{
			item2Labels = new Vector.<LabelPanel2ObjEdit>();
			canHaveChild = true;
			items = new Vector.<ObjEdit>();
			itemLabels = new Vector.<LabelPanel>();
			vScroll = new VScrollList();
			vScroll.list.mouseEnabled = false;
			vScroll.list.mouseChildren = false;
			vScroll.width = 150;
			addChild(vScroll);
		}
		
		protected function listBuild(scroll:VScrollList):void {
			var obj:ObjEdit;
			var labelPanel:LabelPanel;
			
			while (item2Labels.length > 0) {
				item2Labels.pop();
			}
			
			for each (obj in items) {
				labelPanel = new LabelPanel();
				labelPanel.teks.text = obj.typeStr;
				labelPanel.addEventListener(MouseEvent.CLICK, labelPanelOnClick);
				scroll.addItem(labelPanel);
				itemLabels.push(labelPanel);
				item2Labels.push(new LabelPanel2ObjEdit(labelPanel, obj));
			}
			
			MainEditor.inst.halAktif.deselect();
			if (items.length > 0) {
				items[0].select();
				selectedItem = items[0];
			}
		}
		
		protected override function viewOnClick(evt:MouseEvent):void
		{
			if (Move.isMoving == false) {
				evt.stopPropagation();
				MainEditor.inst.activeObject = this;
				MainEditor.inst.panel.changePanel2(panelActiveCreate());
			}
			
			super.viewOnClick(evt);
		}
		
		protected function modalPanelAddItemCreate():BasePanel {
			var panel:AddChildPanel; //= new BasePanel();
			
			panel =  AddChildPanel.create().setFilter([ObjEdit.TY_BUTTON, ObjEdit.TY_TEXT]).buildList();
			panel.callBackSelectedObject = function(idx):void {
				//TODO:
			}
			
			return panel;
		}
		
		public override function destroy():void {
			var labelPanel:LabelPanel;
			var item:ObjEdit;
			
			while (itemLabels.length > 0) {
				labelPanel = itemLabels.pop();
				labelPanel.destroy();
			}
			
			while (items.length > 0) {
				item = items.pop();
				item.destroy();
			}
			
			vScroll.destroy();
			removeChildren();
			super.destroy();
		}
		
		protected function panelItemCreate():BasePanel {
			var panel:BasePanel = new BasePanel();
			var scroll:VScrollList = new VScrollList();
			var obj:ObjEdit;
			var labelPanel:LabelPanel;
			var vLayout:VLayout = new VLayout();
			var panelTombol:VLayout = new VLayout();
			
			panel.setLabel('scroll item');
			
			scroll = new VScrollList();
			panel.cont.addItem(scroll);
			
			listBuild(scroll);
			
			panel.addTombol2('tambah', function(tbl:Tombol):void {
				MainEditor.inst.panel.modalShow(modalPanelAddItemCreate());
			});
			
			return panel;
		}
		
		public override function changeProp(prop:String, value:String):Boolean {
			if (super.changeProp(prop,value)) {
				return true;
			}
			else if (prop == "width") {
				vScroll.width = parseInt(value + '');
			}
			else if (prop == 'height') {
				vScroll.height = parseInt(value + '');
			}
			else {
				trace('prop unhandled' + prop);
			}
			
			return true;
		}
		
		protected function labelPanelOnClick(evt:MouseEvent):void {
			var labelPanel:LabelPanel;
			
			for each (labelPanel in itemLabels) {
				labelPanel.deselect();
			}
			
			(evt.currentTarget as LabelPanel).select();
		}
		
		protected override function panelActiveCreate():BasePanel {
			var panel:BasePanel = new BasePanel();
			
			panel.addTombol2('prop >>', function(tbl):void {
				MainEditor.inst.panel.changePanel2(panelPropCreate());
			});
			
			panel.addTombol2('items >>', function(tbl):void {
				trace('edit on click');
				MainEditor.inst.panel.changePanel2(panelItemCreate());
			});
			
			panelAddCommonAction(panel);
			
			panel.addTombol2('<<<', function(tbl):void {
				trace('edit on click');
				MainEditor.inst.panel.changePanel2(new MenuPanel());
			});
			
			return panel;
		}		
		
	}

}