package objs 
{
	import fg.ui.Panel3D;
	import fg.ui.Tombol;
	import panels.BasePanel;
	import flash.events.MouseEvent;
	import fg.inters.Move;
	
	public class ObjPanel extends ObjEdit
	{
		protected var panel:Panel3D;
		
		public function ObjPanel() 
		{
			super();
			_type = ObjEdit.TY_PANEL;
			_typeStr = 'Panel 3D';
			panel = new Panel3D();
			id = 'panel' + ObjEdit.createId();
			addChild(panel);
		}
		
		protected override function viewOnClick(evt:MouseEvent):void
		{
			super.viewOnClick(evt);
			evt.stopPropagation();
			if (Move.isMoving == false) {
				evt.stopPropagation();
				MainEditor.inst.activeObject = this;
				MainEditor.inst.panel.changePanel2(panelActiveCreate());
			}
		}		
		
		protected override function panelActiveCreate():BasePanel {
			var panel:BasePanel = new BasePanel();
			
			panel.addTombol2('prop >', function(tbl:Tombol):void {
				MainEditor.inst.panel.changePanel2(panelPropCreate());
			});
			
			panelAddCommonAction(panel);
			
			return panel;
		}
		
		public override function changeProp(prop:String, value:String):Boolean
		{
			if (super.changeProp(prop,value)) {
				return true;
			}
			else if (prop == 'width') {
				panel.width = parseInt(value);
				return true;
			}
			else if (prop == 'height') {
				panel.height = parseInt(value);
				return true;
			}
			else {
				trace('prop unhandled' + prop);
			}
			
			return false;
		}		
		
		protected function refresh(w:Number, h:Number):void {
			panel.width = w;
			panel.height = h;
		}
		
		public override function set width(value:Number):void {
			refresh(value, height);
		}
		
		public override function set height(value:Number):void {
			refresh(width, value);
		}		
		
	}

}