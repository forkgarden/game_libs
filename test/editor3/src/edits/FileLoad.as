package edits 
{

	import flash.events.Event;
	import flash.filesystem.File;
	import flash.net.FileFilter;
	import flash.utils.ByteArray;
	
	public class FileLoad 
	{
		private var fileRefLoad:File = new File();
		private var _callBack:Function;
		private var path:String = '';

		public function FileLoad() 
		{
			fileRefLoad.addEventListener(Event.SELECT, fileRefLoadSelected);
			fileRefLoad.addEventListener(Event.CANCEL, fileRefLoadCanceled);
			fileRefLoad.addEventListener(Event.COMPLETE, fileRefLoadComplete);	
		}
		
		public function browse(filterDesc:String = "image", filterExt:String = "*.png"):void {
			fileRefLoad.browse([new FileFilter(filterDesc, filterExt)]);
		}
		
		private function fileRefLoadSelected(e:Event):void {
			//trace(e);
			path = fileRefLoad.nativePath;
			fileRefLoad.load();
		}
		
		private function fileRefLoadCanceled(e:Event):void {
			trace('file cancelled');
		}
		
		private function fileRefLoadComplete(e:Event):void {
			var byteAr:ByteArray;
			var str:String;
			
			try {
				trace('file load complete');
				byteAr = e.target.data;
				_callBack(byteAr, fileRefLoad.nativePath);
			}
			catch (e:Error) {
				trace(e.getStackTrace());
			}
		}		
		
		public function get callBack():Function 
		{
			return _callBack;
		}
		
		public function set callBack(value:Function):void 
		{
			_callBack = value;
		}
		
	}

}