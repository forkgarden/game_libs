package edits 
{
	/**
	 * ...
	 * @author test
	 */
	public class TransformData 
	{
		
		protected var _x:Number = 0;
		protected var _y:Number = 0;
		protected var _scaleX:Number = 1;
		protected var _scaleY:Number = 1;
		protected var _rotation:Number = 0;
		protected var _width:Number = 0;
		protected var _height:Number = 0;
		
		public function TransformData() 
		{
			
		}
		
		public function toObj():Object {
			var obj:Object = {};
			
			obj.x = _x;
			obj.y = _y;
			obj.scaleX = _scaleX;
			obj.scaleY = _scaleY;
			obj.rotation = _rotation;
			obj.width = _width;
			obj.height = _height;
			
			return obj;
		}
		
		public function fromObj(obj:Object):void {
			_x = obj.x;
			_y = obj.y;
			_scaleX = obj.scaleX;
			_scaleY = obj.scaleY;
			_rotation = obj.rotation;
			_width = obj.width;
			_height = obj.height;
		}
		
		public function get x():Number 
		{
			return _x;
		}
		
		public function set x(value:Number):void 
		{
			_x = value;
		}
		
		public function get y():Number 
		{
			return _y;
		}
		
		public function set y(value:Number):void 
		{
			_y = value;
		}
		
		public function get scaleX():Number 
		{
			return _scaleX;
		}
		
		public function set scaleX(value:Number):void 
		{
			_scaleX = value;
		}
		
		public function get scaleY():Number 
		{
			return _scaleY;
		}
		
		public function set scaleY(value:Number):void 
		{
			_scaleY = value;
		}
		
		public function get rotation():Number 
		{
			return _rotation;
		}
		
		public function set rotation(value:Number):void 
		{
			_rotation = value;
		}
		
		public function get width():Number 
		{
			return _width;
		}
		
		public function set width(value:Number):void 
		{
			_width = value;
		}
		
		public function get height():Number 
		{
			return _height;
		}
		
		public function set height(value:Number):void 
		{
			_height = value;
		}
		
	}

}