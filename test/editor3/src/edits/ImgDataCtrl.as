package edits {

	public class ImgDataCtrl 
	{
		
		protected var _imgs:Vector.<ImgData> = new Vector.<ImgData>();
		
		public function ImgDataCtrl() 
		{
			
		}
		
		public function reset():void {
			var imgData:ImgData;
			
			while (_imgs.length > 0) {
				imgData = _imgs.pop();
				imgData.destroy();
			}
		}
		
		public function toObj():Object {
			var obj:Object = {};
			var ar:Array = [];
			var imgData:ImgData;
			var imgDataObj:Object;
			
			for each (imgData in _imgs) {
				imgDataObj = imgData.toObj();
				ar.push(imgDataObj);
			}
			
			return ar;
		}
		
		public function fromObj(obj:Object):void {
			var ar:Array = obj as Array;
			var objAr:Object;
			var imgData:ImgData = new ImgData();
			
			if (!ar) throw new Error();
			
			for each (objAr in ar) {
				imgData = new ImgData();
				imgData.fromObj(objAr);
				_imgs.push(imgData);
			}
		}
		
		public function getImgDataById(id:int):ImgData {
			var item:ImgData;
			
			for each (item in _imgs) {
				if (item.id == id) return item;
			}
			
			return null;
		}
		
		protected function itemAdaById(id:int):Boolean {
			var item:ImgData;
			
			for each (item in _imgs) {
				if (item.id == id) return true;
			}
			
			return false;
		}
		
		public function getId():int {
			var id:int;
			
			id = _imgs.length;
			
			while (true) {
				if (itemAdaById(id)) {
					id++;
				}
				else {
					return id;
				}
			}
			
			return 0;
		}
		
		public function addItem(item:ImgData):void {
			imgs.push(item);
		}
		
		public function get imgs():Vector.<ImgData> 
		{
			return _imgs;
		}
		
		public function set imgs(value:Vector.<ImgData>):void 
		{
			_imgs = value;
		}
		
	}

}