package edits 
{
	import fg.ui.HLayout;
	import fg.ui.Tombol;
	import flash.display.Sprite;
	import objs.ObjEdit;
	
	public class StrukturGaris extends Sprite
	{
		private var hLayout:HLayout;
		private var struktur:Vector.<ObjEdit>;
		
		public function StrukturGaris() 
		{
			
		}
		
		public function build(obj:ObjEdit):void {
			var item:ObjEdit;
			var tombol:Tombol;
			
			destroy();
			
			while (struktur.length > 0) struktur.pop();
			
			struktur.push(obj);
			if (obj.parentObj) buildNext(struktur, obj);
			
			//build view
			hLayout = new HLayout();
			for each (item in struktur) {
				tombol = new Tombol();
				tombol.label = item.typeStr;
				hLayout.addItem(tombol);
			}
		}
		
		public function destroy():void {
			var obj:ObjEdit;
			
			//TODO:
		}
		
		public function buildNext(struktur:Vector.<ObjEdit>, obj:ObjEdit):void {
			struktur.push(obj);
			if (obj.parentObj) buildNext(struktur, obj);
		}	
		
	}

}