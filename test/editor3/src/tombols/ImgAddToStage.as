package tombols 
{
	import fg.ui.Tombol;
	import flash.events.MouseEvent;
	
	public class ImgAddToStage extends Tombol
	{
		
		public function ImgAddToStage() 
		{
			setLabel('put');
		}
		
		protected override function viewOnClick(e:MouseEvent):void {
			//load more image
			fileLoad = new FileLoad();
			fileLoad.browse();
			fileLoad.callBack = fileOnLoad;
		}			
		
		
	}

}