package panels 
{
	import fg.ui.Tombol;
	import fg.ui.VScrollList;
	import flash.events.MouseEvent;
	import objs.ObjEdit;
	import objs.ObjPanel;
	import objs.ObjText;
	import objs.ObjTombol;
	import objs.ObjVScrollList;
	import ui.Label;
	import ui.LabelPanel;

	public class MenuPanel extends BasePanel
	{
		
		protected var tombolPanelTambah:Tombol = new Tombol();
		
		protected function compListBuild():VScrollList {
			var scroll:VScrollList;
			var obj:ObjEdit;
			var label:LabelPanel;
			
			scroll = new VScrollList();
			scroll.width = 150;
			for each (obj in MainEditor.inst.halAktif.childs) {
				label = new LabelPanel();
				label.setWidth(150);
				label.teks.text = obj.typeStr;
				label.obj = obj;
				label.addEventListener(MouseEvent.CLICK, function(evt:MouseEvent):void {
					MainEditor.inst.halAktif.deselect();
					(evt.currentTarget as LabelPanel).obj.select();
				});
				scroll.addItem(label);
			}
			return scroll;
		}
		
		public override function destroy():void {
			super.destroy();
			
		}
		
		protected function panelComponentCreate():BasePanel {
			var panel:BasePanel;
			var tombol:ObjTombol;
			
			panel = new BasePanel();
			panel.setLabel('Component:');
			panel.cont.addItem(compListBuild());
			panel.addTombol2('Tambah', function(tbl:Tombol):void {
				MainEditor.inst.panel.modalShow(AddChildPanel.create().buildList().setCallBackSelectObject(
					function(objIdx:int):void {
						if (objIdx == ObjEdit.TY_BUTTON) {
							tombol = new ObjTombol();
							MainEditor.inst.halAktif.addChild2(tombol);
						}
						else if (objIdx == ObjEdit.TY_VSCROLL_LIST) {
							MainEditor.inst.halAktif.addChild2(new ObjVScrollList());
						}
						else if (objIdx == ObjEdit.TY_TEXT) {
							MainEditor.inst.halAktif.addChild2(new ObjText());
						}
						else if (objIdx == ObjEdit.TY_PANEL) {
							MainEditor.inst.halAktif.addChild2(new ObjPanel());
						}
						else {
							trace('add object undefined ' + objIdx);
						}
						MainEditor.inst.panel.changePanel2(new MenuPanel());
					}
				));
			});
			
			return panel;
		}
		
		public function MenuPanel() 
		{
			//super();
			
			/*
			addTombol2('test', function(tbl:Tombol):void {
				trace('test clicked');
			});
			*/
			
			addTombol2('component', function(tbl:Tombol):void {
				MainEditor.inst.panel.changePanel2(panelComponentCreate());
			});
			
			addTombol2('Library', function(tbl:Tombol):void {
				MainEditor.inst.panel.changePanel2(new LibPanel());
			});
			
			addTombol2('Unlock All', function(tbl:Tombol):void {
				MainEditor.inst.halAktif.unLock();
			});
			
			addTombol2('Unhide All', function(tbl:Tombol):void {
				MainEditor.inst.halAktif.unHide();
			});
			
			/*
			addTombol2('Tombol', function(tbl:Tombol):void {
				MainEditor.inst.head.addChild2(new ObjTombol());
			});
			*/
			
			addTombol2('Load', function(tbl:Tombol):void {
				MainEditor.inst.load();
			})
			
			addTombol2("Save", function(tbl:Tombol):void {
				MainEditor.inst.save();
			})
			
			/*
			addTombol2("vscrolllist", function(tbl:Tombol):void {
				MainEditor.inst.head.addChild2(new ObjVScrollList());
			})
			*/
			
		}
		
		protected override function tombolOnClick(tbl:Tombol):void {
			
		}
		
	}

}