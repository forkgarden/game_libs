package panels 
{
	import edits.ImgData;
	import edits.ImgDataCtrl;
	import fg.ui.Tombol;
	import fg.ui.VScrollList;
	import objs.ObjEdit;
	import objs.ObjImage;
	import tombols.ImgLoad;
	import ui.BmpIcon;

	public class LibPanel extends BasePanel
	{
		protected var scroll:VScrollList;
		protected var selectedIcon:BmpIcon;
		protected var bmps:Vector.<BmpIcon>;
		
		public function LibPanel() 
		{
			var bmpIcon:BmpIcon;
			var imgLoad:ImgLoad;
			
			scroll = new VScrollList();
			scroll.width = 90;
			
			bmps = new Vector.<BmpIcon>();
			
			_cont.addItem(scroll);
			
			selectedIcon = null
			updateBmpSelectedIconView();
			
			imgLoad = new ImgLoad();
			imgLoad.callBack = imgOnLoad;
			_cont.addItem(new ImgLoad());
			
			/*
			addTombol2(
				' put ', 
				function(tbl:Tombol):void {
					var activeObj:ObjEdit;
					var child:ObjImage;
					
					activeObj = MainEditor.inst.activeObject;
					
					if (activeObj.type == ObjEdit.TY_CONTAINER) {
						child = new ObjImage();
						child.setImgData(selectedIcon.imgData);
						child.refresh();
						
						activeObj.addChild2(child);
						//MainEditor.inst.render();
					}
					else {
						trace('active obj is not a container');
					}
					
				}
			)
			
			addTombol2(' << ', function(tbl:Tombol):void {
				MainEditor.inst.panel.changePanel2(new MenuPanel());
			});
			*/
			
		}
		
		public override function refresh():void {
			var imgData:ImgData;
			var imgCtrl:ImgDataCtrl = MainEditor.inst.imgCtrl;
			var bmpIcon:BmpIcon;
			
			scroll.clear();
			scroll.refresh();
			
			for each (imgData in imgCtrl.imgs) {
				bmpIcon = new BmpIcon();
				bmpIcon.setImgData(imgData);
				bmpIcon.render();
				bmpIcon.setOnClick(iconOnClick);
				bmps.push(bmpIcon);
				
				scroll.addItem(bmpIcon);
			}
		}
		
		public function iconOnClick(icon:BmpIcon):void {
			trace('icon on click');
			selectedIcon = icon;
			updateBmpSelectedIconView();
		}
		
		public override function destroy():void {
			super.destroy();
			var item:BmpIcon;
			
			removeChildren();
			
			while (bmps.length > 0) {
				item = bmps.pop();
				item.destroy();
			}
			
			scroll.destroy();
			selectedIcon = null;
			scroll = null;
			bmps = null;
		}
		
		protected function imgOnLoad(imgLoad:ImgLoad):void {
			refresh();
		}
		
		protected function updateBmpSelectedIconView():void {
			var bmpIcon:BmpIcon;
			
			trace('update selected icon view');
			trace('selected icon ' + selectedIcon);
			
			for each (bmpIcon in bmps) {
				bmpIcon.borderUpdate(false);
				if (bmpIcon == selectedIcon) {
					trace('border update');
					bmpIcon.borderUpdate(true);
				}
			}
		}
		
		//protected function bmpIconCreate(url:String, bmp:Bitmap):BmpIcon {
			//var res:BmpIcon;
			//
			//res = BmpIcon
					//.create(url, bmp.bitmapData)
					//.setOnClick(
						//function(bmpIcon):void {
							//selectedIcon = bmpIcon;
							//updateBmpSelectedIconView();
						//}
					//);
					//
			//bmps.push(res);
			//return res;
		//}
		
		
		
		
	}

}