package panels 
{
	import objs.ObjEdit;
	import fg.ui.Tombol;
	
	public class AddChildPanel extends BasePanel
	{
		protected var _callBackSelectedObject:Function;
		protected var itemFilter:Vector.<int>;
		
		public function AddChildPanel() 
		{
			itemFilter = new Vector.<int>();
			setFilter([
				ObjEdit.TY_BUTTON,
				ObjEdit.TY_IMAGE,
				ObjEdit.TY_PANEL,
				ObjEdit.TY_TEXT,
				ObjEdit.TY_VSCROLL_LIST
			]);
		}
		
		public function setFilter(ar:Array):AddChildPanel {
			var obj:int;
			
			while (itemFilter.length > 0) {
				itemFilter.pop();
			}
			
			for each (obj in ar) {
				itemFilter.push(obj);
			}
			
			return this;
		}
		
		public function buildList():AddChildPanel {
			var idx:int;
			
			for each (idx in itemFilter) {
				if (idx == ObjEdit.TY_BUTTON) {
					addTombol2('tombol', function(tbl:Tombol):void {
						_callBackSelectedObject(ObjEdit.TY_BUTTON);
						MainEditor.inst.panel.modalClose();
					});					
				}
				else if (idx == ObjEdit.TY_PAGE) {
					addTombol2('container', function(tbl:Tombol):void {
						_callBackSelectedObject(ObjEdit.TY_PAGE);
						MainEditor.inst.panel.modalClose();
					});					
				}
				else if (idx == ObjEdit.TY_IMAGE) {
					addTombol2('image', function(tbl:Tombol):void {
						_callBackSelectedObject(ObjEdit.TY_IMAGE);
						MainEditor.inst.panel.modalClose();
					});					
				}
				else if (idx == ObjEdit.TY_PANEL) {
					addTombol2('panel3D', function(tbl:Tombol):void {
						_callBackSelectedObject(ObjEdit.TY_PANEL);
						MainEditor.inst.panel.modalClose();
					});
					
				}
				else if (idx == ObjEdit.TY_TEXT) {
					addTombol2('text', function(tbl:Tombol):void {
						_callBackSelectedObject(ObjEdit.TY_TEXT);
						MainEditor.inst.panel.modalClose();
					});
					
				}
				else if (idx == ObjEdit.TY_VSCROLL_LIST) {
					addTombol2('VSCrollList', function(tbl:Tombol):void {
						_callBackSelectedObject(ObjEdit.TY_VSCROLL_LIST);
						MainEditor.inst.panel.modalClose();
					});
				}
				else {
					throw new Error();
				}
			}
			
			return this;
		}
		
		public static function create():AddChildPanel {
			return new AddChildPanel();
		}
		
		public function setCallBackSelectObject(func:Function):AddChildPanel {
			_callBackSelectedObject = func;
			return this;
		}
		
		public function get callBackSelectedObject():Function 
		{
			return _callBackSelectedObject;
		}
		
		public function set callBackSelectedObject(value:Function):void 
		{
			_callBackSelectedObject = value;
		}
		
	}

}