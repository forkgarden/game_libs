package panels 
{
	import fg.ui.Input;
	import fg.ui.Panel3D;
	import fg.ui.Tombol;
	import fg.ui.VLayout;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import ui.InputLabel;
	import ui.Label;
	
	public class BasePanel extends Sprite
	{
		protected var back:Panel3D = new Panel3D();
		protected var _cont:VLayout = new VLayout();
		protected var backBtn:Tombol;
		protected var tombols:Vector.<Tombol> = new Vector.<Tombol>();
		protected var inputs:Vector.<Input> = new Vector.<Input>();
		protected var _callBackTombolOnClick:Function;
		
		public function BasePanel() 
		{
			back.width = 150;
			back.height = 600;
			addChild(back);
			
			addChild(_cont);
			
			backBtn = new Tombol();
			backBtn.width = 100;
			backBtn.label = 'Back';
			backBtn.onClick = tombolOnClick;
			
			_cont.x = 8;
			_cont.y = 8;
		}
		
		public function setLabel(str:String):void {
			var label:Label;
			
			label = new Label(str);
			_cont.addItem(label);
		}
		
		public function refresh():void {
			
		}
		
		protected function getTombolById(str:String):Tombol {
			var tombol:Tombol;
			
			for each (tombol in tombols) {
				if (tombol.label == str) return tombol;
			}
			
			throw new Error();
			return null;
		}
		
		protected function btnCreate(str:String):Tombol {
			var tombol:Tombol;
			
			tombol = new Tombol();
			tombol.label = str;
			tombol.id = str;
			tombol.onClick = tombolOnClick;
			tombol.width = 100;
			
			return tombol;
		}
		
		protected function addTombol(label:String):void {
			var tombol:Tombol;
			
			tombol = btnCreate(label);
			
			_cont.addItem(tombol);
			tombols.push(tombol);
		}
		
		public function addInput(input:InputLabel):void {
			_cont.addItem(input);
		}
		
		public function addTombol2(label:String, onClick:Function):void {
			var tombol:Tombol;
			
			tombol = btnCreate(label);
			tombol.onClick = onClick;
			_cont.addItem(tombol);
			tombols.push(tombol);
		}
		
		public function destroy():void {
			var tombol:Tombol;
			var input:Input;
			
			while (inputs.length>0) {
				input = inputs.pop();
				input.destroy();
			}
			
			while (tombols.length > 0) {
				tombol = tombols.pop();
				tombol.destroy();
			}
			
			cont.destroy();
			back.destroy();
			backBtn.destroy();
			_callBackTombolOnClick = null;
			
			removeChildren();
		}
		
		protected function tombolOnClick(tbl:Tombol):void {
			
		}
		
		public function get cont():VLayout 
		{
			return _cont;
		}
		
		public function get callBackTombolOnClick():Function 
		{
			return _callBackTombolOnClick;
		}
		
		public function set callBackTombolOnClick(value:Function):void 
		{
			_callBackTombolOnClick = value;
		}
		
	}

}