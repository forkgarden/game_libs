/*
 * TODO:
 * buat layout yang bisa di save, di recreate
 * 
 * */
package
{
	import edits.EditorPanel;
	import edits.FileLoad;
	import edits.ImgDataCtrl;
	import edits.Panel;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.utils.ByteArray;
	import flash.utils.setTimeout;
	import objs.ObjHalaman;
	import objs.ObjEdit;
	import panels.MenuPanel;
	
	public class MainEditor extends Sprite 
	{
		
		protected var _editorPanel:edits.EditorPanel;
		protected var _panel:edits.Panel;
		protected var _activeObject:ObjEdit;
		protected var _halAktif:ObjHalaman;
		protected var _imgCtrl:ImgDataCtrl;
		protected var fileLoader:FileLoad;
		protected var buildStr:String;
		protected var buildObj:Object;
		protected var halamans:Vector.<ObjHalaman>;
		//protected var halAktif:ObjHalaman;
		
		protected static var _inst:MainEditor;
		
		public function MainEditor() 
		{
			_inst = this;
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event = null):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			_halAktif = new ObjHalaman();
			_halAktif.flHead = true;
			_activeObject = _halAktif;
			
			_imgCtrl = new ImgDataCtrl();
			_editorPanel = new edits.EditorPanel();
			_editorPanel.addEventListener(MouseEvent.CLICK, editorPanelOnClick);
			addChild(_editorPanel);
			
			panel = new edits.Panel();
			addChild(panel);
			panel.x = 650;
			panel.changePanel2(new MenuPanel());
			
			halamans = new Vector.<ObjHalaman>();
			halamans.push(_halAktif);
			
			render();
			
			this.stage.addEventListener(KeyboardEvent.KEY_DOWN, keyOnDown, false, 0, true);
			
		}
		
		protected function editorPanelOnClick(e:MouseEvent):void {
			_panel.changePanel2(new MenuPanel());
		}
		
		protected function renderObj(obj:ObjEdit):void {
			var item:ObjEdit;
			
			for each(item in obj.childs) {
				obj.addChild(item);
				renderObj(item);
				item.refresh();
			}
		}
		
		public function render():void {
			_editorPanel.cont.removeChildren();
			_editorPanel.cont.addChild(_halAktif);
			_halAktif.refresh();
			renderObj(_halAktif);
		}
		
		protected function bmpOnLoaded():void {
		}
		
		protected function keyOnDown(e:KeyboardEvent):void {
			if (e.keyCode == 27) {
				panel.changePanel(MenuPanel);
			}
		}
		
		public function build():void {
			_halAktif.destroy();
			
			_imgCtrl.fromObj(buildObj.imgCtrl);
			_halAktif = new ObjHalaman();
			_halAktif.fromObj(buildObj.head);
			addChild(_halAktif);
			
			render();
		}
		
		public function load():void {
			if (!fileLoader) fileLoader = new FileLoad();
			fileLoader.browse("txt file", "*.txt");
			fileLoader.callBack = fileLoadOnLoaded;
		}
		
		public function save():void {
			var obj:Object;
			var str:String;
			var file:File;
			var fileStream:FileStream;
			var fileContent:String;
			
			obj = {};
			obj.head = _halAktif.toObj();
			obj.imgCtrl = _imgCtrl.toObj();
			
			file = new File();
			file = File.desktopDirectory.resolvePath('save.txt');
			
			fileContent = (JSON.stringify(obj));
			
			fileStream = new FileStream();
			fileStream.open(file, FileMode.WRITE);
			fileStream.writeUTFBytes(fileContent);
			fileStream.close();
		}
		
		protected function fileLoadOnLoaded(byteAr:ByteArray, path:String):void {
			var str:String;
			var obj:Object;
			
			str = byteAr.readUTFBytes(byteAr.length);
			obj = JSON.parse(str);
			
			buildObj = obj;
			
			setTimeout(build, 0);
			
			//build();
		}
		
		static public function get inst():MainEditor 
		{
			return _inst;
		}
		
		public function get editorPanel():edits.EditorPanel 
		{
			return _editorPanel;
		}
		
		public function get panel():edits.Panel 
		{
			return _panel;
		}
		
		public function set panel(value:edits.Panel):void 
		{
			_panel = value;
		}
		
		public function get activeObject():ObjEdit 
		{
			return _activeObject;
		}
		
		public function set activeObject(value:ObjEdit):void 
		{
			_activeObject = value;
		}
		
		public function get halAktif():ObjHalaman 
		{
			return _halAktif;
		}
		
		public function set halAktif(value:ObjHalaman):void 
		{
			_halAktif = value;
		}
		
		public function get imgCtrl():ImgDataCtrl 
		{
			return _imgCtrl;
		}
		
		public function set imgCtrl(value:ImgDataCtrl):void 
		{
			_imgCtrl = value;
		}
		
	}
	
}