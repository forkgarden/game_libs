package
{
	import fg.ui.VScrollList;
	import flash.desktop.NativeApplication;
	import flash.events.Event;
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.ui.Multitouch;
	import flash.ui.MultitouchInputMode;
	
	/**
	 * ...
	 * @author test
	 */
	public class TestScroll extends Sprite 
	{
		[Embed(source = "../bin/imgs/heli.jpg")]
		private var Img:Class;
		
		private var scroll:VScrollList;
		
		public function TestScroll() 
		{
			stage.scaleMode = StageScaleMode.NO_SCALE;
			stage.align = StageAlign.TOP_LEFT;
			stage.addEventListener(Event.DEACTIVATE, deactivate);
			
			// touch or gesture?
			Multitouch.inputMode = MultitouchInputMode.TOUCH_POINT;
			
			// Entry point
			// New to AIR? Please read *carefully* the readme.txt files!
			scroll = new VScrollList();
			scroll.addItem(new Img());
			scroll.addItem(new Img());
			scroll.addItem(new Img());
			addChild(scroll);
		}
		
		private function deactivate(e:Event):void 
		{
			// make sure the app behaves well (or exits) when in background
			//NativeApplication.nativeApplication.exit();
		}
		
	}
	
}