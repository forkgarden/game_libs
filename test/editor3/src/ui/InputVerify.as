package ui 
{
	import fg.ui.HLayout;
	import fg.ui.Input;
	import fg.ui.Panel3D;
	import fg.ui.Teks;
	import fg.ui.Tombol;
	import fg.ui.VLayout;
	import flash.display.Sprite;
	import flash.text.TextFieldType;
	
	public class InputVerify extends Sprite
	{
		private var back:Panel3D;
		private var input:Input;
		private var label:Label;
		private var layout:VLayout;
		private var hLayout:HLayout;
		private var tbl:Tombol;
		
		public function InputVerify() 
		{
			layout = new VLayout();
			label = new Label('label');
			layout.addItem(label);
			
			input = new Input();
			layout.addItem(input);
			
			tbl = new Tombol();
			tbl.width = 48;
			tbl.setLabel('check');
			layout.addItem(tbl);
			
			addChild(layout);
		}
		
	}

}