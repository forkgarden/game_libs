package ui 
{
	import edits.TransformData;
	import fg.ui.Panel3D;
	import fg.ui.Teks;
	import flash.display.Sprite;
	import objs.ObjEdit;

	public class LabelPanel extends Sprite
	{
		protected var back:Panel3D;
		protected var _teks:Teks;
		protected var _trans:TransformData;
		protected var borderSelect:Sprite;
		protected var _enable:Boolean = true;
		protected var _obj:ObjEdit;
		
		public function LabelPanel() 
		{
			borderSelect = new Sprite();
			borderSelect.mouseEnabled = false;
			
			back = new Panel3D();
			
			_trans = new TransformData();
			_trans.width = 120;
			_trans.height = 30;
			
			_teks = new Teks();
			
			addChild(back);
			addChild(_teks);
			addChild(borderSelect);
			
			mouseChildren = false;
			render();
		}
		
		public function setWidth(n:Number):void {
			_trans.width = n;
			render();
		}
		
		public function destroy():void {
			back.destroy();
			_teks.destroy();
			removeChildren();
			_trans = null;
			back = null;
			_teks = null;
		}
		
		public function select():void {
			borderSelect.graphics.clear();
			borderSelect.graphics.lineStyle(3, 0xffff00);
			borderSelect.graphics.drawRect(0, 0, width, height);
		}
		
		public function deselect():void {
			borderSelect.graphics.clear();
		}
		
		public function get trans():TransformData 
		{
			return _trans;
		}
		
		public function get teks():Teks 
		{
			return _teks;
		}
		
		public function get enable():Boolean 
		{
			return _enable;
		}
		
		public function set enable(value:Boolean):void 
		{
			_enable = value;
			if (_enable) {
				_teks.textColor = 0x0;
			}
			else {
				_teks.textColor = 0xeeeeee;
			}
		}
		
		public function get obj():ObjEdit 
		{
			return _obj;
		}
		
		public function set obj(value:ObjEdit):void 
		{
			_obj = value;
		}
		
		public function render():void {
			back.setWidth(_trans.width);
			back.setHeight(_trans.height);
			_teks.width = _trans.width;
			_teks.height = _trans.height;
		}
		
		
	}
}