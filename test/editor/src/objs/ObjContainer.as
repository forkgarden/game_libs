package objs 
{
	import flash.display.DisplayObject;
	import flash.display.Sprite;

	public class ObjContainer extends ObjEdit
	{		
		public function ObjContainer() 
		{
			super();
			refresh(32, 32);
		}
		
		public static function create():ObjContainer {
			var res:ObjContainer;
			
			res = new ObjContainer();
			return res;
		}
		
		protected function refresh(w:int, h:int):void {
			graphics.beginFill(0xffff00, .3);
			graphics.drawRect(0, 0, 32, 32);
			graphics.endFill();
		}
	}

}