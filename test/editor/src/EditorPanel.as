package 
{
	import fg.ui.Panel3D;
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	/**
	 * ...
	 * @author test
	 */
	public class EditorPanel extends Sprite
	{
		protected var _back:Panel3D = new Panel3D();
		protected var _cont:Sprite = new Sprite();
		
		public function EditorPanel() 
		{
			_back = new Panel3D();
			_back.width = 640;
			_back.height = 480;
			
			addChild(_back);
			
			_cont = new Sprite();
			addChild(_cont);
		}
		
		public function get back():Panel3D 
		{
			return _back;
		}
		
		public function get cont():Sprite 
		{
			return _cont;
		}
		
		public function set cont(value:Sprite):void 
		{
			_cont = value;
		}
		
		public function resize(w:Number, h:Number):void {
			_back.setWidth(w).setHeight(h);
		}
		
	}

}