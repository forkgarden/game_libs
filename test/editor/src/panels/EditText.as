package panels 
{
	import fg.ui.Teks;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFieldType;
	
	public class EditText extends Sprite
	{
		protected var _edit:Teks;
		protected var _callBack:Function;
		
		public function EditText() 
		{
			_edit = new Teks();
			_edit.type = TextFieldType.INPUT;
			_edit.border = true;
			_edit.height = _edit.textHeight + 4;
			_edit.autoSize = TextFieldAutoSize.NONE;
			_edit.wordWrap = false;
			addChild(_edit);
			
			_edit.addEventListener(Event.CHANGE, editOnChange, false, 0, true);
		}
		
		protected function editOnChange(e:Event):void {
			_callBack(_edit);
		}
		
		public function get callBack():Function 
		{
			return _callBack;
		}
		
		public function set callBack(value:Function):void 
		{
			_callBack = value;
		}
		
		public function get edit():Teks 
		{
			return _edit;
		}
		
		public function set edit(value:Teks):void 
		{
			_edit = value;
		}
		
	}

}