package panels 
{
	import fg.ui.Panel3D;
	import fg.ui.Tombol;
	import fg.ui.VLayout;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	public class BasePanel extends Sprite
	{
		protected var back:Panel3D = new Panel3D();
		protected var cont:VLayout = new VLayout();
		protected var backBtn:Tombol;
		protected var tombols:Vector.<Tombol> = new Vector.<Tombol>();
		
		public function BasePanel() 
		{
			back.width = 150;
			back.height = 600;
			addChild(back);
			
			addChild(cont);
			
			backBtn = new Tombol();
			backBtn.width = 100;
			backBtn.label = 'Back';
			backBtn.onClick = tombolOnClick;
			
			cont.x = 8;
			cont.y = 8;
		}
		
		public function refresh():void {
			
		}
		
		protected function getTombolById(str:String):Tombol {
			var tombol:Tombol;
			
			for each (tombol in tombols) {
				if (tombol.label == str) return tombol;
			}
			
			throw new Error();
			return null;
		}
		
		protected function btnCreate(str:String):Tombol {
			var tombol:Tombol;
			
			tombol = new Tombol();
			tombol.label = str;
			tombol.id = str;
			tombol.onClick = tombolOnClick;
			tombol.width = 100;
			
			return tombol;
		}
		
		protected function addTombol(label:String):void {
			var tombol:Tombol;
			
			tombol = btnCreate(label);
			
			cont.addItem(tombol);
			tombols.push(tombol);
		}
		
		protected function addTombol2(label:String, onClick:Function):void {
			var tombol:Tombol;
			
			tombol = btnCreate(label);
			tombol.onClick = onClick;
			cont.addItem(tombol);
			tombols.push(tombol);
		}
		
		public function destroy():void {
			
			while (tombols.length > 0) {
				tombols.pop();
			}
			
			removeChildren();
		}
		
		protected function tombolOnClick(tbl:Tombol):void {
			
		}
		
	}

}