package panels 
{
	import fg.ui.Teks;
	import flash.text.TextFieldAutoSize;

	public class Label extends Teks {
		
		public function Label(label:String) {
			super();
			text = label;
			height = textHeight + 4;
			autoSize = TextFieldAutoSize.NONE;
		}
		
	}

}