package tombols 
{
	import edits.ImgData;
	import fg.ui.Tombol;
	import flash.display.Bitmap;
	import flash.display.Loader;
	import flash.display.LoaderInfo;
	import flash.display.Sprite;
	import flash.errors.IOError;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.MouseEvent;
	import edits.FileLoad;
	import flash.system.ImageDecodingPolicy;
	import flash.utils.ByteArray;
	
	public class ImgLoad extends Tombol
	{
		private var fileLoad:edits.FileLoad;
		private var loader:Loader;
		private var _callBack:Function;
		
		public function ImgLoad() 
		{
			setLabel('load');
			
		}
		
		protected override function viewOnClick(e:MouseEvent):void {
			//load more image
			fileLoad = new FileLoad();
			fileLoad.browse();
			fileLoad.callBack = fileOnLoad;
		}		
		
		protected function fileOnLoad(byte:ByteArray):void {
			loader = new Loader();
			
			loader.contentLoaderInfo.addEventListener(Event.COMPLETE, loadOnComplete);
			loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, loadOnError);
			loader.contentLoaderInfo.addEventListener(Event.INIT, loadOnInit);
			loader.loadBytes(byte);
		}
		
		protected function loadOnInit(e:Event):void {
			
		}
		
		protected function loadOnComplete(e:Event):void {
			trace('load complete');
			var imgData:ImgData;
			
			imgData = new ImgData();
			imgData.url = '';
			imgData.id = MainEditor.inst.imgCtrl.getId();
			imgData.bmp = ((e.currentTarget as LoaderInfo).content as Bitmap);
			
			MainEditor.inst.imgCtrl.addItem(imgData);
			MainEditor.inst.panel.panelCr.refresh();
		}
		
		protected function loadOnError(e:IOErrorEvent):void {
			
		}
		
		public function get callBack():Function 
		{
			return _callBack;
		}
		
		public function set callBack(value:Function):void 
		{
			_callBack = value;
		}
		
	}

}