package edits 
{
	/**
	 * ...
	 * @author test
	 */
	public class TransformData 
	{
		
		protected var _x:Number = 0;
		protected var _y:Number = 0;
		protected var _scaleX:Number = 1;
		protected var _scaleY:Number = 1;
		protected var _rotation:Number = 0;
		
		public function TransformData() 
		{
			
		}
		
		public function get x():Number 
		{
			return _x;
		}
		
		public function set x(value:Number):void 
		{
			_x = value;
		}
		
		public function get y():Number 
		{
			return _y;
		}
		
		public function set y(value:Number):void 
		{
			_y = value;
		}
		
		public function get scaleX():Number 
		{
			return _scaleX;
		}
		
		public function set scaleX(value:Number):void 
		{
			_scaleX = value;
		}
		
		public function get scaleY():Number 
		{
			return _scaleY;
		}
		
		public function set scaleY(value:Number):void 
		{
			_scaleY = value;
		}
		
		public function get rotation():Number 
		{
			return _rotation;
		}
		
		public function set rotation(value:Number):void 
		{
			_rotation = value;
		}
		
	}

}