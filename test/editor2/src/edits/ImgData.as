package edits 
{
	import flash.display.Bitmap;

	public class ImgData 
	{
		
		protected var _url:String = '';
		protected var _bmp:Bitmap;
		protected var _id:int = 0;
		
		public function ImgData() 
		{
			
		}
		
		public static function create():ImgData {
			return new ImgData();
		}
		
		public function get url():String 
		{
			return _url;
		}
		
		public function set url(value:String):void 
		{
			_url = value;
		}
		
		public function get bmp():Bitmap 
		{
			return _bmp;
		}
		
		public function set bmp(value:Bitmap):void 
		{
			if (value == null) throw new Error();
			_bmp = value;
		}
		
		public function get id():int 
		{
			return _id;
		}
		
		public function set id(value:int):void 
		{
			_id = value;
		}
		
	}

}