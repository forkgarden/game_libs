package edits 
{
	/**
	 * ...
	 * @author test
	 */
	public class ImgDataCtrl 
	{
		
		protected var _imgs:Vector.<ImgData> = new Vector.<ImgData>();
		
		public function ImgDataCtrl() 
		{
			
		}
		
		protected function itemAdaById(id:int):Boolean {
			var item:ImgData;
			
			for each (item in _imgs) {
				if (item.id == id) return true;
			}
			
			return false;
		}
		
		public function getId():int {
			var id:int;
			
			id = _imgs.length;
			
			while (true) {
				if (itemAdaById(id)) {
					id++;
				}
				else {
					return id;
				}
			}
			
			return 0;
		}
		
		public function addItem(item:ImgData):void {
			imgs.push(item);
		}
		
		public function get imgs():Vector.<ImgData> 
		{
			return _imgs;
		}
		
		public function set imgs(value:Vector.<ImgData>):void 
		{
			_imgs = value;
		}
		
	}

}