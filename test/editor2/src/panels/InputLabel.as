package panels 
{
	import fg.ui.Input;
	import fg.ui.Teks;
	import fg.ui.VLayout;
	import flash.display.Sprite;

	public class InputLabel extends Sprite
	{
		protected var vLayout:VLayout;
		protected var _onChange:Function;
		protected var _id:String;
		protected var _input:Input;
			
		public function InputLabel(title:String, id:String = null) 
		{
			var label:Label;
			
			if (id == null) id = title;
			
			_id = id;
			
			vLayout = new VLayout();
			addChild(vLayout);
			
			label = new Label(title);
			vLayout.addItem(label);
			
			_input = new Input();
			_input.onChange = editOnChange;
			vLayout.addItem(_input);
		}
		
		protected function editOnChange(teks:Teks):void {
			_onChange(this, teks);
		}
		
		public function get onChange():Function 
		{
			return _onChange;
		}
		
		public function set onChange(value:Function):void 
		{
			_onChange = value;
		}
		
		public function get id():String 
		{
			return _id;
		}
		
		public function set id(value:String):void 
		{
			_id = value;
		}
		
		public function get input():Input 
		{
			return _input;
		}
		
		public function set input(value:Input):void 
		{
			_input = value;
		}
		
	}

}