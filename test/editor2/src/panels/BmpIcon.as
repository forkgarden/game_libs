package panels 
{
	import edits.ImgData;
	import fg.ui.Panel3D;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Graphics;
	import flash.display.SimpleButton;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import objs.ObjEdit;

	public class BmpIcon extends Sprite
	{
		protected var back:Panel3D;
		protected var url:String;
		protected var bmpData:BitmapData;
		protected var tombol:SimpleButton;
		protected var callBack:Function;
		protected var border:Sprite;
		protected var _imgData:ImgData;
		
		public function BmpIcon() 
		{}
		
		public function setImgData(data:ImgData):BmpIcon {
			this._imgData = _imgData;
			return this;
		}
		
		public function render():BmpIcon {
			this.bmpData = bmpData;
			tombol = new SimpleButton(getUpState(), getUpState(), getDownState(), getUpState());
			addChild(tombol);
			
			border = new Sprite();
			border.mouseEnabled = false;
			border.mouseChildren = false;
			addChild(border);
			
			return this;
		}
		
		public function destroy():void {
			
		}
		
		public function borderUpdate(selected:Boolean = true):void {
			var g:Graphics;
			
			g = border.graphics;
			g.clear();
			
			if (selected) {
				g.lineStyle(3, 0xffff00, 1);
				g.drawRect(0, 0, 64, 64);
			}
		}
		
		public function setOnClick(func:Function):BmpIcon {
			callBack = func;
			tombol.addEventListener(MouseEvent.CLICK, tombolOnClick);
			return this;
		}
		
		public static function create():BmpIcon {
			var res:BmpIcon;
			
			res = new BmpIcon();
			return res;
		}
		
		protected function tombolOnClick(e:MouseEvent):void {
			callBack(this);
		}
		
		protected function getDownState():Sprite {
			var spr:Sprite;
			var back:Panel3D;
			var bmp:Bitmap;
			
			bmp = new Bitmap(bmpData);
			bmpResize(bmp);
			
			back = new Panel3D();
			back.width = 64;
			back.height = 64;
			back.reverse = true;
			spr = new Sprite();
			spr.addChild(back);
			spr.addChild(bmp);
			
			return spr;
		}
		
		protected function getUpState():Sprite {
			var spr:Sprite;
			var back:Panel3D;
			var bmp:Bitmap;
			
			bmp = new Bitmap(bmpData);
			bmpResize(bmp);
			
			back = new Panel3D();
			back.width = 64;
			back.height = 64;
			
			spr = new Sprite();
			spr.addChild(back);
			spr.addChild(bmp);
			
			return spr;
		}
		
		protected function bmpResize(bmp:Bitmap):void {
			var ratio:Number = 0;
			
			bmp.scaleX = 1;
			bmp.scaleY = 1;
			
			ratio = Math.min(60.0 / bmp.width, 60 / bmp.height);
			
			bmp.scaleX = ratio;
			bmp.scaleY = ratio;
			bmp.x = 32 - bmp.width / 2;
			bmp.y = 32 - bmp.height / 2;
		}
		
		public function get imgData():ImgData 
		{
			return _imgData;
		}
		
		public function set imgData(value:ImgData):void 
		{
			_imgData = value;
		}
		
		
	}
}