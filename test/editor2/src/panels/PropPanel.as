package panels 
{
	import fg.ui.Input;
	import fg.ui.Teks;
	import flash.events.Event;
	import flash.text.TextFieldAutoSize;
	import objs.ObjEdit;
	import objs.ObjText;
	
	public class PropPanel extends BasePanel {
		
		public function PropPanel() {
			var input:InputLabel;
			var teks:ObjText;
			
			input = new InputLabel('x:', 'pos_x');
			input.input.teks.text = ObjEdit.itemActive.x + '';
			input.onChange = inputOnChange;
			cont.addItem(input);
			
			input = new InputLabel('y:', 'pos_y');
			input.input.teks.text = ObjEdit.itemActive.y + '';
			input.onChange = inputOnChange;
			cont.addItem(input);
			
			input = new InputLabel('width:', 'width');
			input.input.teks.text = ObjEdit.itemActive.width + '';
			input.onChange = inputOnChange;
			cont.addItem(input);
			
			input = new InputLabel('height:', 'height');
			input.input.teks.text = ObjEdit.itemActive.height + '';
			input.onChange = inputOnChange;
			cont.addItem(input);
			
			if (ObjEdit.itemActive is ObjText) {
				teks = ObjEdit.itemActive as ObjText;
				
				input = new InputLabel('teks:');
				input.input.teks.text = teks.teks.text;
				
				input.onChange = function(input:InputLabel, txtField:Teks):void {
					teks.teks.text = txtField.text;
				}
				
				cont.addItem(input);
			}
		}
		
		protected function refresh():void {
			
		}
		
		protected function inputOnChange(input:InputLabel, teks:Teks):void {
			trace('input on change ' + input);
			
			/*
			
			if (input.id == 'pos_x') {
				ObjEdit.itemActive.x = parseFloat(teks.text);
			}
			else if (input.id == 'pos_y') {
				ObjEdit.itemActive.y = parseFloat(teks.text);
			}
			else if (input.id == 'width') {
				ObjEdit.itemActive.width = parseFloat(teks.text);
			}
			else if (input.id == 'height') {
				ObjEdit.itemActive.height = parseFloat(teks.text);
			}
			*/
		}
		
	}

}