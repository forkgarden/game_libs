package panels 
{
	import fg.ui.Tombol;
	import flash.events.MouseEvent;
	import objs.ObjEdit;
	import panels.BasePanel;
	
	public class ObjActivePanel extends BasePanel
	{
		
		protected var tblProp:Tombol = new Tombol();
		protected var tblDelete:Tombol = new Tombol();
		
		public function ObjActivePanel() 
		{
			super();
			addTombol('properties');
			addTombol('delete');
			addTombol('back');
			addTombol('add child');
		}
		
		protected override function tombolOnClick(tbl:Tombol):void {
			/*
			if (tbl == getTombolById('properties')) {
				MainEditor.inst.panel.changePanel(PropPanel);
			}
			else if (tbl == getTombolById('delete')) {
				ObjEdit.deleteObj(ObjEdit.itemActive);
			}
			else if (tbl == getTombolById('back')) {
				MainEditor.inst.panel.changePanel(MenuPanel);
			}
			else if (tbl == getTombolById('add child')) {
				MainEditor.inst.panel.changePanel(AddChildPanel);
			}
			else {
				throw new Error();
			}
			*/
		}		
		
	}

}