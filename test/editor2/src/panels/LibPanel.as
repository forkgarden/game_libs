package panels 
{
	import edits.ImgData;
	import edits.ImgDataCtrl;
	import fg.ui.Tombol;
	import fg.ui.VScrollList;
	import objs.ObjEdit;
	import objs.ObjImage;
	import tombols.ImgLoad;

	public class LibPanel extends BasePanel
	{
		protected var scroll:VScrollList;
		protected var selectedIcon:BmpIcon;
		protected var bmps:Vector.<BmpIcon>;
		
		public function LibPanel() 
		{
			//var bmp:BmpLoader;
			var bmpIcon:BmpIcon;
			var imgLoad:ImgLoad;
			
			scroll = new VScrollList();
			scroll.width = 90;
			
			bmps = new Vector.<BmpIcon>();
			
			cont.addItem(scroll);
			
			selectedIcon = null
			updateBmpSelectedIconView();
			
			imgLoad = new ImgLoad();
			imgLoad.callBack = imgOnLoad;
			cont.addItem(new ImgLoad());
			
			addTombol2(
				' put ', 
				function(tbl:Tombol):void {
					var activeObj:ObjEdit;
					
					activeObj = MainEditor.inst.activeObject;
					
					if (activeObj.type == ObjEdit.TY_CONTAINER) {
						activeObj.childs.push(
							ObjImage.create()
								.setImgData(selectedIcon.imgData)
								.render()
						);
						MainEditor.inst.render();
					}
					else {
						
					}
					
				}
			)
			
			addTombol2(' << ', function(tbl:Tombol):void {
				MainEditor.inst.panel.changePanel2(new MenuPanel());
			});
			
		}
		
		public override function refresh():void {
			var imgData:ImgData;
			var imgCtrl:ImgDataCtrl = MainEditor.inst.imgCtrl;
			
			scroll.clear();
			scroll.refresh();
			
			//trace('LibPanel:refresh');
			
			for each (imgData in imgCtrl.imgs) {
				//trace('refresh ' + imgData);
				scroll.addItem(
					BmpIcon.create()
						.setImgData(imgData)
						.setOnClick(iconOnClick)
						.render()
				);
			}
		}
		
		public function iconOnClick(icon:BmpIcon):void {
			selectedIcon = icon;
			updateBmpSelectedIconView();
		}
		
		public override function destroy():void {
			super.destroy();
			var item:BmpIcon;
			
			removeChildren();
			
			while (bmps.length > 0) {
				item = bmps.pop();
				item.destroy();
			}
			
			scroll.destroy();
			selectedIcon = null;
			scroll = null;
			bmps = null;
		}
			
		protected function imgOnLoad(imgLoad:ImgLoad):void {
			refresh();
		}
		
		protected function updateBmpSelectedIconView():void {
			var bmpIcon:BmpIcon;
			
			for each (bmpIcon in bmps) {
				bmpIcon.borderUpdate(false);
				if (bmpIcon == selectedIcon) {
					bmpIcon.borderUpdate(true);
				}
			}
		}
		
		//protected function bmpIconCreate(url:String, bmp:Bitmap):BmpIcon {
			//var res:BmpIcon;
			//
			//res = BmpIcon
					//.create(url, bmp.bitmapData)
					//.setOnClick(
						//function(bmpIcon):void {
							//selectedIcon = bmpIcon;
							//updateBmpSelectedIconView();
						//}
					//);
					//
			//bmps.push(res);
			//return res;
		//}
		
		
		
		
	}

}