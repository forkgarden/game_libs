/*
 * TODO:
 * save-load
 * 
 * 
 * */
package
{
	import edits.ImgDataCtrl;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import objs.ObjContainer;
	import objs.ObjEdit;
	import panels.MenuPanel;
	
	public class MainEditor extends Sprite 
	{
		
		protected var _editorPanel:EditorPanel;
		protected var _panel:Panel;
		//protected var _bmp:BmpBLoader;
		protected var _activeObject:ObjEdit;
		protected var _head:ObjEdit;
		protected var _imgCtrl:ImgDataCtrl;
		
		protected static var _inst:MainEditor;
		
		public function MainEditor() 
		{
			_inst = this;
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event = null):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			_activeObject = ObjContainer.create();
			_head = _activeObject;
			
			_imgCtrl = new ImgDataCtrl();
			_editorPanel = new EditorPanel();
			addChild(_editorPanel);
			
			panel = new Panel();
			addChild(panel);
			panel.x = 650;
			panel.changePanel2(new MenuPanel());
			
			render();
			
			this.stage.addEventListener(KeyboardEvent.KEY_DOWN, keyOnDown, false, 0, true);
			
		}
		
		protected function renderObj(obj:ObjEdit):void {
			var item:ObjEdit;
			
			for each(item in obj.childs) {
				obj.addChild(item);
				renderObj(item);
				item.render();
			}
		}
		
		public function render():void {
			_editorPanel.cont.removeChildren();
			_editorPanel.cont.addChild(_head);
			_head.render();
			renderObj(_head);
		}
		
		protected function bmpOnLoaded():void {
		}
		
		protected function keyOnDown(e:KeyboardEvent):void {
			if (e.keyCode == 27) {
				panel.changePanel(MenuPanel);
			}
		}
		
		public function save():void {
			
		}
		
		public function load():void {
			
		}
		
		static public function get inst():MainEditor 
		{
			return _inst;
		}
		
		public function get editorPanel():EditorPanel 
		{
			return _editorPanel;
		}
		
		public function get panel():Panel 
		{
			return _panel;
		}
		
		public function set panel(value:Panel):void 
		{
			_panel = value;
		}
		
		public function get activeObject():ObjEdit 
		{
			return _activeObject;
		}
		
		public function set activeObject(value:ObjEdit):void 
		{
			_activeObject = value;
		}
		
		public function get head():ObjEdit 
		{
			return _head;
		}
		
		public function set head(value:ObjEdit):void 
		{
			_head = value;
		}
		
		public function get imgCtrl():ImgDataCtrl 
		{
			return _imgCtrl;
		}
		
		public function set imgCtrl(value:ImgDataCtrl):void 
		{
			_imgCtrl = value;
		}
		
	}
	
}