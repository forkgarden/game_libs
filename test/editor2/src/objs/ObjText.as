package objs 
{
	import fg.ui.Teks;

	public class ObjText extends ObjEdit
	{
		protected var _teks:Teks;
		
		public function ObjText() 
		{
			_type = ObjEdit.TY_TEXT;
			_teks = new Teks();
			this.addChild(_teks);
		}
		
		public function get teks():Teks 
		{
			return _teks;
		}
		
		public override function save():Object {
			var obj:Object;
			
			obj.teks = _teks.text;
			
			return obj;
		}
		
		public override function load(obj:Object):void {
			_teks.text = obj.teks;
		}
		
	}

}