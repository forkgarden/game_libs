package objs 
{
	import edits.ImgData;
	import flash.display.Bitmap;
	/**
	 * ...
	 * @author test
	 */
	public class ObjImage extends ObjEdit
	{
		//protected var bmpLoader:BmpLoader;
		protected var bmp:Bitmap;
		protected var imgData:ImgData;
		
		public function ObjImage() 
		{
			_type = ObjEdit.TY_IMAGE;
		}
		
		public function setImgData(data:ImgData):ObjImage {
			this.imgData = imgData;
			return this;
		}
		
		public static function create():ObjImage {
			return new ObjImage();
		}
		
		public override function save():Object {
			var obj:Object;			
			return obj;
		}
		
	}

}