package objs 
{
	import fg.ui.Panel3D;
	
	public class ObjPanel extends ObjEdit
	{
		protected var panel:Panel3D;
		
		public function ObjPanel() 
		{
			super();
			_type = ObjEdit.TY_PANEL;
			panel = new Panel3D();
			addChild(panel);
		}
		
		protected function refresh(w:Number, h:Number):void {
			panel.width = w;
			panel.height = h;
		}
		
		public override function set width(value:Number):void {
			refresh(value, height);
		}
		
		public override function set height(value:Number):void {
			refresh(width, value);
		}		
		
	}

}