package objs
{
	import edits.TransformData;
	import fg.inters.Move;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import objs.ObjPanel;
	import panels.ObjActivePanel;
	
	public class ObjEdit extends Sprite
	{
		public static const TY_PANEL:int = 1;
		public static const TY_SPRITE:int = 2;
		public static const TY_TEXT:int = 3;
		public static const TY_IMAGE:int = 4;
		public static const TY_CONTAINER:int = 5;
		
		protected var move:Move;
		protected var _type:Number;
		protected var _childs:Vector.<ObjEdit>;
		protected var _transformData:TransformData;
		
		public function ObjEdit()
		{
			move = new Move(this, MainEditor.inst.stage);
			addEventListener(MouseEvent.CLICK, viewOnClick);
			_transformData = new TransformData();
			_childs = new Vector.<ObjEdit>();
		}
		
		public function render():void {
			x = _transformData.x;
			y = _transformData.y;
			scaleX  = _transformData.scaleX;
			scaleY = _transformData.scaleY;
			rotation = _transformData.rotation;
		}
		
		public function changeProp(prop:String, value:String):void
		{
			trace('change prop ' + prop + '/value ' + value);
		}
		
		public function save():Object
		{
			return {};
		}
		
		public function load(obj:Object):void
		{
			trace('load ' + obj);
		}
		
		public function destroy():void
		{	
			var obj:ObjEdit;
			
			move.destroy();
			removeChildren();
			for each (obj in _childs) {
				obj.destroy();
			}
		}
		
		protected function viewOnClick(e:MouseEvent):void
		{
			MainEditor.inst.activeObject = this;
			MainEditor.inst.panel.changePanel(ObjActivePanel);
		}
		
		public function get transformData():TransformData 
		{
			return _transformData;
		}
		
		public function set transformData(value:TransformData):void 
		{
			_transformData = value;
		}
		
		public function get childs():Vector.<ObjEdit> 
		{
			return _childs;
		}
		
		public function set childs(value:Vector.<ObjEdit>):void 
		{
			_childs = value;
		}
		
		public function get type():Number 
		{
			return _type;
		}
		
		public function set type(value:Number):void 
		{
			_type = value;
		}
	
	}

}