package 
{
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import panels.BasePanel;
	import panels.MenuPanel;
	import panels.ObjActivePanel;
	
	public class Panel extends Sprite
	{
		
		//protected var _cont:Sprite = new Sprite();
		protected var _panelCr:BasePanel;
		
		public function Panel() 
		{
		}
		
		public function changePanel(panel:Class):void {
			if (_panelCr) _panelCr.destroy();
			_panelCr = new panel();
			removeChildren();
			addChild(_panelCr);
		}
		
		public function changePanel2(panel:BasePanel):void {
			if (_panelCr) _panelCr.destroy();
			_panelCr = panel;
			removeChildren();
			addChild(panel);
		}
		
		public function get panelCr():BasePanel 
		{
			return _panelCr;
		}
		
		public function set panelCr(value:BasePanel):void 
		{
			_panelCr = value;
		}
	}

}