package {
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.utils.Dictionary;
	/**
	 * ...
	 * @author 
	 */
	public class TSprite extends Sprite {
	
		protected var animated:Boolean = false;
		protected var frameDatas:Vector.<Object> = new Vector.<Object>();
		protected var frames:Array;
		protected var frameCrIdx:int = 0;
		protected var isLooping:Boolean = true;
		protected var rect:Rectangle = new Rectangle();
		protected var frameHCount:int = 0;
		protected var disp:DisplayObject;
		protected var frameDim:Point = new Point();
				
		public function TSprite(d:DisplayObject, frameWidth:Number, frameHeight:Number, frame0:Number) {
			disp = d;
			addChild(disp);
			
			frameDim.x = frameWidth;
			frameDim.y = frameHeight;
			frameHCount = d.width / frameWidth;
			rect.x = 0;
			rect.y = 0;
			rect.width = frameWidth;
			rect.height = frameHeight;
			scrollRect = rect;
			
			//throw new Error();
		}
		
		protected function rectUpdate():void {
				rect.x = (frames[frameCrIdx] % frameHCount) * frameDim.x;
				rect.y = Math.floor(frames[frameCrIdx] / frameHCount) * frameDim.y;
				
				trace("frame cr idx " + frameCrIdx + "/rect " + rect.x + "/" + rect.y);
				trace("frame isi " + frames[frameCrIdx]);
				scrollRect = rect;	
		}
		
		public function setAnimationFrames(name:String, data:Array):void {
			var obj:Object = new Object();
			
			obj = {
				name : name,
				data : data
			}
			
			frameDatas.push(obj);
		}
		
		public function animate(seqName:String):void {
			var frameData:Array;
			var obj:Object;
			
			for each (obj in frameDatas) {
				if (obj.name == seqName) {
					frameData = obj.data;
				}
			}
			
			if (frameData == null) throw new Error('animation frame data not found');
			
			animated = true;
			frames = frameData;
			frameCrIdx = 0;
			trace('frames ' + frames);
		}
		
		public function update():void {
			
			if (animated) {
		
				rectUpdate();
				
				frameCrIdx++;
				
				if (frameCrIdx >= frames.length) {
					if (isLooping) {
						frameCrIdx = 0;
					}
					else {
						frameCrIdx = frames.length - 1;
						animated = false;
												
						//todo: call end animation call back
					}
				}
			}
			

		}
		
	}

}