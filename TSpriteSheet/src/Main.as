package{
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.Rectangle;
	
	/**
	 * ...
	 * @author 
	 */
	public class Main extends Sprite {
		
		[Embed(source = "../libs/sprites.png")]
		private var Img:Class;
		private var img:Bitmap = new Img();
		
		private var spr2:TSprite; 
		private var spr:Sprite;
		private var rect:Rectangle =  new Rectangle(0, 0, 32, 32);
		
		public function Main() {
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event = null):void {
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			spr2 = new TSprite(img, 32, 32, 0);
			spr2.setAnimationFrames("walk", [1, 0, 1, 2]);
			spr2.animate("walk");
			addChild(spr2);
			
			/*
			spr = new Sprite();
			spr.addChild(img);
			spr.scrollRect = rect;
			
			addChild(spr);
			
			rect.x = 16;
			spr.scrollRect = rect;
			*/			
			
			addEventListener(Event.ENTER_FRAME, update);
		}
		
		private function update(e:Event):void {
			spr2.update();
		}
		
	}
	
}